package fun.pplm.framework.poplar.web.smscode.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fun.pplm.framework.poplar.common.service.RedisService;
import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.web.smscode.bean.po.SmsCodeSendPo;
import fun.pplm.framework.poplar.web.smscode.bean.po.SmsCodeVerifyPo;
import fun.pplm.framework.poplar.web.smscode.config.SmsCodeConfig;
import fun.pplm.framework.poplar.web.smscode.psi.SmsCodeGeneratePsi;
import fun.pplm.framework.poplar.web.smscode.psi.SmsCodeSendPsi;

/**
 * 
 * 短信服务
 * @author OracleGao
 *
 */
@Service
public class SmsCodeService {
	private static Logger logger = LoggerFactory.getLogger(SmsCodeService.class);
	@Autowired
	private SmsCodeConfig smsCodeConfig;
	@Autowired
	private SmsCodeSendPsi smsCodeSender;
	@Autowired
	private SmsCodeGeneratePsi smsCodeGenerator;
	@Autowired
	private RedisService redisService;
	
	/**
	 * 发送短信验证码
	 * @param po 发送参数
	 * @return 验证码
	 */
	public String send(SmsCodeSendPo po) {
		String smsCode = null;
		String phone = po.getPhone();
		if (smsCodeConfig.getMock()) {
			smsCode = smsCodeConfig.getMockSmsCode();
			logger.warn("短信验证码服务mock模式, smsCode: {}, 参数: {}", smsCode, Json.string(po));
		} else {
			Long interval = smsCodeConfig.getIntervalSecond();
			String intervalKey = getSmsCodeIntervalKey(phone);
			if (redisService.exists(intervalKey)) {
				throw new RuntimeException("短信验证码发送间隔要大于" + interval + "秒");
			}
			Integer smsCodeSize = smsCodeConfig.getSmsCodeSize();
			if (smsCodeSize == null || smsCodeSize < 4 || smsCodeSize > 8) {
				logger.warn("invalid smsCodeSize: {}, use default instead: {}", smsCodeSize, 6);
				smsCodeSize = 6;
			}
			smsCode = smsCodeGenerator.generate(smsCodeSize);
			smsCodeSender.sendSmsCode(phone, po.getAppId(), smsCode);
			logger.debug("发送短信验证码, smsCode: {}, 参数: {}", smsCode, Json.string(po));
			redisService.setExpire(intervalKey, interval);
		}
		String expireKey = getSmsCodeExpireKey(phone, po.getAppId());
		redisService.setExpire(expireKey, smsCode, smsCodeConfig.getExpireSecond());
		return smsCode;
	}
	
	/**
	 * 校验短信验证码
	 * @param po 校验参数
	 * @return 校验结果
	 */
	public boolean verify(SmsCodeVerifyPo po) {
		String expireKey = getSmsCodeExpireKey(po.getPhone(), po.getAppId());
		boolean flag = po.getSmsCode().equals(redisService.get(expireKey));
		logger.debug("校验短信验证码, 结果: {}, 参数: {}", flag, Json.string(po));
		return flag;
	}
	
	/**
	 * 获取短信验证码过期key
	 * @param phone 电话号码
	 * @param appId 应用id
	 * @return key
	 */
	private String getSmsCodeExpireKey(String phone, String appId) {
		return smsCodeConfig.getExpireKeyPrefix() + "-" + phone + "-" + appId;
	}
	
	/**
	 * 获取短信验证码发送间隔key
	 * @param phone 电话号码
	 * @return key
	 */
	private String getSmsCodeIntervalKey(String phone) {
		return smsCodeConfig.getIntervalKeyPrefix() + "-" + phone;
	}
	
}
