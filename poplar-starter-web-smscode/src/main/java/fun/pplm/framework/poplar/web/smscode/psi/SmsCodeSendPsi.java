package fun.pplm.framework.poplar.web.smscode.psi;

import fun.pplm.framework.poplar.web.smscode.exception.SmsCodeException;

/**
 * 
 * 短信验证码发送接口
 * @author OracleGao
 *
 */
public interface SmsCodeSendPsi {
	/**
	 * 发送短息验证码
	 * @param phone 手机号
	 * @param appId 应用id
	 * @param smsCode 验证码
	 */
	public void sendSmsCode(String phone, String appId, String smsCode) throws SmsCodeException;
	
}
