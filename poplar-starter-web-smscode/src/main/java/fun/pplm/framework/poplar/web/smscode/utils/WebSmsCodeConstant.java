package fun.pplm.framework.poplar.web.smscode.utils;

/**
 * 
 * web应用短信验证码常量
 * @author OracleGao
 *
 */
public class WebSmsCodeConstant {
	
	/**
	 * web审计类型 短信验证码发送
	 */
	public static final String WEB_AUDIT_TYPE_WEB_SMSCODE_SEND = "poplar-web-smscode-send";
	/**
	 * web审计类型 短信验证码验证
	 */
	public static final String WEB_AUDIT_TYPE_WEB_SMSCODE_VERIFY = "poplar-web-smscode-verify";
	
}
