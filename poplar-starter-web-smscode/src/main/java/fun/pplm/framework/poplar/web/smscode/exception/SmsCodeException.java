package fun.pplm.framework.poplar.web.smscode.exception;

/**
 * 
 * 短信验证码服务异常
 * @author OracleGao
 *
 */
public class SmsCodeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public SmsCodeException() {
		super();
	}

	public SmsCodeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public SmsCodeException(String message, Throwable cause) {
		super(message, cause);
	}

	public SmsCodeException(String message) {
		super(message);
	}

	public SmsCodeException(Throwable cause) {
		super(cause);
	}

}
