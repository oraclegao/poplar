package fun.pplm.framework.poplar.web.smscode.config;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * 短信服务配置
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.sms")
public class SmsCodeConfig {
	/**
	 * 开启mock模式
	 * 默认false不开启
	 * mock模式不调用发送服务，默认使用mockCode值返回
	 */
	private Boolean mock = false;
	/**
	 * mock模式短信验证码，仅限mock模式
	 * 默认123456
	 */
	private String mockSmsCode = "123456";
	/**
	 * 验证码有效时间
	 * 单位秒
	 * 默认300
	 */
	private Long expireSecond = 300L;
	/**
	 * 验证码有效期缓存key前缀
	 */
	private String expireKeyPrefix = "poplar-sms";
	/**
	 * 短信验证码长度
	 * 4~8位
	 * 默认6位
	 */
	private Integer smsCodeSize = 6;
	/**
	 * 同一手机号允许发送短信的间隔
	 * 单位秒
	 * 默认60
	 */
	private Long intervalSecond = 60L;
	/**
	 * 允许发送短信的间隔缓存前缀
	 */
	private String intervalKeyPrefix = "poplar-smsi";
	
	public SmsCodeConfig() {
		super();
	}

	public Boolean getMock() {
		return mock;
	}

	public void setMock(Boolean mock) {
		this.mock = mock;
	}

	public String getMockSmsCode() {
		return mockSmsCode;
	}

	public void setMockSmsCode(String mockSmsCode) {
		this.mockSmsCode = mockSmsCode;
	}

	public Long getExpireSecond() {
		return expireSecond;
	}

	public void setExpireSecond(Long expireSecond) {
		this.expireSecond = expireSecond;
	}

	public String getExpireKeyPrefix() {
		return expireKeyPrefix;
	}

	public void setExpireKeyPrefix(String expireKeyPrefix) {
		this.expireKeyPrefix = expireKeyPrefix;
	}

	public Integer getSmsCodeSize() {
		return smsCodeSize;
	}

	public void setSmsCodeSize(Integer smsCodeSize) {
		this.smsCodeSize = smsCodeSize;
	}

	public Long getIntervalSecond() {
		return intervalSecond;
	}

	public void setIntervalSecond(Long intervalSecond) {
		this.intervalSecond = intervalSecond;
	}

	public String getIntervalKeyPrefix() {
		return intervalKeyPrefix;
	}

	public void setIntervalKeyPrefix(String intervalKeyPrefix) {
		this.intervalKeyPrefix = intervalKeyPrefix;
	}

	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("mock", mock);
		map.put("mockSmsCode", mockSmsCode);
		map.put("expireSecond", expireSecond);
		map.put("expireKeyPrefix", expireKeyPrefix);
		map.put("smsCodeSize", smsCodeSize);
		map.put("intervalSecond", intervalSecond);
		map.put("intervalKeyPrefix", intervalKeyPrefix);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
	
	
}
