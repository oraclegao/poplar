package fun.pplm.framework.poplar.web.smscode.service;

import java.util.concurrent.ThreadLocalRandom;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Component;

import fun.pplm.framework.poplar.web.smscode.psi.SmsCodeGeneratePsi;

/**
 * 
 * 默认短信验证码生成器
 * @author OracleGao
 *
 */
@Component
@ConditionalOnMissingBean(SmsCodeGeneratePsi.class)
public class DefaultSmsCodeGenerateService implements SmsCodeGeneratePsi {
	/**
	 * 线程安全的随机数生成类
	 */
	private ThreadLocalRandom random = ThreadLocalRandom.current();

	@Override
	public String generate(int smsCodeSize) {
		StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < smsCodeSize; i++) {
        	stringBuilder.append(random.nextInt(10));
        }
		return stringBuilder.toString();
	}

}
