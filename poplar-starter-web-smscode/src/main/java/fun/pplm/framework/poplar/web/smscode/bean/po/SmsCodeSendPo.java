package fun.pplm.framework.poplar.web.smscode.bean.po;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 短信验证码发送参数
 * @author OracleGao
 *
 */
@ApiModel(description = "短信验证码发送参数")
public class SmsCodeSendPo {
	@NotBlank
	@ApiModelProperty(value = "手机号码")
	private String phone;
	@NotBlank
	@ApiModelProperty(value = "应用id")
	private String appId;
	
	public SmsCodeSendPo() {
		super();
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}
	
}
