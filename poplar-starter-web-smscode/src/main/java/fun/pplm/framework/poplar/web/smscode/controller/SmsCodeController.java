package fun.pplm.framework.poplar.web.smscode.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fun.pplm.framework.poplar.web.audit.annotation.Audit;
import fun.pplm.framework.poplar.web.resp.Resp;
import fun.pplm.framework.poplar.web.smscode.bean.po.SmsCodeSendPo;
import fun.pplm.framework.poplar.web.smscode.bean.po.SmsCodeVerifyPo;
import fun.pplm.framework.poplar.web.smscode.service.SmsCodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import static fun.pplm.framework.poplar.web.smscode.utils.WebSmsCodeConstant.WEB_AUDIT_TYPE_WEB_SMSCODE_SEND;
import static fun.pplm.framework.poplar.web.smscode.utils.WebSmsCodeConstant.WEB_AUDIT_TYPE_WEB_SMSCODE_VERIFY;

/**
 * 
 * web短信验证码服务
 * @author OracleGao
 *
 */
@Api(value = "smsCodeController", tags = "短信验证码服务")
@RestController
@RequestMapping(path = "/smsCode", produces = MediaType.APPLICATION_JSON_VALUE)
public class SmsCodeController {
	@Autowired
	private SmsCodeService smsCodeService;
	
	@Audit(value = "发送短信验证码", types = WEB_AUDIT_TYPE_WEB_SMSCODE_SEND)
	@ApiOperation(value = "发送短信验证码", notes = "发送短信验证码")
	@PostMapping("/send")
	public Resp<Void> sendSmsCode(@Valid @RequestBody SmsCodeSendPo po) {
		smsCodeService.send(po);
		return Resp.success();
	}
	
	@Audit(value = "校验短信验证码", types = WEB_AUDIT_TYPE_WEB_SMSCODE_VERIFY)
	@ApiOperation(value = "校验短信验证码", notes = "校验短信验证码")
	@PostMapping("/verify")
	public Resp<Boolean> verifySmsCode(@Valid @RequestBody SmsCodeVerifyPo po) {
		return Resp.success(smsCodeService.verify(po));
	}
	
}
