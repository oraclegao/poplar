package fun.pplm.framework.poplar.web.smscode.psi;

/**
 * 
 * 短信验证码生成接口
 * @author OracleGao
 *
 */
public interface SmsCodeGeneratePsi {
	/**
	 * 生成短信验证码
	 * @param smsCodeSize 验证码长度
	 * @return 验证码
	 */
	public String generate(int smsCodeSize);
}
