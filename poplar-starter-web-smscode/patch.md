## 1.10.19
- 优化po类的swagger文档说明
- 能够正确处理相同号码短信发送时间间隔
- 使用RedisService新的api

## 1.8.16
- 服务接口类后缀更名为Psi，并根据业务优化类名: SmsCodeSender -> SmsCodeSendPsi
- 服务接口类后缀更名为Psi，并根据业务优化类名: SmsCodeGenerator -> SmsCodeGeneratePsi
- 优化类名: DefaultSmsCodeGenerator -> DefaultSmsCodeGenerateService

## 1.8.15
- 接口添加采集审计数据功能

## 1.6.10
- 新增poplar-starter-web-smscode
- 短信验证码服务
- 需要业务实现SmsCodeSender接口对接具体短信网关发送服务