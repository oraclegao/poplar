## 1.10.19
- 移除ModbusRegisterConfigPsi，变更为ModbusRegisterPsi并放入poplar-common-modbus
- 移除ModbusRegisterModel，ModbusTcpOnReadyEvent放入poplar-common-modbus,并更换包名
- ModbusUtil.initData更名为initWords使语义更加准确
- 移除ModbusUtil，放入poplar-common-modbus
- 配置项添加useRtuOverTcp开关选项，优化并新增配置项注释
- 增加写保持寄存器字值越界告警日志
- 移除数据初始化侦听接口ModbusDataInitPsi，可以通过侦听事件ModbusTcpOnReadyEvent实现
- 移除生命周期侦听接口ModbusLifecycleListener
- 移除ModbusReadRegParam，ModbusInitParam，ModbusWriteHrParam类和相关方法
- ModbusTcpService重构，继承poplar-common-modbus模块的ModbusTcpPsa抽象类
- ModbusTcpConfig更名ModbusMasterTcpConfig并继承poplar-common-modbus模块的ModbusTcpConfig

## 1.8.16
- 服务接口类后缀更名为Psi，并优化类名: ModbusRegisterLoader -> ModbusRegisterConfigPsi
- 服务接口类后缀更名为Psi，并优化类名: ModbusDataInitializer -> ModbusDataInitPsi
- 侦听接口类后缀更名为Pli: ModbusLifecycleListener -> ModbusLifecyclePli

## 1.6.12
- 优化读取输入寄存器log
- 修复ModbusTcpSerice自动注入bug
- ModbusUtil工具类添加整型拆分成字的工具方法
- ModbusUtil工具类添加高低位字合并整型值工具方法
- 添加regCode大写敏感配置和相应的功能实现，默认大小写不敏感

## 1.5.7
- 修复bug：ModbusTcpService中手动初始化涉及的类增加在Autowired注释中required = false，解决手动初始化时springboot启动报找不到注入bean异常
- ModbusTcpService添加@PreDestory销毁方法

## 1.4.6
### 优化ModbusTcpService实例化和注入方式
- 配置项poplar.modbus.tcp.enabled移除
- 增加配置项poplar.modbus.tcp.mock替代enabled，默认false，true时启动调试模式不连接modbus tcp服务端口
- 增加配置项poplar.modbus.tcp.autoInject，默认true，false时不自动实例化并注入spring容器
- 增加配置项poplar.modbus.tcp.autoInit，默认true，false时需要手动初始化服务

### 添加工具类
- 添加ModbusUtil工具类

## 1.3.5
- 增加初始化连接重试机制
- 增加寄存器动态地址管理
- 增加保持寄存器写入数据int型

## 1.3.4
- 完善方法参数校验
- 增加初始化后的写入方法和读取回调方法
- 增加参数对象的写入和读取方法
- 增加ModbusTcpService生命周期侦听接口
- 增加发送onReady的springboot事件

## 1.3.3
- 重构ModbusTcpService类，规范方法命名，支持读取有符号数

## 1.3.1
- 新增modbus tcp服务