package fun.pplm.framework.poplar.modbus.tcp.config;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.modbus.config.ModbusTcpConfig;

/**
 * 
 * modbus tcp master 配置信息
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.modbus.tcp")
public class ModbusMasterTcpConfig extends ModbusTcpConfig {
	private static Logger logger = LoggerFactory.getLogger(ModbusMasterTcpConfig.class);
	/**
	 * 自动实例化并注入springboot容器
	 * 默认 true
	 */
	private Boolean autoInject = true;
	/**
	 * 自动初始化服务
	 * 默认 true
	 */
	private Boolean autoInit = true;
	/**
	 * mock状态，不创建modbus tcp连接，调试用
	 * 默认 false
	 */
	private Boolean mock = false;
	/**
	 * Socket timeout in milliseconds
	 * 默认3000
	 */
	private Integer timeout = 3000;
	/**
	 * amount of retries for opening the connection for executing the transaction.
	 * 默认3
	 */
	private Integer retries = 3;
	/**
	 * True if the RTU protocol should be used over TCP
	 * 默认false
	 */
	private Boolean useRtuOverTcp = false;
	/**
	 * 初始化连接重试次数
	 * 0: 不重试
	 * < 0: 无限次，直到连接成功
	 * > 0: 重试次数
	 * 默认: -1
	 * 重试时间间隔(单位：秒): 2^(n-1),最大间隔: 32s
	 */
	private Integer initRetries = -1;
	
	public ModbusMasterTcpConfig() {
		super();
	}

	@PostConstruct
	private void init() {
		logger.debug(this.getClass().getSimpleName() + "注入成功, config: {}", this);
	}
	
	public Boolean getAutoInject() {
		return autoInject;
	}

	public void setAutoInject(Boolean autoInject) {
		this.autoInject = autoInject;
	}

	public Boolean getAutoInit() {
		return autoInit;
	}

	public void setAutoInit(Boolean autoInit) {
		this.autoInit = autoInit;
	}

	public Boolean getMock() {
		return mock;
	}

	public void setMock(Boolean mock) {
		this.mock = mock;
	}

	public Integer getTimeout() {
		return timeout;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

	public Integer getRetries() {
		return retries;
	}

	public void setRetries(Integer retries) {
		this.retries = retries;
	}

	public Integer getInitRetries() {
		return initRetries;
	}

	public void setInitRetries(Integer initRetries) {
		this.initRetries = initRetries;
	}

	public Boolean getUseRtuOverTcp() {
		return useRtuOverTcp;
	}

	public void setUseRtuOverTcp(Boolean useRtuOverTcp) {
		this.useRtuOverTcp = useRtuOverTcp;
	}

	protected Map<String, Object> memberMap(){
		Map<String, Object> map = super.memberMap();
		map.put("autoInject", autoInject);
		map.put("autoInit", autoInit);
		map.put("timeout", timeout);
		map.put("retries", retries);
		map.put("useRtuOverTcp", useRtuOverTcp);
		map.put("initRetries", initRetries);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
	
}
