package fun.pplm.framework.poplar.modbus.tcp.service;

import java.io.Closeable;
import java.util.List;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.ghgande.j2mod.modbus.ModbusException;
import com.ghgande.j2mod.modbus.facade.ModbusTCPMaster;
import com.ghgande.j2mod.modbus.procimg.InputRegister;
import com.ghgande.j2mod.modbus.procimg.Register;
import com.ghgande.j2mod.modbus.procimg.SimpleInputRegister;
import com.ghgande.j2mod.modbus.procimg.SimpleRegister;

import fun.pplm.framework.poplar.common.validator.BeanValidator;
import fun.pplm.framework.poplar.modbus.event.ModbusTcpOnReadyEvent;
import fun.pplm.framework.poplar.modbus.psa.ModbusTcpPsa;
import fun.pplm.framework.poplar.modbus.psi.ModbusRegisterDataPsi;
import fun.pplm.framework.poplar.modbus.psi.ModbusRegisterPsi;
import fun.pplm.framework.poplar.modbus.tcp.config.ModbusMasterTcpConfig;

/**
 * 
 * modbus tpc 服务
 * @author OracleGao
 *
 */
@Service
@ConditionalOnProperty(name = "poplar.modbus.tcp.autoInject", havingValue = "true", matchIfMissing = true)
public class ModbusTcpService extends ModbusTcpPsa implements HealthIndicator, Closeable {
	private static Logger logger = LoggerFactory.getLogger(ModbusTcpService.class);

	/**
	 * 初始化重试最大等待时间间隔
	 * 单位：秒
	 */
	private static final Integer INIT_RETRY_MAX_INTERVAL = 32;

	@Autowired(required = false)
	private ModbusMasterTcpConfig modbusMasterTcpConfig;

	@Autowired(required = false)
	private ModbusRegisterPsi modbusRegisterService;
	
	@Autowired(required = false)
	private ModbusRegisterDataPsi modbusRegisterDataService;
	
	@Autowired(required = false)
	private BeanValidator beanValidator;
	
	private ModbusTCPMaster master;

	@EventListener(ApplicationStartedEvent.class)
	protected void autoInit(ApplicationStartedEvent event) {
		if (modbusMasterTcpConfig.getAutoInit()) {
			logger.debug("初始化modbus master tcp服务...");
			super.init(modbusMasterTcpConfig, beanValidator, modbusRegisterService, modbusRegisterDataService);
			event.getApplicationContext().publishEvent(new ModbusTcpOnReadyEvent());
			logger.debug("初始化modbus master tcp服务完成");
		}
	}

	public void init(ModbusMasterTcpConfig modbusMasterTcpConfig, BeanValidator beanValidator, ModbusRegisterPsi modbusRegisterService) {
		init(modbusMasterTcpConfig, beanValidator, modbusRegisterService, null);
	}
	
	public void init(ModbusMasterTcpConfig modbusMasterTcpConfig, BeanValidator beanValidator, ModbusRegisterPsi modbusRegisterService, ModbusRegisterDataPsi modbusRegisterDataService) {
		logger.debug("初始化modbus master tcp服务...");
		super.init(modbusMasterTcpConfig, beanValidator, modbusRegisterService, modbusRegisterDataService);
		logger.debug("初始化modbus master tcp服务完成");
	}
	
	@Override
	protected void initModbus() {
		if (modbusMasterTcpConfig.getMock()) {
			logger.warn("modbus服务mock状态启动");
			return;
		}
		logger.debug("初始化modbus tcp master...");
		master = new ModbusTCPMaster(modbusMasterTcpConfig.getHost(), modbusMasterTcpConfig.getPort(), modbusMasterTcpConfig.getTimeout(), false, modbusMasterTcpConfig.getUseRtuOverTcp());
		master.setRetries(modbusMasterTcpConfig.getRetries());
		int initRetries = modbusMasterTcpConfig.getInitRetries();
		int initRetryInterval = 1;
		while(true) {
			try {
				master.connect();
				break;
			} catch (Exception e) {
				if (initRetries == 0) {
					throw new RuntimeException("modbus tcp master连接失败", e);
				} else {
					logger.warn("modbus tcp master连接失败，等待{}秒重连...", initRetryInterval);
					try {
						Thread.sleep(initRetryInterval * 1000L);
					} catch (InterruptedException e1) {
						logger.error(e.getMessage(), e);
					}
					if (initRetryInterval < INIT_RETRY_MAX_INTERVAL) {
						initRetryInterval = initRetryInterval << 1;
					}
					if(initRetries > 0) {
						initRetries--;
					}
				}
			}
		}
		logger.debug("初始化modbus tcp master成功");
	}
	
	/**
	 * 读线圈状态
	 * 
	 * @param code 寄存器编码
	 * @return 线圈状态值
	 */
//	public Boolean readCoil(String code) {
//		return false;
//	}

	/**
	 * 读多线圈状态
	 * 
	 * @param code 寄存器编码
	 * @return 线圈状态值数组
	 */
//	public Boolean[] readCoils(String code) {
//		return null;
//	}

	/**
	 * 读离散输入状态
	 * 
	 * @param code 寄存器编码
	 * @return 离散输入状态值
	 */
//	public Boolean readDiscrete(String code) {
//		return false;
//	}

	/**
	 * 读多离散输入状态
	 * 
	 * @param code 寄存器编码
	 * @return 离散输入状态值数组
	 */
//	public Boolean[] readDiscretes(String code) {
//		return null;
//	}

	/**
	 * 写线圈状态
	 * 
	 * @param code 寄存器编码
	 * @param data 线圈状态值
	 */
//	public void writeCoil(String code, Boolean data) {
//		
//	}

	/**
	 * 写多线圈状态
	 * 
	 * @param code 寄存器编码
	 * @param data 线圈状态值数组
	 */
//	public void writeCoils(String code, Boolean[] data) {
//		
//	}

	/**
	 * 写多线圈状态
	 * 
	 * @param code 寄存器编码
	 * @param data 线圈状态值集合
	 */
//	public void writeCoils(String code, Collection<Boolean> data) {
//
//	}

	/**
	 * 返回j2mod modbus tcp master对象
	 * @return ModbusTCPMaster
	 */
	public ModbusTCPMaster getMaster() {
		return master;
	}

	public ModbusMasterTcpConfig getModbusMasterTcpConfig() {
		return modbusMasterTcpConfig;
	}

	public void setModbusMasterTcpConfig(ModbusMasterTcpConfig modbusMasterTcpConfig) {
		super.modbusTcpConfig = modbusMasterTcpConfig;
		this.modbusMasterTcpConfig = modbusMasterTcpConfig;
	}

	public Boolean getMock() {
		return this.modbusMasterTcpConfig.getMock();
	}
	
	public ModbusRegisterPsi getModbusRegisterService() {
		return modbusRegisterService;
	}

	public void setModbusRegisterService(ModbusRegisterPsi modbusRegisterService) {
		this.modbusRegisterService = modbusRegisterService;
	}

	@Override
	protected Register[] readHoldingRegisters(int slaveId, int startAddress, int wordCount) throws ModbusException {
		if(modbusMasterTcpConfig.getMock()) {
			Register[] registers = new Register[wordCount];
			for (int i = 0; i < wordCount; i++) {
				registers[i] = new SimpleRegister(0);
			}
			return registers;
		}
		return master.readMultipleRegisters(slaveId, startAddress, wordCount);
	}

	@Override
	protected InputRegister[] readInputRegisters(int slaveId, int startAddress, int wordCount) throws ModbusException {
		if(modbusMasterTcpConfig.getMock()) {
			InputRegister[] registers = new InputRegister[wordCount];
			for (int i = 0; i < wordCount; i++) {
				registers[i] = new SimpleInputRegister(0);
			}
			return registers;
		}
		return master.readInputRegisters(slaveId, startAddress, wordCount);
	}

	@Override
	protected void writeHoldingRegisters(int slaveId, int startAddress, List<Integer> data) throws ModbusException {
		if(!modbusMasterTcpConfig.getMock()) {
			int size = data.size();
			Register[] registers = new Register[size];
			for (int i = 0; i < size; i++) {
				registers[i] = new SimpleRegister(data.get(i));
			}
			master.writeMultipleRegisters(slaveId, startAddress, registers);
		}
	}
	
	@Override
	public Health health() {
		if (!modbusMasterTcpConfig.getMock()) {
			if (master.isConnected()) {
				return Health.up().build();
			} else {
				return Health.down().build();
			}
		}
		return Health.unknown().build();
	}

	@PreDestroy
	@Override
	public void close() {
		logger.debug("modbus服务停止开始...");
		if (master != null) {
			logger.info("modbus tcp master断开连接");
			master.disconnect();
		}
		logger.debug("modbus服务停止完成");
	}
	
}
