package fun.pplm.framework.poplar.modbus.tcp.controller;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fun.pplm.framework.poplar.modbus.controller.ModbusTcpController;
import io.swagger.annotations.Api;

/**
 * 
 * modbus master服务api
 * @author OracleGao
 *
 */
@Api(value = "modbusMasterTcpController", tags = "modbus api")
@ConditionalOnExpression("${poplar.modbus.tcp.apiEnabled:true}")
@Validated
@RestController
@RequestMapping(path = "/modbus/master", produces = MediaType.APPLICATION_JSON_VALUE)
public class ModbusMasterTcpController extends ModbusTcpController {

}
