## 1.11.22
- 完善MqttService初始化日志

## 1.10.21
- 增加初始连接mqtt服务器重试机制

## 1.8.16
- 服务接口类后缀更名为Pli,并优化类名: MqttConfigInterceptor -> MqttConfigPsi
