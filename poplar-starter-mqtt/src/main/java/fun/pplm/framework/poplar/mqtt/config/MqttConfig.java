package fun.pplm.framework.poplar.mqtt.config;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.mqtt.config.psi.MqttConfigPsi;

/**
 * mqtt配置
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.mqtt")
public class MqttConfig {
	private static Logger logger = LoggerFactory.getLogger(MqttConfig.class);
	
	private String uri = "tcp://127.0.0.1:1883";
	private String username = "";
	private String password = "";
	private String clientId = "poplar";
	private Integer timeoutSecond = 30;
	private Integer keepAliveSecond = 60;
	private Boolean cleanSession = true;
	private Boolean automaticReconnect = true;
	private Integer subQos = 1;
	private Integer pubQos = 1;
	private Boolean pubRetained = false;
	private Boolean pubDuplicate = false;
	private String charset = "utf-8";
	private List<String> topics = Collections.emptyList();
	/**
	 * 初始化连接重试次数
	 * 0: 不重试
	 * < 0: 无限次，直到连接成功
	 * > 0: 重试次数
	 * 默认: -1
	 * 重试时间间隔(单位：秒): 2^(n-1),最大间隔: 32s
	 */
	private Integer initRetries = -1;
	
	@Autowired(required = false)
	private MqttConfigPsi mqttConfigPsi;
	
	@EventListener(ApplicationStartedEvent.class)
	protected void init() {
		if (mqttConfigPsi != null) {
			mqttConfigPsi.intercept(this);
		}
		logger.debug(this.getClass().getSimpleName() + "注入成功, config: {}", this);
	}
	
	public MqttConnectOptions getOptions() {
		MqttConnectOptions options = new MqttConnectOptions();
		if (StringUtils.isNoneBlank(username)) {
			options.setUserName(username);
		}
		if (StringUtils.isNotBlank(password)) {
			options.setPassword(password.toCharArray());
		}
        options.setConnectionTimeout(timeoutSecond);
        options.setKeepAliveInterval(keepAliveSecond);
        options.setCleanSession(cleanSession);
        options.setAutomaticReconnect(automaticReconnect);
        return options;
	}
	
	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public Integer getTimeoutSecond() {
		return timeoutSecond;
	}

	public void setTimeoutSecond(Integer timeoutSecond) {
		this.timeoutSecond = timeoutSecond;
	}

	public Integer getKeepAliveSecond() {
		return keepAliveSecond;
	}

	public void setKeepAliveSecond(Integer keepAliveSecond) {
		this.keepAliveSecond = keepAliveSecond;
	}

	public Boolean getCleanSession() {
		return cleanSession;
	}

	public void setCleanSession(Boolean cleanSession) {
		this.cleanSession = cleanSession;
	}

	public Boolean getAutomaticReconnect() {
		return automaticReconnect;
	}

	public void setAutomaticReconnect(Boolean automaticReconnect) {
		this.automaticReconnect = automaticReconnect;
	}

	public Integer getSubQos() {
		return subQos;
	}

	public void setSubQos(Integer subQos) {
		this.subQos = subQos;
	}

	public Integer getPubQos() {
		return pubQos;
	}

	public void setPubQos(Integer pubQos) {
		this.pubQos = pubQos;
	}

	public Boolean getPubRetained() {
		return pubRetained;
	}

	public void setPubRetained(Boolean pubRetained) {
		this.pubRetained = pubRetained;
	}

	public Boolean getPubDuplicate() {
		return pubDuplicate;
	}

	public void setPubDuplicate(Boolean pubDuplicate) {
		this.pubDuplicate = pubDuplicate;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public List<String> getTopics() {
		return topics;
	}

	public void setTopics(List<String> topics) {
		this.topics = topics;
	}

	public Integer getInitRetries() {
		return initRetries;
	}

	public void setInitRetries(Integer initRetries) {
		this.initRetries = initRetries;
	}

	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("uri", uri);
		map.put("username", username);
		map.put("password", password);
		map.put("clientId", clientId);
		map.put("timeoutSecond", timeoutSecond);
		map.put("keepAliveSecond", keepAliveSecond);
		map.put("cleanSession", cleanSession);
		map.put("automaticReconnect", automaticReconnect);
		map.put("subQos", subQos);
		map.put("pubQos", pubQos);
		map.put("pubRetained", pubRetained);
		map.put("pubDuplicate", pubDuplicate);
		map.put("charset", charset);
		map.put("topics", topics);
		map.put("initRetries", initRetries);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
	
}
