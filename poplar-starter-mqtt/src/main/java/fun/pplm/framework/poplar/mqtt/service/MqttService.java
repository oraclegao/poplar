package fun.pplm.framework.poplar.mqtt.service;

import java.io.IOException;

import javax.annotation.PreDestroy;

import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import fun.pplm.framework.poplar.mqtt.config.MqttConfig;

/**
 * 
 * mqtt服务封装
 * @author OracleGao
 *
 */
@Service
public class MqttService implements HealthIndicator {
	private static Logger logger = LoggerFactory.getLogger(MqttService.class);	
	
	/**
	 * 初始化重试最大等待时间间隔
	 * 单位：秒
	 */
	private static final Integer INIT_RETRY_MAX_INTERVAL = 32;
	
	@Autowired
	private MqttConfig config;
	
	@Autowired
	private MqttCallback mqttCallback;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	private MqttClient client;

	public MqttService() {
		super();
	}
	
	@EventListener(ApplicationReadyEvent.class)
	protected void init() {
		logger.debug("mqtt服务初始化开始...");
		try {
			client = new MqttClient(config.getUri(), config.getClientId(), new MemoryPersistence());
		} catch (MqttException e) {
			throw new RuntimeException(e);
		}
		client.setCallback(mqttCallback);
		MqttConnectOptions options = config.getOptions();
		int initRetries = config.getInitRetries();
		int initRetryInterval = 1;
		while(true) {
			try {
				client.connect(options);
				logger.debug("mqtt server连接成功");
				break;
			} catch (Exception e) {
				if (initRetries == 0) {
					throw new RuntimeException("mqtt server连接失败", e);
				} else {
					logger.warn("mqtt server连接失败，等待{}秒重连...", initRetryInterval);
					try {
						Thread.sleep(initRetryInterval * 1000L);
					} catch (InterruptedException e1) {
						logger.error(e.getMessage(), e);
					}
					if (initRetryInterval < INIT_RETRY_MAX_INTERVAL) {
						initRetryInterval = initRetryInterval << 1;
					}
					if(initRetries > 0) {
						initRetries--;
					}
				}
			}
		}
		try {
			for (String topic : config.getTopics()) {
				client.subscribe(topic, config.getSubQos());
			}
		} catch (MqttException e) {
			throw new RuntimeException(e);
		}
		logger.debug("mqtt服务初始化完成");
	}
	
	/**
	 * 发布mqtt消息
	 * @param topic mqtt topic
	 * @param msgObj 消息对象 对象会被自动转换成json string进行发送, String对象按照string发送
	 * @throws MqttException mqtt异常
	 */
	public void publish(String topic, Object msgObj) throws MqttException {
		MqttMessage mqttMessage = new MqttMessage();
		mqttMessage.setQos(config.getPubQos());
		mqttMessage.setRetained(config.getPubRetained());
		String message = null;
		try {
			if (msgObj instanceof String) {
				message = (String) msgObj;
			} else {
				message = objectMapper.writeValueAsString(msgObj);
			}
			logger.info("发布消息: {}", message);
			mqttMessage.setPayload(message.getBytes(config.getCharset()));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		publish(topic, mqttMessage);
	}
	
	public void publish(String topic, MqttMessage mqttMessage) throws MqttException {
		client.publish(topic, mqttMessage);
	}
	
	@PreDestroy
	public void destory() {
		if (client != null) {
			logger.debug("mqtt服务停止开始...");
			try {
				client.disconnect();
				client.close();
			} catch (MqttException e) {
				logger.error(e.getMessage(), e);
			}
			logger.debug("mqtt服务停止完成");
		}
	}

	@Override
	public Health health() {
		if (client != null) {
			if (client.isConnected()) {
				return Health.up().build();
			} else {
				return Health.outOfService().build();
			}
		}
		return Health.down().build();
	}
	
}
