package fun.pplm.framework.poplar.mqtt.handler;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;

import fun.pplm.framework.poplar.mqtt.config.MqttConfig;

/**
 *
 * mqtt 基本回调实现
 * @author OracleGao
 *
 */
@ConditionalOnMissingBean(MqttCallback.class)
public class MqttCallbackBaseHandler implements MqttCallback {
	private Logger logger = LoggerFactory.getLogger(MqttCallbackBaseHandler.class);
	
	@Autowired
	protected MqttConfig config;
	
	@Override
	public void connectionLost(Throwable e) {
		logger.error(e.getMessage(), e);
	}

	@Override
	public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
		String message = new String(mqttMessage.getPayload(), config.getCharset()); 
		logger.info("mqtt message arrived, topic: {}, message: {}", topic, message);
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		MqttException e = token.getException();
		if (e != null) {
			logger.error(e.getMessage(), e);
		} else {
			logger.info("mqtt delivery complete");
		}
	}

	public MqttConfig getConfig() {
		return config;
	}

	public void setConfig(MqttConfig config) {
		this.config = config;
	}

}
