package fun.pplm.framework.poplar.mqtt.config.psi;

import fun.pplm.framework.poplar.mqtt.config.MqttConfig;

/**
 * 
 * Poplar Service Interface
 * mqtt配置拦截器服务接口
 * @author OracleGao
 *
 */
public interface MqttConfigPsi {
	public default void intercept(MqttConfig mqttConfig) {}
}
