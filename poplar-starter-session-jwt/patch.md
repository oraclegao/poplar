## 1.11.21
- 增加createSession(SessionPii session, boolean setCookieFlag)方法实现创建session时设置cookie

## 1.6.11
- 增加会话更新接口实现(不支持会话更新)
- 完善会话清理接口实现(不支持会话清理)