package fun.pplm.framework.poplar.jwt.configurer;

import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.common.session.configurer.SessionInterceptorConfigurer;

/**
 * 
 * jwt 拦截器配置
 * @author OracleGao
 *
 */
@Configuration
public class JwtInterceptorConfigurer extends SessionInterceptorConfigurer {

}
