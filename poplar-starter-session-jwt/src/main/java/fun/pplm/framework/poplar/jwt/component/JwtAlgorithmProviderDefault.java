package fun.pplm.framework.poplar.jwt.component;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Component;

import com.auth0.jwt.algorithms.Algorithm;

/**
 * 
 * jwt 默认算法提供者
 * 采用HMAC512算法
 * @author OracleGao
 *
 */
@Component
@ConditionalOnMissingBean(JwtAlgorithmProvider.class)
public class JwtAlgorithmProviderDefault implements JwtAlgorithmProvider {
	private Logger logger = LoggerFactory.getLogger(JwtAlgorithmProviderDefault.class);
	
	@Value("${poplar.session.jwt.algo.hmac512.secret:poplarHmac512Secret}")
	private String secret;
	
	private Algorithm algorithm;
	
    @PostConstruct
    private void init() {
    	logger.debug(this.getClass().getSimpleName() + "注入成功");
    }
	
	@Override
	public synchronized Algorithm getAlgorithm() {
		if (algorithm == null) {
			algorithm = Algorithm.HMAC512(secret);
		}
		return algorithm;
	}

}
