package fun.pplm.framework.poplar.jwt.plus;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Component;

import com.auth0.jwt.JWTCreator.Builder;

/**
 * 默认jwt token 构建插件
 * @author OracleGao
 *
 */
@Component
@ConditionalOnMissingBean(JwtTokenBuilderPlus.class)
public class JwtTokenBuilderPlusDefault implements JwtTokenBuilderPlus {
	private Logger logger = LoggerFactory.getLogger(JwtTokenBuilderPlusDefault.class);
	
    @PostConstruct
    private void init() {
    	logger.debug(this.getClass().getSimpleName() + "注入成功");
    }
	
	@Override
	public void beforeBuild(Builder jwtBuilder) {}

}
