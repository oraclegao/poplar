package fun.pplm.framework.poplar.jwt.interceptor;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.security.sasl.AuthenticationException;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;

import fun.pplm.framework.poplar.common.session.interceptor.SessionInterceptor;
import fun.pplm.framework.poplar.common.session.pii.SessionPii;
import fun.pplm.framework.poplar.jwt.component.JwtAlgorithmProvider;
import fun.pplm.framework.poplar.jwt.config.JwtConfig;

/**
 * 
 * jwt 拦截器
 * @author OracleGao
 *
 */
@Component
public class JwtInterceptor extends SessionInterceptor {
	private Logger logger = LoggerFactory.getLogger(JwtInterceptor.class);
	
	@Autowired
	private JwtConfig jwtConfig;
	@Autowired
	private JwtAlgorithmProvider jwtAlgorithmProvider;
	@Autowired
	private ObjectMapper objectMapper;

    @PostConstruct
    private void init() {
    	logger.debug(this.getClass().getSimpleName() + "注入成功");
    }
	
	@Override
	protected SessionPii validate(HttpServletRequest request, String token, boolean refreshExpire,
			Class<? extends SessionPii> sessionClass) throws AuthenticationException {
		DecodedJWT jwt = JWT.require(jwtAlgorithmProvider.getAlgorithm()).build().verify(token);
		String sessionJson = jwt.getClaim(jwtConfig.getSessionClaimKey()).asString();
		try {
			return objectMapper.readValue(sessionJson, sessionClass);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			throw new AuthenticationException(e.getMessage(), e);
		}
	}
	
}
