package fun.pplm.framework.poplar.jwt.config;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.json.Json;

@Configuration
@ConfigurationProperties(prefix = "poplar.session.jwt")
public class JwtConfig {
	private Logger logger = LoggerFactory.getLogger(JwtConfig.class);
	
	/**
	 * 使用claim存放session信息的key
	 */
	private String sessionClaimKey = "poplarSession";

	public JwtConfig() {
		super();
	}

    @PostConstruct
    private void init() {
    	logger.debug(this.getClass().getSimpleName() + "注入成功, config: {}", this);
    }
	
	public String getSessionClaimKey() {
		return sessionClaimKey;
	}

	public void setSessionClaimKey(String sessionClaimKey) {
		this.sessionClaimKey = sessionClaimKey;
	}

	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("sessionClaimKey", sessionClaimKey);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
	
}
