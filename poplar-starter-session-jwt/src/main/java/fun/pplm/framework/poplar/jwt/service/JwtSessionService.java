package fun.pplm.framework.poplar.jwt.service;

import java.time.Instant;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator.Builder;
import com.auth0.jwt.interfaces.DecodedJWT;

import fun.pplm.framework.poplar.common.session.bean.Auth;
import fun.pplm.framework.poplar.common.session.config.SessionConfig;
import fun.pplm.framework.poplar.common.session.pii.SessionPii;
import fun.pplm.framework.poplar.common.session.service.SessionHttpService;
import fun.pplm.framework.poplar.common.session.service.SessionService;
import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.jwt.component.JwtAlgorithmProvider;
import fun.pplm.framework.poplar.jwt.config.JwtConfig;
import fun.pplm.framework.poplar.jwt.plus.JwtTokenBuilderPlus;

/**
 * 
 * jwt会话服务
 * @author OracleGao
 *
 */
@Service
public class JwtSessionService implements SessionService {
	private Logger logger = LoggerFactory.getLogger(JwtSessionService.class);
	
	@Autowired
	private SessionConfig sessionConfig;
	@Autowired
	private JwtConfig jwtConfig;
	
	@Autowired
	private SessionHttpService sessionHttpService;
	
	@Autowired
	private JwtAlgorithmProvider jwtAlgorithmProvider;
	@Autowired
	private JwtTokenBuilderPlus jwtTokenBuilderPlus;

	@PostConstruct
	private void init () {
		logger.debug(this.getClass().getSimpleName() + "注入成功");
	}
	
	public DecodedJWT verify(String token) {
		return JWT.require(jwtAlgorithmProvider.getAlgorithm()).build().verify(token);
	}

	public Auth createSession(SessionPii session) {
		return createSession(session);
	}
	
	@Override
	public Auth createSession(SessionPii session, boolean setCookieFlag) {
		Builder builder = JWT.create();
		
		jwtTokenBuilderPlus.beforeBuild(builder);
		
		long expireSecond = sessionConfig.getExpireSecond();
		Long expireAt = System.currentTimeMillis() + expireSecond * 1000;
		builder.withExpiresAt(Instant.ofEpochMilli(expireAt));
		
		builder.withClaim(jwtConfig.getSessionClaimKey(), Json.string(session));
		
		String token = builder.sign(jwtAlgorithmProvider.getAlgorithm());
		
		if (setCookieFlag) {
			sessionHttpService.setCookieToken(token, "/", (int)expireSecond);
		}
		
		Auth auth = new Auth();
		auth.setToken(token);
		auth.setExpireAt(expireAt);
		return auth;
	}

	/**
	 * jwt不支持session更新
	 * @param session 会话信息
	 */
	@Override
	public void updateSession(SessionPii session) {
		throw new RuntimeException("jwt不支持session更新，使用createSession创建新的会话");
	}

	/**
	 * jwt不支持session更新
	 * @param session 会话信息
	 */
	@Override
	public void updateSession(String token, SessionPii session) {
		throw new RuntimeException("jwt不支持session更新，使用createSession创建新的会话");
	}
	
	/**
	 * jwt不支持清理，token超时失效
	 */
	@Override
	public void cleanSession() {
		throw new RuntimeException("jwt不支持session清理，token超时失效");
	}

	/**
	 * jwt不支持清理，token超时失效
	 */
	@Override
	public void cleanSession(String token) {
		throw new RuntimeException("jwt不支持session清理，token超时失效");
	}
	
}
