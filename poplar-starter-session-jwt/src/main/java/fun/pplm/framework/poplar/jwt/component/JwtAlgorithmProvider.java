package fun.pplm.framework.poplar.jwt.component;

import com.auth0.jwt.algorithms.Algorithm;

/**
 * 
 * jwt 算法提供者接口
 * @author OracleGao
 *
 */
public interface JwtAlgorithmProvider {
	public Algorithm getAlgorithm();
}
