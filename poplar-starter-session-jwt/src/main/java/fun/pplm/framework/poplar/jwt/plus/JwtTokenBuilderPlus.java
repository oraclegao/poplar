package fun.pplm.framework.poplar.jwt.plus;

import com.auth0.jwt.JWTCreator.Builder;

/**
 * 
 * jwt token 构建插件
 * 构建token前执行，用于处理claim属性
 * @author OracleGao
 *
 */
public interface JwtTokenBuilderPlus {
	
	/**
	 * 构建token前添加claim属性，比如Issuer, Subject
	 * @param jwtBuilder jwtBuilder
	 */
	public void beforeBuild(Builder jwtBuilder);
	
}
