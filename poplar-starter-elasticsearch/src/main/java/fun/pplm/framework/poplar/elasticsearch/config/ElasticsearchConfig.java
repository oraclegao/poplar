package fun.pplm.framework.poplar.elasticsearch.config;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

import co.elastic.clients.elasticsearch.ElasticsearchAsyncClient;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;

import fun.pplm.framework.poplar.json.Json;

@Configuration
@ConditionalOnMissingBean(ElasticsearchClient.class)
public class ElasticsearchConfig {

	@Value("#{'${poplar.elasticsearch.hosts}'.split(',')}")
	private String[] hosts;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Bean
	public ElasticsearchClient elasticsearchClient() {
		ElasticsearchTransport transport = createTransport();
		return new ElasticsearchClient(transport);
	}
	
	@Bean
	public ElasticsearchAsyncClient elasticsearchAsyncClient() {
		ElasticsearchTransport transport = createTransport();
		return new ElasticsearchAsyncClient(transport);
	}
	
	private ElasticsearchTransport createTransport() {
		RestClient restClient = RestClient.builder(getHttpHosts()).build();
		return new RestClientTransport(restClient, new JacksonJsonpMapper(objectMapper));
	}
	
	private HttpHost[] getHttpHosts() {
		HttpHost[] httpHosts = new HttpHost[hosts.length];
		for (int i = 0; i < hosts.length; i++) {
			httpHosts[i] = HttpHost.create(hosts[i]);
		}
		return httpHosts;
	}
	
	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("hosts", hosts);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
	
}
