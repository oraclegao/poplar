package fun.pplm.framework.poplar.modbus.bean.po;

import java.util.List;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * 写入寄存器参数
 * @author OracleGao
 *
 */
@ApiModel(description = "写入寄存器参数")
public class WriteRegisterPo extends ReadRegisterPo {
	@NotEmpty
	@ApiModelProperty(value = "写入字列表，16位整型")
	private List<Integer> words;
	
	public WriteRegisterPo() {
		super();
	}

	public List<Integer> getWords() {
		return words;
	}

	public void setWords(List<Integer> words) {
		this.words = words;
	}

}
