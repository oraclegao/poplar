package fun.pplm.framework.poplar.modbus.psi;

import java.util.List;
import java.util.stream.Collectors;


import fun.pplm.framework.poplar.modbus.bean.model.ModbusRegisterModel;
import fun.pplm.framework.poplar.modbus.bean.qo.ModbusRegisterQo;

import static fun.pplm.framework.poplar.common.utils.SqlFilterUtil.*;

/**
 * 
 * Poplar Service interface
 * modbus服务接口
 * @author OracleGao
 *
 */
public interface ModbusPsi {
	/**
	 * 根据条件查询输入寄存器
	 * @param qo 查询条件
	 * @return 输入寄存器列表
	 */
	public default List<ModbusRegisterModel> getInputRegisters(ModbusRegisterQo qo) {
		List<ModbusRegisterModel> models = getInputRegisterModels();
		return filterRegisters(qo, models);
	}
	
	/**
	 * 根据条件查询保持寄存器
	 * @param qo 查询条件
	 * @return 保持寄存器列表
	 */
	public default List<ModbusRegisterModel> getHoldingRegisters(ModbusRegisterQo qo) {
		List<ModbusRegisterModel> models = getHoldingRegisterModels();
		return filterRegisters(qo, models);
	}
	
	/**
	 * 获取输入寄存器model
	 * @return 输入寄存器model列表
	 */
	List<ModbusRegisterModel> getInputRegisterModels();
	
	/**
	 * 获取保持寄存器model
	 * @return 保持寄存器model列表
	 */
	List<ModbusRegisterModel> getHoldingRegisterModels();
	
	/**
	 * 根据寄存器编码获取寄存器信息
	 * @param regCode 寄存器编码
	 * @param regType 寄存器类型
	 * @return 寄存器信息
	 */
	public List<ModbusRegisterModel> getRegisters(String regCode, String regType);
	
	/**
	 * 根据查询条件过滤寄存器
	 * @param qo 查询条件
	 * @param models 待过滤的寄存器model列表
	 * @return 查询结果
	 */
	default List<ModbusRegisterModel> filterRegisters(ModbusRegisterQo qo, List<ModbusRegisterModel> models) {
		return models.stream().filter(model -> {
			return like(model.getRegName(), qo.getRegName()) 
					&& like(model.getRegCode(), qo.getRegCode()) 
					&& eq(model.getRegSign(), qo.getRegSign()) 
					&& ge(model.getStartAddress(), qo.getStartAddressBegin())
					&& le(model.getStartAddress(), qo.getStartAddressEnd())
					&& ge(model.getWordCount(), qo.getWordCountBegin())
					&& le(model.getWordCount(), qo.getWordCountEnd());
		}).collect(Collectors.toList());
	}
	
}
