package fun.pplm.framework.poplar.modbus.bean.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * modbus寄存器数据
 * @author OracleGao
 *
 */
@ApiModel(description = "modbus寄存器数据")
public class RegisterVo {
	@ApiModelProperty(value = "整型数据")
	private int[] intData;
	@ApiModelProperty(value = "二进制格式字符串数据")
	private String[] binaryData;
	@ApiModelProperty(value = "十六进制格式字符串数据")
	private String[] hexData;

	public RegisterVo() {
		super();
	}

	public int[] getIntData() {
		return intData;
	}

	public void setIntData(int[] intData) {
		this.intData = intData;
	}

	public String[] getBinaryData() {
		return binaryData;
	}

	public void setBinaryData(String[] binaryData) {
		this.binaryData = binaryData;
	}

	public String[] getHexData() {
		return hexData;
	}

	public void setHexData(String[] hexData) {
		this.hexData = hexData;
	}

}
