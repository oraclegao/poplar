package fun.pplm.framework.poplar.modbus.bean.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

import fun.pplm.framework.poplar.common.validator.annotation.Item;
import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * modbus寄存器模型
 * 当前版本不支持线圈和离散量类型
 * @author OracleGao
 *
 */
public class ModbusRegisterModel implements Comparable<ModbusRegisterModel> {
	/**
	 * 寄存器名称
	 */
	private String regName;
	/**
	 * 寄存器编码
	 */
	@NotBlank(message = "regCode is blank")
	private String regCode;
	/**
	 * 寄存器描述
	 */
	private String regDesc;
	/**
	 * 寄存器类型
	 * HR: 保持寄存器
	 * IR: 输入寄存器
	 */
	@NotBlank
	@Item(value = {"HR", "IR"}, caseSensitive = false)
	private String regType;
	/**
	 * 寄存器符号类型
	 * 0(或null): 无符号，
	 * 1: 有符号
	 */
	@Min(value = 0)
	@Max(value = 1)
	private Integer regSign;
	/**
	 * 起始地址
	 */
	@PositiveOrZero
	private Integer startAddress;
	/**
	 * 字数
	 */
	@Positive
	@Min(value = 1)
	private Integer wordCount;
	/**
	 * 相同regCode动态地址的排序顺序
	 * 默认:0(null)
	 */
	@Min(value = 0)
	private Integer addrOrder;

	public ModbusRegisterModel() {
		super();
	}
	
	public String getRegName() {
		return regName;
	}

	public void setRegName(String regName) {
		this.regName = regName;
	}

	public String getRegCode() {
		return regCode;
	}

	public void setRegCode(String regCode) {
		this.regCode = regCode;
	}

	public String getRegDesc() {
		return regDesc;
	}

	public void setRegDesc(String regDesc) {
		this.regDesc = regDesc;
	}

	public String getRegType() {
		return regType;
	}

	public void setRegType(String regType) {
		this.regType = regType;
	}

	public Integer getRegSign() {
		return regSign;
	}

	public void setRegSign(Integer regSign) {
		this.regSign = regSign;
	}

	public Integer getStartAddress() {
		return startAddress;
	}

	public void setStartAddress(Integer startAddress) {
		this.startAddress = startAddress;
	}

	public Integer getWordCount() {
		return wordCount;
	}

	public void setWordCount(Integer wordCount) {
		this.wordCount = wordCount;
	}

	public Integer getAddrOrder() {
		return addrOrder;
	}

	public void setAddrOrder(Integer addrOrder) {
		this.addrOrder = addrOrder;
	}

	@Override
	public String toString() {
		return Json.string(this);
	}

	@Override
	public int compareTo(ModbusRegisterModel model) {
		if (regCode.equals(model.regCode)) {
			return addrOrder - model.addrOrder;
		}
		return regCode.compareTo(model.regCode);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ModbusRegisterModel) {
			ModbusRegisterModel model = (ModbusRegisterModel) obj;
			if (regCode.equals(model.regCode) && regType.equals(model.regType) && addrOrder == model.addrOrder) {
				return true;
			}
		}
		return false;
	}
	
}
