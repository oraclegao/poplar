package fun.pplm.framework.poplar.modbus.utils;

/**
 * 
 * modbus常量
 * @author OracleGao
 *
 */
public final class ModbusConstant {
	/**
	 * 无符号字最小值
	 */
	public static final int WORD_UNSIGN_MIN_VALUE = 0;
	/**
	 * 无符号字最大值
	 */
	public static final int WORD_UNSIGN_MAX_VALUE = 65535;
	/**
	 * 有符号字最小值
	 */
	public static final int WORD_SIGN_MIN_VALUE = Short.MIN_VALUE;
	/**
	 * 有符号字最大值
	 */
	public static final int WORD_SIGN_MAX_VALUE = Short.MAX_VALUE;
	
	/**
	 * 寄存器类型: 保持寄存器
	 */
	public static final String REG_TYPE_HR = "HR";
	/**
	 * 寄存器类型: 输入寄存器
	 */
	public static final String REG_TYPE_IR = "IR";
	
}
