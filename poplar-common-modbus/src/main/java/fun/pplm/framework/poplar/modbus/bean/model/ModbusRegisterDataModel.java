package fun.pplm.framework.poplar.modbus.bean.model;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import fun.pplm.framework.poplar.common.validator.annotation.Item;

/**
 * 
 * modbus 寄存器数据模型
 * 在modbus服务启动后，用于初始化寄存器数据
 * @author OracleGao
 *
 */
public class ModbusRegisterDataModel {
	/**
	 * 从站id
	 * 默认null使用全局配置
	 */
	private Integer slaveId;
	/**
	 * 寄存器编码
	 */
	@NotBlank(message = "regCode is blank")
	private String regCode;
	/**
	 * 寄存器类型
	 * HR: 保持寄存器
	 * IR: 输入寄存器
	 */
	@NotBlank
	@Item(value = {"HR", "IR"}, caseSensitive = false)
	private String regType;
	/**
	 * 寄存器数据
	 */
	@NotEmpty
	private List<Integer> regData;
	
	public ModbusRegisterDataModel() {
		super();
	}

	public Integer getSlaveId() {
		return slaveId;
	}

	public void setSlaveId(Integer slaveId) {
		this.slaveId = slaveId;
	}

	public String getRegCode() {
		return regCode;
	}

	public void setRegCode(String regCode) {
		this.regCode = regCode;
	}

	public String getRegType() {
		return regType;
	}

	public void setRegType(String regType) {
		this.regType = regType;
	}

	public List<Integer> getRegData() {
		return regData;
	}

	public void setRegData(List<Integer> regData) {
		this.regData = regData;
	}

}
