package fun.pplm.framework.poplar.modbus.utils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * modbus工具类
 * 
 * @author OracleGao
 *
 */
public final class ModbusUtil {

	/**
	 * word字转换成为格式化的16位二进制字符串，主要用于可读日志输出 0000 0000 0000 0000
	 * @param word 单字
	 * @return 二进制格式字符串
	 */
	public static String word2FormatBinary(int word) {
		String binary = Integer.toBinaryString(Short.toUnsignedInt((short) word));
		StringBuilder stringBuilder = new StringBuilder();
		int i = 16;
		while (i-- - binary.length() > 0) {
			stringBuilder.append("0");
		}
		stringBuilder.append(binary);
		stringBuilder.insert(4, " ");
		stringBuilder.insert(9, " ");
		stringBuilder.insert(14, " ");
		return stringBuilder.toString();
	}

	/**
	 * word字转换成为格式化的16位二进制字符串，主要用于可读日志输出 word2FormatBinary 方法名称缩写
	 * @param word 单字
	 * @return 二进制格式字符串
	 */
	public static String w2FB(int word) {
		return word2FormatBinary(word);
	}

	public static String[] w2FB(int[] words) {
		String[] binaries = new String[words.length];
		for (int i = 0; i < words.length; i++) {
			binaries[i] = w2FB(words[i]);
		}
		return binaries;
	}

	public static String[] w2FB(Integer[] words) {
		String[] binaries = new String[words.length];
		for (int i = 0; i < words.length; i++) {
			binaries[i] = w2FB(words[i]);
		}
		return binaries;
	}

	/**
	 * 字数组转换成为格式化的16位二进制字符串数组，主要用于可读日志输出
	 * @param words 字数组
	 * @return 二进制格式字符串数组
	 */
	public static String ws2FB(Integer[] words) {
		String[] ws = new String[words.length];
		for (int i = 0; i < words.length; i++) {
			ws[i] = w2FB(words[i]);
		}
		return Json.string(ws);
	}
	
	/**
	 * 字数组转换成为格式化的16位二进制字符串数组，主要用于可读日志输出
	 * @param words 字数组
	 * @return 二进制格式字符串数组
	 */
	public static String ws2FB(int[] words) {
		String[] ws = new String[words.length];
		for (int i = 0; i < words.length; i++) {
			ws[i] = w2FB(words[i]);
		}
		return Json.string(ws);
	}

	/**
	 * word字转换成为4位十六进制字符串，主要用于可读日志输出
	 * @param word 单字
	 * @return 十六进制格式字符串
	 */
	public static String word2FormatHex(int word) {
		String binary = Integer.toHexString(Short.toUnsignedInt((short) word));
		StringBuilder stringBuilder = new StringBuilder();
		int i = 4;
		while (i-- - binary.length() > 0) {
			stringBuilder.append("0");
		}
		stringBuilder.append(binary);
		return stringBuilder.toString();
	}

	/**
	 * word字转换成为4位十六进制字符串，主要用于可读日志输出 word2FormatHex 方法名称缩写
	 * @param word 单字
	 * @return 十六进制格式字符串
	 */
	public static String w2FH(int word) {
		return word2FormatHex(word);
	}

	public static String[] w2FH(int[] words) {
		String[] hex = new String[words.length];
		for (int i = 0; i < words.length; i++) {
			hex[i] = word2FormatHex(words[i]);
		}
		return hex;
	}

	public static String[] w2FH(Integer[] words) {
		String[] hex = new String[words.length];
		for (int i = 0; i < words.length; i++) {
			hex[i] = word2FormatHex(words[i]);
		}
		return hex;
	}

	/**
	 * 整型值拆分成2个字
	 * @param value 待拆分的整型值
	 * @return 字数组，低16位索引0,高16位索引1
	 */
	public static int[] int2Words(int value) {
		int[] words = new int[2];
		words[0] = value & 0x0000FFFF;
		words[1] = (value >> 16) & 0x0000FFFF;
		return words;
	}

	/**
	 * 低位字高位字组装整型值
	 * @param low  低位字
	 * @param high 高位字
	 * @return 整型值
	 */
	public static Integer words2Int(Integer low, Integer high) {
		return (high << 16) | low;
	}

	/**
	 * 低位字高位字组装整型值
	 * @param low  低位字
	 * @param high 高位字
	 * @return 整型值
	 */
	public static int words2Int(int low, int high) {
		return (high << 16) | low;
	}
	
	public static int[] initWords(int size) {
		return initWords(size, 0);
	}

	public static int[] initWords(int size, int value) {
		int[] data = new int[size];
		for (int i = 0; i < size; i++) {
			data[i] = value;
		}
		return data;
	}

	public static Integer[] initIntegerWords(int size) {
		return initIntegerWords(size, 0);
	}
	
	public static Integer[] initIntegerWords(int size, int value) {
		Integer[] data = new Integer[size];
		for (int i = 0; i < size; i++) {
			data[i] = value;
		}
		return data;
	}
	
	public static List<Integer> initWordList(int size) {
		return initWordList(size, 0);
	}
	
	public static List<Integer> initWordList(int size, int value) {
		List<Integer> data = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			data.add(value);
		}
		return data;
	}

	/**
	 * 获取当前时间字数组
	 * @return words[0]:年 words[1]:月 words[2]:日 words[3]:时 words[4]:分 words[5]:秒
	 */
	public static Integer[] now() {
		LocalDateTime now = LocalDateTime.now();
		return new Integer[] { 
			now.getYear(), 
			now.getMonthValue(), 
			now.getDayOfMonth(), 
			now.getHour(), 
			now.getMinute(),
			now.getSecond() 
		};
	}

}
