package fun.pplm.framework.poplar.modbus.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.ghgande.j2mod.modbus.ModbusException;

import fun.pplm.framework.poplar.modbus.bean.po.ReadRegisterPo;
import fun.pplm.framework.poplar.modbus.bean.po.WriteRegisterPo;
import fun.pplm.framework.poplar.modbus.bean.vo.RegisterVo;
import fun.pplm.framework.poplar.modbus.psa.ModbusTcpPsa;
import fun.pplm.framework.poplar.modbus.utils.ModbusUtil;
import fun.pplm.framework.poplar.web.resp.Resp;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * modbus tcp服务api
 * @author OracleGao
 *
 */
public class ModbusTcpController extends ModbusController {
	@Autowired
	private ModbusTcpPsa modbusTcpService;
	
	@ApiOperation(value = "输入寄存器(IR)数据读取", notes = "读取输入寄存器数据")
	@GetMapping(value = "/read/IRs")
	public Resp<RegisterVo> readIRs(@Valid ReadRegisterPo po) throws ModbusException {
		int[] intData = modbusTcpService.readInputRegisters(po);
		RegisterVo vo = new RegisterVo();
		vo.setIntData(intData);
		vo.setBinaryData(ModbusUtil.w2FB(intData));
		vo.setHexData(ModbusUtil.w2FH(intData));
		return Resp.success(vo);
	}
	
	@ApiOperation(value = "保持寄存器(HR)数据读取", notes = "读取保持寄存器数据")
	@GetMapping(value = "/read/HRs")
	public Resp<RegisterVo> readHRs(@Valid ReadRegisterPo po) throws ModbusException {
		int[] intData = modbusTcpService.readHoldingRegisters(po);
		RegisterVo vo = new RegisterVo();
		vo.setIntData(intData);
		vo.setBinaryData(ModbusUtil.w2FB(intData));
		vo.setHexData(ModbusUtil.w2FH(intData));
		return Resp.success(vo);
	}
	
	@ApiOperation(value = "保持寄存器(HR)数据写入", notes = "写入保持寄存器数据")
	@PostMapping(value = "/write/HRs")
	public Resp<Void> writeHRs(@Valid @RequestBody WriteRegisterPo po) throws ModbusException {
		modbusTcpService.writeHoldingRegisters(po);
		return Resp.success();
	}
	
}
