package fun.pplm.framework.poplar.modbus.psi;

import java.util.List;

import fun.pplm.framework.poplar.modbus.bean.model.ModbusRegisterDataModel;

/**
 * 
 * modbus寄存器数据服务接口
 * 在modbus服务启动后，用于初始化寄存器数据
 * @author OracleGao
 *
 */
public interface ModbusRegisterDataPsi {
	
	/**
	 * 装载寄存器数据
	 * @return 寄存器数据列表
	 */
	public List<ModbusRegisterDataModel> loadData();
	
}
