package fun.pplm.framework.poplar.modbus.bean.qo;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.PositiveOrZero;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * modbus寄存器信息查询参数
 * @author OracleGao
 *
 */
@ApiModel(description = "modbus寄存器信息查询参数")
public class ModbusRegisterQo {
	@ApiModelProperty(value = "寄存器名称")
	private String regName;
	@ApiModelProperty(value = "寄存器编码")
	private String regCode;
	@Min(value = 0)
	@Max(value = 1)
	@ApiModelProperty(value = "寄存器符号类型 0: 无符号, 1: 有符号")
	private Integer regSign;
	@PositiveOrZero
	@ApiModelProperty(value = "起始地址左闭区间")
	private Integer startAddressBegin;
	@PositiveOrZero
	@ApiModelProperty(value = "起始地址右闭区间")
	private Integer startAddressEnd;
	@Min(value = 1)
	@ApiModelProperty(value = "字数左闭区间")
	private Integer wordCountBegin;
	@Min(value = 1)
	@ApiModelProperty(value = "字数右闭区间")
	private Integer wordCountEnd;

	public ModbusRegisterQo() {
		super();
	}

	public String getRegName() {
		return regName;
	}

	public void setRegName(String regName) {
		this.regName = regName;
	}

	public String getRegCode() {
		return regCode;
	}

	public void setRegCode(String regCode) {
		this.regCode = regCode;
	}

	public Integer getRegSign() {
		return regSign;
	}

	public void setRegSign(Integer regSign) {
		this.regSign = regSign;
	}

	public Integer getStartAddressBegin() {
		return startAddressBegin;
	}

	public void setStartAddressBegin(Integer startAddressBegin) {
		this.startAddressBegin = startAddressBegin;
	}

	public Integer getStartAddressEnd() {
		return startAddressEnd;
	}

	public void setStartAddressEnd(Integer startAddressEnd) {
		this.startAddressEnd = startAddressEnd;
	}

	public Integer getWordCountBegin() {
		return wordCountBegin;
	}

	public void setWordCountBegin(Integer wordCountBegin) {
		this.wordCountBegin = wordCountBegin;
	}

	public Integer getWordCountEnd() {
		return wordCountEnd;
	}

	public void setWordCountEnd(Integer wordCountEnd) {
		this.wordCountEnd = wordCountEnd;
	}
	
}
