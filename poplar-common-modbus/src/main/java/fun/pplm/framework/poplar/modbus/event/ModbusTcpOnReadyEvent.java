package fun.pplm.framework.poplar.modbus.event;

import java.time.Clock;

import org.springframework.context.ApplicationEvent;

/**
 * 
 * modbus tcp 服务ready事件
 * @author OracleGao
 *
 */
public class ModbusTcpOnReadyEvent extends ApplicationEvent {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8948226776124702237L;
	
	public ModbusTcpOnReadyEvent() {
		super("modbus tcp service on ready");
	}

	public ModbusTcpOnReadyEvent(Object source, Clock clock) {
		super(source, clock);
	}

	public ModbusTcpOnReadyEvent(Object source) {
		super(source);
	}

}
