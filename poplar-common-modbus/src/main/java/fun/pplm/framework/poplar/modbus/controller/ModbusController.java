package fun.pplm.framework.poplar.modbus.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;

import fun.pplm.framework.poplar.common.utils.BeanUtil;
import fun.pplm.framework.poplar.modbus.bean.model.ModbusRegisterModel;
import fun.pplm.framework.poplar.modbus.bean.qo.ModbusRegisterQo;
import fun.pplm.framework.poplar.modbus.bean.vo.ModbusRegisterVo;
import fun.pplm.framework.poplar.modbus.psi.ModbusPsi;
import fun.pplm.framework.poplar.web.resp.Resp;

import io.swagger.annotations.ApiOperation;

public class ModbusController {
	@Autowired
	private ModbusPsi modbusService;
	
	@ApiOperation(value = "输入寄存器(IR)信息查询", notes = "获取输入寄存器信息")
	@GetMapping(value = "/IRs")
	public Resp<List<ModbusRegisterVo>> getIRs(@Valid ModbusRegisterQo qo) {
		List<ModbusRegisterModel> models = modbusService.getInputRegisters(qo);
		List<ModbusRegisterVo> vos = BeanUtil.converts(models, ModbusRegisterVo.class);
		return Resp.success(vos);
	}
	
	@ApiOperation(value = "保持寄存器(HR)信息查询", notes = "获取保持寄存器信息")
	@GetMapping(value = "/HRs")
	public Resp<List<ModbusRegisterVo>> getHRs(@Valid ModbusRegisterQo qo) {
		List<ModbusRegisterModel> models = modbusService.getHoldingRegisters(qo);
		List<ModbusRegisterVo> vos = BeanUtil.converts(models, ModbusRegisterVo.class);
		return Resp.success(vos);
	}
	
}
