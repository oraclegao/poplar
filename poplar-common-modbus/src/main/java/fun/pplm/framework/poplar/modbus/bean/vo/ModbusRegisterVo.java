package fun.pplm.framework.poplar.modbus.bean.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * modbus寄存器信息
 * @author OracleGao
 *
 */
@ApiModel(description = "modbus寄存器信息")
public class ModbusRegisterVo {
	@ApiModelProperty(value = "寄存器名称")
	private String regName;
	@ApiModelProperty(value = "寄存器编码")
	private String regCode;
	@ApiModelProperty(value = "寄存器描述")
	private String regDesc;
	@ApiModelProperty(value = "寄存器类型 HR: 保持寄存器(Holding Register), IR: 输入寄存器(Input Register)")
	private String regType;
	@ApiModelProperty(value = "寄存器符号类型 0: 无符号, 1: 有符号")
	private Integer regSign;
	@ApiModelProperty(value = "起始地址 同 ref或offset")
	private Integer startAddress;
	@ApiModelProperty(value = "字数")
	private Integer wordCount;
	@ApiModelProperty(value = "相同regCode动态地址的排序顺序")
	private Integer addrOrder;
	
	public ModbusRegisterVo() {
		super();
	}

	public String getRegName() {
		return regName;
	}

	public void setRegName(String regName) {
		this.regName = regName;
	}

	public String getRegCode() {
		return regCode;
	}

	public void setRegCode(String regCode) {
		this.regCode = regCode;
	}

	public String getRegDesc() {
		return regDesc;
	}

	public void setRegDesc(String regDesc) {
		this.regDesc = regDesc;
	}

	public String getRegType() {
		return regType;
	}

	public void setRegType(String regType) {
		this.regType = regType;
	}

	public Integer getRegSign() {
		return regSign;
	}

	public void setRegSign(Integer regSign) {
		this.regSign = regSign;
	}

	public Integer getStartAddress() {
		return startAddress;
	}

	public void setStartAddress(Integer startAddress) {
		this.startAddress = startAddress;
	}

	public Integer getWordCount() {
		return wordCount;
	}

	public void setWordCount(Integer wordCount) {
		this.wordCount = wordCount;
	}

	public Integer getAddrOrder() {
		return addrOrder;
	}

	public void setAddrOrder(Integer addrOrder) {
		this.addrOrder = addrOrder;
	}
	
}
