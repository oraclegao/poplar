package fun.pplm.framework.poplar.modbus.bean.po;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * 读取寄存器参数
 * @author OracleGao
 *
 */
@ApiModel(description = "读取寄存器参数")
public class ReadRegisterPo {
	@NotBlank
	@ApiModelProperty(value = "寄存器编码")
	private String regCode;
	@ApiModelProperty(value = "从站id，默认取全局配置")
	private Integer slaveId;
	
	public ReadRegisterPo() {
		super();
	}

	public String getRegCode() {
		return regCode;
	}

	public void setRegCode(String regCode) {
		this.regCode = regCode;
	}

	public Integer getSlaveId() {
		return slaveId;
	}

	public void setSlaveId(Integer slaveId) {
		this.slaveId = slaveId;
	}
	
}
