package fun.pplm.framework.poplar.modbus.config;

import java.util.LinkedHashMap;
import java.util.Map;

import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * modbus tcp配置顶级类
 * @author OracleGao
 *
 */
public class ModbusTcpConfig {
	/**
	 * 服务绑定host地址
	 * 默认127.0.0.1
	 */
	private String host = "127.0.0.1";
	/**
	 * 服务端口
	 * 默认502
	 */
	private Integer port = 502;
	/**
	 * 从站id
	 * 默认1
	 */
	private Integer slaveId = 1;
	/**
	 * True if the RTU protocol should be used over TCP
	 * 默认false
	 */
	private Boolean useRtuOverTcp = false;
	/**
	 * regCode 大小写敏感
	 * true:敏感,false:不敏感
	 * 默认 false
	 */
	private Boolean regCodeCaseSensitive = false;
	/**
	 * 开启api服务
	 * 默认true 开启
	 */
	private Boolean apiEnabled = true;

	public ModbusTcpConfig() {
		super();
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public Integer getSlaveId() {
		return slaveId;
	}

	public void setSlaveId(Integer slaveId) {
		this.slaveId = slaveId;
	}

	public Boolean getUseRtuOverTcp() {
		return useRtuOverTcp;
	}

	public void setUseRtuOverTcp(Boolean useRtuOverTcp) {
		this.useRtuOverTcp = useRtuOverTcp;
	}

	public Boolean getRegCodeCaseSensitive() {
		return regCodeCaseSensitive;
	}

	public void setRegCodeCaseSensitive(Boolean regCodeCaseSensitive) {
		this.regCodeCaseSensitive = regCodeCaseSensitive;
	}
	
	public Boolean getApiEnabled() {
		return apiEnabled;
	}

	public void setApiEnabled(Boolean apiEnabled) {
		this.apiEnabled = apiEnabled;
	}

	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("host", host);
		map.put("port", port);
		map.put("slaveId", slaveId);
		map.put("useRtuOverTcp", useRtuOverTcp);
		map.put("regCodeCaseSensitive", regCodeCaseSensitive);
		map.put("apiEnabled", apiEnabled);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
	
}
