package fun.pplm.framework.poplar.modbus.psi;

import java.util.List;

import fun.pplm.framework.poplar.modbus.bean.model.ModbusRegisterModel;

/**
 * 
 * Poplar Service Interface
 * 获取寄存器服务接口
 * @author OracleGao
 *
 */
public interface ModbusRegisterPsi {
	
	/**
	 * 装载寄存器
	 * @return 寄存器列表
	 */
	public List<ModbusRegisterModel> load();

}
