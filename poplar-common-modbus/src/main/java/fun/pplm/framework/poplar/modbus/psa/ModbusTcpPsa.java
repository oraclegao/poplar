package fun.pplm.framework.poplar.modbus.psa;

import static fun.pplm.framework.poplar.modbus.utils.ModbusConstant.REG_TYPE_HR;
import static fun.pplm.framework.poplar.modbus.utils.ModbusConstant.REG_TYPE_IR;
import static fun.pplm.framework.poplar.modbus.utils.ModbusConstant.WORD_SIGN_MAX_VALUE;
import static fun.pplm.framework.poplar.modbus.utils.ModbusConstant.WORD_SIGN_MIN_VALUE;
import static fun.pplm.framework.poplar.modbus.utils.ModbusConstant.WORD_UNSIGN_MAX_VALUE;
import static fun.pplm.framework.poplar.modbus.utils.ModbusConstant.WORD_UNSIGN_MIN_VALUE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ghgande.j2mod.modbus.ModbusException;
import com.ghgande.j2mod.modbus.procimg.InputRegister;
import com.ghgande.j2mod.modbus.procimg.Register;

import fun.pplm.framework.poplar.common.utils.ArrayUtil;
import fun.pplm.framework.poplar.common.validator.BeanValidator;
import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.modbus.bean.model.ModbusRegisterDataModel;
import fun.pplm.framework.poplar.modbus.bean.model.ModbusRegisterModel;
import fun.pplm.framework.poplar.modbus.bean.po.ReadRegisterPo;
import fun.pplm.framework.poplar.modbus.bean.po.WriteRegisterPo;
import fun.pplm.framework.poplar.modbus.config.ModbusTcpConfig;
import fun.pplm.framework.poplar.modbus.psi.ModbusPsi;
import fun.pplm.framework.poplar.modbus.psi.ModbusRegisterDataPsi;
import fun.pplm.framework.poplar.modbus.psi.ModbusRegisterPsi;

/**
 * 
 * Poplar Service abstract
 * modbus tcp服务抽象类
 * @author OracleGao
 *
 */
public abstract class ModbusTcpPsa implements ModbusPsi {
	private static Logger logger = LoggerFactory.getLogger(ModbusTcpPsa.class);
	
	protected ModbusTcpConfig modbusTcpConfig;
	
	private BeanValidator beanValidator;

	protected ModbusRegisterPsi modbusRegisterService;
	
	protected ModbusRegisterDataPsi modbusRegisterDataService;

	/**
	 * regCode -&gt; modbusRegisterModels
	 */
	protected volatile Map<String, List<ModbusRegisterModel>> holdingRegisterMap = new HashMap<>();
	/**
	 * regCode -&gt; modbusRegisterModels
	 */
	protected volatile Map<String, List<ModbusRegisterModel>> inputRegisterMap = new HashMap<>();
	
	public ModbusTcpPsa() {
		super();
	}
	
	protected void init(ModbusTcpConfig modbusTcpConfig, BeanValidator beanValidator, ModbusRegisterPsi modbusRegisterService) {
		init(modbusTcpConfig, beanValidator, modbusRegisterService, null);
	}

	protected void init(ModbusTcpConfig modbusTcpConfig, BeanValidator beanValidator, ModbusRegisterPsi modbusRegisterService, ModbusRegisterDataPsi modbusRegisterDataService) {
		this.modbusTcpConfig = modbusTcpConfig;
		this.beanValidator = beanValidator;
		this.modbusRegisterService = modbusRegisterService;
		this.modbusRegisterDataService = modbusRegisterDataService;
		initRegisterMap();
		initModbus();
		initRegisterData();
	}
	
	/**
	 * 初始化寄存器管理信息
	 */
	private void initRegisterMap() {
		List<ModbusRegisterModel> models = modbusRegisterService.load();
		if (models.isEmpty()) {
			throw new ValidationException("寄存器元数据为空");
		}
		logger.debug("初始化寄存器元数据开始...");
		String regCode;
		for (ModbusRegisterModel model : models) {
			try {
				beanValidator.validate(model);
			} catch (ValidationException e) {
				throw new ValidationException(e.getMessage() + ", model: " + model, e);
			}
			if (model.getRegSign() == null) {
				model.setRegSign(0);
			}
			if (model.getAddrOrder() == null) {
				model.setAddrOrder(0);
			}
			regCode = model.getRegCode();
			if(!modbusTcpConfig.getRegCodeCaseSensitive()) {
				regCode = regCode.toLowerCase();
			}
			Map<String, List<ModbusRegisterModel>> map = null;
			switch (model.getRegType().toUpperCase()) {
			case REG_TYPE_HR:
				map = holdingRegisterMap;
				break;
			case REG_TYPE_IR:
				map = inputRegisterMap;
				break;
			}
			if (map.containsKey(regCode)) {
				models = map.get(regCode);
				if (models.contains(model)) {
					throw new ValidationException("寄存器配置信息重复, regCode: " + regCode + ", regType: " + model.getRegType() + ", addrOrder: " + model.getAddrOrder());
				}
				models.add(model);
			} else {
				models = new ArrayList<>();
				models.add(model);
				map.put(regCode, models);
			}
			inputRegisterMap.values().forEach(Collections::sort);
			holdingRegisterMap.values().forEach(Collections::sort);
		}
		logger.debug("初始化寄存器元数据完成");
	}
	
	/**
	 * 子类实现该方法初始化具体的modbus服务
	 */
	protected abstract void initModbus();
	
	/**
	 * 在modbus服务初始化完成后初始化寄存器数据
	 */
	protected void initRegisterData() {
		if (modbusRegisterDataService != null) {
			List<ModbusRegisterDataModel> models = modbusRegisterDataService.loadData();
			if (models.size() > 0) {
				logger.debug("初始化寄存器数据开始...");
				Integer slaveId = null;
				for (ModbusRegisterDataModel model : models) {
					slaveId = model.getSlaveId();
					if (slaveId == null) {
						slaveId = modbusTcpConfig.getSlaveId();
					}
					switch(model.getRegType()) {
					case REG_TYPE_HR:
						initHoldingRegisterData(slaveId, model.getRegCode(), model.getRegData());
						break;
					case REG_TYPE_IR:
						initInputRegisterData(slaveId, model.getRegCode(), model.getRegData());
						break;
					}
				}
				logger.debug("初始化寄存器数据完成");
			}
		}
	}
	
	/**
	 * 初始化保持寄存器数据
	 * @param slaveId 从站id
	 * @param regCode 寄存器编码
	 * @param data 数据
	 */
	protected void initHoldingRegisterData(int slaveId, String regCode, List<Integer> data) {
		try {
			writeHoldingRegisters(slaveId, regCode, data);
		} catch (ModbusException e) {
			logger.warn("跳过异常的保持寄存器初始化数据, slaveId: {}, regCode: {}, data: {}, message: {}", slaveId, regCode, Json.string(data), e.getMessage());
		}
	}
	
	/**
	 * 初始化输入寄存器数据
	 * @param slaveId 从站id
	 * @param regCode 寄存器编码
	 * @param data 数据
	 */
	protected void initInputRegisterData(int slaveId, String regCode, List<Integer> data) {
		logger.warn("无法初始化输入寄存器数据, slaveId: {}, regCode: {}, data: {}", slaveId, regCode, Json.string(data));
	}
	
	/**
	 * 读保持寄存器
	 * @param regCode 寄存器编码
	 * @return 读取数据
	 * @throws ModbusException modbus异常
	 */
	public int readHoldingRegister(String regCode) throws ModbusException {
		return readHoldingRegisters(regCode)[0];
	}

	/**
	 * 读多保持寄存器
	 * @param regCode 寄存器编码
	 * @return 读取数据
	 * @throws ModbusException modbus异常
	 */
	public int[] readHoldingRegisters(String regCode) throws ModbusException {
		return readHoldingRegisters(null, regCode);
	}
	
	/**
	 * 读多保持寄存器
	 * @param po 寄存器参数
	 * @return 读取数据
	 * @throws ModbusException modbus异常
	 */
	public int[] readHoldingRegisters(ReadRegisterPo po) throws ModbusException {
		beanValidator.validate(po);
		return readHoldingRegisters(po.getSlaveId(), po.getRegCode());
	}
	
	/**
	 * 读多保持寄存器
	 * @param slaveId 从站id
	 * @param regCode 寄存器编码
	 * @return 读取数据
	 * @throws ModbusException modbus异常
	 */
	public int[] readHoldingRegisters(Integer slaveId, String regCode) throws ModbusException {
		return readRegisters(slaveId, regCode, REG_TYPE_HR);
	}

	/**
	 * 读输入寄存器
	 * @param regCode 寄存器编码
	 * @return 读取数据
	 * @throws ModbusException modbus异常
	 */
	public int readInputRegister(String regCode) throws ModbusException {
		return readInputRegisters(regCode)[0];
	}

	/**
	 * 读多输入寄存器
	 * @param regCode 寄存器编码
	 * @return 读取数据
	 * @throws ModbusException modbus异常
	 */
	public int[] readInputRegisters(String regCode) throws ModbusException {
		return readInputRegisters(null, regCode);
	}
	
	/**
	 * 读多输入寄存器
	 * @param po 读寄存器参数
	 * @return 读取数据
	 * @throws ModbusException modbus异常
	 */
	public int[] readInputRegisters(ReadRegisterPo po) throws ModbusException {
		beanValidator.validate(po);
		return readInputRegisters(po.getSlaveId(), po.getRegCode());
	}
	
	/**
	 * 读多输入寄存器
	 * @param slaveId 从站id
	 * @param regCode 寄存器编码
	 * @return 读取数据
	 * @throws ModbusException modbus异常
	 */
	public int[] readInputRegisters(Integer slaveId, String regCode) throws ModbusException {
		return readRegisters(slaveId, regCode, REG_TYPE_IR);
	}

	public int[] readRegisters(String regCode, String regType) throws ModbusException {
		return readRegisters(null, regCode, regType);
	}
	
	public int[] readRegisters(Integer slaveId, String regCode, String regType) throws ModbusException {
		if (StringUtils.isBlank(regCode)) {
			throw new ValidationException("regCode is blank");
		}
		if (slaveId == null) {
			slaveId = modbusTcpConfig.getSlaveId();
		}
		List<ModbusRegisterModel> models = getRegisters(regCode, regType);
		int size = models.size();
		int[][] data2D = new int[size][];
		ModbusRegisterModel model = null;
		for (int idx = 0; idx < size; idx++) {
			model = models.get(idx);
			int startAddress = model.getStartAddress();
			int wordCount = model.getWordCount();
			int[] values = new int[wordCount];
			InputRegister[] registers = null;
			switch (regType) {
			case REG_TYPE_HR:
				logger.debug("读取保持寄存器, addr: {}, count: {}", startAddress, wordCount);
				registers = readHoldingRegisters(slaveId, startAddress, wordCount);
				break;
			case REG_TYPE_IR:
				logger.debug("读取输入寄存器, addr: {}, count: {}", startAddress, wordCount);
				registers = readInputRegisters(slaveId, startAddress, wordCount);
				break;
			default:
				throw new RuntimeException("无效寄存器类型, regType: " + regType);
			}
			if (model.getRegSign() == 1) { // 有符号数
				for (int i = 0; i < wordCount; i++) {
					values[i] = (int) registers[i].toShort();
				}
			} else { // 无符号数
				for (int i = 0; i < wordCount; i++) {
					values[i] = (int) registers[i].toUnsignedShort();
				}
			}
			data2D[idx] = values;
			logger.debug("读取寄存器数据, regCode: {}, regType: {}, startAddress: {}, wordCount: {}, data: {}", regCode, regType, startAddress, wordCount, Json.string(values));
		}
		if (size == 1) {
			return data2D[0];
		} else {
			int[] data = ArrayUtil.expandedByRow(data2D);
			logger.debug("整合寄存器动态地址数据片段, regCode: {}, regType: {}, wordCount: {}, data: {}", regCode, regType, data.length, Json.string(data));
			return data;
		}
	}
	
	/**
	 * 写保持寄存器
	 * @param regCode 寄存器编码
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeHoldingRegister(String regCode, int data) throws ModbusException {
		writeHoldingRegister(null, regCode, data);
	}
	
	/**
	 * 写保持寄存器
	 * @param slaveId 从站id
	 * @param regCode 寄存器编码
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeHoldingRegister(Integer slaveId, String regCode, int data) throws ModbusException {
		List<Integer> values = new ArrayList<>();
		values.add(data);
		writeHoldingRegisters(slaveId, regCode, values);
	}
	
	/**
	 * 写多保持寄存器
	 * @param regCode 寄存器编码
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeHoldingRegisters(String regCode, int[] data) throws ModbusException {
		writeHoldingRegisters(null, regCode, data);
	}
	
	/**
	 * 写多保持寄存器
	 * @param slaveId 从站id
	 * @param regCode 寄存器编码
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeHoldingRegisters(Integer slaveId, String regCode, int[] data) throws ModbusException {
		if (data == null) {
			throw new ValidationException("data is null");
		}
		;
		List<Integer> values = new ArrayList<>();
		for (Integer value : data) {
			values.add(value);
		}
		writeHoldingRegisters(regCode, values);
	}
	
	/**
	 * 写多保持寄存器
	 * @param regCode 寄存器编码
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeHoldingRegisters(String regCode, Integer[] data) throws ModbusException {
		writeHoldingRegisters(null, regCode, data);
	}

	/**
	 * 写多保持寄存器
	 * @param slaveId 从站id
	 * @param regCode 寄存器编码
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeHoldingRegisters(Integer slaveId, String regCode, Integer[] data) throws ModbusException {
		if (data == null) {
			throw new ValidationException("data is null");
		}
		writeHoldingRegisters(slaveId, regCode, Arrays.asList(data));
	}
	
	/**
	 * 写多保持寄存器
	 * @param regCode 寄存器编码
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeHoldingRegisters(String regCode, List<Integer> data) throws ModbusException {
		writeHoldingRegisters(null, regCode, data);
	}
	
	/**
	 * 写多保持寄存器
	 * @param po 写寄存器参数
	 * @throws ModbusException modbus异常
	 */
	public void writeHoldingRegisters(WriteRegisterPo po) throws ModbusException {
		beanValidator.validate(po);
		writeHoldingRegisters(po.getSlaveId(), po.getRegCode(), po.getWords());
	}
	
	/**
	 * 写多保持寄存器
	 * @param slaveId 从站id
	 * @param regCode 寄存器编码
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeHoldingRegisters(Integer slaveId, String regCode, List<Integer> data) throws ModbusException {
		writeRegisters(slaveId, regCode, REG_TYPE_HR, data);
	}
	
	/**
	 * 写多寄存器<br>
	 * 寄存器连续地址的数据，数据字长不足地址字长会自动填充0，大于地址字长的数据会被截取<br>
	 * 动态地址数据会按照动态地址顺序和字长拆分后顺序写入<br>
	 * 保持寄存器动态地址的数据，数据字长不等于动态地址字长和，会抛出ValidationException
	 * @param slaveId 从站id
	 * @param regCode 寄存器编码
	 * @param regType 寄存器类型 HR:保持寄存器 IR:输入寄存器
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeRegisters(Integer slaveId, String regCode, String regType, List<Integer> data) throws ModbusException {
		if (StringUtils.isBlank(regCode)) {
			throw new ValidationException("regCode is blank");
		}
		if (data == null) {
			throw new ValidationException("data is null");
		}
		if (slaveId == null) {
			slaveId = modbusTcpConfig.getSlaveId();
		}
		List<ModbusRegisterModel> models = getRegisters(regCode, regType);
		int size = models.size();
		if (size == 1) {
			ModbusRegisterModel model = models.get(0);
			int startAddress = model.getStartAddress();
			int wordCount = model.getWordCount();
			List<Integer> values = fixData(regCode, wordCount, data);
			checkData(regCode, model.getRegSign(), values, 0);
			logger.debug("写入寄存器, regCode: {}, regType: {}, startAddress: {}, wordCount: {}, data: {}", regCode, regType, startAddress, wordCount, Json.string(values));
			switch (regType) {
			case REG_TYPE_HR:
				writeHoldingRegisters(slaveId, startAddress, values);
				break;
			case REG_TYPE_IR:
				writeInputRegisters(slaveId, startAddress, values);
				break;
			default:
				throw new RuntimeException("无效寄存器类型, regCode: " + regCode + ", regType: " + regType);
			}
		} else if (size > 1) {
			int length = 0;
			for (ModbusRegisterModel model : models) {
				length += model.getWordCount();
			}
			int dataSize = data.size();
			if (length != dataSize) {
				throw new ValidationException("动态地址字长与数据字长不一致, addr word count: " + length + ", data word count: " + dataSize);
			}
			logger.debug("动态写入寄存器, regCode: {}, regType: {}, wordCount: {}, data: {}", regCode, regType, dataSize, Json.string(data));
			List<Integer> subData = null;
			int beginIdx = 0;
			int endIdx = 0;
			int wordCount = 0;
			int startAddress = 0;
			for (ModbusRegisterModel model : models) {
				wordCount = model.getWordCount();
				startAddress = model.getStartAddress();
				endIdx = beginIdx + wordCount;
				subData = data.subList(beginIdx, endIdx);
				checkData(regCode, model.getRegSign(), subData, beginIdx);
				logger.debug("数据片段动态写入寄存器, regCode: {}, regType: {}, startAddress: {}, wordCount: {}, beginIdx: {}, endIdx: {}, subData: {}", regCode, regType, startAddress, wordCount, beginIdx, endIdx, Json.string(subData));
				switch (regType) {
				case REG_TYPE_HR:
					writeHoldingRegisters(slaveId, startAddress, subData);
					break;
				case REG_TYPE_IR:
					writeInputRegisters(slaveId, startAddress, subData);
					break;
				default:
					throw new RuntimeException("无效寄存器类型, regCode: " + regCode + ", regType: " + regType);
				}
				beginIdx = endIdx;
			}
		} else {
			throw new ValidationException("寄存器信息为空, regCode: " + regCode + ", regType: " + regType);
		}
	}
	
	/**
	 * 不足字长填充0，超过字长截取
	 * @param data      数据
	 * @param wordCount 字长
	 */
	private List<Integer> fixData(String regCode, int wordCount, List<Integer> data) {
		int size = data.size();
		List<Integer> values = new ArrayList<>();
		if (size < wordCount) {
			values.addAll(data);
			for (int i = 0; i < wordCount - size; i++) {
				values.add(0);
			}
			logger.warn("数据长度小于字长，使用0填充到字长, regCode: {}, size: {}, wordCount: {}, data: {}", regCode, size, wordCount, Json.string(values));
		} else if (size > wordCount) {
			values = data.subList(0, wordCount);
			logger.warn("数据长度大于字长，数据被截取到字长, regCode: {}, size: {}, wordCount: {}, data: {}, truncatedData: {}", regCode, size, wordCount, Json.string(values), Json.string(data.subList(wordCount, data.size())));
		} else {
			values.addAll(data);
		}
		return values;
	}
	
	private void checkData(String regCode, int regSign, List<Integer> data, int offset) {
		int value = 0;
		if (regSign == 1) { //有符号
			for (int i = 0; i < data.size(); i++) {
				value = data.get(i);
				if (value < WORD_SIGN_MIN_VALUE || value > WORD_SIGN_MAX_VALUE) {
					logger.warn("有符号字越界[{}, {}], regCode:{}, idx: {} value: {}", WORD_SIGN_MIN_VALUE, WORD_SIGN_MAX_VALUE, regCode, i + offset, value);
				}
			}
		} else { //无符号
			for (int i = 0; i < data.size(); i++) {
				value = data.get(i);
				if (value < WORD_UNSIGN_MIN_VALUE || value > WORD_UNSIGN_MAX_VALUE) {
					logger.warn("无符号字越界[{}, {}], regCode:{}, idx: {}, value: {}", WORD_UNSIGN_MIN_VALUE, WORD_UNSIGN_MAX_VALUE, regCode, i + offset, value);
				}
			}
		}
	}
	
	@Override
	public List<ModbusRegisterModel> getInputRegisterModels() {
		return inputRegisterMap.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
	}
	
	@Override
	public List<ModbusRegisterModel> getHoldingRegisterModels() {
		return holdingRegisterMap.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
	}

	@Override
	public List<ModbusRegisterModel> getRegisters(String regCode, String regType) {
		if (StringUtils.isBlank(regCode)) {
			throw new ValidationException("regCode is blank");
		}
		if (!modbusTcpConfig.getRegCodeCaseSensitive()) {
			regCode = regCode.toLowerCase();
		}
		switch (regType) {
		case REG_TYPE_HR:
			if (!holdingRegisterMap.containsKey(regCode)) {
				throw new ValidationException("没有找到保持寄存器配置信息, regCode: " + regCode);
			}
			return holdingRegisterMap.get(regCode);
		case REG_TYPE_IR:
			if (!inputRegisterMap.containsKey(regCode)) {
				throw new ValidationException("没有找到输入寄存器配置信息, regCode: " + regCode);
			}
			return inputRegisterMap.get(regCode);
		default:
			throw new ValidationException("无效寄存器类型, regType: " + regType);
		}
	}
	
	/**
	 * 读取保持寄存器
	 * @param slaveId 从站id
	 * @param startAddress 起始地址
	 * @param wordCount 字长
	 * @return 保持寄存器数组
	 * @throws ModbusException modbus异常
	 */
	protected abstract Register[] readHoldingRegisters(int slaveId, int startAddress, int wordCount) throws ModbusException;

	/**
	 * 读取输入寄存器
	 * @param slaveId 从站id
	 * @param startAddress 起始地址
	 * @param wordCount 字长
	 * @return 输入寄存器数组
	 * @throws ModbusException modbus异常
	 */
	protected abstract InputRegister[] readInputRegisters(int slaveId, int startAddress, int wordCount) throws ModbusException;

	/**
	 * 写多保持寄存器
	 * @param slaveId 从站id
	 * @param startAddress 起始地址
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	protected abstract void writeHoldingRegisters(int slaveId, int startAddress, List<Integer> data) throws ModbusException;
	
	/**
	 * 写入输入寄存器
	 * @param slaveId 从站id
	 * @param startAddress 起始地址
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	protected void writeInputRegisters(int slaveId, int startAddress, List<Integer> data) throws ModbusException {
		throw new ModbusException("can not write input registers");
	}
	
	public ModbusRegisterPsi getModbusRegisterService() {
		return modbusRegisterService;
	}

	public void setModbusRegisterService(ModbusRegisterPsi modbusRegisterService) {
		this.modbusRegisterService = modbusRegisterService;
	}
}
