## 1.8.16
- 抽象类类名后缀更名为Psa: SerialPortService -> SerialPortPsa
- 侦听接口类名后缀更名为Pli: SerialPortEventListener -> SerialPortEventPli
- 规范串口监听服务与串口服务映射获取名称回调函数的函数名：serialPortNamesListened -> serialPortNames
- 添加初始化串口服务失败的监听回调函数，并且初始化失败不再直接抛异常，而是允许业务服务通过实现SerialPortConnectPli接口，处理串口初始化失败的情况

## 1.6.11
- 串口业务侦听接口回调函数重命名，避免与串口回调函数重名，导致内部函数回调重写冲突

## 1.5.7
- 修复串口抽象服务类初始化config对象空指针问题
- 增加对于不需要发送指令的Void类型，不启动消费队列线程功能
- 添加只接收串口数据的串口服务实现类ReceiveOnlySerialPortService
- 串口侦听器增加获取需要侦听的串口名称集合，当服务中存在多个串口服务时，springboot会将所有侦听器自动注册到到所有串口服务中，串口服务通过匹配侦听器返回的串口名称集合来配置侦听关系。侦听器默认侦听所有串口服务
- SerialPortService添加@PreDestory销毁方法

## 1.4.6
- 初始化
- 提供串口服务框架