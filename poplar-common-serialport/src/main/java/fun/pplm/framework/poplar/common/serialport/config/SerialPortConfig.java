package fun.pplm.framework.poplar.common.serialport.config;

/**
 * 
 * 串口配置
 * @author OracleGao
 *
 */
public class SerialPortConfig {
	/**
	 * 串口名称
	 */
	private String name;
	/**
	 * 波特率
	 * 默认: 9600
	 */
	private Integer baudrate = 9600;
	/**
	 * 数据位
	 * 默认: 8
	 */
	private Integer dataBits = 8;
	/**
	 * 停止位
	 * 默认: 1
	 */
    private Integer stopBits = 1;
    /**
     * 奇偶校验
     * 默认: 0
     */
    private Integer parity = 0;
    /**
     * mock模式
     * 默认 false
     * true: 启动mock模式，不连接串口，用于调试
     */
    private Boolean mock = false;
	/**
	 * 初始化连接重试次数
	 * 0: 不重试
	 * < 0: 无限次，直到连接成功
	 * > 0: 重试次数
	 * 默认: -1
	 * 重试时间间隔(单位：秒): 2^(n-1),最大间隔: 32s
	 */
	private Integer initRetries = -1;
	/**
	 * 断线自动重连
	 * 默认: ture
	 */
	private Boolean autoReconnect = true;

	public SerialPortConfig() {
		super();
	}

	public SerialPortConfig(String name, Integer baudrate) {
		super();
		this.name = name;
		this.baudrate = baudrate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getBaudrate() {
		return baudrate;
	}

	public void setBaudrate(Integer baudrate) {
		this.baudrate = baudrate;
	}

	public Integer getDataBits() {
		return dataBits;
	}

	public void setDataBits(Integer dataBits) {
		this.dataBits = dataBits;
	}

	public Integer getStopBits() {
		return stopBits;
	}

	public void setStopBits(Integer stopBits) {
		this.stopBits = stopBits;
	}

	public Integer getParity() {
		return parity;
	}

	public void setParity(Integer parity) {
		this.parity = parity;
	}

	public Boolean getMock() {
		return mock;
	}

	public void setMock(Boolean mock) {
		this.mock = mock;
	}

	public Integer getInitRetries() {
		return initRetries;
	}

	public void setInitRetries(Integer initRetries) {
		this.initRetries = initRetries;
	}

	public Boolean getAutoReconnect() {
		return autoReconnect;
	}

	public void setAutoReconnect(Boolean autoReconnect) {
		this.autoReconnect = autoReconnect;
	}
	
}
