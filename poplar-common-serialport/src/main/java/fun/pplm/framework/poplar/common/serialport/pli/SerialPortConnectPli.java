package fun.pplm.framework.poplar.common.serialport.pli;

import fun.pplm.framework.poplar.common.serialport.config.SerialPortConfig;

/**
 * 
 * 串口连接侦听
 * @author OracleGao
 *
 */
public interface SerialPortConnectPli extends SerialPortPli {
	/**
	 * 串口连接失败回调
	 * 该方法与串口初始化方法在同一线程内，需要及时执行完成并返回
	 * @param config 串口配置
	 */
	public void failed(SerialPortConfig config);

}
