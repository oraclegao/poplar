package fun.pplm.framework.poplar.common.serialport.pli;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortEvent;

/**
 * 
 * Poplar Listener Interface
 * 串口事件侦听器接口
 * @author OracleGao
 *
 */
public interface SerialPortEventPli extends SerialPortPli {
	
	/**
	 * 监听的串口事件回调
	 * <p>
	 * 默认返回 {@link SerialPort#LISTENING_EVENT_DATA_AVAILABLE} | {@link SerialPort#LISTENING_EVENT_DATA_RECEIVED} 
	 * @see SerialPort#LISTENING_EVENT_DATA_AVAILABLE
	 * @see SerialPort#LISTENING_EVENT_DATA_RECEIVED
	 * @see SerialPort#LISTENING_EVENT_DATA_WRITTEN
	 * @see SerialPort#LISTENING_EVENT_PORT_DISCONNECTED
	 * @see SerialPort#LISTENING_EVENT_BREAK_INTERRUPT
	 * @see SerialPort#LISTENING_EVENT_CARRIER_DETECT
	 * @see SerialPort#LISTENING_EVENT_CTS
	 * @see SerialPort#LISTENING_EVENT_DSR
	 * @see SerialPort#LISTENING_EVENT_RING_INDICATOR
	 * @see SerialPort#LISTENING_EVENT_FRAMING_ERROR
	 * @see SerialPort#LISTENING_EVENT_FIRMWARE_OVERRUN_ERROR
	 * @see SerialPort#LISTENING_EVENT_SOFTWARE_OVERRUN_ERROR
	 * @see SerialPort#LISTENING_EVENT_PARITY_ERROR
	 * @return 事件值
	 */
	public default int eventsListened() {
		return SerialPort.LISTENING_EVENT_DATA_AVAILABLE | SerialPort.LISTENING_EVENT_DATA_RECEIVED;
	}
	
	/**
	 * 串口事件达到回调
	 * @param event 事件对象
	 */
	public void eventArrived(SerialPortEvent event);
	
}
