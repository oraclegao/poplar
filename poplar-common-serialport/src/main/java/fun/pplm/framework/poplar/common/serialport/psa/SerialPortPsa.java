package fun.pplm.framework.poplar.common.serialport.psa;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PreDestroy;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

import fun.pplm.framework.poplar.common.runnable.ConvertionQueue;
import fun.pplm.framework.poplar.common.serialport.config.SerialPortConfig;
import fun.pplm.framework.poplar.common.serialport.pli.SerialPortConnectPli;
import fun.pplm.framework.poplar.common.serialport.pli.SerialPortEventPli;
import fun.pplm.framework.poplar.common.utils.ByteUtil;
import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * Poplar Service abstract
 * 串口服务抽象框架
 * @author OracleGao
 *
 * @param <T> 串口数据类型
 */
public abstract class SerialPortPsa<T> extends ConvertionQueue<T, byte[]> implements SerialPortDataListener, HealthIndicator, Closeable {
	private static Logger logger = LoggerFactory.getLogger(SerialPortPsa.class);
	/**
	 * 初始化重试最大等待时间间隔
	 * 单位：秒
	 */
	private static final Integer INIT_RETRY_MAX_INTERVAL = 32;
	
	private volatile SerialPort serialPort;
	
	protected SerialPortConfig config;
	
	protected volatile boolean destory = false; 
	
	@Autowired(required = false)
	private List<SerialPortConnectPli> serialPortConnectPlis = new ArrayList<>();
	
	@Autowired(required = false)
	private List<SerialPortEventPli> serialPortEventPlis = new ArrayList<>();
	
	public SerialPortPsa(int queueSize, long interval) {
		super(queueSize, interval);
	}

	public SerialPortPsa(int queueSize, String name, long interval) {
		super(queueSize, name, interval);
	}

	public SerialPortPsa(int queueSize, String name) {
		super(queueSize, name);
	}

	public SerialPortPsa(int queueSize) {
		super(queueSize);
	}
	
	public void init(SerialPortConfig config) {
		this.config = config;
		if (StringUtils.isBlank(super.name)) {
			super.name = this.getClass().getSimpleName();
		}
		String serialPortName = config.getName();
		if (StringUtils.isBlank(serialPortName)) {
			throw new RuntimeException("串口名不能为空");
		}
		//只保留当前串口服务侦听器
		serialPortEventPlis = serialPortEventPlis.stream().filter(pli -> {
			List<String> names = pli.serialPortNames();
			if (names == null || names.size() == 0) {
				return true;
			} else {
				for (String name : names) {
					if (serialPortName.equalsIgnoreCase(name)) {
						return true;
					}
				}
			}
			return false;
		}).collect(Collectors.toList());
		//只保留当前串口服务侦听器
		serialPortConnectPlis = serialPortConnectPlis.stream().filter(pli -> {
			List<String> names = pli.serialPortNames();
			if (names == null || names.size() == 0) {
				return true;
			} else {
				for (String name : names) {
					if (serialPortName.equalsIgnoreCase(name)) {
						return true;
					}
				}
			}
			return false;
		}).collect(Collectors.toList());
		if (config.getMock()) {
			logger.warn("{}: 串口服务mock状态启动", name);
		} else {
			serialPort = SerialPort.getCommPort(serialPortName);
	        serialPort.setBaudRate(config.getBaudrate());
	        serialPort.setNumDataBits(config.getDataBits());
	        serialPort.setNumStopBits(config.getStopBits());
	        serialPort.setParity(config.getParity());
	        serialPort.addDataListener(this);
	        connect(config.getInitRetries());
		}
		String typeName = Json.getGenericTypeName(this, SerialPortPsa.class, 0);
		if (!"java.lang.Void".equals(typeName)) {
			super.startup();
		}
	}
	
	public void connect(int initRetries) {
		if (serialPort.isOpen()) {
			return;
		}
		int initRetryInterval = 1;
		String serialPortName = config.getName();
		while(true) {
			if (destory) {
				break;
			}
			if (serialPort.openPort()) {
				logger.debug("{}: 打开串口 {} 成功", name, serialPortName);
				break;
			}
			if (initRetries == 0) {
				if (serialPortConnectPlis.size() > 0) {
					serialPortConnectPlis.forEach(pli -> pli.failed(config));
					break;
				} else {
					throw new RuntimeException(name + ": 打开串口 " + serialPortName + " 失败");
				}
			} else {
				logger.warn("{}: 打开串口 {} 失败，等待{}秒重试...", name, serialPortName, initRetryInterval);
				try {
					Thread.sleep(initRetryInterval * 1000L);
				} catch (InterruptedException e) {
					logger.error(e.getMessage(), e);
				}
				if (initRetryInterval < INIT_RETRY_MAX_INTERVAL) {
					initRetryInterval = initRetryInterval << 1;
				}
				if(initRetries > 0) {
					initRetries--;
				}
			}
		}
	}
	
	@Override
	protected final byte[] getConsumed(T t) throws Exception {
		return getSendData(t);
	}
	
	/**
	 * 对象转换成发送串口的字节数组
	 * @param t 对象
	 * @return 字节数组
	 */
	protected abstract byte[] getSendData(T t);
	
	@Override
	protected void consume(byte[] p) {
		logger.debug("{}: 发送串口数据{}", name, ByteUtil.bytes2Hexs(p));
		if (!config.getMock()) {
			if (serialPort.isOpen()) {
				serialPort.writeBytes(p, p.length);
			} else {
				logger.error("{}: 串口状态异常, 丢掉发送数据", name);
			}
		}
	}
	
	/**
	 * 串口服务回调
	 * @return 事件值
	 */
	@Override
	public int getListeningEvents() {
		int eventType = 0;
		for (SerialPortEventPli serialPortEventPli : serialPortEventPlis) {
			eventType |= serialPortEventPli.eventsListened();
		}
		if (config.getAutoReconnect()) {
			eventType |= SerialPort.LISTENING_EVENT_PORT_DISCONNECTED;
		}
		return eventType;
	}

	/**
	 * 串口服务回调
	 * @param event 串口事件
	 */
	@Override
	public void serialEvent(SerialPortEvent event) {
		int eventType = event.getEventType();
		for (SerialPortEventPli serialPortEventPli : serialPortEventPlis) {
			if ((serialPortEventPli.eventsListened() & eventType) > 0) {
				serialPortEventPli.eventArrived(event);
			}
		}
		if ((eventType & SerialPort.LISTENING_EVENT_PORT_DISCONNECTED) > 0) {
			logger.warn("{}: 串口连接断开", name);
			if (!destory) {
				serialPort.closePort();
				if (config.getAutoReconnect()) {
					logger.debug("{}: 尝试重新打开串口", name);
					try {
						Thread.sleep(1);
					} catch (Exception e) {}
					connect(-1);
				}
			}
		}
	}

	public void addEventListener(SerialPortEventPli eventListener) {
		this.serialPortEventPlis.add(eventListener);
	}
	
	public void addEventListeners(List<SerialPortEventPli> eventListeners) {
		this.serialPortEventPlis.addAll(eventListeners);
	}
	
	public void removeEventListener(int index) {
		this.serialPortEventPlis.remove(index);
	}
	
	public boolean containsEventListener(SerialPortEventPli eventListener) {
		return serialPortEventPlis.contains(eventListener);
	}
	
	public List<SerialPortEventPli> getEventListeners() {
		return serialPortEventPlis;
	}

	public void setEventListeners(List<SerialPortEventPli> eventListeners) {
		this.serialPortEventPlis = eventListeners;
	}
	
	public void addConnectListener(SerialPortConnectPli connectListener) {
		this.serialPortConnectPlis.add(connectListener);
	}
	
	public void addConnectListeners(List<SerialPortConnectPli> connectListeners) {
		this.serialPortConnectPlis.addAll(connectListeners);
	}
	
	public void removeConnectListener(int index) {
		this.serialPortConnectPlis.remove(index);
	}
	
	public boolean containsConnectListener(SerialPortConnectPli connectListener) {
		return serialPortConnectPlis.contains(connectListener);
	}
	
	public List<SerialPortConnectPli> getSerialPortConnectPlis() {
		return serialPortConnectPlis;
	}

	public void setSerialPortConnectPlis(List<SerialPortConnectPli> serialPortConnectPlis) {
		this.serialPortConnectPlis = serialPortConnectPlis;
	}

	public SerialPortConfig getConfig() {
		return config;
	}

	public void setConfig(SerialPortConfig config) {
		this.config = config;
	}

	/**
	 * 实现健康检查接口
	 */
	@Override
	public Health health() {
		if (!config.getMock()) {
			if (serialPort.isOpen()) {
				return Health.up().build();
			} else {
				return Health.down().build();
			}
		}
		return Health.unknown().build();
	}
	
	/**
	 * 关闭串口
	 */
	@PreDestroy
	@Override
	public void close() {
		logger.info("{}: 串口服务停止开始...", name);
		destory = true;
		if (serialPort != null) {
			if (serialPort.isOpen()) {
				serialPort.closePort();
				serialPort = null;
			}
		}
		logger.info("{}: 串口服务停止完成", name);
	}
	
}
