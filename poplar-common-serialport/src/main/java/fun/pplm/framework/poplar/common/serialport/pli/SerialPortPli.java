package fun.pplm.framework.poplar.common.serialport.pli;

import java.util.Collections;
import java.util.List;

public interface SerialPortPli {
	/**
	 * 获取需要侦听的串口名称集合
	 * 项目中只有一个串口服务时可以忽略该方法
	 * 当项目中存在多个串口服务时，springboot会自动注册所有侦听器到所有串口服务，用于串口服务过滤非当前串口服务的侦听器
	 * @return 需要侦听的串口名集合
	 */
	public default List<String> serialPortNames() {
		return Collections.emptyList();
	}
}
