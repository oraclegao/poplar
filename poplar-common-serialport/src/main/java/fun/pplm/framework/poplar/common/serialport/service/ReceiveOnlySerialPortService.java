package fun.pplm.framework.poplar.common.serialport.service;

import java.util.concurrent.TimeUnit;

import fun.pplm.framework.poplar.common.serialport.psa.SerialPortPsa;

/**
 * 
 * 只接收数据的串口服务
 * @author OracleGao
 *
 */
public class ReceiveOnlySerialPortService extends SerialPortPsa<Void> {

	public ReceiveOnlySerialPortService() {
		super(1);
	}
	
	public ReceiveOnlySerialPortService(String name) {
		super(1, name);
	}

	public final boolean offer(Void o) throws Exception {
		throw new RuntimeException("不支持串口指令发送");
	}
	
	public final boolean offer(Void o, long timeout, TimeUnit timeUnit) throws Exception {
		throw new RuntimeException("不支持串口指令发送");
	}
	
	@Override
	protected final byte[] getSendData(Void t) {
		throw new RuntimeException("不支持串口指令发送");
	}

	@Override
	public void startup() {
		throw new RuntimeException("不支持串口指令发送, 无法启动指令消费线程");
	}

	@Override
	public void shutdown() {
		throw new RuntimeException("不支持串口指令发送, 无法停止指令消费线程");
	}
	
}
