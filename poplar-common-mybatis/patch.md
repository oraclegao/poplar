## 1.11.21
- JsonBaseTypeHandler修复反序列化允许列值为空字符串的情况

## 1.6.12
- 添加DatetimeTypeHandler和DateTypeHandler

## 1.6.10
- 增加mybait plus配置类，为JacksonTypeHandler自动注入框架ObjectMapper对象

## 1.5.9
- 集成用户会话功能，优化基础实体类属性自动注入用户会话中的属性
- 添加PageQo对象支持mybatis plus分页查询

## 1.5.7
- 增加mybatis plus基础实体类
- 增加基础实体类属性自动注入

## 1.0.0
- typehandler类型映射工具类
- 封装分页工具类