package fun.pplm.framework.poplar.common.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * @author OracleGao
 * @param <T> 反射对象泛型
 * 
 */
public class JsonBaseTypeHandler<T> extends BaseTypeHandler<T> {
	
	private JavaType javaType;
	
	private Class<T> clazz;
	
	public JsonBaseTypeHandler(Class<T> clazz) {
		super();
		initSimpleJsonJavaType(clazz);
	}
	
	public JsonBaseTypeHandler(Class<T> clazz, Class<?> parametrized) {
		super();
		initParametrizedJsonJavaType(clazz, parametrized);
	}
	
	private void initSimpleJsonJavaType(Class<T> clazz) {
		if (Collection.class.isAssignableFrom(clazz)) {
			initParametrizedJsonJavaType(Object.class, clazz);
		} else {
			this.javaType = TypeFactory.defaultInstance().constructSimpleType(clazz, new JavaType[] {});
		}
	}
	
	private void initParametrizedJsonJavaType(Class<?> clazz, Class<?> parametrized) {
		this.javaType = TypeFactory.defaultInstance().constructParametricType(parametrized, clazz);
	}
	
	private String serialize (T parameter) {
		if (parameter == null) {
			if (Collection.class.isAssignableFrom(javaType.getRawClass())) {
				return "[]";
			} else {
				return "{}";
			}
		}
		return Json.string(parameter);
	}
	
	private T deserialize(String src) {
		String temp = src;
		if (StringUtils.isBlank(temp)) {
			if (Collection.class.isAssignableFrom(javaType.getRawClass())) {
				temp = "[]";
			} else {
				temp = "{}";
			}
		}
		return Json.bean(temp, javaType);
	}
	
	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, T parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, serialize(parameter));
	}

	@Override
	public T getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return deserialize(rs.getString(columnName));
	}

	@Override
	public T getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return deserialize(rs.getString(columnIndex));
	}

	@Override
	public T getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return deserialize(cs.getString(columnIndex));
	}

	public JavaType getJavaType() {
		return javaType;
	}

	public void setJavaType(JavaType javaType) {
		this.javaType = javaType;
	}

	public Class<T> getClazz() {
		return clazz;
	}

	public void setClazz(Class<T> clazz) {
		this.clazz = clazz;
	}

}
