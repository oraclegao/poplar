package fun.pplm.framework.poplar.common.mybatis.page;

import com.github.pagehelper.PageInfo;

import fun.pplm.framework.poplar.common.page.Pagination;

/**
 * 
 * 封装分页数据返回结果
 * @author OracleGao
 * create 2022年6月21日
 *
 */
public class PaginationHelper {
	
	public static <T> Pagination<T> create(PageInfo<T> pageInfo) {
		return Pagination.create(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), pageInfo.getPages(), pageInfo.getList());
	}
	
}
