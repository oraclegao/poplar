package fun.pplm.framework.poplar.common.mybatis.config;

import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;

import fun.pplm.framework.poplar.json.config.ObjectMapperConfig;

/**
 * 
 * mybatis plus 配置
 * @author OracleGao
 *
 */
@Configuration
public class PoplarMybatisPlusConfig {
	
	static {
		//入驻poplar框架ObjectMapper对象
		JacksonTypeHandler.setObjectMapper(ObjectMapperConfig.OBJECT_MAPPER);
	}
	
}
