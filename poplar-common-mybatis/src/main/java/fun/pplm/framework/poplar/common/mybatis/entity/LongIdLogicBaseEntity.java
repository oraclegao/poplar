package fun.pplm.framework.poplar.common.mybatis.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * 
 * long主键逻辑删除基本实体
 * @author OracleGao
 *
 * @param <T> 实体泛型
 * 
 */
public class LongIdLogicBaseEntity<T extends Model<?>> extends LongIdBaseEntity<T> {

	private static final long serialVersionUID = 1L;

	@TableField(value = "del_flag", fill = FieldFill.INSERT)
	private Boolean delFlag;

	public LongIdLogicBaseEntity() {
		super();
	}

	public Boolean getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Boolean delFlag) {
		this.delFlag = delFlag;
	}
	
}
