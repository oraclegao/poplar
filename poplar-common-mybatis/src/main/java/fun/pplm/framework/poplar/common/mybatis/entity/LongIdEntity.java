package fun.pplm.framework.poplar.common.mybatis.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * 
 * long主键实体
 * @author OracleGao
 *
 * @param <T> 实体泛型
 * 
 */
public class LongIdEntity<T extends Model<?>> extends LongIdBaseEntity<T> {

	private static final long serialVersionUID = 1L;

	@TableField(value = "creator_id", insertStrategy = FieldStrategy.NOT_NULL, updateStrategy = FieldStrategy.NOT_NULL, whereStrategy = FieldStrategy.NOT_NULL, fill = FieldFill.INSERT)
	private Long creatorId;
	@TableField(value = "creator_name", insertStrategy = FieldStrategy.NOT_EMPTY, updateStrategy = FieldStrategy.NOT_EMPTY, whereStrategy = FieldStrategy.NOT_EMPTY, fill = FieldFill.INSERT)
	private String creatorName;
	@TableField(value = "create_at", insertStrategy = FieldStrategy.NOT_NULL, updateStrategy = FieldStrategy.NOT_NULL, whereStrategy = FieldStrategy.NOT_NULL, fill = FieldFill.INSERT)
	private LocalDateTime createAt;
	@TableField(value = "updator_id", insertStrategy = FieldStrategy.NOT_NULL, updateStrategy = FieldStrategy.NOT_NULL, whereStrategy = FieldStrategy.NOT_NULL, fill = FieldFill.INSERT_UPDATE)
	private Long updatorId;
	@TableField(value = "updator_name", insertStrategy = FieldStrategy.NOT_EMPTY, updateStrategy = FieldStrategy.NOT_EMPTY, whereStrategy = FieldStrategy.NOT_EMPTY, fill = FieldFill.INSERT_UPDATE)
	private String updatorName;
	@TableField(value = "update_at", insertStrategy = FieldStrategy.NOT_NULL, updateStrategy = FieldStrategy.NOT_NULL, whereStrategy = FieldStrategy.NOT_NULL, fill = FieldFill.INSERT_UPDATE)
	private LocalDateTime updateAt;

	public LongIdEntity() {
		super();
	}

	public Long getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public LocalDateTime getCreateAt() {
		return createAt;
	}

	public void setCreateAt(LocalDateTime createAt) {
		this.createAt = createAt;
	}

	public Long getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(Long updatorId) {
		this.updatorId = updatorId;
	}

	public String getUpdatorName() {
		return updatorName;
	}

	public void setUpdatorName(String updatorName) {
		this.updatorName = updatorName;
	}

	public LocalDateTime getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(LocalDateTime updateAt) {
		this.updateAt = updateAt;
	}

}
