package fun.pplm.framework.poplar.common.mybatis.typehandler;

import java.util.Set;
/**
 * 
 * @author OracleGao
 * @param <T> 反射Set泛型
 * 
 */
public class JsonSetTypeHandler<T> extends JsonBaseTypeHandler<T> {

	public JsonSetTypeHandler(Class<T> clazz) {
		super(clazz, Set.class);
	}

}
