package fun.pplm.framework.poplar.common.mybatis.typehandler;

import java.util.List;
/**
 * 
 * @author OracleGao
 * @param <T> 反射List泛型
 * 
 */
public class JsonListTypeHandler<T> extends JsonBaseTypeHandler<T> {

	public JsonListTypeHandler(Class<T> clazz) {
		super(clazz, List.class);
	}

}
