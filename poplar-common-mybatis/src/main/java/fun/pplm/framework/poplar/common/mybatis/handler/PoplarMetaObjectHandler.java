package fun.pplm.framework.poplar.common.mybatis.handler;

import java.time.LocalDateTime;

import org.apache.ibatis.reflection.MetaObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;

import fun.pplm.framework.poplar.common.session.pii.SessionPii;
import fun.pplm.framework.poplar.common.session.service.SessionHttpService;
import fun.pplm.framework.poplar.common.utils.ObjectFieldUtils;

/**
 * 
 * 实体属性值自动注入
 * @author OracleGao
 *
 */
@Component
@ConditionalOnMissingBean(MetaObjectHandler.class)
public class PoplarMetaObjectHandler implements MetaObjectHandler {
	private static Logger logger = LoggerFactory.getLogger(PoplarMetaObjectHandler.class);
	
	@Autowired(required = false)
	private SessionHttpService sessionHttpService;
	/**
	 * 用户会话信息对象中获取创建者和更新者id的属性名称
	 * <br>
	 * 默认userId
	 * @see fun.pplm.framework.poplar.common.session.pii.SessionPii
	 */
	@Value("${poplar.mybatis-plus.meta.sessionIdField:userId}")
	private String sessionIdField;
	/**
	 * 用户会话信息对象中获取创建者和更新者name的属性名称
	 * <br>
	 * 默认username
	 * @see fun.pplm.framework.poplar.common.session.pii.SessionPii
	 */
	@Value("${poplar.mybatis-plus.meta.sessionNameField:username}")
	private String sessionNameField;
	
	@Override
	public void insertFill(MetaObject metaObject) {
		setFieldValue(metaObject, "delFlag", false);
		setFieldValue(metaObject, "createAt", LocalDateTime.now());
		SessionPii session = getSession();
		if (session != null) {
			Object value = ObjectFieldUtils.getFieldValue(session, sessionIdField);
			if (value != null) {
				setFieldValue(metaObject, "creatorId", value);
			}
			value = ObjectFieldUtils.getFieldValue(session, sessionNameField);
			if (value != null) {
				setFieldValue(metaObject, "creatorName", value);
			}
		} else {
			logger.debug("no session");
		}
	}
	
	@Override
	public void updateFill(MetaObject metaObject) {
		setFieldValue(metaObject, "updateAt", LocalDateTime.now());
		SessionPii session = getSession();
		if (session != null) {
			Object value = ObjectFieldUtils.getFieldValue(session, sessionIdField);
			if (value != null) {
				setFieldValue(metaObject, "updatorId", value);
			}
			value = ObjectFieldUtils.getFieldValue(session, sessionNameField);
			if (value != null) {
				setFieldValue(metaObject, "updatorName", value);
			}
		} else {
			logger.debug("no session");
		}
	}

	private void setFieldValue(MetaObject metaObject, String fieldName, Object value) {
		if (metaObject.hasGetter(fieldName) && metaObject.hasSetter(fieldName)) {
			if (getFieldValByName(fieldName, metaObject) == null) {
				setFieldValByName(fieldName, value, metaObject);
			}
		}
	}
	
	/**
	 * 获取会话信息
	 * @return
	 */
	private SessionPii getSession() {
		if(sessionHttpService != null) {
			Class<? extends SessionPii> sessionClass = SessionPii.concreteSessionClass;
			if (sessionClass == null) {
				return null;
			}
			return sessionHttpService.getSession(sessionClass);
		}
		return null;
	}
	
}
