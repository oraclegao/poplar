package fun.pplm.framework.poplar.common.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * 
 * long主键基本实体
 * @author OracleGao
 *
 * @param <T> 实体泛型
 * 
 */
public abstract class LongIdBaseEntity<T extends Model<?>> extends Model<T>{

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(value = "id", type = IdType.ASSIGN_ID)
	private Long id;

	public LongIdBaseEntity() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
