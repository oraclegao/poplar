package fun.pplm.framework.poplar.common.mybatis.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

/**
 * 
 * 日期转换
 * 格式: yyyy-MM-dd
 * @author OracleGao
 *
 */
public class DateTypeHandler extends BaseTypeHandler<Date> {
	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, Date parameter, JdbcType jdbcType)
			throws SQLException {
		if (parameter == null) {
			ps.setString(i, "");
		} else {
			ps.setString(i, format.format(parameter));
		}
	}

	@Override
	public Date getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return rs.getDate(columnName);
	}

	@Override
	public Date getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return rs.getDate(columnIndex);
	}

	@Override
	public Date getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return cs.getDate(columnIndex);
	}
}
