package fun.pplm.framework.poplar.common.mybatis.page.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * 分页查询参数
 * @author OracleGao
 */
@ApiModel(value = "分页查询参数")
public class PageQo {
	@ApiModelProperty(value = "页号, 默认1", example = "1")
	private Long page = 1L;
	@ApiModelProperty(value = "每页条数, 默认10", example = "10")
	private Long size = 10L;
	
	public PageQo() {
		super();
	}

	public Long getPage() {
		return page;
	}

	public void setPage(Long page) {
		this.page = page;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public <T> IPage<T> pageInst(Class<T> clazz) {
		Page<T> pageInst = new Page<>();
		if (page == null) {
			pageInst.setCurrent(1);
		} else {
			pageInst.setCurrent(page);
		}
		if (size == null) {
			pageInst.setSize(10);
		} else {
			pageInst.setSize(size);
		}
		return pageInst;
	}
	
}
