## 1.11.22
- 修复文件下载Content-Disposition http响应头中文件名中存在特殊符号bug

## 1.11.21
- 默认文件后缀过滤器增加从MultipartFile.getContentType()获取类型并判断白名单的逻辑
- 增加上传文件计算md5值功能，可在配置文件中开启
- 完善文件下载content-type属性，默认从文件对象获取

## 1.10.20
- 增加文件上传后缀白名单校验功能
- 修复文件apiEnabled配置参数可以正确启用api

## 1.10.19
- 优化po，qo，vo类的swagger文档说明

## 1.8.17
- 优化PdfConvertService注入逻辑，当禁用pdf功能时，不注入。

## 1.8.16
- pdf转换controller中api通过配置项进行启用（整合）和停用（不整合）
- 添加api开关配置项apiEnabled，默认false停用（不整合）
- 修复文件上传支持swagger文档
- 添加删除文件相关api和接口
- 服务接口类后缀更名为Psi: WebFileMetaDataService -> WebFileMetaDataPsi
- 侦听接口类后缀更名为Pli: ZipPackageTaskListener -> ZipPackageTaskPli

## 1.8.15
- 接口添加采集审计数据功能

## 1.6.13
- PdfConvertService增加新的转换方法
- WebFileService增加新的获取文件资源对象方法

## 1.6.10
- 新增poplar-starter-web-file
- 文件上传
- 文件下载
- docx,doc,xls,xlsx文件转pdf下载
- 文件转pdf或pdf文件加水印下载
- 多文件zip包下载
- 文件zip异步打包服务
- 上传的文件保存在服务器本地磁盘
- 需要业务实现WebFileMetaDataService接口，持久化和读取文件元数据