package fun.pplm.framework.poplar.web.file.bean.po;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * 删除文件参数
 * @author OracleGao
 *
 */
@ApiModel(description = "删除文件参数")
public class WebFileDeletePo {
	@NotBlank
	@ApiModelProperty(value = "文件id")
	private String fileId;
	@ApiModelProperty(value = "是否递归删除空文件夹, 默认true, true是, false否")
	private Boolean deleteEmptyDirs = true;

	public WebFileDeletePo() {
		super();
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public Boolean getDeleteEmptyDirs() {
		return deleteEmptyDirs;
	}

	public void setDeleteEmptyDirs(Boolean deleteEmptyDirs) {
		this.deleteEmptyDirs = deleteEmptyDirs;
	}
	
}
