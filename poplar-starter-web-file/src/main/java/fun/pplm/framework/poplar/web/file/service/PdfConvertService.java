package fun.pplm.framework.poplar.web.file.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import fun.pplm.framework.poplar.web.file.bean.model.PdfWatermarkModel;
import fun.pplm.framework.poplar.web.file.config.PdfConvertConfig;

/**
 * 
 * pdf转换服务
 * @author OracleGao
 *
 */
@Service
@ConditionalOnProperty(prefix = "poplar.web.file.pdf.convert", name = "enabled", havingValue = "true")
public class PdfConvertService {
	private Logger logger = LoggerFactory.getLogger(PdfConvertService.class);
	
	@Autowired
	private PdfConvertConfig pdfConvertConfig;

	@Autowired
	private RestTemplate restTemplate;
	
	private String serviceUrl;
	
	@PostConstruct
	protected void init() {
		if (pdfConvertConfig.getEnabled()) {
			logger.debug("pdf转换服务初始化开始");
			serviceUrl = pdfConvertConfig.getLibreofficeProtocol() + "://" + pdfConvertConfig.getLibreofficeHost() + ":" + pdfConvertConfig.getLibreofficePort() + "/lool/convert-to/pdf";
			logger.debug("pdf转换服务初始化完成");
		}
	}

	/**
	 * 转换pdf
	 * @param file 需要转换的文件
	 * @return pdf资源对象
	 */
	public Resource convert(String file) {
		Resource fileResource = new FileSystemResource(new File(file));
		return convert(fileResource);
	}
	
	/**
	 * 转换pdf
	 * @param fileResource 文件资源对象
	 * @return pdf资源对象
	 */
	public Resource convert(Resource fileResource) {
		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add("data", fileResource);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
		ResponseEntity<Resource> responseEntity = restTemplate.postForEntity(serviceUrl, requestEntity, Resource.class);
		return responseEntity.getBody();
	}
	
	/**
	 * 添加水印
	 * @param fileResource 文件资源
	 * @param model 水印参数
	 * @return pdf资源流
	 * @throws IOException 异常
	 * @throws DocumentException 异常
	 */
    public Resource addWatermark(Resource fileResource, PdfWatermarkModel model) throws IOException, DocumentException {
    	int watermarkHeigth = model.getHeight();
        int watermarkWidth = model.getWidth();
        int watermarkSpacing = model.getSpacing();

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        
    	PdfReader pdfReader = null;
    	PdfStamper pdfStamper = null;
    	try {
            pdfReader = new PdfReader(fileResource.getInputStream());
            
            // 创建PDF文件的模板，可以对模板的内容修改，重新生成新PDF文件
            pdfStamper = new PdfStamper(pdfReader, byteArrayOutputStream);
             // 设置水印字体
            BaseFont baseFont = BaseFont.createFont(model.getFontName(), model.getFontEncode(), BaseFont.EMBEDDED);
             // 设置PDF内容的Graphic State 图形状态
            PdfGState pdfGraPhicState = new PdfGState();
            pdfGraPhicState.setFillOpacity(model.getFillOpacity());
            pdfGraPhicState.setStrokeOpacity(model.getStrokeOpacity());
            PdfContentByte pdfContent = null;
            Rectangle pageRectangle = null;
             for (int i = 1; i <= pdfReader.getNumberOfPages(); i++) {
                // 获取当前页面矩形区域
                pageRectangle = pdfReader.getPageSizeWithRotation(i);
                // 获取当前页内容，之后会在页面内容的上方加水印
                if (model.getLayer() == 1) {
                	//内容上层加水印
                	pdfContent = pdfStamper.getOverContent(i);
                } else {
                	//内容下层加水印
                	pdfContent = pdfStamper.getUnderContent(i);
                }
                pdfContent.saveState();
                pdfContent.setGState(pdfGraPhicState);

                pdfContent.beginText();
                pdfContent.setFontAndSize(baseFont, model.getFontSize());
                 // 在高度和宽度维度每隔watermarkSpacing距离添加一个水印
                for (int height = 0; height < pageRectangle.getHeight() + watermarkSpacing; height += watermarkSpacing) {
                    for (int width = 0; width < pageRectangle.getWidth() + watermarkSpacing; width += watermarkSpacing) {
                        // 添加水印文字并旋转
                        pdfContent.showTextAligned(Element.ALIGN_CENTER, model.getWatermark(), width - watermarkWidth, height - watermarkHeigth, model.getRotation());
                    }
                }
                pdfContent.endText();
            }
        } finally {
        	if (pdfStamper != null) {
        		pdfStamper.close();
        	}
        	if (pdfReader != null) {
        		pdfReader.close();
        	}
        }
    	Resource resource = new ByteArrayResource(byteArrayOutputStream.toByteArray());
        return resource;
    }
    
}
