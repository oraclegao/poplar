package fun.pplm.framework.poplar.web.file.psi;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * Poplar Service Interface
 * 文件上传校验单服务接口
 * @author OracleGao
 *
 */
public interface FileUploadValidationPsi {
	
	/**
	 * 校验文件后缀
	 * @param fileType 上传时指定的文件后缀
	 * @param file 上传的文件
	 * @return true通过，在白名单中，false不通过，不在在白名单中
	 */
	public boolean postfixValidate(String fileType, MultipartFile file);
	
}
