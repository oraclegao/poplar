package fun.pplm.framework.poplar.web.file.pli;

import fun.pplm.framework.poplar.web.file.bean.model.ZipPackageModel;
import fun.pplm.framework.poplar.web.file.bean.model.WebFileMetaDataModel;

/**
 * 
 * zip打包任务侦听接口
 * @author OracleGao
 *
 */
public interface ZipPackageTaskPli {

	/**
	 * 打包成功时的回调
	 * @param zipPackageModel zip打包参数
	 * @param webFileMetaDataModel 文件元数据
	 */
	public void success(ZipPackageModel zipPackageModel, WebFileMetaDataModel webFileMetaDataModel);
	
	/**
	 * 打包异常时的回调
	 * @param zipPackageModel zip打包参数
	 * @param e 异常
	 */
	public default void exception(ZipPackageModel zipPackageModel, Throwable e) {}
	
}
