package fun.pplm.framework.poplar.web.file.psi;

import java.util.List;

import fun.pplm.framework.poplar.web.file.bean.model.WebFileMetaDataModel;

/**
 * 
 * Poplar Service Interface
 * web应用文件元数据服务接口
 * @author OracleGao
 *
 */
public interface WebFileMetaDataPsi {
	/**
	 * 保存文件元数据;
	 * @param model 文件元数据模型
	 */
	public void save(WebFileMetaDataModel model);
	
	/**
	 * 根据fileId查询单个文件元数据;
	 * @param fileId 文件id
	 * @return 文件元数据
	 */
	public WebFileMetaDataModel getByFileId(String fileId);
	
	/**
	 * 批量获取文件元数据;
	 * @param fileIds 文件id数组;
	 * @return 文件元数据数组
	 */
	public List<WebFileMetaDataModel> getByFileIds(List<String> fileIds);
	
	/**
	 * 是否开启删除功能;
	 * 默认false;
	 * 确保覆盖该方法返回true并实现delByFileId方法;
	 * @return 是否开启删除功能
	 */
	public default boolean deletable() {
		return false;
	}
	
	/**
	 * 根据fileId删除单个文件元数据;
	 * 文件服务会在成功删除文件后调用该方法删除元数据,重写该该方法需要确保元数据删除成功;
	 * @param fileId 文件id
	 */
	public default void delByFileId(String fileId) {
		throw new RuntimeException("请覆盖并实现delByFileId删除文件元数据方法");
	}
	
}
