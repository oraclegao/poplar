package fun.pplm.framework.poplar.web.file.controller;

import static fun.pplm.framework.poplar.web.file.utils.WebFileConstant.WEB_AUDIT_TYPE_WEB_FILE_TO_PDF_DOWNLOAD;
import static fun.pplm.framework.poplar.web.file.utils.WebFileConstant.WEB_AUDIT_TYPE_WEB_FILE_TO_WATERMARK_PDF_DOWNLOAD;

import java.io.IOException;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itextpdf.text.DocumentException;

import fun.pplm.framework.poplar.web.audit.annotation.Audit;
import fun.pplm.framework.poplar.web.file.bean.po.WatermarkPdfPo;
import fun.pplm.framework.poplar.web.file.service.WebFileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 
 * web应用pdf服务
 * @author OracleGao
 *
 */
@ConditionalOnExpression("${poplar.web.file.pdf.convert.enabled:false} && ${poplar.web.file.apiEnabled:false}")
@Api(value = "webFilePdfController", tags = "web应用文件服务")
@Validated
@RestController
@RequestMapping(path = "/file", produces = MediaType.APPLICATION_JSON_VALUE)
public class WebFilePdfController {
	@Autowired
	private WebFileService webFileService;
	
	@Audit(value = "文件转pdf下载", types = WEB_AUDIT_TYPE_WEB_FILE_TO_PDF_DOWNLOAD)
	@ApiOperation(value = "文件转pdf下载", notes = "文件转pdf下载, 支持doc和docx")
	@GetMapping("/download/toPdf")
	public ResponseEntity<Resource> download2Pdf(@ApiParam(value = "fileId", required = true) @NotBlank @RequestParam("fileId") String fileId) throws IOException {
		return webFileService.download2Pdf(fileId);
	}
	
	@Audit(value = "文件转pdf带水印下载", types = WEB_AUDIT_TYPE_WEB_FILE_TO_WATERMARK_PDF_DOWNLOAD)
	@ApiOperation(value = "文件转pdf带水印下载", notes = "文件转pdf带水印下载, 支持pdf,doc,docx,xls,xlsx")
	@PostMapping("/download/toWatermarkPdf")
	public ResponseEntity<Resource> download2WatermarkPdf(@Valid @RequestBody WatermarkPdfPo po) throws IOException, DocumentException {
		return webFileService.download2WatermarkPdf(po);
	}
	
}
