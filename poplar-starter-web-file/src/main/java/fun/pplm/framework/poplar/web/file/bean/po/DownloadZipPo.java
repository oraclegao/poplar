package fun.pplm.framework.poplar.web.file.bean.po;

import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import fun.pplm.framework.poplar.common.validator.annotation.Item;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * 多文件zip打包下载参数
 * @author OracleGao
 *
 */
@ApiModel(description = "多文件zip打包下载参数")
public class DownloadZipPo {
	@NotEmpty
	@ApiModelProperty(value = "文件id数组")
	private List<String> fileIds;

	@ApiModelProperty(value = "压缩包名称，默认package")
	private String zipFileName = "package";
	
	@Item({"deflated", "stored"})
	@ApiModelProperty(value = "打包方法deflated:压缩，stored:存储，默认deflated")
	private String zipMethod = "deflated";
	
	@Max(value = 9)
	@Min(value = 0)
	@ApiModelProperty(value = "压缩等级 0-9，默认3，当method是Deflate时")
	private Integer zipLevel = 3;

	public DownloadZipPo() {
		super();
	}

	public List<String> getFileIds() {
		return fileIds;
	}

	public void setFileIds(List<String> fileIds) {
		this.fileIds = fileIds;
	}

	public String getZipFileName() {
		return zipFileName;
	}

	public void setZipFileName(String zipFileName) {
		this.zipFileName = zipFileName;
	}

	public String getZipMethod() {
		return zipMethod;
	}

	public void setZipMethod(String zipMethod) {
		this.zipMethod = zipMethod;
	}

	public Integer getZipLevel() {
		return zipLevel;
	}

	public void setZipLevel(Integer zipLevel) {
		this.zipLevel = zipLevel;
	}
	
}
