package fun.pplm.framework.poplar.web.file.task;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import fun.pplm.framework.poplar.common.runnable.ConsumptionQueue;
import fun.pplm.framework.poplar.web.file.bean.model.ZipPackageModel;
import fun.pplm.framework.poplar.web.file.bean.model.WebFileMetaDataModel;
import fun.pplm.framework.poplar.web.file.config.ZipPackageTaskConfig;
import fun.pplm.framework.poplar.web.file.pli.ZipPackageTaskPli;
import fun.pplm.framework.poplar.web.file.service.WebFileService;

/**
 * 
 * zip打包任务
 * @author OracleGao
 *
 */
@Service
@ConditionalOnProperty(prefix = "poplar.web.file.zip.package.task", name = "enabled", havingValue = "true")
public class ZipPackageTask extends ConsumptionQueue<ZipPackageModel> {
	private static Logger logger =LoggerFactory.getLogger(ZipPackageTask.class);
	
	@Autowired
	private WebFileService webFileService;
	
	@Autowired
	private ZipPackageTaskConfig config;

	@Autowired(required = false)
	private List<ZipPackageTaskPli> plis;
	
	public ZipPackageTask() {
		super(128);
	}

	@PostConstruct
	protected void init() {
		logger.debug("初始化zip异步打包服务开始...");
		super.setInterval(config.getInterval());
		super.startup();
		logger.debug("初始化zip异步打包服务完成");
	}
	
	@Override
	protected void consume(ZipPackageModel model) {
		try {
			WebFileMetaDataModel webFileMetaDataModel = webFileService.zipPackage(model);
			if (plis != null && plis.size() > 0) {
				plis.forEach(listener -> listener.success(model, webFileMetaDataModel));
			}
		} catch (IOException e) {
			logger.error(e.getMessage(),e );
			if (plis != null && plis.size() > 0) {
				plis.forEach(pli -> pli.exception(model, e));
			}
		}
	}

	public void addListener(ZipPackageTaskPli listener) {
		plis.add(listener);
	}
	
	public void addListeners(List<ZipPackageTaskPli> listeners) {
		plis.addAll(listeners);
	}
	
	public List<ZipPackageTaskPli> getListeners() {
		return plis;
	}

	public void setListeners(List<ZipPackageTaskPli> listeners) {
		this.plis = listeners;
	}

}
