package fun.pplm.framework.poplar.web.file.bean.model;

import java.time.LocalDateTime;

/**
 * 
 * web应用文件服务元数据模型
 * @author OracleGao
 *
 */
public class WebFileMetaDataModel {
	/**
	 * 文件id,
	 * 32位uuid
	 */
	private String fileId;
	/**
	 * 文件名
	 */
	private String fileName;
	/**
	 * 文件类型
	 * 文件后缀
	 */
	private String fileType;
	/**
	 * 文件大小
	 * 单位字节
	 */
	private Long fileLength;
	/**
	 * 文件标签
	 * 用于业务分组
	 * 如果filePath为空，作为文件存储路径使用
	 * 如果filePath和fileTag都为空，默认存储在根目录下
	 */
	private String fileTag;
	/**
	 * 文件存储路径
	 * 如果为空，则使用fileTag作为存储路径
	 */
	private String filePath;
	/**
	 * 文件md5 hex值
	 * 配置文件中开启时计算该值 poplar.web.file.fileUploadMd5Enabled: true
	 * 默认null
	 */
	private String fileMd5;
	/**
	 * 磁盘上相对路径文件uri
	 */
	private String relativeUri;
	/**
	 * 上传时间
	 * 格式yyyy-MM-dd HH:mi:ss
	 */
	private LocalDateTime uploadAt;
	
	public WebFileMetaDataModel() {
		super();
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Long getFileLength() {
		return fileLength;
	}

	public void setFileLength(Long fileLength) {
		this.fileLength = fileLength;
	}

	public String getFileTag() {
		return fileTag;
	}

	public void setFileTag(String fileTag) {
		this.fileTag = fileTag;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getRelativeUri() {
		return relativeUri;
	}

	public void setRelativeUri(String relativeUri) {
		this.relativeUri = relativeUri;
	}

	public LocalDateTime getUploadAt() {
		return uploadAt;
	}

	public void setUploadAt(LocalDateTime uploadAt) {
		this.uploadAt = uploadAt;
	}

	public String getFileMd5() {
		return fileMd5;
	}

	public void setFileMd5(String fileMd5) {
		this.fileMd5 = fileMd5;
	}

}
