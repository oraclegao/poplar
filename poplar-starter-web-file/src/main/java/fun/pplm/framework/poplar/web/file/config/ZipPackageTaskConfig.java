package fun.pplm.framework.poplar.web.file.config;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * zip异步打包服务配置
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.web.file.zip.package.task")
public class ZipPackageTaskConfig {
	/**
	 * 是否启用
	 * 默认false不启用
	 */
	private Boolean enabled = false;
	
	/**
	 * 执行间隔
	 * 单位ms
	 * 默认1000
	 */
	private Long interval = 1000L;

	public ZipPackageTaskConfig() {
		super();
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Long getInterval() {
		return interval;
	}

	public void setInterval(Long interval) {
		this.interval = interval;
	}
	
	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("enabled", enabled);
		map.put("interval", interval);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
	
}
