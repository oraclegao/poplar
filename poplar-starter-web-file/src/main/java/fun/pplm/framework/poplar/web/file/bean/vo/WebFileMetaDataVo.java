package fun.pplm.framework.poplar.web.file.bean.vo;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * web应用文件元数据vo
 * @author OracleGao
 *
 */
@ApiModel(description = "web应用文件元数据vo")
public class WebFileMetaDataVo {
	@ApiModelProperty(value = "文件id, 32位uuid")
	private String fileId;
	@ApiModelProperty(value = "文件名")
	private String fileName;
	@ApiModelProperty(value = "文件类型, 文件后缀")
	private String fileType;
	@ApiModelProperty(value = "文件大小,单位字节")
	private Long fileLength;
	@ApiModelProperty(value = "文件标签，用于业务分组")
	private String fileTag;
	@ApiModelProperty(value = "文件存储路径")
	private String filePath;
	@ApiModelProperty(value = "磁盘上相对路径文件uri")
	private String relativeUri;
	@ApiModelProperty(value = "上传时间，格式yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime uploadAt;
	
	public WebFileMetaDataVo() {
		super();
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Long getFileLength() {
		return fileLength;
	}

	public void setFileLength(Long fileLength) {
		this.fileLength = fileLength;
	}

	public String getFileTag() {
		return fileTag;
	}

	public void setFileTag(String fileTag) {
		this.fileTag = fileTag;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getRelativeUri() {
		return relativeUri;
	}

	public void setRelativeUri(String relativeUri) {
		this.relativeUri = relativeUri;
	}

	public LocalDateTime getUploadAt() {
		return uploadAt;
	}

	public void setUploadAt(LocalDateTime uploadAt) {
		this.uploadAt = uploadAt;
	}

}
