package fun.pplm.framework.poplar.web.file.bean.model;

/**
 * 
 * pdf水印model
 * @author OracleGao
 *
 */
public class PdfWatermarkModel {
	/**
	 * 水印文字
	 * 默认 保密
	 */
	private String watermark = "保密";
	/**
	 * 水印高
	 * 默认 60
	 */
	private Integer height = 60;
	/**
	 * 水印宽
	 * 默认 120
	 */
	private Integer width = 120;
	/**
	 * 间距
	 * 默认300
	 */
	private Integer spacing = 300;
	/**
	 * 填充透明度
	 * 默认0.1
	 */
	private Float fillOpacity = 0.1F;
	/**
	 * 边框透明度
	 * 默认0.1
	 */
	private Float strokeOpacity = 0.1F;
	/**
	 * 字体名称
	 * 默认STSong-Light
	 */
	private String fontName = "STSong-Light";
	/**
	 * 字体编码
	 * UniGB-UCS2-H
	 */
	private String fontEncode = "UniGB-UCS2-H";
	/**
	 * 字体大小
	 * 默认30
	 */
	private Integer fontSize = 40;
	/**
	 * 旋转角度
	 * 默认30
	 */
	private Float rotation = 30F;
	/**
	 * 水印图层
	 * 1内容上方,2内容下方
	 * 默认1内容上方
	 */
	private Integer layer = 1;

	public PdfWatermarkModel() {
		super();
	}

	public String getWatermark() {
		return watermark;
	}

	public void setWatermark(String watermark) {
		this.watermark = watermark;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getSpacing() {
		return spacing;
	}

	public void setSpacing(Integer spacing) {
		this.spacing = spacing;
	}

	public Float getFillOpacity() {
		return fillOpacity;
	}

	public void setFillOpacity(Float fillOpacity) {
		this.fillOpacity = fillOpacity;
	}

	public Float getStrokeOpacity() {
		return strokeOpacity;
	}

	public void setStrokeOpacity(Float strokeOpacity) {
		this.strokeOpacity = strokeOpacity;
	}

	public String getFontName() {
		return fontName;
	}

	public void setFontName(String fontName) {
		this.fontName = fontName;
	}

	public String getFontEncode() {
		return fontEncode;
	}

	public void setFontEncode(String fontEncode) {
		this.fontEncode = fontEncode;
	}

	public Integer getFontSize() {
		return fontSize;
	}

	public void setFontSize(Integer fontSize) {
		this.fontSize = fontSize;
	}

	public Float getRotation() {
		return rotation;
	}

	public void setRotation(Float rotation) {
		this.rotation = rotation;
	}

	public Integer getLayer() {
		return layer;
	}

	public void setLayer(Integer layer) {
		this.layer = layer;
	}
	
}
