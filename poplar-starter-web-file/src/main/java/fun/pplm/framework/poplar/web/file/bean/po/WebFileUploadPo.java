package fun.pplm.framework.poplar.web.file.bean.po;

import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * 文件上传参数
 * @author OracleGao
 *
 */
@ApiModel(description = "文件上传参数")
public class WebFileUploadPo {
	@ApiModelProperty(value = "指定文件名，默认取上传文件名")
	private String fileName;
	@ApiModelProperty(value = "指定文件类型，默认取上传文件名后缀")
	private String fileType;
	@ApiModelProperty(value = "文件标签用于业务分组，如果filePath为空，作为文件存储路径使用，如果filePath和fileTag都为空，默认存储在根目录下")
	private String fileTag;
	@ApiModelProperty(value = "指定文件存储相对路径")
	private String filePath;
	@NotNull
	@ApiModelProperty(value = "上传的文件")
	private MultipartFile file;
	
	public WebFileUploadPo() {
		super();
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFileTag() {
		return fileTag;
	}

	public void setFileTag(String fileTag) {
		this.fileTag = fileTag;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}
	
}
