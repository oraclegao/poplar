package fun.pplm.framework.poplar.web.file.service;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.web.file.config.WebFileConfig;
import fun.pplm.framework.poplar.web.file.psi.FileUploadValidationPsi;

/**
 * 
 * 默认的文件上传校验服务
 * @author OracleGao
 *
 */
@Service
@ConditionalOnMissingBean(FileUploadValidationPsi.class)
public class DefaultFileUploadValidationService implements FileUploadValidationPsi {
	private static Logger logger = LoggerFactory.getLogger(DefaultFileUploadValidationService.class);
	
	@Autowired
	private WebFileConfig webFileConfig;
	
	private boolean postfixPass = false;
	
	private Map<String, Void> postfixWhiteListMap;
	
	@PostConstruct
	protected void init() {
		logger.debug("初始化文件上传校验服务开始...");
		String whiteListStr = webFileConfig.getFileUploadPostfixWhiteList().trim();
		if ("*".equals(whiteListStr)) {
			postfixPass = true;
		} else {
			postfixWhiteListMap = new LinkedHashMap<>();
			String[] whites = whiteListStr.split(",");
			for (String white : whites) {
				if (white != null) {
					postfixWhiteListMap.put(white.trim().toLowerCase(), null);
				}
			}
			logger.debug("文件后缀白名单列表: {}", Json.string(postfixWhiteListMap.keySet()));
		}
		logger.debug("初始化文件上传校验服务开始完成");
	}
	
	@Override
	public boolean postfixValidate(String fileType, MultipartFile file) {
		if (postfixPass) {
			return true;
		}
		if (fileType != null) {
			if (postfixWhiteListMap.containsKey(fileType.trim().toLowerCase())) {
				return true;
			}
		}
		String originFileName = file.getOriginalFilename();
		if (StringUtils.isNotBlank(originFileName)) {
			String originFileType = FilenameUtils.getExtension(originFileName);
			if (originFileType != null) {
				if (postfixWhiteListMap.containsKey(originFileType.trim().toLowerCase())) {
					return true;
				} else {
					String contentType = file.getContentType();
					if (StringUtils.isNotBlank(contentType)) {
						String[] temp = contentType.split("/");
						if (temp.length > 1) {
							if (StringUtils.isNotBlank(temp[1])) {
								if (postfixWhiteListMap.containsKey(temp[1].trim().toLowerCase())) {
									return true;
								}
							}
						}
					}
				}
			}
		}
		return false;
	}

}
