package fun.pplm.framework.poplar.web.file.bean.po;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * 下载转带水印pdf参数
 * @author OracleGao
 *
 */
@ApiModel(description = "下载转带水印pdf参数")
public class WatermarkPdfPo {
	@NotBlank
	@ApiModelProperty(value = "fileId")
	private String fileId;
	
	@NotBlank
	@ApiModelProperty(value = "水印文字")
	private String watermark;

	public WatermarkPdfPo() {
		super();
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getWatermark() {
		return watermark;
	}

	public void setWatermark(String watermark) {
		this.watermark = watermark;
	}
	
}
