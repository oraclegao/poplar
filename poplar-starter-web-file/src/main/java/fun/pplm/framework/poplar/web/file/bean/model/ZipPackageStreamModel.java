package fun.pplm.framework.poplar.web.file.bean.model;

import java.util.List;

/**
 * 
 * zip打包成流model
 * @author OracleGao
 *
 */
public class ZipPackageStreamModel {
	/**
	 * 打包的文件id集合
	 */
	private List<String> fileIds;
	
	/**
	 * 打包方法
	 * deflated:压缩，stored:存储
	 * 默认deflated
	 */
	private String zipMethod = "deflated";
	
	/**
	 * 压缩等级 0-9
	 * 默认3
	 * 当method是Deflate时
	 */
	private Integer zipLevel = 3;

	public ZipPackageStreamModel() {
		super();
	}

	public List<String> getFileIds() {
		return fileIds;
	}

	public void setFileIds(List<String> fileIds) {
		this.fileIds = fileIds;
	}

	public String getZipMethod() {
		return zipMethod;
	}

	public void setZipMethod(String zipMethod) {
		this.zipMethod = zipMethod;
	}

	public Integer getZipLevel() {
		return zipLevel;
	}

	public void setZipLevel(Integer zipLevel) {
		this.zipLevel = zipLevel;
	}
	
}
