package fun.pplm.framework.poplar.web.file.config;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * web应用文件pdf转换服务配置
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.web.file.pdf.convert")
public class PdfConvertConfig {
	/**
	 * 是否开启
	 * 默认false
	 */
	private Boolean enabled = false;
	/**
	 * libreoffice host
	 */
	private String libreofficeHost;
	/**
	 * libreoffice 端口
	 */
	private Integer libreofficePort;
	
	/**
	 * libreoffice协议 https / http
	 * 默认http
	 */
	private String libreofficeProtocol = "http";

	public PdfConvertConfig() {
		super();
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getLibreofficeHost() {
		return libreofficeHost;
	}

	public void setLibreofficeHost(String libreofficeHost) {
		this.libreofficeHost = libreofficeHost;
	}

	public Integer getLibreofficePort() {
		return libreofficePort;
	}

	public void setLibreofficePort(Integer libreofficePort) {
		this.libreofficePort = libreofficePort;
	}
	
	public String getLibreofficeProtocol() {
		return libreofficeProtocol;
	}

	public void setLibreofficeProtocol(String libreofficeProtocol) {
		this.libreofficeProtocol = libreofficeProtocol;
	}

	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("enabled", enabled);
		map.put("libreofficeHost", libreofficeHost);
		map.put("libreofficePort", libreofficePort);
		map.put("libreofficeProtocol", libreofficeProtocol);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
	
}
