package fun.pplm.framework.poplar.web.file.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.validation.ValidationException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileSystem;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.itextpdf.text.DocumentException;

import fun.pplm.framework.poplar.common.utils.Uuid;
import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.web.file.bean.model.ZipPackageModel;
import fun.pplm.framework.poplar.web.file.bean.model.ZipPackageStreamModel;
import fun.pplm.framework.poplar.web.file.bean.model.PdfWatermarkModel;
import fun.pplm.framework.poplar.web.file.bean.model.WebFileMetaDataModel;
import fun.pplm.framework.poplar.web.file.bean.po.DownloadZipPo;
import fun.pplm.framework.poplar.web.file.bean.po.WatermarkPdfPo;
import fun.pplm.framework.poplar.web.file.bean.po.WebFileDeletePo;
import fun.pplm.framework.poplar.web.file.bean.po.WebFileUploadPo;
import fun.pplm.framework.poplar.web.file.bean.vo.WebFileMetaDataVo;
import fun.pplm.framework.poplar.web.file.config.PdfConvertConfig;
import fun.pplm.framework.poplar.web.file.config.WebFileConfig;
import fun.pplm.framework.poplar.web.file.psi.FileUploadValidationPsi;
import fun.pplm.framework.poplar.web.file.psi.WebFileMetaDataPsi;

/**
 * 
 * web应用文件服务
 * @author OracleGao
 *
 */
@Service
public class WebFileService {
	private static Logger logger = LoggerFactory.getLogger(WebFileService.class);

	private static FileSystem fileSystem = FileSystem.getCurrent();
	
	@Autowired
	private WebFileConfig webFileConfig;
	@Autowired
	private PdfConvertConfig pdfConvertConfig;
	@Autowired(required = false)
	private WebFileMetaDataPsi webFileMetaDataPsi;
	@Autowired(required = false)
	private PdfConvertService pdfConvertService;
	@Autowired
	private FileUploadValidationPsi fileUploadValidationService;
	
	/**
	 * 文件上传
	 * @param po 上传参数
	 * @return 上传结果vo
	 * @throws IOException IO异常
	 */
	public WebFileMetaDataVo upload(WebFileUploadPo po) throws IOException {
		logger.debug("接收到上传文件: {}", Json.string(po));
		MultipartFile multipartfile = po.getFile();
		String originFileName = multipartfile.getOriginalFilename();
		if(StringUtils.isBlank(originFileName)) {
			throw new IOException("file参数中找不到上传文件");
		}
		String originFileType = FilenameUtils.getExtension(originFileName);

		String temp = null;
		
		//生成fileId 32位uuid
		String fileId = Uuid.uuid32();
		
		//获取filePath参数
		String filePath = null;
		temp = po.getFilePath();
		if (StringUtils.isNotBlank(temp)) {
			temp = normalizePath(temp);
			if (!isLegalFilePath(temp)) {
				throw new RuntimeException("filePath含有非法字符或含有系统保留文件名");
			}
			filePath = temp;
		}
		
		//获取fileTag参数
		String fileTag = null;
		temp = po.getFileTag();
		if (StringUtils.isNotBlank(temp)) {
			fileTag = temp;
			//filePath为空时尝试使用fileTag填充
			if (StringUtils.isBlank(filePath)) {
				logger.debug("filePath为空，使用fileTag作为filePath");
				temp = normalizePath(temp);
				if (!isLegalFilePath(temp)) {
					temp = toLegalFilePath(temp, '-');
					logger.warn("fileTag作为filePath存在非法字符，非法字符统一替换为'-', filePath: {}", temp);
				}
				filePath = temp;
			}
		}
		
		//处理默认filePath
		if (StringUtils.isBlank(filePath)) {
			logger.debug("filePath和fileTag均为空，存入根目录下");
			filePath = "/";
		}
		
		//获取fileName参数
		String fileName = null;
		temp = po.getFileName();
		if (StringUtils.isBlank(temp)) {
			fileName = FilenameUtils.getBaseName(originFileName);
			logger.debug("fileName参数为空使用上传文件名: {}", fileName);
		} else {
			if(!fileSystem.isLegalFileName(temp)) {
				throw new RuntimeException("fileName含有非法字符或系统保留文件名");
			} 
			fileName = temp;
		}
		
		//获取fileType参数
		String fileType = null;
		temp = po.getFileType();
		if(!fileUploadValidationService.postfixValidate(temp, multipartfile)) {
			throw new ValidationException("非法的文件类型");
		}
		if (StringUtils.isBlank(temp)) {
			fileType = originFileType;
			logger.debug("fileType参数为空使用上传文件名后缀: {}", fileType);
		} else {
			if(!fileSystem.isLegalFileName(temp)) {
				throw new RuntimeException("文件类型(后缀)名含有非法字符");
			} 
			fileType = temp;	
		}
		
		//获取relativeUri参数
		String relativeUri = null;
		if (StringUtils.isBlank(fileType)) {
			relativeUri = filePath + fileId;
		} else {
			relativeUri = filePath + fileId + "." + fileType;
		}
		
		//文件上传预处理
		File file = new File(webFileConfig.getRootPath(), relativeUri);
		File parentFile = file.getParentFile();
		if (!parentFile.exists()) {
			parentFile.mkdirs();
		}

		//上传文件
		multipartfile.transferTo(file);

		//组装model
		WebFileMetaDataModel model = new WebFileMetaDataModel();
		model.setFileId(fileId);
		model.setFileName(fileName);
		model.setFileType(fileType);
		model.setFileLength(file.length());
		model.setFileTag(fileTag);
		model.setFilePath(filePath);
		model.setRelativeUri(relativeUri);
		model.setUploadAt(LocalDateTime.now());
		
		if (webFileConfig.getFileUploadMd5Enabled()) {
			logger.debug("计算文件md5值开始...");
			try (FileInputStream fileInputStream = new FileInputStream(file)) {
	            String fileMd5 = DigestUtils.md5Hex(fileInputStream);
	            model.setFileMd5(fileMd5);
	        }
			logger.debug("计算文件md5值成功");
		}
		
		//持久化文件元数据
		webFileMetaDataPsi.save(model);
		
		logger.debug("保存文件成功, file: {}, model: {}", file.getAbsolutePath(), Json.string(model));
		WebFileMetaDataVo vo = new WebFileMetaDataVo();
		BeanUtils.copyProperties(model, vo);
		return vo;
	}
	
	/**
	 * 判断文件路径是否合法
	 * @param filePath 文件路径
	 * @return 是true否false合法
	 */
	private boolean isLegalFilePath(String filePath) {
		for(String path : filePath.split("/")) {
			if (StringUtils.isNotBlank(path)) {
				if (!fileSystem.isLegalFileName(path)) {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * 转换成合法文件路径
	 * @param filePath 文件路径
	 * @param replacement 替换非法字符的字符
	 * @return 合法的文件路径
	 */
	private String toLegalFilePath(String filePath, char replacement) {
		StringBuilder stringBuilder = new StringBuilder();
		for(String path : filePath.split("/")) {
			stringBuilder.append(fileSystem.toLegalFileName(path, replacement)).append("/");
		}
		return normalizePath(stringBuilder.toString());
	}
	
	/**
	 * 标准化路径
	 * @param filePath 路径
	 * @return 标准化的路径
	 */
	private String normalizePath(String filePath) {
		String path = FilenameUtils.normalize(filePath, true);
		if (!path.startsWith("/")) {
			path = "/" + path;
		}
		if (!path.endsWith("/")) {
			path = path + "/";
		}
		return path;
	}
	
	/**
	 * 获取元数据文件信息
	 * @param fileId 文件id
 	 * @return 文件元数据信息
	 */
	public WebFileMetaDataModel getMetaData(String fileId) {
		return webFileMetaDataPsi.getByFileId(fileId);
	}
	
	/**
	 * 获取文件资源对象
	 * @param fileId 文件id
	 * @return 文件资源对象
	 */
	public Resource getFileResource(String fileId) {
		WebFileMetaDataModel model = webFileMetaDataPsi.getByFileId(fileId);
		if (model == null) {
			throw new RuntimeException("无效的fileId: " + fileId);
		}
		String file = webFileConfig.getRootPath() + model.getRelativeUri();
		Resource fileResource = new FileSystemResource(file);
		if (!fileResource.exists()) {
			throw new RuntimeException("文件不存在, fileId: " + fileId);
		}
		return fileResource;
	}
	
	/**
	 * 文件下载
	 * @param fileId 文件id
	 * @return 文件流响应实体
	 * @throws IOException io异常
	 */
	public ResponseEntity<Resource> download(String fileId) throws IOException {
		WebFileMetaDataModel model = webFileMetaDataPsi.getByFileId(fileId);
		if (model == null) {
			throw new RuntimeException("无效的fileId: " + fileId);
		}
		String file = webFileConfig.getRootPath() + model.getRelativeUri();
		FileSystemResource fileResource = new FileSystemResource(file);
		if (!fileResource.exists()) {
			throw new RuntimeException("文件不存在, fileId: " + fileId);
		}
		logger.debug("下载文件: {}, 文件大小: {}", file, fileResource.contentLength());
		return getResponseEntity(model, fileResource);
	}
	
	/**
	 * 组装文件下载流响应信息
	 * @param model 文件元数据
	 * @param resource 文件资源
	 * @return 文件下载流响应信息
	 * @throws IOException 异常
	 */
	private ResponseEntity<Resource> getResponseEntity(WebFileMetaDataModel model, Resource resource) throws IOException {
		String fileName = model.getFileName() + "." + model.getFileType();
		String encodedFileName = URLEncoder.encode(fileName, "utf-8").replaceAll("\\+", "%20");
		HttpHeaders headers = new HttpHeaders();
		String fileContentType = Files.probeContentType(resource.getFile().toPath());
		if (StringUtils.isNotBlank(fileContentType)) {
			headers.add(HttpHeaders.CONTENT_TYPE, fileContentType);
		} else {
			headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_VALUE);
		}
		headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + fileName + "\";filename*=utf-8''" + encodedFileName);
		return ResponseEntity.ok()
				.headers(headers)
				.contentLength(resource.contentLength())
				.body(resource);
	}
	
	public ResponseEntity<Resource> download2Pdf(String fileId) throws IOException {
		if (!pdfConvertConfig.getEnabled()) {
			throw new RuntimeException("pdf功能被禁用，需要在配置文件打开。");
		}
		WebFileMetaDataModel model = webFileMetaDataPsi.getByFileId(fileId);
		if (model == null) {
			throw new RuntimeException("无效的fileId");
		}
		String file = webFileConfig.getRootPath() + model.getRelativeUri();
		Resource fileResource = new FileSystemResource(file);
		if (!fileResource.exists()) {
			throw new RuntimeException("fileId对应的文件不存在");
		}
		String fileType = model.getFileType();
		if ("pdf".equalsIgnoreCase(fileType)) {
			return download(fileId);
		} else if ("docx".equalsIgnoreCase(fileType) || "doc".equalsIgnoreCase(fileType) || "xls".equalsIgnoreCase(fileType) || "xlsx".equalsIgnoreCase(fileType)) {
			Resource pdfResource = pdfConvertService.convert(file);
			model.setFileType("pdf");
			return getResponseEntity(model, pdfResource);
		} else {
			throw new RuntimeException("文件类型不支持转换pdf: " + fileType);
		}
	}
	
	public ResponseEntity<Resource> download2WatermarkPdf(WatermarkPdfPo po) throws IOException, DocumentException {
		if (!pdfConvertConfig.getEnabled()) {
			throw new RuntimeException("pdf功能被禁用，需要在配置文件打开。");
		}
		WebFileMetaDataModel model = webFileMetaDataPsi.getByFileId(po.getFileId());
		if (model == null) {
			throw new RuntimeException("无效的fileId");
		}
		String file = webFileConfig.getRootPath() + model.getRelativeUri();
		Resource fileResource = new FileSystemResource(file);
		if (!fileResource.exists()) {
			throw new RuntimeException("fileId对应的文件不存在");
		}
		String fileType = model.getFileType();
		PdfWatermarkModel pdfWatermarkModel = new PdfWatermarkModel();
		pdfWatermarkModel.setWatermark(po.getWatermark());
		if ("pdf".equalsIgnoreCase(fileType)) {
			Resource pdfResource = pdfConvertService.addWatermark(fileResource, pdfWatermarkModel);
			return getResponseEntity(model, pdfResource); 
		} else if ("docx".equalsIgnoreCase(fileType) || "doc".equalsIgnoreCase(fileType) || "xls".equalsIgnoreCase(fileType) || "xlsx".equalsIgnoreCase(fileType)) {
			Resource pdfResource = pdfConvertService.convert(file);
			pdfResource = pdfConvertService.addWatermark(pdfResource, pdfWatermarkModel);
			model.setFileType("pdf");
			return getResponseEntity(model, pdfResource);
		} else {
			throw new RuntimeException("文件类型不支持转换pdf: " + fileType);
		}
	}
	
	/**
	 * 多文件zip包下载
	 * @param po 打包参数
	 * @return zip文件下载流响应信息
	 * @throws IOException  异常
	 */
	public ResponseEntity<Resource> downloadZip(DownloadZipPo po) throws IOException {
		ZipPackageStreamModel model = new ZipPackageStreamModel();
		model.setFileIds(po.getFileIds());
		model.setZipLevel(po.getZipLevel());
		model.setZipMethod(po.getZipMethod());
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		zipPackageStream(model, byteArrayOutputStream);
        Resource resource = new ByteArrayResource(byteArrayOutputStream.toByteArray());
		String zipFileName = po.getZipFileName() + ".zip";
		String encodedZipFileName = URLEncoder.encode(zipFileName, "utf-8");
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Disposition", "attachment;filename=" + encodedZipFileName + ";filename*=utf-8''" + encodedZipFileName);
		return ResponseEntity.ok()
				.headers(headers)
				.contentLength(resource.contentLength())
				.contentType(MediaType.APPLICATION_OCTET_STREAM)
				.body(resource);
	}
	
	/**
	 * zip打包流
	 * @param model 打包参数
	 * @param outputStream 输出的zip打包流
	 * @throws IOException 异常
	 */
	private void zipPackageStream(ZipPackageStreamModel model, OutputStream outputStream) throws IOException {
		List<String> fileIds = model.getFileIds();
		List<WebFileMetaDataModel> models = webFileMetaDataPsi.getByFileIds(fileIds);
		if (models.size() == 0) {
			throw new RuntimeException("无效的fileId集合: " + fileIds.toString());
		}
		if(fileIds.size() > models.size()) {
			List<String> modelFileIds = models.stream().map(WebFileMetaDataModel::getFileId).collect(Collectors.toList());
			List<String> invalidFileIds = new ArrayList<>();
			for (String fileId : fileIds) {
				if(!modelFileIds.contains(fileId)) {
					invalidFileIds.add(fileId);
				}
			}
			throw new RuntimeException("存在无效的fileIds: " + invalidFileIds.toString());
		}
		
        ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);
        if ("deflated".equals(model.getZipMethod())) {
        	zipOutputStream.setMethod(ZipEntry.DEFLATED);
        	zipOutputStream.setLevel(model.getZipLevel());
        } else {
        	zipOutputStream.setMethod(ZipEntry.STORED);
        }
        
        Map<String, Integer> fileNameMap = new HashMap<>();
        String fileName = null;
        String fileType = null;
        Integer version = null;
        ZipEntry zipEntry = null;
        FileInputStream fileInputStream = null;
        int readBytes = 0;
        byte[] buffer = new byte[4096];
        for (WebFileMetaDataModel webFileMetaDataModel : models) {
        	fileName = webFileMetaDataModel.getFileName();
        	fileType = webFileMetaDataModel.getFileType();
        	if(StringUtils.isNotBlank(fileType)) {
        		fileName = fileName + "." + fileType;
        	}
        	if (fileNameMap.containsKey(fileName)) {
        		version = fileNameMap.get(fileName) + 1;
        		fileNameMap.put(fileName, version);
        		fileName = webFileMetaDataModel.getFileName() + "(" + version + ")";
        		if(StringUtils.isNotBlank(fileType)) {
            		fileName = fileName + "." + fileType;
            	}
        	} else {
        		fileNameMap.put(fileName, 0);
        	}
        	try {
        		zipEntry = new ZipEntry(fileName);
            	zipOutputStream.putNextEntry(zipEntry);
        		fileInputStream = new FileInputStream(webFileConfig.getRootPath() + webFileMetaDataModel.getRelativeUri());
                while ((readBytes = fileInputStream.read(buffer)) != -1) {
                    zipOutputStream.write(buffer, 0, readBytes);
                }
        	} finally {
        		if(zipOutputStream != null) {
        			zipOutputStream.closeEntry();
        		}
        		if (fileInputStream != null) {
        			fileInputStream.close();
        		}
        	}
        }
        zipOutputStream.finish();
        zipOutputStream.close();
	}
	
	/**
	 * zip打包
	 * @param model zip打包参数
	 * @return zip文件元数据
	 * @throws IOException 异常
	 */
	public WebFileMetaDataModel zipPackage(ZipPackageModel model) throws IOException {
		String temp = null;
		
		//生成fileId 32位uuid
		String fileId = Uuid.uuid32();
		//获取filePath参数
		String filePath = null;
		temp = model.getFilePath();
		if (StringUtils.isNotBlank(temp)) {
			temp = normalizePath(temp);
			if (!isLegalFilePath(temp)) {
				throw new RuntimeException("filePath含有非法字符或含有系统保留文件名");
			}
			filePath = temp;
		}
		
		//获取fileTag参数
		String fileTag = null;
		temp = model.getFileTag();
		if (StringUtils.isNotBlank(temp)) {
			fileTag = temp;
			//filePath为空时尝试使用fileTag填充
			if (StringUtils.isBlank(filePath)) {
				logger.debug("filePath为空，使用fileTag作为filePath");
				temp = normalizePath(temp);
				if (!isLegalFilePath(temp)) {
					temp = toLegalFilePath(temp, '-');
					logger.warn("fileTag作为filePath存在非法字符，非法字符统一替换为'-', filePath: {}", temp);
				}
				filePath = temp;
			}
		}
		
		//处理默认filePath
		if (StringUtils.isBlank(filePath)) {
			logger.debug("filePath和fileTag均为空，存入根目录下");
			filePath = "/";
		}
		
		//获取fileName参数
		String fileName = model.getFileName();
		if (StringUtils.isBlank(fileName)) {
			fileName = fileId;
		} else {
			if(!fileSystem.isLegalFileName(fileName)) {
				throw new RuntimeException("fileName含有非法字符或系统保留文件名");
			} 
		}
		
		//获取fileType参数
		String fileType = "zip";
		
		//获取relativeUri参数
		String relativeUri = filePath + fileId + "." + fileType;
		
		//文件上传预处理
		File file = new File(webFileConfig.getRootPath(), relativeUri);
		File parentFile = file.getParentFile();
		if (!parentFile.exists()) {
			parentFile.mkdirs();
		}

		FileOutputStream fileOutputStream = null;
		try {
			fileOutputStream = new FileOutputStream(file);
			zipPackageStream(model, fileOutputStream);
		} finally {
			if (fileOutputStream != null) {
				fileOutputStream.close();
			}
		}
		//组装model
		WebFileMetaDataModel webFileMetaDataModel = new WebFileMetaDataModel();
		webFileMetaDataModel.setFileId(fileId);
		webFileMetaDataModel.setFileName(fileName);
		webFileMetaDataModel.setFileType(fileType);
		webFileMetaDataModel.setFileLength(file.length());
		webFileMetaDataModel.setFileTag(fileTag);
		webFileMetaDataModel.setFilePath(filePath);
		webFileMetaDataModel.setRelativeUri(relativeUri);
		webFileMetaDataModel.setUploadAt(LocalDateTime.now());
		//持久化文件元数据
		webFileMetaDataPsi.save(webFileMetaDataModel);
		
		logger.debug("保存文件成功, file: {}, model: {}", file.getAbsolutePath(), Json.string(model));
		return webFileMetaDataModel;
	}
	
	/**
	 * 删除文件
	 * @param fileId 文件id
	 * @param deleteEmptyDirs 递归删除空目录 true删除, false不删除
	 * @return 被删除文件的元数据信息,如果fileId不存在则返回null
	 */
	public WebFileMetaDataModel delete(String fileId, boolean deleteEmptyDirs) {
		if(!webFileMetaDataPsi.deletable()) {
			throw new RuntimeException("需要重写实现WebFileMetaDataService.deletable方法返回true，并重写WebFileMetaDataService.delByFileId方法，开启文件删除功能");
		}
		WebFileMetaDataModel model = webFileMetaDataPsi.getByFileId(fileId);
		if (model == null) {
			return null;
		}
		String fileUri = webFileConfig.getRootPath() + model.getRelativeUri();
		File file = new File(fileUri);
		if (file.exists()) {
			file.delete();
			if (deleteEmptyDirs) {
				File rootPath = new File(webFileConfig.getRootPath());
				file = file.getParentFile();
				while(!rootPath.equals(file) && file.list().length == 0) {
					file.delete();
					file = file.getParentFile();
				}
			}
		}
		webFileMetaDataPsi.delByFileId(fileId);
		return model;
	}
	
	/**
	 * 删除文件，如果是空文件夹则一并删除
	 * @param fileId 文件id
	 * @return 被删除文件的元数据信息
	 */
	public WebFileMetaDataModel delete(String fileId) {
		WebFileMetaDataModel model = delete(fileId, true);
		return model;
	}
	
	public WebFileMetaDataModel delete(WebFileDeletePo po) {
		return delete(po.getFileId(), po.getDeleteEmptyDirs());
	}
	
}
