package fun.pplm.framework.poplar.web.file.config;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * web应用文件服务配置
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.web.file")
public class WebFileConfig {
	/**
	 * 文件存储根路径
	 */
	private String rootPath;
	/**
	 * 是否开启http restful api
	 * 默认false不开启
	 */
	private Boolean apiEnabled = false;
	
	/**
	 * 文件上传后缀白名单
	 * 默认 * 所有后缀
	 * 文件后缀使用英文‘,’逗号分隔
	 * 无后缀的文件想加入白名单可以使用“" "”或“pdf,,doc”或“doc, ,”方式实现
	 */
	private String fileUploadPostfixWhiteList = "*";
	
	/**
	 * 是否计算上传文件的md5值
	 * 默认false
	 */
	private Boolean fileUploadMd5Enabled = false;
	
	public WebFileConfig() {
		super();
	}
	
	@PostConstruct
	protected void init() {
		if (StringUtils.isBlank(rootPath)) {
			throw new RuntimeException("需要配置web应用文件服务本地存储路径: poplar.web.file.rootPath");
		}
		rootPath = FilenameUtils.normalizeNoEndSeparator(rootPath, true);
		if (StringUtils.isBlank(rootPath)) {
			throw new RuntimeException("无效的web应用文件服务本地存储路径配置");
		}
		File path = new File(rootPath);
		if (!path.exists()) {
			path.mkdirs();
		}
	}
	
	public String getRootPath() {
		return rootPath;
	}

	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}

	public Boolean getApiEnabled() {
		return apiEnabled;
	}

	public void setApiEnabled(Boolean apiEnabled) {
		this.apiEnabled = apiEnabled;
	}

	public String getFileUploadPostfixWhiteList() {
		return fileUploadPostfixWhiteList;
	}

	public void setFileUploadPostfixWhiteList(String fileUploadPostfixWhiteList) {
		this.fileUploadPostfixWhiteList = fileUploadPostfixWhiteList;
	}

	public Boolean getFileUploadMd5Enabled() {
		return fileUploadMd5Enabled;
	}

	public void setFileUploadMd5Enabled(Boolean fileUploadMd5Enabled) {
		this.fileUploadMd5Enabled = fileUploadMd5Enabled;
	}

	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("rootPath", rootPath);
		map.put("apiEnabled", apiEnabled);
		map.put("fileUploadPostfixWhiteList", fileUploadPostfixWhiteList);
		map.put("fileUploadMd5Enabled", fileUploadMd5Enabled);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}

}
