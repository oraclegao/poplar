package fun.pplm.framework.poplar.web.file.controller;

import java.io.IOException;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fun.pplm.framework.poplar.common.utils.BeanUtil;
import fun.pplm.framework.poplar.web.audit.annotation.Audit;
import fun.pplm.framework.poplar.web.file.bean.model.WebFileMetaDataModel;
import fun.pplm.framework.poplar.web.file.bean.po.DownloadZipPo;
import fun.pplm.framework.poplar.web.file.bean.po.WebFileDeletePo;
import fun.pplm.framework.poplar.web.file.bean.po.WebFileUploadPo;
import fun.pplm.framework.poplar.web.file.bean.vo.WebFileMetaDataVo;
import fun.pplm.framework.poplar.web.file.service.WebFileService;
import fun.pplm.framework.poplar.web.resp.Resp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import static fun.pplm.framework.poplar.web.file.utils.WebFileConstant.WEB_AUDIT_TYPE_WEB_FILE_UPLOAD;
import static fun.pplm.framework.poplar.web.file.utils.WebFileConstant.WEB_AUDIT_TYPE_WEB_FILE_DOWNLOAD;
import static fun.pplm.framework.poplar.web.file.utils.WebFileConstant.WEB_AUDIT_TYPE_WEB_FILE_ZIP_DOWNLOAD;
import static fun.pplm.framework.poplar.web.file.utils.WebFileConstant.WEB_AUDIT_TYPE_WEB_FILE_DELETE;

/**
 * 
 * web应用文件服务
 * @author OracleGao
 *
 */
@ConditionalOnProperty(prefix = "poplar.web.file", name = "apiEnabled", havingValue = "true")
@Api(value = "webFileController", tags = "web应用文件服务")
@Validated
@RestController
@RequestMapping(path = "/file", produces = MediaType.APPLICATION_JSON_VALUE)
public class WebFileController {
	@Autowired
	private WebFileService webFileService;
	
	@Audit(value = "文件上传", types = WEB_AUDIT_TYPE_WEB_FILE_UPLOAD)
	@ApiOperation(value = "文件上传", notes = "文件上传")
	@PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public Resp<WebFileMetaDataVo> upload(@Valid WebFileUploadPo po)
			throws IOException {
		WebFileMetaDataVo vo = webFileService.upload(po);
		return Resp.success(vo);
	}
	
	@ApiOperation(value = "文件信息", notes = "文件信息")
	@GetMapping("/info")
	public Resp<WebFileMetaDataVo> info(@ApiParam(value = "fileId", required = true) @NotBlank @RequestParam("fileId") String fileId) {
		WebFileMetaDataModel model = webFileService.getMetaData(fileId);
		if (model == null) {
			throw new RuntimeException("无效的fileId");
		}
		WebFileMetaDataVo vo = BeanUtil.newFrom(model, WebFileMetaDataVo.class);
		return Resp.success(vo);
	}
	
	@Audit(value = "文件下载", types = WEB_AUDIT_TYPE_WEB_FILE_DOWNLOAD)
	@ApiOperation(value = "文件下载", notes = "文件下载")
	@GetMapping("/download")
	public ResponseEntity<Resource> download(@ApiParam(value = "fileId", required = true) @NotBlank @RequestParam("fileId") String fileId) throws IOException {
		return webFileService.download(fileId);
	}
	
	@Audit(value = "文件zip包下载", types = WEB_AUDIT_TYPE_WEB_FILE_ZIP_DOWNLOAD)
	@ApiOperation(value = "文件zip包下载", notes = "文件zip包下载")
	@PostMapping("/download/zip")
	public ResponseEntity<Resource> downloadZip(@Valid @RequestBody DownloadZipPo po) throws IOException {
		return webFileService.downloadZip(po);
	}
	
	@Audit(value = "删除文件", types = WEB_AUDIT_TYPE_WEB_FILE_DELETE)
	@ApiOperation(value = "删除文件", notes = "删除文件")
	@PostMapping("/delete")
	public Resp<WebFileMetaDataVo> delete(@Valid WebFileDeletePo po) {
		if (po.getDeleteEmptyDirs() == null) {
			po.setDeleteEmptyDirs(true);
		}
		WebFileMetaDataModel model = webFileService.delete(po);
		if (model == null) {
			throw new RuntimeException("fileId不存在");
		}
		WebFileMetaDataVo vo = BeanUtil.newFrom(model, WebFileMetaDataVo.class);
		return Resp.success(vo);
	}
	
}
