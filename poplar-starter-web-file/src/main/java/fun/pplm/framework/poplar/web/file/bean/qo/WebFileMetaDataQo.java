package fun.pplm.framework.poplar.web.file.bean.qo;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * web应用文件元数据查询参数
 * @author OracleGao
 *
 */
@ApiModel(description = "web应用文件元数据查询参数")
public class WebFileMetaDataQo {
	@ApiModelProperty(value = "文件id, 32位uuid")
	private String fileId;
	@ApiModelProperty(value = "文件名")
	private String fileName;
	@ApiModelProperty(value = "文件类型")
	private String fileType;
	@ApiModelProperty(value = "文件标签，用于业务分组")
	private String fileTag;
	@ApiModelProperty(value = "文件存储路径")
	private String filePath;
	@ApiModelProperty(value = "磁盘上相对路径文件uri")
	private String relativeUri;
	@ApiModelProperty(value = "上传时间开始，格式yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime uploadBeginAt;
	@ApiModelProperty(value = "上传时间结束，格式yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime uploadEndAt;
	
	public WebFileMetaDataQo() {
		super();
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFileTag() {
		return fileTag;
	}

	public void setFileTag(String fileTag) {
		this.fileTag = fileTag;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getRelativeUri() {
		return relativeUri;
	}

	public void setRelativeUri(String relativeUri) {
		this.relativeUri = relativeUri;
	}

	public LocalDateTime getUploadBeginAt() {
		return uploadBeginAt;
	}

	public void setUploadBeginAt(LocalDateTime uploadBeginAt) {
		this.uploadBeginAt = uploadBeginAt;
	}

	public LocalDateTime getUploadEndAt() {
		return uploadEndAt;
	}

	public void setUploadEndAt(LocalDateTime uploadEndAt) {
		this.uploadEndAt = uploadEndAt;
	}
	
}
