package fun.pplm.framework.poplar.web.file.bean.model;

/**
 *
 * zip打包model
 * @author OracleGao
 *
 */
public class ZipPackageModel extends ZipPackageStreamModel {
	private Long taskId;
	/**
	 * 压缩包名称
	 * 默认取文件id
	 */
	private String fileName;

	/**
	 * 文件标签用于业务分组，如果filePath为空，作为文件存储路径使用，如果filePath和fileTag都为空，默认存储在根目录下
	 */
	private String fileTag;

	/**
	 * 指定文件存储相对路径
	 */
	private String filePath;

	public ZipPackageModel() {
		super();
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileTag() {
		return fileTag;
	}

	public void setFileTag(String fileTag) {
		this.fileTag = fileTag;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

}
