package fun.pplm.framework.poplar.web.file.utils;

/**
 * 
 * web应用文件常量
 * @author OracleGao
 *
 */
public class WebFileConstant {
	/**
	 * web审计类型 文件上传
	 */
	public static final String WEB_AUDIT_TYPE_WEB_FILE_UPLOAD = "poplar-web-file-upload";
	/**
	 * web审计类型 文件下载
	 */
	public static final String WEB_AUDIT_TYPE_WEB_FILE_DOWNLOAD = "poplar-web-file-download";
	/**
	 * web审计类型 文件转pdf下载
	 */
	public static final String WEB_AUDIT_TYPE_WEB_FILE_TO_PDF_DOWNLOAD = "poplar-web-file-to-pdf-download";
	/**
	 * web审计类型 文件转带水印pdf下载
	 */
	public static final String WEB_AUDIT_TYPE_WEB_FILE_TO_WATERMARK_PDF_DOWNLOAD = "poplar-web-file-to-watermark-pdf-download";
	/**
	 * web审计类型 文件打zip包下载
	 */
	public static final String WEB_AUDIT_TYPE_WEB_FILE_ZIP_DOWNLOAD = "poplar-web-file-zip-download";
	/**
	 * web审计类型 文件删除
	 */
	public static final String WEB_AUDIT_TYPE_WEB_FILE_DELETE = "poplar-web-file-delete";
	
}
