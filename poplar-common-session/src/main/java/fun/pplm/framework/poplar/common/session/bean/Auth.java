package fun.pplm.framework.poplar.common.session.bean;

/**
 * 
 * auth类
 * 登录成功后返回的信息
 * @author OracleGao
 *
 */
public class Auth {
	
	/**
	 * token
	 */
	private String token;
	/**
	 * 过期时间，单位毫秒
	 */
	private Long expireAt;

	public Auth() {
		super();
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Long getExpireAt() {
		return expireAt;
	}

	public void setExpireAt(Long expireAt) {
		this.expireAt = expireAt;
	}

}
