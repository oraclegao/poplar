package fun.pplm.framework.poplar.common.session.service;

import fun.pplm.framework.poplar.common.session.bean.Auth;
import fun.pplm.framework.poplar.common.session.pii.SessionPii;

/**
 * 
 * 会话服务
 * 具体实现登录和登出功能使用
 * 无需业务服务实现，由框架子模块或starter服务实现
 * @author OracleGao
 *
 */
public interface SessionService {
	/**
	 * 创建session返回auth
	 * @param session 会话对象
	 * @return auth对象
	 */
	public Auth createSession(SessionPii session);
	
	/**
	 * 创建session返回auth。
	 * 
	 * @param session 会话对象
	 * @param setCookieFlag ture设置cookie，path设置为"/"，超时时间同session超时时间；false不设置cookie。
	 * @return auth对象
	 */
	public Auth createSession(SessionPii session, boolean setCookieFlag);
	
	/**
	 * 更新当前会话session
	 * @param session 会话信息
	 */
	public void updateSession(SessionPii session);
	
	/**
	 * 更新指定token会话session
	 * @param token token
	 * @param session 会话信息
	 */
	public void updateSession(String token, SessionPii session);
	
	/**
	 * 清理当前会话session
	 */
	public void cleanSession();
	
	/**
	 * 清理指定token会话session
	 * @param token token
	 */
	public void cleanSession(String token);
	
}
