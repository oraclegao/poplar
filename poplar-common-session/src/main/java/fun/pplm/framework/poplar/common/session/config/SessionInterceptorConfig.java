package fun.pplm.framework.poplar.common.session.config;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.web.session.psi.SessionWhitePathPsi;
import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * 会话拦截器配置
 * 由具体拦截器继承并配置具体会话拦截器
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.session.interceptor")
public class SessionInterceptorConfig {
	private static Logger logger = LoggerFactory.getLogger(SessionInterceptorConfig.class);
	
	@Autowired(required = false)
	private List<SessionWhitePathPsi> sessionWhitePathServices = new ArrayList<>();
	/**
	 * 鉴权路径
	 * 默认/**
	 */
    private List<String> authPathPatterns = new ArrayList<>();
    /**
     * 白名单路径
     * 默认 /favicon.ico
     */
    private List<String> whitePathPatterns = new ArrayList<>();
    
	public SessionInterceptorConfig() {
		super();
	}

    @PostConstruct
    private void init() {
    	if (!sessionWhitePathServices.isEmpty()) {
    		for (SessionWhitePathPsi sessionWhitePathService : sessionWhitePathServices) {
    			whitePathPatterns.addAll(sessionWhitePathService.whitePathPatterns());
    		}
    	}
    	whitePathPatterns.add("/favicon.ico");
    	whitePathPatterns.add("/error");
    	logger.debug(this.getClass().getSimpleName() + "注入成功, config: {}", this);
    }
	
	public List<String> getAuthPathPatterns() {
		return authPathPatterns;
	}

	public void setAuthPathPatterns(List<String> authPathPatterns) {
		this.authPathPatterns = authPathPatterns;
	}

	public List<String> getWhitePathPatterns() {
		return whitePathPatterns;
	}

	public void setWhitePathPatterns(List<String> whitePathPatterns) {
		this.whitePathPatterns = whitePathPatterns;
	}

	public List<SessionWhitePathPsi> getSessionWhitePathServices() {
		return sessionWhitePathServices;
	}

	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("authPathPatterns", authPathPatterns);
		map.put("whitePathPatterns", whitePathPatterns);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
    
}
