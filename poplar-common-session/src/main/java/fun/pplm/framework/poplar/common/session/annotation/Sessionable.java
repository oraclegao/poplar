package fun.pplm.framework.poplar.common.session.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * 用户会话类注解
 * 当存在多个{@code fun.pplm.framework.poplar.common.session.bean.Session}的实现类时，使用该注解标识唯一
 * @see fun.pplm.framework.poplar.common.session.pii.SessionPii
 * @author OracleGao
 *
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Sessionable {

}
