package fun.pplm.framework.poplar.common.session.pii;

import fun.pplm.framework.poplar.common.session.annotation.Sessionable;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfoList;
import io.github.classgraph.ScanResult;

/**
 * 
 * Poplar Identification Interface
 * 会话信息标识接口
 * @author OracleGao
 *
 */
public interface SessionPii {
	/**
	 * 具体的Session类
	 */
	public static Class<? extends SessionPii> concreteSessionClass = getConcreteSessionClass();
	
	/**
	 * 获取实现Session接口的具体Session类
	 * @return 具体Session类
	 */
	static Class<? extends SessionPii> getConcreteSessionClass() {		
	    try (ScanResult scanResult = new ClassGraph().enableClassInfo().enableAnnotationInfo().scan()) {
	    	ClassInfoList classInfoList = scanResult.getClassesWithAnnotation(Sessionable.class);
	    	int size = classInfoList.size();
	    	if (size == 0) {
	    		classInfoList = scanResult.getClassesImplementing(SessionPii.class);
	    		size = classInfoList.size();
	    		if (size == 1) {
	    			return classInfoList.get(0).loadClass(SessionPii.class);
	    		} else if (size == 0) {
	    			throw new RuntimeException("找不到fun.pplm.framework.poplar.common.session.pii.SessionPii的实现类.");
	    		} else {
	    			throw new RuntimeException("找到多个fun.pplm.framework.poplar.common.session.pii.SessionPii的实现类，请使用fun.pplm.framework.poplar.common.session.annotation.Sessionable注解标注唯一.");
	    		}
	    	} else if (size == 1) {
	    		return classInfoList.get(0).loadClass(SessionPii.class);
	    	} else {
	    		throw new RuntimeException("fun.pplm.framework.poplar.common.session.annotation.Sessionable注解了多个类，只能唯一注解.");
	    	}
		}
	}
	
}
