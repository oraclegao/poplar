package fun.pplm.framework.poplar.common.session.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fun.pplm.framework.poplar.common.session.config.SessionConfig;
import fun.pplm.framework.poplar.web.swagger.psi.SwaggerSecurityPsi;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.service.contexts.SecurityContext;

/**
 * 
 * 会话security服务接口
 * @author OracleGao
 *
 */
@Service
public class SessionSwaggerSecurityService implements SwaggerSecurityPsi {
	@Autowired
	private SessionConfig sessionConfig;

	@Override
	public List<SecurityScheme> getSecuritySchemes() {
		return Arrays.asList(new ApiKey(sessionConfig.getHeaderKey(), sessionConfig.getHeaderKey(), "header"));
	}

	@Override
	public List<SecurityContext> getSecurityContexts() {
		return Arrays.asList(SecurityContext.builder().securityReferences(defaultAuth()).build());
	}

	private List<SecurityReference> defaultAuth() {
		return Arrays.asList(new SecurityReference(sessionConfig.getHeaderKey(), new AuthorizationScope[] {new AuthorizationScope("global", "accessEverything")}));
	}
	
}
