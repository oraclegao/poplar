package fun.pplm.framework.poplar.common.session.service;

import java.io.IOException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import fun.pplm.framework.poplar.common.session.config.SessionConfig;
import fun.pplm.framework.poplar.common.session.pii.SessionPii;
import fun.pplm.framework.poplar.web.utils.HttpServletUtil;

/**
 * 
 * 使用http request对象管理session相关信息
 * @author OracleGao
 *
 */
@Service
public class SessionHttpService {
	private Logger logger = LoggerFactory.getLogger(SessionHttpService.class);
	
	@Autowired
	private SessionConfig sessionConfig;
	@Autowired
	private ObjectMapper objectMapper;
	
	/**
	 * 从http request 属性中获取会话信息
	 * @param <T> 具体session类
	 * @param clazz 具体session类
	 * @return 具体session对象
	 */
	public <T extends SessionPii> T getSession(Class<T> clazz) {
		Object obj = HttpServletUtil.getRequestAttribute(sessionConfig.getRequestAttrKey());
		if (obj == null) {
			return null;
		}
		T t = null;
		try {
			t = objectMapper.readValue(obj.toString(), clazz);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		return t;
	}
	
    /**
     * 获取当前会话token
     * @return token字符串
     */
	public String getToken() {
		String token = HttpServletUtil.getHeaderValue(sessionConfig.getHeaderKey());
		if (token == null) {
			token = HttpServletUtil.getCookieValue(sessionConfig.getCookieKey());
		}
		return token;
	}
	
	/**
	 * 在cookie中设置token
	 * @param token token值
	 * @param path cookie路径
	 * @param maxAge cookie有效期，单位秒
	 */
	public void setCookieToken(String token, String path, int maxAge) {
		Cookie cookie = new Cookie(sessionConfig.getCookieKey(), token);
		cookie.setPath("/");
		cookie.setMaxAge(maxAge);
		setCookie(cookie);
	}
	
	public void setCookie(Cookie cookie) {
		HttpServletResponse response = HttpServletUtil.getResponse();
		if (response != null) {
			response.addCookie(cookie);
		} else {
			logger.warn("HttpServletResponse is null, skip set cookie");
		}
	}
	
}
