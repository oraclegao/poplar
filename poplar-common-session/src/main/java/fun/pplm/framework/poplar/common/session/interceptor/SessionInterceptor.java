package fun.pplm.framework.poplar.common.session.interceptor;

import javax.security.sasl.AuthenticationException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;

import fun.pplm.framework.poplar.common.session.config.SessionConfig;
import fun.pplm.framework.poplar.common.session.exception.SessionValidateException;
import fun.pplm.framework.poplar.common.session.pii.SessionPii;
import fun.pplm.framework.poplar.common.session.psi.SessionValidatePsi;
import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * 基础会话拦截器
 * @author OracleGao
 *
 */
public abstract class SessionInterceptor implements HandlerInterceptor {
	@Autowired
	private SessionConfig sessionConfig;
	
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private SessionValidatePsi sessionValidateService;
	
	private Class<? extends SessionPii> sessionClass = SessionPii.concreteSessionClass;
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String token = getHeaderValue(request, sessionConfig.getHeaderKey());
		if (StringUtils.isBlank(token)) {
			token = getCookieValue(request, sessionConfig.getCookieKey());
		}
		if (StringUtils.isNotBlank(token)) {
			SessionPii session = validate(request, token, sessionConfig.getRefreshExpire(), sessionClass);
			if (session == null) {
				return false;
			}
			if (sessionValidateService != null) {
				if (!sessionValidateService.validate(session, request)) {
					throw new SessionValidateException("session validate exception");
				}
			}
			String sessionJson = Json.string(session);
			request.setAttribute(sessionConfig.getRequestAttrKey(), sessionJson);
			return true;
		}
		throw new SessionValidateException("session token not found, path: " + request.getServletPath());
	}

	/**
	 * 校验token并返回session对象
	 * @param request http请求对象
	 * @param token token字符串
     * @param refreshExpire 刷新会话过期时间
     * @param sessionClass 具体会话信息类
	 * @return 会话对象
	 * @throws AuthenticationException 鉴权异常
	 */
	protected abstract SessionPii validate(HttpServletRequest request, String token, boolean refreshExpire, Class<? extends SessionPii> sessionClass) throws AuthenticationException;
	
	/**
	 * 获取请求头中的值
	 * @param request http请求对象
	 * @param key 请求头key
	 * @return 请求头值
	 */
	protected String getHeaderValue(HttpServletRequest request, String key) {
		return request.getHeader(key);
	}
	
	/**
	 * 获取cookie中的值
	 * @param request http请求对象
	 * @param key cookie key
	 * @return cookie值
	 */
	protected String getCookieValue(HttpServletRequest request, String key) {
		Cookie[] cookies = request.getCookies();
    	if (cookies != null && cookies.length > 0) {
    	    for (Cookie cookie : cookies) {
    	        if (key.equals(cookie.getName())) {
    	            return cookie.getValue().trim();
    	        }
    	    }
    	}
    	return null;
	}
	
}
