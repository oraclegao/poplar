package fun.pplm.framework.poplar.common.session.exception;

import javax.security.sasl.AuthenticationException;

/**
 * 
 * 会话校验异常
 * @author OracleGao
 *
 */
public class SessionValidateException extends AuthenticationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6518521910026494551L;

	public SessionValidateException() {
		super();
	}

	public SessionValidateException(String detail, Throwable ex) {
		super(detail, ex);
	}

	public SessionValidateException(String detail) {
		super(detail);
	}

}
