package fun.pplm.framework.poplar.common.session.configurer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import fun.pplm.framework.poplar.common.session.config.SessionInterceptorConfig;
import fun.pplm.framework.poplar.common.session.interceptor.SessionInterceptor;

/**
 * 
 * 会话拦截器配置
 * 由具体拦截器继承并配置具体会话拦截器
 * @author OracleGao
 *
 */
public abstract class SessionInterceptorConfigurer implements WebMvcConfigurer {
	@Autowired
	protected SessionInterceptorConfig sessionInterceptorConfig;
	@Autowired
	private SessionInterceptor sessionInterceptor;
    
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration registration = registry.addInterceptor(sessionInterceptor);
        
        List<String> authPathPatterns = sessionInterceptorConfig.getAuthPathPatterns();
        List<String> whitePathPatterns = sessionInterceptorConfig.getWhitePathPatterns();
        
        if (authPathPatterns.isEmpty()) {
        	registration.addPathPatterns("/**");
        } else {
        	registration.addPathPatterns(authPathPatterns);
        }
        
        if (!whitePathPatterns.isEmpty()) {
        	registration.excludePathPatterns(whitePathPatterns);
        }
	}
	
}
