package fun.pplm.framework.poplar.common.session.config;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * 会话配置
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.session")
public class SessionConfig {
	private Logger logger = LoggerFactory.getLogger(SessionConfig.class);
	
	/**
	 * token 过期时间
	 * 默认1800秒
	 */
	private Long expireSecond = 1800L;
	
	/**
	 * 用户访问刷新token过期时间
	 */
	private Boolean refreshExpire = true;
	
	/**
	 * session http header key
	 */
	private String headerKey = "token";
	/**
	 * session cookie key
	 */
	private String cookieKey = "token";
	
	/**
	 * http request对象的attribute属性传递session对象的key
	 * 默认 poplarSession
	 * @see {@link javax.servlet.ServletRequest} setAttribute() getAttribute()
	 */
	private String requestAttrKey = "poplarSession";

    @PostConstruct
    private void init() {
    	logger.debug(this.getClass().getSimpleName() + "注入成功, config: {}", this);
    }
    
	public SessionConfig() {
		super();
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Long getExpireSecond() {
		return expireSecond;
	}

	public void setExpireSecond(Long expireSecond) {
		this.expireSecond = expireSecond;
	}

	public Boolean getRefreshExpire() {
		return refreshExpire;
	}

	public void setRefreshExpire(Boolean refreshExpire) {
		this.refreshExpire = refreshExpire;
	}

	public String getHeaderKey() {
		return headerKey;
	}

	public void setHeaderKey(String headerKey) {
		this.headerKey = headerKey;
	}

	public String getCookieKey() {
		return cookieKey;
	}

	public void setCookieKey(String cookieKey) {
		this.cookieKey = cookieKey;
	}

	public String getRequestAttrKey() {
		return requestAttrKey;
	}

	public void setRequestAttrKey(String requestAttrKey) {
		this.requestAttrKey = requestAttrKey;
	}

	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("expireSecond", expireSecond);
		map.put("refreshExpire", refreshExpire);
		map.put("headerKey", headerKey);
		map.put("cookieKey", cookieKey);
		map.put("requestAttrKey", requestAttrKey);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
	
}
