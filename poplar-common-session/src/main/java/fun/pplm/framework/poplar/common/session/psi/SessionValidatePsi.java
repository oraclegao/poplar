package fun.pplm.framework.poplar.common.session.psi;

import javax.servlet.http.HttpServletRequest;

import fun.pplm.framework.poplar.common.session.exception.SessionValidateException;
import fun.pplm.framework.poplar.common.session.pii.SessionPii;

/**
 * 
 * Poplar Service Interface
 * session对象校验对象回调接口
 * @author OracleGao
  * @param <T> session类
 * 
 */
public interface SessionValidatePsi<T extends SessionPii> {
	
	/**
	 * 校验会话对象
	 * @param session 会话对象
     * @param request http请求对象
	 * @return true成功 false失败
	 * @throws SessionValidateException 会话校验异常
	 */
	public boolean validate(T session, HttpServletRequest request) throws SessionValidateException;
	
}
