## 1.11.21
- 使用classgraph替换reflections，扫描SessionPii实现类
- SessionService增加createSession(SessionPii session, boolean setCookieFlag)方法实现创建session时设置cookie
- SessionHttpService增加setCookieToken()相关方法，将token设置到http接口response的cookie中

## 1.8.17
- 增加session白名单回调服务注入，允许其他服务注入白名单url patterns
- 优化session拦截器配置，将/error和/favicon.ico作为默认会话白名单项

## 1.8.16
- 标识接口类后缀更名为Pii: Session -> SessionPii
- 接口类类名后缀更名为Psi，并优化类名SessionValidator -> SessionValidatePsi

## 1.8.15
- 使用HttpServletUtil替换AppContext

## 1.6.11
- 增加会话更新接口
- 完善会话清理接口

## 1.6.10
- 优化getSession方法

## 1.5.9
- 优化框架逻辑

## 1.5.8
- 增加无法获取token值时异常提示信息
