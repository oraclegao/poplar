package fun.pplm.framework.poplar.web.swagger.configurer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import fun.pplm.framework.poplar.web.swagger.config.SwaggerConfig;
import fun.pplm.framework.poplar.web.swagger.config.SwaggerConfig.SwaggerAuth;

/**
 * 
 * 授权鉴权配置
 * @author OracleGao
 *
 */
@Configuration
@EnableWebSecurity
@ConditionalOnMissingBean(WebSecurityConfigurerAdapter.class)
public class SwaggerSecurityConfigurer extends WebSecurityConfigurerAdapter {
	@Autowired
	private SwaggerConfig swaggerConfig;
	
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable();
		SwaggerAuth auth = swaggerConfig.getAuth();
		if (auth.getEnabled()) {
			httpSecurity.csrf().disable().authorizeRequests()
				.antMatchers("/swagger-ui/**", "/doc.html", "/v2/api-docs").authenticated()
				.and().formLogin().loginPage("/swagger-login").permitAll();
		}
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		SwaggerAuth auth = swaggerConfig.getAuth();
		if (auth.getEnabled()) {
			PasswordEncoder encoder = passwordEncoder();
			authenticationManagerBuilder.inMemoryAuthentication().passwordEncoder(encoder).withUser(auth.getUsername())
					.password(encoder.encode(auth.getPassword())).roles("USER");
		} else {
			authenticationManagerBuilder.inMemoryAuthentication().withUser("user").password("{noop}").roles("USER");
		}
	}
	
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
}
