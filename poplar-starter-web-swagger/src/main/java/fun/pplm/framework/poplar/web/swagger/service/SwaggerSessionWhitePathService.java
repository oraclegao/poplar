package fun.pplm.framework.poplar.web.swagger.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.stereotype.Service;

import fun.pplm.framework.poplar.web.session.psi.SessionWhitePathPsi;

/**
 * 
 * swagger会话白名单url pettern服务
 * 将swagger相关url作为白名单注入session拦截服务
 * @author OracleGao
 *
 */
@Service
@ConditionalOnClass(name = "fun.pplm.framework.poplar.common.session.config.SessionInterceptorConfig")
public class SwaggerSessionWhitePathService implements SessionWhitePathPsi {

	@Override
	public List<String> whitePathPatterns() {
		List<String> patterns = new ArrayList<>();
		patterns.add("/swagger-login");
		patterns.add("/v2/api-docs");
		patterns.add("/doc.html");
		patterns.add("/poplar-swagger-ui/**");
		patterns.add("/webjars/bootstrap/**");
		patterns.add("/webjars/bycdao-ui/**");
		patterns.add("/swagger-ui/**");
		patterns.add("/swagger-resources/**");
		return patterns;
	}

}
