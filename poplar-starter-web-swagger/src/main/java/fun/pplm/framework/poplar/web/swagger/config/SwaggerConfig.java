package fun.pplm.framework.poplar.web.swagger.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.web.swagger.psi.SwaggerSecurityPsi;

import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

/**
 * 
 * swagger配置
 * @author OracleGao
 *
 */
@Configuration
@EnableSwagger2
@ConfigurationProperties("poplar.swagger")
public class SwaggerConfig {
	private static Logger logger = LoggerFactory.getLogger(SwaggerConfig.class);

	@Autowired(required = false)
	private SwaggerSecurityPsi swaggerSecurityPsi;
	
	private List<String> basePackages = new ArrayList<>();

	/**
	 * basePackege包括poplar框架的package
	 * 默认true
	 */
	private Boolean includePoplarPackage = true;
	private String title = "接口文档";
	private String desc = "接口文档描述";
	private String version = "1.0";
	
	private SwaggerAuth auth = new SwaggerAuth();

	@Bean
	public Docket createRestApi() {
		logger.debug("初始化Swagger, config: {}", this);
		Docket docket = new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
				.apis(getBasePackagePredicates()).paths(PathSelectors.any()).build();
		if (swaggerSecurityPsi != null) {
			List<SecurityScheme> securitySchemes = swaggerSecurityPsi.getSecuritySchemes();
			if (securitySchemes != null && securitySchemes.size() > 0) {
				docket.securitySchemes(securitySchemes);
			}
			List<SecurityContext> securityContexts = swaggerSecurityPsi.getSecurityContexts();
			if (securityContexts != null && securityContexts.size() > 0) {
				docket.securityContexts(securityContexts);
			}
		}
		return docket;
	}

	/**
	 * 获取多个basePackage
	 * @return basePackage谓词
	 */
	private Predicate<RequestHandler> getBasePackagePredicates() {
		Predicate<RequestHandler> predicate = null;
		if (includePoplarPackage || basePackages.size() == 0) {
			predicate = RequestHandlerSelectors.basePackage("fun.pplm.framework.poplar");
		}
		for (String basePackage : basePackages) {
			if (predicate == null) {
				predicate = RequestHandlerSelectors.basePackage(basePackage);
			} else {
				predicate = predicate.or(RequestHandlerSelectors.basePackage(basePackage));
			}
		}
		return predicate;
	}
	
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title(title).description(desc).version(version).build();
	}

	public List<String> getBasePackages() {
		return basePackages;
	}

	public void setBasePackages(List<String> basePackages) {
		this.basePackages = basePackages;
	}

	public Boolean getIncludePoplarPackage() {
		return includePoplarPackage;
	}

	public void setIncludePoplarPackage(Boolean includePoplarPackage) {
		this.includePoplarPackage = includePoplarPackage;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	public SwaggerAuth getAuth() {
		return auth;
	}

	public void setAuth(SwaggerAuth auth) {
		this.auth = auth;
	}

	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("basePackages", basePackages);
		map.put("title", title);
		map.put("desc", desc);
		map.put("version", version);
		map.put("auth", auth);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
	
	/**
	 * 
	 * swagger访问权限配置
	 * @author OracleGao
	 *
	 */
	public static class SwaggerAuth {
		/**
		 * 是否启用
		 * 默认 否
		 */
		private Boolean enabled = false;
		/**
		 * 用户名
		 */
		private String username;
		/**
		 * 密码
		 */
		private String password;
		
		public SwaggerAuth() {
			super();
		}

		public Boolean getEnabled() {
			return enabled;
		}

		public void setEnabled(Boolean enabled) {
			this.enabled = enabled;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}
	}
	
}