package fun.pplm.framework.poplar.web.swagger.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import fun.pplm.framework.poplar.web.swagger.config.SwaggerConfig;

import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * swagger登录controller
 * @author OracleGao
 *
 */
@ApiIgnore
@Controller
@ConditionalOnProperty(prefix = "poplar.swagger.auth", name = "enabled", havingValue = "true")
public class SwaggerAuthController {
	@Autowired
	private SwaggerConfig swaggerConfig;
	
	@GetMapping("/swagger-login")
    public String redirectLogin(Model model){
		model.addAttribute("title", swaggerConfig.getTitle());
        return "poplar-swagger-login";
    }
	
}
