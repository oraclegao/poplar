## 1.8.17
- 添加文档的授权和鉴权支持（仅swagger html和api，不含业务接口）
- 新增SwaggerSessionWhitePathService服务，把swagger相关url作为白名单注入session拦截服务，使session不拦截swagger相关服务

## 1.8.16
- 模块名称更名为poplar-starter-web-swagger
- security相关对象由SwaggerSecurityPsi实现类提供

## 1.6.9
- 增加配置项includePoplarPackage，basePackage是否包括poplar框架包

## 1.5.8
- 移除poplar.swagger.basePackage配置
- 增加poplar.swagger.basePackages配置，支持扫描多个基本包
- 默认增加fun.pplm.framework.poplar框架基础包扫描
