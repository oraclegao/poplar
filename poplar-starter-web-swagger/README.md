# poplar-swagger-starter

swagger starter

## 配置项
- poplar.swagger.basePackage: 扫描的包，默认fun.pplm
- poplar.swagger.title: 接口文档标题，默认接口文档
- poplar.swagger.desc: 接口文档描述，默认接口文档描述
- poplar.swagger.version: 接口文档版本，默认1.0
- poplar.swagger.headerTokenKey: http求情头部会话token key，默认token
