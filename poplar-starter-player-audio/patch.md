## 1.10.19
- 支持系统默认音响热插拔
- AudioPlayer类添加播放器状态常量
- 修复播放状态转换bug
- AudioUtil工具添加音频流合成和音频流时长计算静态方法

## 1.9.18
- 音频播放器starter
- 支持java源生音频格式播放，包括wav和mp3两个音频格式
- 仅在win10系统调试，其它系统未经过调试