package fun.pplm.framework.poplar.player.audio.config;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * 音频播放器配置
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.player.audio")
@ConditionalOnExpression("${poplar.player.audio.enabled:true}")
public class AudioPlayerConfig {
	private Boolean enabled = true;
	private Boolean autoInit = true;
	/**
	 * 音频播放设备名称
	 * javax.sound.sampled.Mixer.Info.name
	 * 默认null，系统主音频播放设备，会随系统主音频播放设备变化而变化
	 */
	private String name;
	
	/**
	 * 播放完成后自动关闭并释放资源
	 * 默认true
	 * true是，false否
	 */
	private Boolean autoClose = true;
	
	/**
	 * 关闭音频设备需要等待操作系统释放音频资源的时长(仅在autoClose为false时生效)
	 * 如果该值过小，会导致关闭音频之后立即载入新的音频文件，操作系统还没释放完旧音频资源，会继续释放新音频文件前面一部分，导致音频丢失前1秒内容
	 * 单位毫秒
	 * 默认400
	 */
	private Long releaseTime = 400L;

	public AudioPlayerConfig() {
		super();
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getAutoInit() {
		return autoInit;
	}

	public void setAutoInit(Boolean autoInit) {
		this.autoInit = autoInit;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getAutoClose() {
		return autoClose;
	}

	public void setAutoClose(Boolean autoClose) {
		this.autoClose = autoClose;
	}

	public Long getReleaseTime() {
		return releaseTime;
	}

	public void setReleaseTime(Long releaseTime) {
		this.releaseTime = releaseTime;
	}

	@Override
	public String toString() {
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("enabled", enabled);
		map.put("autoInit", autoInit);
		map.put("name", name);
		map.put("autoClose", autoClose);
		map.put("releaseTime", releaseTime);
		return Json.string(map);
	}
	
}
