package fun.pplm.framework.poplar.player.audio.component;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ExecutorService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.SourceDataLine;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;

import fun.pplm.framework.poplar.common.config.ThreadPoolConfig;
import fun.pplm.framework.poplar.common.utils.ThreadPoolUtil;
import fun.pplm.framework.poplar.player.audio.bean.AudioPlayerInfo;
import fun.pplm.framework.poplar.player.audio.config.AudioPlayerConfig;
import fun.pplm.framework.poplar.player.audio.utils.AudioUtil;

/**
 * 
 * 音频播放器
 * 基于行源实现
 * @author OracleGao
 *
 */
@Deprecated
@ConditionalOnExpression("${poplar.player.audio.enabled:true}")
public class AudioSourceLinePlayer {
	private static Logger logger = LoggerFactory.getLogger(AudioSourceLinePlayer.class);
	
	@Autowired
	protected AudioPlayerConfig audioPlayerConfig;
	
	protected ThreadPoolConfig threadPoolConfig;
	
	private ExecutorService executerService;
	
	private volatile Boolean playing = false;

	protected AudioPlayerInfo audioPlayerInfo;
	
	@PostConstruct
	protected void autoInit() {
		if (audioPlayerConfig.getAutoInit()) {
			this.init(audioPlayerConfig.getName());
		}
	}

	/**
	 * 初始化音频播放设备
	 * @param name 音频播放设备名称 javax.sound.sampled.Mixer.Info.name 如果blank，默认null，获取系统主音频播放设备，会随系统主音频播放设备变化而变化
	 */
	public void init(String name) {
		logger.debug("初始化音频播放器开始...");
		if (StringUtils.isBlank(name)) {
			List<AudioPlayerInfo> infos = AudioUtil.getAudioPlayerInfos();
			if (!infos.isEmpty()) {
				this.audioPlayerInfo = infos.get(0);
			} else {
				throw new RuntimeException("找不到系统音频播放设备");
			}
		} else {
			this.audioPlayerInfo = AudioUtil.getAudioPlayerInfo(name);
			if (audioPlayerInfo == null) {
				throw new RuntimeException("无效的音频播放设备名称: " + name);
			}
		}
		initThreadPool();
		logger.debug("初始化音频播放器完成");
	}

	private void initThreadPool() {
		threadPoolConfig = new ThreadPoolConfig();
		threadPoolConfig.setCorePoolSize(1);
		threadPoolConfig.setMaximumPoolSize(1);
		threadPoolConfig.setKeepAliveTimeSecond(0);
		threadPoolConfig.setQueueCapacity(32);
		executerService = ThreadPoolUtil.newThreadPool(threadPoolConfig);
	}
	
	/**
	 * 非阻塞播放音频
	 * 方法内会创建新的线程用于音乐播放
	 * @param audioFiles 音频文件名字符串，支持classpath:前缀，在类路径上检索播放文件，classpath:assets/1.wav
	 */
	public void play(String... audioFiles) {
		checkInit();
		playing = true;
		executerService.submit(() -> {
			AudioInputStream[] audioInputStreams = new AudioInputStream[audioFiles.length];
			try {
				InputStream inputStream = null;
				File file = null;
				for (int i = 0; i < audioFiles.length; i++) {
					if (audioFiles[i].startsWith("classpath:")) {
						String classpathFile = audioFiles[i].replace("classpath:", "").trim();
						inputStream = AudioSourceLinePlayer.class.getClassLoader().getResourceAsStream(classpathFile);
						if (inputStream == null) {
							throw new Exception(audioFiles[i] + " not found");
						}
						audioInputStreams[i] = AudioSystem.getAudioInputStream(new BufferedInputStream(inputStream));
					} else {
						file = new File(audioFiles[i]);
						if (!file.exists()) {
							throw new Exception(audioFiles[i] + " not exists");
						}
						if (!file.canRead()) {
							throw new Exception(audioFiles[i] + " can not read");
						}
						audioInputStreams[i] = AudioSystem.getAudioInputStream(file);
					}
				}
				blockPlay(audioInputStreams);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			} finally {
				for (AudioInputStream audioInputStream : audioInputStreams) {
					if (audioInputStream != null) {
						try {
							audioInputStream.close();
						} catch (IOException e) {
							logger.error(e.getMessage(), e);
						}
					}
				}
				playing = false;
			}
		});
	}
	
	/**
	 * 在当前线程阻塞播放
	 * @param audioInputStreams 音频流
	 */
	public void blockPlay(AudioInputStream... audioInputStreams) {
		if (audioInputStreams == null || audioInputStreams.length == 0) {
			return;
		}
		SourceDataLine sourceDataLine = audioPlayerInfo.getSourceDataLine();
		playing = true;
		AudioFormat audioTargetFormat = getAudioTargetFormat(audioInputStreams[0]);
		AudioInputStream audioInputStream = synthesizeAudio(audioTargetFormat, audioInputStreams);
		AudioInputStream pcmStream = AudioSystem.getAudioInputStream(audioTargetFormat, audioInputStream);
		try {
			sourceDataLine.open(audioTargetFormat);
			sourceDataLine.start();
			int bufferSize = (int) audioTargetFormat.getSampleRate() * audioTargetFormat.getFrameSize();
			byte[] buffer = new byte[bufferSize];
			int readCount;
			while ((readCount = pcmStream.read(buffer, 0, buffer.length)) >= 0) {
				sourceDataLine.write(buffer, 0, readCount);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (sourceDataLine != null) {
				sourceDataLine.drain();
				sourceDataLine.stop();
				sourceDataLine.close();
			}
			playing = false;
		}
	}
	
	public boolean inited() {
		return audioPlayerInfo != null;
	}
	
	private void checkInit() {
		if (!inited()) {
			throw new RuntimeException("音频播放器没有初始化");
		}
	}
	
	private AudioFormat getAudioTargetFormat(AudioInputStream audioInputStream) {
		AudioFormat baseFormat = audioInputStream.getFormat();
		AudioFormat audioFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, baseFormat.getSampleRate(), 16,
				baseFormat.getChannels(), baseFormat.getChannels() * 2, baseFormat.getSampleRate(), false);
		return audioFormat;
	}

	private AudioInputStream synthesizeAudio(AudioFormat targetFormat, AudioInputStream... audioInputStreams) {
		long frameLength = 0;
		List<AudioInputStream> streams = new ArrayList<>();
		for (AudioInputStream audioInputStream : audioInputStreams) {
			streams.add(audioInputStream);
			frameLength += audioInputStream.getFrameLength();
		}
		Enumeration<AudioInputStream> enumeration = Collections.enumeration(streams);
		SequenceInputStream sequenceStream = new SequenceInputStream(enumeration);
		AudioInputStream audioInputStream = new AudioInputStream(sequenceStream, targetFormat, frameLength);
		return audioInputStream;
	}

	public Boolean getPlaying() {
		return playing;
	}

	public AudioPlayerConfig getAudioPlayerConfig() {
		return audioPlayerConfig;
	}

	public void setAudioPlayerConfig(AudioPlayerConfig audioPlayerConfig) {
		this.audioPlayerConfig = audioPlayerConfig;
	}

	public AudioPlayerInfo getAudioPlayerInfo() {
		return audioPlayerInfo;
	}

	public void setAudioPlayerInfo(AudioPlayerInfo audioPlayerInfo) {
		this.audioPlayerInfo = audioPlayerInfo;
	}
    
	@PreDestroy
	protected void destroy() {
		ThreadPoolUtil.destoryThreadPool(executerService, threadPoolConfig);
	}
	
}
