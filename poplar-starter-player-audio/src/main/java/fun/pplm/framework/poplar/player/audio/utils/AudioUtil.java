package fun.pplm.framework.poplar.player.audio.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Line;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.Mixer.Info;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.validation.ValidationException;

import fun.pplm.framework.poplar.player.audio.bean.AudioPlayerInfo;

/**
 * 
 * 音频工具类
 * @author OracleGao
 *
 */
public class AudioUtil {
	/**
	 * 获取系统所有音频设备行源
	 * @return 音频设备行源列表
	 */
	public static List<AudioPlayerInfo> getAudioPlayerInfos() {
		List<AudioPlayerInfo> audioPlayerInfos = new ArrayList<>();
		AudioPlayerInfo audioPlayerInfo = null;
		Mixer.Info[] mixerInfo = AudioSystem.getMixerInfo();
    	Line line = null;
    	for (Mixer.Info info : mixerInfo) {
            Mixer mixer = AudioSystem.getMixer(info);
            Line.Info[] sourceLineInfos = mixer.getSourceLineInfo();
            for(Line.Info sourceLineInfo : sourceLineInfos) {
				try {
					line = mixer.getLine(sourceLineInfo);
					if (line instanceof javax.sound.sampled.SourceDataLine) {
						audioPlayerInfo = new AudioPlayerInfo();
						audioPlayerInfo.setMixer(mixer);
						audioPlayerInfo.setMixerInfo(info);
						audioPlayerInfo.setSourceLineInfo(sourceLineInfo);
						audioPlayerInfo.setSourceDataLine((SourceDataLine) line);
						audioPlayerInfos.add(audioPlayerInfo);
	            	}
				} catch (LineUnavailableException e) {}
            }
    	}
    	return audioPlayerInfos;
	}
	
	/**
	 * 根据音频播放设备名称获取音频播放器信息
	 * @param name 音频播放设备名称 javax.sound.sampled.Mixer.Info.name
	 * @return 音频播放器信息
	 */
	public static AudioPlayerInfo getAudioPlayerInfo(String name) {
		List<AudioPlayerInfo> infos = getAudioPlayerInfos();
		for (AudioPlayerInfo info : infos) {
			if (info.getMixerInfo().getName().equals(name)) {
				return info;
			}
		}
		return null;
	}
	
	/**
	 * 获取系统音频播放设备混音器信息
	 * @return 音频播放设备混音器列表
	 */
	public static List<Info> getMixerInfos() {
		List<AudioPlayerInfo> infos = getAudioPlayerInfos();
		return infos.stream().map(e -> e.getMixerInfo()).collect(Collectors.toList());
	}
	
	/**
	 * 根据音频文件合成音频流
	 * 流在使用完成之后需要调用clsoe关闭流释放资源
	 * @param audioFiles 音频文件名字符串，支持classpath:前缀，在类路径上检索播放文件，classpath:assets/1.wav
	 * @return 合成的音频流
	 * @throws IOException io异常
	 * @throws UnsupportedAudioFileException 无效音频格式异常 
	 */
	public static AudioInputStream synthesizeAudio(List<String> audioFiles) throws UnsupportedAudioFileException, IOException {
		if (audioFiles.isEmpty()) {
			return null;
		}
		if (audioFiles.size() == 1) {
			return synthesizeAudio(audioFiles.get(0));
		}
		return synthesizeAudio(audioFiles.toArray(new String[0]));
	}
	
	/**
	 * 根据音频文件合成音频流
	 * 流在使用完成之后需要调用clsoe关闭流释放资源
	 * @param audioFiles 音频文件名字符串，支持classpath:前缀，在类路径上检索播放文件，classpath:assets/1.wav
	 * @return 合成的音频流
	 * @throws IOException io异常
	 * @throws UnsupportedAudioFileException 无效音频格式异常 
	 */
	public static AudioInputStream synthesizeAudio(String... audioFiles) throws UnsupportedAudioFileException, IOException {
		AudioInputStream[] audioInputStreams = new AudioInputStream[audioFiles.length];
		InputStream inputStream = null;
		File file = null;
		for (int i = 0; i < audioFiles.length; i++) {
			if (audioFiles[i].startsWith("classpath:")) {
				String classpathFile = audioFiles[i].replace("classpath:", "").trim();
				inputStream = AudioUtil.class.getClassLoader().getResourceAsStream(classpathFile);
				if (inputStream == null) {
					throw new ValidationException(audioFiles[i] + " not found");
				}
				audioInputStreams[i] = AudioSystem.getAudioInputStream(new BufferedInputStream(inputStream));
			} else {
				file = new File(audioFiles[i]);
				if (!file.exists()) {
					throw new ValidationException(audioFiles[i] + " not exists");
				}
				if (!file.canRead()) {
					throw new ValidationException(audioFiles[i] + " can not read");
				}
				audioInputStreams[i] = AudioSystem.getAudioInputStream(file);
			}
		}
		return synthesizeAudio(audioInputStreams);
	}
	
	/**
	 * 音频流合成
	 * @param audioInputStreams 音频流
	 * @return 合成的音频流
	 */
	public static AudioInputStream synthesizeAudio(AudioInputStream... audioInputStreams) {
		if (audioInputStreams.length == 1) {
			return audioInputStreams[0];
		}
		AudioFormat audioFormat = audioInputStreams[0].getFormat();
		long frameLength = 0;
		List<AudioInputStream> streams = new ArrayList<>();
		for (AudioInputStream audioInputStream : audioInputStreams) {
			streams.add(audioInputStream);
			frameLength += audioInputStream.getFrameLength();
		}
		Enumeration<AudioInputStream> enumeration = Collections.enumeration(streams);
		SequenceInputStream sequenceStream = new SequenceInputStream(enumeration);
		AudioInputStream audioInputStream = new AudioInputStream(sequenceStream, audioFormat, frameLength);
		return audioInputStream;
	}
	
	/**
	 * 获取音频流时长
	 * 单位毫秒
	 * @param audioInputStream 音频流
	 * @return 音频流时长
	 */
	public static long getAudioDuration(AudioInputStream audioInputStream) {
		long frameLength = audioInputStream.getFrameLength();
		float frameRate = audioInputStream.getFormat().getFrameRate();
		return (long)(frameLength / frameRate * 1000);
	} 
	
}
