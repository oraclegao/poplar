package fun.pplm.framework.poplar.player.audio.bean;

import javax.sound.sampled.Line;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.SourceDataLine;

/**
 * 
 * 音频播放器信息
 * @author OracleGao
 *
 */
public class AudioPlayerInfo {
	/**
	 * 混音器
	 */
	private Mixer mixer;
	/**
	 * 混音器信息
	 */
	private Mixer.Info mixerInfo;
	/**
	 * 行源信息
	 */
	private Line.Info sourceLineInfo;
	/**
	 * 行源
	 */
	private SourceDataLine sourceDataLine;

	public AudioPlayerInfo() {
		super();
	}

	public Mixer getMixer() {
		return mixer;
	}

	public void setMixer(Mixer mixer) {
		this.mixer = mixer;
	}

	public Mixer.Info getMixerInfo() {
		return mixerInfo;
	}

	public void setMixerInfo(Mixer.Info mixerInfo) {
		this.mixerInfo = mixerInfo;
	}

	public Line.Info getSourceLineInfo() {
		return sourceLineInfo;
	}

	public void setSourceLineInfo(Line.Info sourceLineInfo) {
		this.sourceLineInfo = sourceLineInfo;
	}

	public SourceDataLine getSourceDataLine() {
		return sourceDataLine;
	}

	public void setSourceDataLine(SourceDataLine sourceDataLine) {
		this.sourceDataLine = sourceDataLine;
	}
	
}
