package fun.pplm.framework.poplar.player.audio.component;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineEvent.Type;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import fun.pplm.framework.poplar.player.audio.bean.AudioPlayerInfo;
import fun.pplm.framework.poplar.player.audio.config.AudioPlayerConfig;
import fun.pplm.framework.poplar.player.audio.utils.AudioUtil;

/**
 * 
 * 音频播放器
 * 基于clip剪辑实现
 * 音频播放完毕
 * @author OracleGao
 *
 */
@Component
@ConditionalOnExpression("${poplar.player.audio.enabled:true}")
public class AudioPlayer {
	private static Logger logger = LoggerFactory.getLogger(AudioPlayer.class);
	
	/**
	 * 音频播放器状态
	 * 未初始化
	 */
	public final static int AUDIO_PLAYER_STATUS_UNINIT = -1;
	/**
	 * 音频播放器状态
	 * 准备完毕
	 */
	public final static int AUDIO_PLAYER_STATUS_READY = 0;
	/**
	 * 音频播放器状态
	 * 装载完毕/播放完毕
	 */
	public final static int AUDIO_PLAYER_STATUS_LOADED = 1;
	/**
	 * 音频播放器状态
	 * 播放中
	 */
	public final static int AUDIO_PLAYER_STATUS_PLAYING = 2;
	/**
	 * 音频播放器状态
	 * 暂停
	 */
	public final static int AUDIO_PLAYER_STATUS_PAUSE = 3;
	
	@Autowired
	protected AudioPlayerConfig audioPlayerConfig;
	
	protected AudioPlayerInfo audioPlayerInfo;
	
	protected volatile Clip clip;
	
	/**
	 * 播放器状态
	 * 默认-1
	 * -1: 未初始化
	 * 0: ready准备完毕
	 * 1: loaded装载完毕/播放完毕
	 * 2: playing播放中
	 * 3: pasue暂停
	 */
	protected volatile int status = AUDIO_PLAYER_STATUS_UNINIT;
	
	@PostConstruct
	protected void autoInit() {
		if (audioPlayerConfig.getAutoInit()) {
			this.init(audioPlayerConfig.getName());
		}
	}
	
	public void init() {
		init(null);
	}
	/**
	 * 初始化音频播放设备
	 * @param name 音频播放设备名称 javax.sound.sampled.Mixer.Info.name 如果blank，默认null，获取系统主音频播放设备，会随系统主音频播放设备变化而变化
	 */
	public synchronized void init(String name) {
		if(this.status != AUDIO_PLAYER_STATUS_UNINIT) {
			throw new RuntimeException("音频播放器已经初始化完成");
		}
		logger.debug("初始化音频播放器开始...");
		if (StringUtils.isBlank(name)) {
			List<AudioPlayerInfo> infos = AudioUtil.getAudioPlayerInfos();
			if (!infos.isEmpty()) {
				this.audioPlayerInfo = infos.get(0);
			} else {
				throw new RuntimeException("找不到系统音频播放设备");
			}
		} else {
			this.audioPlayerInfo = AudioUtil.getAudioPlayerInfo(name);
			if (audioPlayerInfo == null) {
				throw new RuntimeException("无效的音频播放设备名称: " + name);
			}
		}
		this.status = AUDIO_PLAYER_STATUS_READY;
		logger.debug("初始化音频播放器完成");
	}
	
	public void load(String... audioFiles) {
		checkInit();
		AudioInputStream audioInputStream = null;
		try {
			audioInputStream = AudioUtil.synthesizeAudio(audioFiles);
			load(audioInputStream);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	public synchronized void load(AudioInputStream... audioInputStreams) throws LineUnavailableException, IOException {
		checkInit();
		terminate();
		try {
			clip = AudioSystem.getClip(audioPlayerInfo.getMixerInfo());
		} catch(IllegalArgumentException e) {
			if (StringUtils.isBlank(audioPlayerConfig.getName())) { //支持系统默认音响热插拔
				List<AudioPlayerInfo> infos = AudioUtil.getAudioPlayerInfos();
				if (!infos.isEmpty()) {
					 AudioPlayerInfo audioPlayerInfo = infos.get(0);
					 clip = AudioSystem.getClip(audioPlayerInfo.getMixerInfo());
					 this.audioPlayerInfo = audioPlayerInfo;
				} else {
					throw e;
				}
			} else {
				throw e;
			}
		} 
		clip.addLineListener(new LineListener() {
			@Override
			public void update(LineEvent event) {
				Type type = event.getType();
				if (type == LineEvent.Type.STOP) {
		        	if(clip.getFramePosition() == clip.getFrameLength()) {
		        		if(audioPlayerConfig.getAutoClose()) {
		        			terminate();
		        		} else {
		        			clip.setMicrosecondPosition(0L);
		        			status = AUDIO_PLAYER_STATUS_LOADED;
		        		}
		        	}
		        	
		        }
			}
		});
		AudioInputStream audioInputStream = AudioUtil.synthesizeAudio(audioInputStreams);
		clip.open(audioInputStream);
		status = AUDIO_PLAYER_STATUS_LOADED;
	}
	
	public void play(AudioInputStream... audioInputStreams) throws LineUnavailableException, IOException {
		load(audioInputStreams);
		start();
	}
	
	/**
	 * 播放音频
	 * @param audioFiles 音频文件名字符串，支持classpath:前缀，在类路径上检索播放文件，classpath:assets/1.wav
	 */
	public void play(String... audioFiles) {
		load(audioFiles);
		start();
	}
	
	/**
	 * 播放器状态
	 * 默认-1
	 * -1: 未初始化
	 * 0: ready准备完毕
	 * 1: loaded装载完毕/播放完毕
	 * 2: playing播放中
	 * 3: pasue暂停
	 * @return 状态
	 */
	public int getStatus() {
		return this.status;
	}
	
	public synchronized void start() {
		checkLoaded();
		if (status != AUDIO_PLAYER_STATUS_PLAYING) {
			clip.start();
			status = AUDIO_PLAYER_STATUS_PLAYING;
		}
	}

	/**
	 * 暂停
	 */
	public synchronized void pause() {
		checkLoaded();
		if (status == AUDIO_PLAYER_STATUS_PLAYING) {
			clip.stop();
			clip.flush();
			status = AUDIO_PLAYER_STATUS_PAUSE;
		}
	}
	
	/**
	 * 继续，从暂停位置继续播放
	 */
	public synchronized void resume() {
		checkLoaded();
		if (status == AUDIO_PLAYER_STATUS_PAUSE) {
			clip.start();
			status = AUDIO_PLAYER_STATUS_PLAYING;
		}
	}
	
	/**
	 * 停止播放并返回音频开头
	 */
	public void stop() {
		checkLoaded();
		clip.stop();
		clip.setMicrosecondPosition(0L);
		status = AUDIO_PLAYER_STATUS_LOADED;
	}
	
	/**
	 * 终止音频播放，并关闭当前clip释放资源
	 */
	public synchronized void terminate() {
		checkInit();
		if(clip != null) {
			clip.stop();
			clip.close();
			clip = null;
			if(!audioPlayerConfig.getAutoClose()) {
				try {
					Thread.sleep(audioPlayerConfig.getReleaseTime()); //等待操作系统完全释放音频资源
				} catch (InterruptedException e) {
					logger.error(e.getMessage(), e);
				}
			}
			status = AUDIO_PLAYER_STATUS_READY;
		}
	}
	
	/**
	 * 设置相对于0的播放位置
	 * 单位毫秒
	 * @param millisecondPosition 播放位置
	 */
	public void setMillisecondPosition(long millisecondPosition) {
		setMicrosecondPosition(millisecondPosition * 1000);
	}
	/**
	 * 设置相对于0的播放位置
	 * 单位微秒
	 * @param microsecondPosition 播放位置
	 */
	public synchronized void setMicrosecondPosition(long microsecondPosition) {
		checkLoaded();
		clip.setMicrosecondPosition(microsecondPosition);
	}
	
	public long getMillisecondPosition() {
		checkLoaded();
		return clip.getMicrosecondPosition() / 1000;
	}
	
	public long getMicrosecondPosition() {
		checkLoaded();
		return clip.getMicrosecondPosition();
	}
	
	public long getMicrosecondLength() {
		checkLoaded();
		return clip.getMicrosecondLength();
	}
	
	public long getMillisecondLength() {
		checkLoaded();
		return clip.getMicrosecondLength() / 1000;
	}
	
	public int getFrameLength() {
		checkLoaded();
		return clip.getFrameLength();
	}
	
	public Integer getFramePosition() {
		checkLoaded();
		return clip.getFramePosition();
	}
	
	public void setFramePosition(int framePostion) {
		checkLoaded();
		clip.setFramePosition(framePostion);
	}
	
	/**
	 * 获取音频格式
	 * @return 音频格式，如果音频已经播放完毕则返回null
	 */
	public AudioFormat getFormat() {
		checkLoaded();
		return clip.getFormat();
	}
	
	private void checkInit() {
		if (status == AUDIO_PLAYER_STATUS_UNINIT) {
			throw new RuntimeException("音频播放器没有初始化");
		}
	}
	
	private void checkLoaded() {
		checkInit();
		if (status == AUDIO_PLAYER_STATUS_READY) {
			throw new RuntimeException("没有装载音频");
		}
	}
	
	@PreDestroy
	protected void destroy() {
		terminate();
		status = AUDIO_PLAYER_STATUS_UNINIT;
	}
	
}
