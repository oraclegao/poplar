package fun.pplm.framework.poplar.websocket.component;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Indexed;

/**
 * 
 * websocket 服务
 * @author OracleGao
 *
 */
@Indexed
@ServerEndpoint(value = "/pws/{id}")
public class WebsocketServer {
	private Logger logger = LoggerFactory.getLogger(WebsocketServer.class);
	
	private Session session;
	private String id;
	
	public WebsocketServer() {
		super();
		logger.debug("create websocket server");
	}

	@OnOpen
    public void onOpen(Session session, @PathParam("id") String id) {
        this.session = session;
        this.id = id;
        logger.debug("创建新的websocket连接, id: {}", id);
    }

    @OnClose
    public void onClose() {
    	logger.debug("关闭websocket连接, id: {}", id);
    }
	
    @OnMessage
    public void onMessage(String message) {
    	logger.debug("websocket receive message, id: {}, message: {}", id, message);
    }
    
}
