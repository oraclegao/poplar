package fun.pplm.framework.poplar.websocket.config;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
//import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * websocket 配置
 * @author OracleGao
 *
 */
@Configuration
//@ConfigurationProperties(prefix = "poplar.websocket")
@ComponentScan("fun.pplm.framework.poplar.websocket")
public class WebsocketConfig {
	private Logger logger = LoggerFactory.getLogger(WebsocketConfig.class);
//	private String uriPrefix = "pws";
	
    @Bean
    @ConditionalOnMissingBean(ServerEndpointExporter.class)
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }

    @PostConstruct
    private void init() {
    	logger.debug("初始化websocket");
    }
    
//	public String getUriPrefix() {
//		return uriPrefix;
//	}
//
//	public void setUriPrefix(String uriPrefix) {
//		this.uriPrefix = uriPrefix;
//	}
    
	@Override
	public String toString() {
		Map<String, Object> map = new LinkedHashMap<>();
//		map.put("uriPrefix", uriPrefix);
		return Json.string(map);
	}
	
}
