package fun.pplm.framework.poplar.scanner.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import fun.pplm.framework.poplar.common.serialport.service.ReceiveOnlySerialPortService;
import fun.pplm.framework.poplar.scanner.config.ScannerConfig;

/**
 * 
 * 扫码枪服务
 * @author OracleGao
 *
 */
@Service
public class ScannerService extends ReceiveOnlySerialPortService  {
	private static Logger logger = LoggerFactory.getLogger(ScannerService.class);
	
	@Autowired
	private ScannerConfig scannerConfig;
	
	public ScannerService() {
		super("扫码枪服务");
	}

	@EventListener(ApplicationReadyEvent.class)
	protected void init() {
		if (scannerConfig.getAutoInit()) {
			logger.info("{}初始化...", name);
			super.init(scannerConfig.getSerialPort());
			logger.info("{}初始化完成", name);
		}
	}

}
