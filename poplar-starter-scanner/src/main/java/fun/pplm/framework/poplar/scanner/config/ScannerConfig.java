package fun.pplm.framework.poplar.scanner.config;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.common.serialport.config.SerialPortConfig;
import fun.pplm.framework.poplar.json.Json;

/**
 * 扫码枪配置
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.scanner")
public class ScannerConfig {
	/**
	 * 自动初始化服务
	 */
	private Boolean autoInit = true;
	/**
	 * 重复扫码时间间隔，单位秒，默认5
	 * <=0: 不限制重复扫码时间间隔
	 */
	private Integer repeatInterval = 5;
	
	private SerialPortConfig serialPort = new SerialPortConfig();

	public ScannerConfig() {
		super();
	}

	public Boolean getAutoInit() {
		return autoInit;
	}

	public void setAutoInit(Boolean autoInit) {
		this.autoInit = autoInit;
	}

	public Integer getRepeatInterval() {
		return repeatInterval;
	}

	public void setRepeatInterval(Integer repeatInterval) {
		this.repeatInterval = repeatInterval;
	}

	public SerialPortConfig getSerialPort() {
		return serialPort;
	}

	public void setSerialPort(SerialPortConfig serialPort) {
		this.serialPort = serialPort;
	}
	
	@Override
	public String toString() {
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("autoInit", autoInit);
		map.put("repeatInterval", repeatInterval);
		map.put("serialPort", serialPort);
		return Json.string(map);
	}
	
}
