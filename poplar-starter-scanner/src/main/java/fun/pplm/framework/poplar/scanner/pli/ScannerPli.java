package fun.pplm.framework.poplar.scanner.pli;

/**
 * 
 * Poplar Listener Interface
 * 扫码枪数据侦听接口
 * @author OracleGao
 *
 */
public interface ScannerPli {
	/**
	 * 消息到达
	 * @param message 扫码枪接收到的消息
	 */
	public void messageArrived(String message);
	
}
