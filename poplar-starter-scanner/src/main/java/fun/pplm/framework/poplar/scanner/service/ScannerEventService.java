package fun.pplm.framework.poplar.scanner.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.fazecast.jSerialComm.SerialPortEvent;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import fun.pplm.framework.poplar.common.serialport.pli.SerialPortEventPli;
import fun.pplm.framework.poplar.scanner.config.ScannerConfig;
import fun.pplm.framework.poplar.scanner.pli.ScannerPli;

/**
 * 
 * 扫码枪串口服务事件服务
 * @author OracleGao
 *
 */
@Service
public class ScannerEventService implements SerialPortEventPli {
	private static Logger logger = LoggerFactory.getLogger(ScannerEventService.class);
	
	@Autowired
	private ScannerConfig scannerConfig;
	
	@Autowired(required = false)
	private List<ScannerPli> plis = new ArrayList<>();
	
	/**
	 * message 缓存，解决不允许连续扫同一二维码情况
	 */
	private Cache<String, String> cache;
	
	private boolean repeatChecked = false;
	
	@EventListener(ApplicationStartedEvent.class)
	private void initCache() {
		Integer repeatInterval = scannerConfig.getRepeatInterval();
		if (repeatInterval > 0) {
			cache = Caffeine.newBuilder().expireAfterWrite(scannerConfig.getRepeatInterval(), TimeUnit.SECONDS).maximumSize(128).build();
			repeatChecked = true;
		}
	}
	
	@Override
	public void eventArrived(SerialPortEvent event) {
		String message = new String(event.getReceivedData());
		logger.debug("扫码枪服务接收到消息, message: {}", message);
		if (repeatChecked) {
			if(cache.getIfPresent(message) != null) {
				logger.warn("重复扫码需要间隔{}秒以上，跳过, message: {}", scannerConfig.getRepeatInterval(), message);
				return;
			}
			cache.put(message, message);
		}
		plis.forEach(sl->sl.messageArrived(message));
	}
	
	@Override
	public List<String> serialPortNames() {
		List<String> names = new ArrayList<>();
		names.add(scannerConfig.getSerialPort().getName());
		return names;
	}

}
