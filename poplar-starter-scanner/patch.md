## 1.11.22
- 扫码枪服务初始化推迟到spring服务启动之后

## 1.8.16
- 侦听接口类后缀更名为Pli: ScannerListener -> ScannerPli

## 1.6.11
- 串口框架调整回调函数名称

## 1.5.7
- 新增串口扫码枪starter