package fun.pplm.framework.poplar.modbus.slave.tcp.config;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.modbus.config.ModbusTcpConfig;

/**
 * 
 * modbus slave tcp 从站配置
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.modbus.slave.tcp")
public class ModbusSlaveTcpConfig extends ModbusTcpConfig {
	/**
	 * Pool size of listener threads
	 * 默认2
	 */
	private Integer poolSize = 2;

	public ModbusSlaveTcpConfig() {
		super();
	}
	
	public Integer getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(Integer poolSize) {
		this.poolSize = poolSize;
	}

	protected Map<String, Object> memberMap(){
		Map<String, Object> map = super.memberMap();
		map.put("poolSize", poolSize);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
	
}
