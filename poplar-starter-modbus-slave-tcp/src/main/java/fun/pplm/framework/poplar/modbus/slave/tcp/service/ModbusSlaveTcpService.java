package fun.pplm.framework.poplar.modbus.slave.tcp.service;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PreDestroy;
import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.ghgande.j2mod.modbus.ModbusException;
import com.ghgande.j2mod.modbus.procimg.InputRegister;
import com.ghgande.j2mod.modbus.procimg.ProcessImage;
import com.ghgande.j2mod.modbus.procimg.Register;
import com.ghgande.j2mod.modbus.procimg.SimpleInputRegister;
import com.ghgande.j2mod.modbus.procimg.SimpleProcessImage;
import com.ghgande.j2mod.modbus.procimg.SimpleRegister;
import com.ghgande.j2mod.modbus.slave.ModbusSlave;
import com.ghgande.j2mod.modbus.slave.ModbusSlaveFactory;

import fun.pplm.framework.poplar.common.validator.BeanValidator;
import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.modbus.bean.model.ModbusRegisterModel;
import fun.pplm.framework.poplar.modbus.bean.po.WriteRegisterPo;
import fun.pplm.framework.poplar.modbus.event.ModbusTcpOnReadyEvent;
import fun.pplm.framework.poplar.modbus.psa.ModbusTcpPsa;
import fun.pplm.framework.poplar.modbus.psi.ModbusRegisterDataPsi;
import fun.pplm.framework.poplar.modbus.psi.ModbusRegisterPsi;
import fun.pplm.framework.poplar.modbus.slave.tcp.config.ModbusSlaveTcpConfig;

import static fun.pplm.framework.poplar.modbus.utils.ModbusConstant.REG_TYPE_IR;

/**
 * 
 * modbus slave tcp 从站服务
 * @author OracleGao
 *
 */
@Service
public class ModbusSlaveTcpService extends ModbusTcpPsa {
	private static Logger logger = LoggerFactory.getLogger(ModbusSlaveTcpService.class);
	
	@Autowired
	private ModbusSlaveTcpConfig modbusSlaveTcpConfig;
	
	@Autowired
	private BeanValidator beanValidator;

	@Autowired
	protected ModbusRegisterPsi modbusRegisterService;
	
	@Autowired(required = false)
	protected ModbusRegisterDataPsi modbusRegisterDataService;
	
	private ModbusSlave modbusSlave;
	
	@EventListener(ApplicationStartedEvent.class)
	protected void init(ApplicationStartedEvent event) {
		logger.debug("初始化modbus从站tcp服务开始...");
		logger.debug("modbus从站tcp服务配置: {}", modbusSlaveTcpConfig);
		super.init(modbusSlaveTcpConfig, beanValidator, modbusRegisterService, modbusRegisterDataService);
		event.getApplicationContext().publishEvent(new ModbusTcpOnReadyEvent());
		logger.debug("初始化modbus从站tcp服务完成");
	}
	
	@Override
	protected void initModbus() {
		try {
			modbusSlave = ModbusSlaveFactory.createTCPSlave(InetAddress.getByName(modbusSlaveTcpConfig.getHost()), modbusSlaveTcpConfig.getPort(), modbusSlaveTcpConfig.getPoolSize(), modbusSlaveTcpConfig.getUseRtuOverTcp());
			SimpleProcessImage processImage = new SimpleProcessImage();
			initRegisters(processImage);
			modbusSlave.addProcessImage(modbusSlaveTcpConfig.getSlaveId(), processImage);
			modbusSlave.open();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}
	
	private void initRegisters(SimpleProcessImage processImage) {
		int startAddress, wordCount;
		List<ModbusRegisterModel> models = getHoldingRegisterModels();
		for (ModbusRegisterModel model : models) {
			startAddress = model.getStartAddress();
			wordCount = model.getWordCount();
			for (int i = startAddress; i < startAddress + wordCount; i++) {
				processImage.addRegister(i, new SimpleRegister(0));
			}
		}
		models = getInputRegisterModels();
		for (ModbusRegisterModel model : models) {
			startAddress = model.getStartAddress();
			wordCount = model.getWordCount();
			for (int i = startAddress; i < startAddress + wordCount; i++) {
				processImage.addInputRegister(i, new SimpleInputRegister(0));
			}
		}
	}
	
	/**
	 * 写输入持寄存器
	 * @param regCode 寄存器编码
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeInputRegister(String regCode, int data) throws ModbusException {
		writeInputRegister(null, regCode, data);
	}
	
	/**
	 * 写输入持寄存器
	 * @param slaveId 从站id
	 * @param regCode 寄存器编码
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeInputRegister(Integer slaveId, String regCode, int data) throws ModbusException {
		List<Integer> values = new ArrayList<>();
		values.add(data);
		writeInputRegisters(slaveId, regCode, values);
	}
	
	/**
	 * 写多输入持寄存器
	 * @param regCode 寄存器编码
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeInputRegisters(String regCode, int[] data) throws ModbusException {
		writeInputRegisters(null, regCode, data);
	}
	
	/**
	 * 写多输入持寄存器
	 * @param slaveId 从站id
	 * @param regCode 寄存器编码
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeInputRegisters(Integer slaveId, String regCode, int[] data) throws ModbusException {
		if (data == null) {
			throw new ValidationException("data is null");
		}
		;
		List<Integer> values = new ArrayList<>();
		for (Integer value : data) {
			values.add(value);
		}
		writeInputRegisters(slaveId, regCode, values);
	}
	
	/**
	 * 写多输入持寄存器
	 * @param regCode 寄存器编码
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeInputRegisters(String regCode, Integer[] data) throws ModbusException {
		writeInputRegisters(null, regCode, data);
	}
	
	/**
	 * 写多输入持寄存器
	 * @param slaveId 从站id
	 * @param regCode 寄存器编码
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeInputRegisters(Integer slaveId, String regCode, Integer[] data) throws ModbusException {
		writeInputRegisters(slaveId, regCode, Arrays.asList(data));
	}
	
	/**
	 * 写多输入持寄存器
	 * @param regCode 寄存器编码
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeInputRegisters(String regCode, List<Integer> data) throws ModbusException {
		writeInputRegisters(null, regCode, data);
	}
	
	/**
	 * 写多输入寄存器
	 * @param po 写寄存器参数
	 * @throws ModbusException modbus异常
	 */
	public void writeInputRegisters(WriteRegisterPo po) throws ModbusException {
		writeInputRegisters(po.getSlaveId(), po.getRegCode(), po.getWords());
	}
	
	/**
	 * 写多输入持寄存器
	 * @param slaveId 从站id
	 * @param regCode 寄存器编码
	 * @param data 写入数据
	 * @throws ModbusException modbus异常
	 */
	public void writeInputRegisters(Integer slaveId, String regCode, List<Integer> data) throws ModbusException {
		writeRegisters(slaveId, regCode, REG_TYPE_IR, data);
	}
	
	@Override
	protected Register[] readInputRegisters(int slaveId, int startAddress, int wordCount) throws ModbusException {
		ProcessImage processImage = modbusSlave.getProcessImage(slaveId);
		if (processImage == null) {
			throw new ModbusException("invalid slaveId: " + slaveId);
		}
		Register[] registers = new Register[wordCount];
		for(int i = 0; i < wordCount; i++) {
			registers[i] = (Register) processImage.getInputRegister(startAddress + i);
		}
		return registers;
	}

	@Override
	protected Register[] readHoldingRegisters(int slaveId, int startAddress, int wordCount) throws ModbusException {
		ProcessImage processImage = modbusSlave.getProcessImage(slaveId);
		if (processImage == null) {
			throw new ModbusException("invalid slaveId: " + slaveId);
		}
		Register[] registers = new Register[wordCount];
		for(int i = 0; i < wordCount; i++) {
			registers[i] = (Register) processImage.getRegister(startAddress + i);
		}
		return registers;
	}

	@Override
	protected void writeHoldingRegisters(int slaveId, int startAddress, List<Integer> data) throws ModbusException {
		ProcessImage processImage = modbusSlave.getProcessImage(slaveId);
		if (processImage == null) {
			throw new ModbusException("invalid slaveId: " + slaveId);
		}
		int size = data.size();
		Register[] registers = processImage.getRegisterRange(startAddress, size);
		for (int i = 0; i < size; i++) {
			registers[i].setValue(data.get(i));
		}
	}
	
	@Override
	public void writeInputRegisters(int slaveId, int startAddress, List<Integer> data) throws ModbusException {
		ProcessImage processImage = modbusSlave.getProcessImage(slaveId);
		if (processImage == null) {
			throw new ModbusException("invalid slaveId: " + slaveId);
		}
		int size = data.size();
		InputRegister[] registers = processImage.getInputRegisterRange(startAddress, size);
		for (int i = 0; i < size; i++) {
			((Register)registers[i]).setValue(data.get(i));
		}
	}
	
	@Override
	protected void initInputRegisterData(int slaveId, String regCode, List<Integer> data) {
		try {
			writeInputRegisters(slaveId, regCode, data);
		} catch (ModbusException e) {
			logger.warn("跳过异常的输入寄存器初始化数据, slaveId: {}, regCode: {}, data: {}, message: {}", slaveId, regCode, Json.string(data), e.getMessage());
		}
	}
	
	@PreDestroy
	protected void destroy() {
		if (modbusSlave != null) {
			modbusSlave.close();
		}
	}
	
}
