package fun.pplm.framework.poplar.modbus.slave.tcp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ghgande.j2mod.modbus.ModbusException;

import fun.pplm.framework.poplar.modbus.bean.po.WriteRegisterPo;
import fun.pplm.framework.poplar.modbus.controller.ModbusTcpController;
import fun.pplm.framework.poplar.modbus.slave.tcp.service.ModbusSlaveTcpService;
import fun.pplm.framework.poplar.web.resp.Resp;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * modbus服务api
 * @author OracleGao
 *
 */
@Api(value = "modbusSlaveTcpController", tags = "modbus api")
@ConditionalOnExpression("${poplar.modbus.slave.tcp.apiEnabled:true}")
@Validated
@RestController
@RequestMapping(path = "/modbus/slave", produces = MediaType.APPLICATION_JSON_VALUE)
public class ModbusSlaveTcpController extends ModbusTcpController {
	@Autowired
	private ModbusSlaveTcpService modbusSlaveTcpService;
	
	@ApiOperation(value = "输入寄存器(IR)数据写入", notes = "写入输入寄存器数据")
	@PostMapping(value = "/write/IRs")
	public Resp<Void> writeIRs(@Valid @RequestBody WriteRegisterPo po) throws ModbusException {
		modbusSlaveTcpService.writeInputRegisters(po);
		return Resp.success();
	}
	
}
