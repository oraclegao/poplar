# poplar-session-starter

会话管理starter

## 配置项
### auth
- poplar.session.auth.pathPatterns: auth token校验的路径模式，默认 /**
- poplar.session.auth.whitePaths: 路径校验白名单

``` yml
poplar.session.token.auth:
  pathPattern: 
    - /v1.0/**
    - /v1.1/**
  whitePaths:
    - /v1.0/auth/login
    - /v1.0/auth/logout
```

### token
- poplar.session.token.prefix: token前缀，默认空""
- poplar.session.token.headerKey: header中的token key，默认值token
- poplar.session.token.cookieKey: cookie中的token key，默认值token
- poplar.session.token.expireSecond: token（会话）超时时间，单位秒，默认1800