package fun.pplm.framework.poplar.session.token.interceptor;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.security.sasl.AuthenticationException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fun.pplm.framework.poplar.common.service.RedisService;
import fun.pplm.framework.poplar.common.session.config.SessionConfig;
import fun.pplm.framework.poplar.common.session.interceptor.SessionInterceptor;
import fun.pplm.framework.poplar.common.session.pii.SessionPii;


/**
 * 
 * session redis方式的拦截器
 * @author OracleGao
 *
 */
@Component
public class TokenInterceptor extends SessionInterceptor {
	private Logger logger = LoggerFactory.getLogger(TokenInterceptor.class);
	
	@Autowired
	private SessionConfig sessionConfig;
	@Autowired
    private RedisService redisService;

    @PostConstruct
    private void init() {
    	logger.debug(this.getClass().getSimpleName() + "注入成功");
    }
    
	@Override
	protected SessionPii validate(HttpServletRequest request, String token, boolean refreshExpire,
			Class<? extends SessionPii> sessionClass) throws AuthenticationException {
		if (StringUtils.isNoneBlank(token) && redisService.exists(token)) {
			try {
				if (refreshExpire) {
					redisService.expire(token, sessionConfig.getExpireSecond());
				}
				SessionPii session = redisService.get(token, sessionClass);
				if (session == null) {
					throw new AuthenticationException("会话信息为空，请重新登录！");
				}
				return session;
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
				throw new AuthenticationException("会话信息异常，请重新登录！");
			}
		}
		throw new AuthenticationException("请登录后再试！");
	}
    
}
