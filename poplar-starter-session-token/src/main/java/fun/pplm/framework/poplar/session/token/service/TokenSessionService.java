package fun.pplm.framework.poplar.session.token.service;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import fun.pplm.framework.poplar.common.service.RedisService;
import fun.pplm.framework.poplar.common.session.bean.Auth;
import fun.pplm.framework.poplar.common.session.config.SessionConfig;
import fun.pplm.framework.poplar.common.session.pii.SessionPii;
import fun.pplm.framework.poplar.common.session.service.SessionHttpService;
import fun.pplm.framework.poplar.common.session.service.SessionService;
import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * @author OracleGao
 *
 */
@Service
@ConfigurationProperties(prefix = "poplar.session.token")
public class TokenSessionService implements SessionService {
	private static final Logger logger = LoggerFactory.getLogger(TokenSessionService.class);

	@Autowired
	private SessionConfig sessionConfig;
	@Autowired
	private RedisService redisService;
	@Autowired
	private SessionHttpService sessionHttpService;
	
	/**
	 * redis key prefix
	 * 默认"pst-"
	 */
	private String redisKeyPrefix = "pst-";
	
	@PostConstruct
	private void init () {
		logger.debug(this.getClass().getSimpleName() + "注入成功");
	}
	
	/**
	 * 
	 * redis存储会话信息，并生成token字符串
	 * @param session 会话对象
	 * @return token字符串
	 * 
	 */
	public Auth createSession(SessionPii session) {
		return createSession(session, false);
	}
	
	public Auth createSession(SessionPii session, boolean setCookieFlag) {
		String token = generateToken();
		long expireSecond = sessionConfig.getExpireSecond();
		Long expireAt = System.currentTimeMillis() + expireSecond * 1000;
		redisService.setExpire(token, session, expireSecond);
		if(setCookieFlag) {
			sessionHttpService.setCookieToken(token, "/", (int)expireSecond);
		}
		Auth auth = new Auth();
		auth.setExpireAt(expireAt);
		auth.setToken(token);
		return auth;
	}

	@Override
	public void updateSession(SessionPii session) {
		String token = sessionHttpService.getToken();
		updateSession(token, session);
	}
	
	@Override
	public void updateSession(String token, SessionPii session) {
		validToken(token);
		if (redisService.exists(token)) {
			redisService.setExpire(token, session, sessionConfig.getExpireSecond());
		} else {
			throw new RuntimeException("token not exists");
		}
	}
	
	@Override
	public void cleanSession() {
		String token = sessionHttpService.getToken();
		if (StringUtils.isNotBlank(token)) {
			redisService.expire(token, 1);
		}
	}
	
	@Override
	public void cleanSession(String token) {
		validToken(token);
		redisService.expire(token, 1);
	}
	
	private void validToken(String token) {
		if (StringUtils.isNotBlank(redisKeyPrefix)) {
			if (token == null || !token.startsWith(redisKeyPrefix)) {
				throw new RuntimeException("invalid token: " + token);
			}
		}
	}
	
	private String generateToken() {
		String token = DigestUtils.md5Hex(UUID.randomUUID().toString());
		if (StringUtils.isNoneBlank(redisKeyPrefix)) {
			return redisKeyPrefix + token;
		}
		return token;
		
	}
	
	protected Map<String, Object> memberMap() {
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("redisKeyPrefix", redisKeyPrefix);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}

}
