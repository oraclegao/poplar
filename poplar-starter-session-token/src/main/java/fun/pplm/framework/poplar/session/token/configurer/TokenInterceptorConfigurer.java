package fun.pplm.framework.poplar.session.token.configurer;

import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.common.session.configurer.SessionInterceptorConfigurer;

/**
 * 
 * token 拦截器配置
 * @author OracleGao
 *
 */
@Configuration
public class TokenInterceptorConfigurer extends SessionInterceptorConfigurer {

}
