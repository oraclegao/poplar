package fun.pplm.framework.poplar.code.aes.config;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * AES对称加密算法配置
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.code.aes")
public class AesConfig {
	/**
	 * key生成器算法
	 * 默认AES
	 */
	private String keyAlgorithm = "AES";
	/**
	 * key生成器key size
	 * 默认256
	 */
	private Integer keySize = 256;
	/**
	 * 加密key
	 */
	private String key = "poplar-aes";
	/**
	 * 加密密码
	 */
	private String password = "Pa!20230705213558";
	/**
	 * 字符集
	 * 默认utf-8
	 */
	private String charset = "utf-8";
	
	/**
	 * 随机数生成算法
	 * 默认SHA1PRNG
	 */
	private String rngAlgorithm = "SHA1PRNG";
	/**
	 * 加密算法
	 * 默认AES/ECB/PKCS5Padding
	 */
	private String cipherAlgorithm = "AES/ECB/PKCS5Padding";

	public AesConfig() {
		super();
	}

	public String getRngAlgorithm() {
		return rngAlgorithm;
	}

	public void setRngAlgorithm(String rngAlgorithm) {
		this.rngAlgorithm = rngAlgorithm;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Integer getKeySize() {
		return keySize;
	}

	public void setKeySize(Integer keySize) {
		this.keySize = keySize;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getKeyAlgorithm() {
		return keyAlgorithm;
	}

	public void setKeyAlgorithm(String keyAlgorithm) {
		this.keyAlgorithm = keyAlgorithm;
	}

	public String getCipherAlgorithm() {
		return cipherAlgorithm;
	}

	public void setCipherAlgorithm(String cipherAlgorithm) {
		this.cipherAlgorithm = cipherAlgorithm;
	}
	
	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("keyAlgorithm", keyAlgorithm);
		map.put("keySize", keySize);
		map.put("key", key);
		map.put("password", password);
		map.put("charset", charset);
		map.put("rngAlgorithm", rngAlgorithm);
		map.put("cipherAlgorithm", cipherAlgorithm);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
	
}
