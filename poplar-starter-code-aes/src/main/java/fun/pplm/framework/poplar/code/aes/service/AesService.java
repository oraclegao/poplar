package fun.pplm.framework.poplar.code.aes.service;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import fun.pplm.framework.poplar.code.aes.config.AesConfig;

/**
 * 
 * aes对称加解密服务
 * 
 * @author OracleGao
 *
 */
@Service
public class AesService {
	@Autowired
	private AesConfig aesConfig;
	@Autowired
	private ObjectMapper objectMapper;

	/**
	 * 创建密码器
	 * 
	 * @param cipherType 密码器类型
	 * @return 密码器
	 * @throws Exception 异常
	 */
	private Cipher createCipher(int cipherType) throws Exception {
		KeyGenerator keyGenerator = KeyGenerator.getInstance(aesConfig.getKeyAlgorithm());
		SecureRandom secureRandom = SecureRandom.getInstance(aesConfig.getRngAlgorithm());
		secureRandom.setSeed(aesConfig.getPassword().getBytes(aesConfig.getCharset()));
		keyGenerator.init(aesConfig.getKeySize(), secureRandom);
		SecretKey secretKey = keyGenerator.generateKey();
		byte[] keyEncodeFormat = secretKey.getEncoded();
		SecretKeySpec key = new SecretKeySpec(keyEncodeFormat, aesConfig.getKeyAlgorithm());
		Cipher cipher = Cipher.getInstance(aesConfig.getCipherAlgorithm());
		cipher.init(cipherType, key);
		return cipher;
	}

	/**
	 * 加密
	 * 
	 * @param content 明文内容
	 * @return 密文
	 * @throws Exception 异常
	 */
	public String encrypt(String content) throws Exception {
		Cipher cipher = createCipher(Cipher.ENCRYPT_MODE);
		return Base64.encodeBase64URLSafeString(cipher.doFinal(content.getBytes(aesConfig.getCharset())));
	}

	/**
	 * 对象json序列化后加密
	 * @param obj 对象 
	 * @return 密文
	 * @throws Exception 异常
	 */
	public String jsonEncrypt(Object obj) throws Exception {
		if (obj instanceof String) {
			return encrypt(obj.toString());
		}
		return encrypt(objectMapper.writeValueAsString(obj));
	}

	/**
	 * 解密
	 * @param secret 密文
	 * @return 明文
	 * @throws Exception 异常
	 */
	public String decrypt(String secret) throws Exception {
		Cipher cipher = createCipher(Cipher.DECRYPT_MODE);
		return new String(cipher.doFinal(Base64.decodeBase64(secret)), aesConfig.getCharset());
	}

	/**
	 * 解密后反序列化成json对象
	 * @param <T> json对象类型
	 * @param secret 密文
	 * @param valueTypeRef 类型
	 * @return 对象
	 * @throws Exception 异常
	 */
	public <T> T jsonDecrypt(String secret, TypeReference<T> valueTypeRef) throws Exception {
		String content = decrypt(secret);
		return objectMapper.readValue(content, valueTypeRef);
	}

	/**
	 * 解密后反序列化成json对象
	 * @param <T> json对象类型
	 * @param secret 密文
	 * @param valueType 类型
	 * @return 对象
	 * @throws Exception 异常
	 */
	@SuppressWarnings("unchecked")
	public <T> T jsonDecrypt(String secret, Class<T> valueType) throws Exception {
		String content = decrypt(secret);
		if (valueType.equals(String.class)) {
			return (T) content;
		}
		return objectMapper.readValue(content, valueType);
	}

}
