# Poplar

- 基于sprinboot和springcloud的java服务框架
- 基于依赖+配置=服务的理念，提供封装好的各类starter实现一站式快速服务集成
- 基于约定大于配置理念，框架默认使用json数据格式，统一采用Bean和JSON互相转换的方式管理数据结构

## 使用方式
1. 引入顶级父类
``` pom.xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>fun.pplm.framework.poplar</groupId>
		<artifactId>poplar</artifactId>
		<version>1.3.5</version>
	</parent>
......
</project>
```
2. 按需集成具体服务【按需引入】
``` pom.xml
		<dependencies>
			......
			<!-- 【按需引入】 -->
			<dependency>
				<groupId>fun.pplm.framework.poplar</groupId>
				<artifactId>poplar-common</artifactId>
			</dependency>
			<dependency>
				<groupId>fun.pplm.framework.poplar</groupId>
				<artifactId>poplar-common-mybatis</artifactId>
			</dependency>
			<dependency>
				<groupId>fun.pplm.framework.poplar</groupId>
				<artifactId>poplar-common-session</artifactId>
			</dependency>
			<dependency>
				<groupId>fun.pplm.framework.poplar</groupId>
				<artifactId>poplar-starter-json</artifactId>
			</dependency>
			<dependency>
				<groupId>fun.pplm.framework.poplar</groupId>
				<artifactId>poplar-starter-swagger</artifactId>
			</dependency>
			<dependency>
				<groupId>fun.pplm.framework.poplar</groupId>
				<artifactId>poplar-starter-elasticsearch</artifactId>
			</dependency>
			<dependency>
				<groupId>fun.pplm.framework.poplar</groupId>
				<artifactId>poplar-starter-modbus-tcp</artifactId>
			</dependency>
			<dependency>
				<groupId>fun.pplm.framework.poplar</groupId>
				<artifactId>poplar-starter-mqtt</artifactId>
			</dependency>
			<dependency>
				<groupId>fun.pplm.framework.poplar</groupId>
				<artifactId>poplar-starter-restful</artifactId>
			</dependency>
			<dependency>
				<groupId>fun.pplm.framework.poplar</groupId>
				<artifactId>poplar-starter-session-jwt</artifactId>
			</dependency>
			<dependency>
				<groupId>fun.pplm.framework.poplar</groupId>
				<artifactId>poplar-starter-session-token</artifactId>
			</dependency>
			<dependency>
				<groupId>fun.pplm.framework.poplar</groupId>
				<artifactId>poplar-starter-web</artifactId>
			</dependency>
			......
		</dependencies>
```
3. 在Application.yml中配置相应的配置项【举例】
``` yml
poplar:
  swagger:
    basePackage: fun.pplm
    version: 1.0.0
    title: 接口文档
    desc: 接口文档详细描述
  mqtt:
    uri: tcp://127.0.0.1:1883
    clientId: example
    subQos: 2
    pubQos: 2
    pubDuplicate: true
    topics:
      - foo/foo1
      - bar/bar1
  modbus.tcp:
    enabled: false
```

## 框架使用例子
- [Poplar Example](https://gitee.com/oraclegao/poplar-example)

### 版本说明
## 1.3.6 plan
- 修复公共依赖commons-beanutils版本信息错误【fixed】

### poplar-common
- 增加线程框架和消费队列相关类【fixed】

### poplar-starter-json
- 优化Json.string()方法【fixed】
- Json类添加泛型工具方法【fixed】

### poplar-starter-restful
- 增加http请求原始response body的debug级别日志输出【fixed】

## 1.3.5
- 添加公共依赖管理apache commons-beanutils-1.9.4

### poplar-starter-modbus-tcp
- 增加初始化连接重试机制
- 增加寄存器动态地址管理
- 增加保持寄存器写入数据int类型

## 1.3.4
### poplar-starter-modbus-tcp
- 完善方法参数校验
- 增加初始化后的写入方法和读取回调方法
- 增加参数对象的写入和读取方法
- 增加ModbusTcpService生命周期侦听接口
- 增加发送onReady的springboot事件

## 1.3.3
### poplar-starter-modbus-tcp
- 重构ModbusTcpService类，规范方法命名，支持读取有符号数

## 1.3.2
### poplar-common
- 修复分页计算总页数bug

## 1.3.1
### poplar-starter-modbus-tcp
- 添加poplar-starter-modbus-tcp 支持modbus tcp服务

## 1.0.0
- java 版本 11
- spring boot 版本 2.5.14
- spring cloud 版本 2020.0.6
- alibaba cloud 版本 2021.0.1.0
- 其余组件版本详见pom.xml
- poplar-common 版本 1.0.0
- poplar-mybatis-common 版本1.0.0
- poplar-json-common 版本1.0.0
- poplar-swagger-starter 版本1.0.0
- poplar-session-token-starter 版本1.0.0
- poplar-elasticsearch-starter 版本1.0.0
- poplar-mqtt-starter 版本1.0.0

# Refs
- [发布maven中央厂库](https://s01.oss.sonatype.org/#welcome)