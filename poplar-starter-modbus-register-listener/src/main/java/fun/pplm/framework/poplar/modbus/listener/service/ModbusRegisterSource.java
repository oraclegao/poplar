package fun.pplm.framework.poplar.modbus.listener.service;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ghgande.j2mod.modbus.ModbusException;

import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.modbus.listener.bean.model.ModbusRegisterListenerModel;
import fun.pplm.framework.poplar.modbus.listener.pli.ModbusRegisterPli;
import fun.pplm.framework.poplar.modbus.psa.ModbusTcpPsa;

/**
 * 
 * modbus寄存器侦听源
 * @author OracleGao
 *
 */
public class ModbusRegisterSource implements Runnable {
	private static Logger logger = LoggerFactory.getLogger(ModbusRegisterSource.class);
	
	private Collection<ModbusRegisterListenerModel> models;
	
	private ModbusRegisterPli modbusRegisterListener;
	
	private ModbusTcpPsa modbusTcpService;
	
	private long nextListenTime = 0;
		
	public ModbusRegisterSource() {
		super();
	}

	public ModbusRegisterSource(ModbusRegisterPli modbusRegisterListener, ModbusTcpPsa modbusTcpService) {
		super();
		this.modbusRegisterListener = modbusRegisterListener;
		this.modbusTcpService = modbusTcpService;
		this.models = modbusRegisterListener.models();
	}

	@Override
	public void run() {
		nextListenTime = System.currentTimeMillis() + modbusRegisterListener.interval();
		for (ModbusRegisterListenerModel model : models) {
			listen(model);
		}
	}
	
	private void listen(ModbusRegisterListenerModel model) {
		String regCode = model.getRegCode();
		String regType = model.getRegType();
		int[] preData = model.getRegData();
		int wordCount = preData.length;
		try {
			int[] data = modbusTcpService.readRegisters(model.getSlaveId(), regCode, regType);
			boolean changed = false;
			for (int i = 0; i < data.length; i++) {
				if (data[i] != preData[i]) {
					changed = true;
					break;
				}
			}
			if (changed) {
				int[] newData = modbusRegisterListener.onDataChanged(model, preData, data);
				if (newData == null) {
					if (!model.getKeepRegData()) {
						model.setRegData(data);
						logger.debug("设置新的监听数据, regCode: {}, regType: {}, regData: {}", regCode, regType, Json.string(data));
					}
				} else {
					if (newData.length != wordCount) {
						logger.warn("忽略不等于字长的新的监听数据, regCode: {}, regType: {}, dataLength: {}, wordCount: {}, newData: {}", regCode, regType, newData.length, wordCount, newData);
					} else {
						model.setRegData(newData);
						logger.debug("设置新的监听数据, regCode: {}, regType: {}, regData: {}", regCode, regType, Json.string(newData));
					}
				}
			}
		} catch (ModbusException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public long getNextListenTime() {
		return nextListenTime;
	}

	public Collection<ModbusRegisterListenerModel> getModels() {
		return models;
	}

}
