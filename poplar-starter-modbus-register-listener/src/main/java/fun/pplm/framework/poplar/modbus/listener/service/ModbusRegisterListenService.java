package fun.pplm.framework.poplar.modbus.listener.service;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ThreadPoolExecutor;

import javax.validation.ValidationException;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import fun.pplm.framework.poplar.common.runnable.LineWorker;
import fun.pplm.framework.poplar.common.utils.ThreadPoolUtil;
import fun.pplm.framework.poplar.common.utils.Uuid;
import fun.pplm.framework.poplar.common.validator.BeanValidator;
import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.modbus.bean.model.ModbusRegisterModel;
import fun.pplm.framework.poplar.modbus.event.ModbusTcpOnReadyEvent;
import fun.pplm.framework.poplar.modbus.listener.bean.model.ModbusRegisterListenerModel;
import fun.pplm.framework.poplar.modbus.listener.config.ModbusRegisterListenerConfig;
import fun.pplm.framework.poplar.modbus.listener.pli.ModbusRegisterPli;
import fun.pplm.framework.poplar.modbus.psa.ModbusTcpPsa;
import fun.pplm.framework.poplar.modbus.utils.ModbusUtil;

/**
 * 
 * modbus 寄存器侦听器服务
 * @author OracleGao
 *
 */
@Service
public class ModbusRegisterListenService extends LineWorker {
	private static Logger logger = LoggerFactory.getLogger(ModbusRegisterListenService.class);
	
	@Autowired
	private ModbusRegisterListenerConfig modbusRegisterListenerConfig;
	
	@Autowired
	private ModbusTcpPsa modbusTcpService;
	@Autowired
	private BeanValidator beanValidator;
	
	private ThreadPoolExecutor threadPoolExecutor;
	
	@Autowired(required = false)
	private volatile List<ModbusRegisterPli> listeners = new CopyOnWriteArrayList<>();
	
	private volatile List<ModbusRegisterSource> sources = new CopyOnWriteArrayList<>();
	
	@EventListener(ModbusTcpOnReadyEvent.class)
	protected void init() {
		logger.debug("初始化modbus寄存器侦听服务开始...");
		setInterval(modbusRegisterListenerConfig.getInterval());
		listeners.forEach(this::addSource);
		threadPoolExecutor = ThreadPoolUtil.newThreadPool(modbusRegisterListenerConfig.getListenThreadPool());
		super.startup();
		logger.debug("初始化modbus寄存器侦听服务完成");
	}
	
	@Override
	protected void execute() {
		final long now = System.currentTimeMillis();
		sources.stream().forEach(e -> {
			if (now >= e.getNextListenTime()) {
				threadPoolExecutor.submit(e);
			}
		});
	}
	
	public synchronized void addListener(ModbusRegisterPli listener) {
		listeners.add(listener);
		addSource(listener);
	}
	
	public synchronized void addListeners(Collection<ModbusRegisterPli> listeners) {
		listeners.forEach(this::addListener);
	}
	
	public synchronized void removeListener(ModbusRegisterPli listener) {
		int idx = listeners.indexOf(listener);
		if (idx != -1) {
			listeners.remove(idx);
			sources.remove(idx);
		}
	}
	
	private void addSource(ModbusRegisterPli listener) {
		Collection<ModbusRegisterListenerModel> models = listener.models();
		if (models.isEmpty()) {
			throw new ValidationException("侦听器models为空");
		}
		for (ModbusRegisterListenerModel model : models) {
			beanValidator.validate(model);
			fixModel(model);
		}
		sources.add(new ModbusRegisterSource(listener, modbusTcpService));
		logger.debug("注册侦听器, name: {}", listener.name());
	}
	
	private void fixModel(ModbusRegisterListenerModel model) {
		String regCode = model.getRegCode();
		String regType = model.getRegType();
		int[] regData = model.getRegData();
		int wordCount = 0;
		List<ModbusRegisterModel> registerModels = modbusTcpService.getRegisters(regCode, regType);
		int registerModelSize = registerModels.size();
		if (registerModelSize == 1) {
			ModbusRegisterModel registerModel = registerModels.get(0);
			wordCount = registerModel.getWordCount();
			if (regData == null) {
				regData = ModbusUtil.initWords(wordCount);
			} else {
				if (regData.length > wordCount) {
					int[] truncatedData = ArrayUtils.subarray(regData, wordCount, regData.length);
						regData = ArrayUtils.subarray(regData, 0, wordCount);
					logger.warn("寄存器数据长度大于字长，自动截取到字长, regCode: {}, regType: {}, wordCount: {}, regData: {}, truncatedData: {}", regCode, regType, wordCount, Json.string(regData), Json.string(truncatedData));
				} else if (regData.length < wordCount) {
					int[] data = ModbusUtil.initWords(wordCount); 
					for (int i = 0; i < regData.length; i++) {
						data[i] = regData[i];
					}
					regData = data;
					logger.warn("寄存器数据长度小于字长，使用0自动填充到字长, regCode: {}, regType: {}, wordCount: {}, regData: {}, truncatedData: {}", regCode, regType, wordCount, Json.string(regData));
				}
			}
		} else if (registerModelSize > 1) { //动态寄存器地址
			for (ModbusRegisterModel registerModel : registerModels) {
				wordCount += registerModel.getWordCount();
			}
			if (regData == null) {
				regData = ModbusUtil.initWords(wordCount);
			} else {
				if (regData.length != wordCount) {
					throw new ValidationException("动态地址寄存器数据长度不等于字长, regCode: " + regCode + ", regType: " + regType + ", wordCount: " + wordCount + ", regDataLength: " + regData.length);
				}
			}
		}
		model.setRegData(regData);
		if (StringUtils.isBlank(model.getId())) {
			model.setId(Uuid.uuid32());
		}
	}
	
	public synchronized void clearListeners() {
		listeners.clear();
		sources.clear();
	}

	public List<ModbusRegisterPli> getListeners() {
		return listeners;
	}
	
	@Override
	protected void destory() {
		ThreadPoolUtil.destoryThreadPool(threadPoolExecutor, modbusRegisterListenerConfig.getListenThreadPool());
		super.destory();
	}
	
}
