package fun.pplm.framework.poplar.modbus.listener.bean.model;

import javax.validation.constraints.NotBlank;

import fun.pplm.framework.poplar.common.validator.annotation.Item;

/**
 * 
 * modbus 寄存器数据侦听信息
 * @author OracleGao
 *
 */
public class ModbusRegisterListenerModel {
	/**
	 * 唯一标识
	 * 默认uuid
	 */
	private String id;
	/**
	 * 从站id
	 * 默认null使用全局配置
	 */
	private Integer slaveId;
	/**
	 * 侦听的寄存器编码
	 */
	@NotBlank
	private String regCode;
	/**
	 * 寄存器类型
	 * HR: 保持寄存器
	 * IR: 输入寄存器
	 */
	@NotBlank
	@Item(value = {"HR", "IR"}, caseSensitive = false)
	private String regType;
	
	/**
	 * 侦听数据
	 * 当寄存器数据与该数据不一致时，触发侦听回调
	 * 可以为空，当为空时默认使用0填充数据
	 */
	private int[] regData;
	
	/**
	 * 保持侦听数据
	 * 默认false
	 * false: 侦听数据发生变化时，在触发侦听回调后，使用新数据覆盖原侦听数据; true: 不覆盖原数据
	 */
	private Boolean keepRegData = false;

	public ModbusRegisterListenerModel() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getSlaveId() {
		return slaveId;
	}

	public void setSlaveId(Integer slaveId) {
		this.slaveId = slaveId;
	}

	public String getRegCode() {
		return regCode;
	}

	public void setRegCode(String regCode) {
		this.regCode = regCode;
	}

	public String getRegType() {
		return regType;
	}

	public void setRegType(String regType) {
		this.regType = regType;
	}

	public int[] getRegData() {
		return regData;
	}

	public void setRegData(int[] regData) {
		this.regData = regData;
	}

	public Boolean getKeepRegData() {
		return keepRegData;
	}

	public void setKeepRegData(Boolean keepRegData) {
		this.keepRegData = keepRegData;
	}
	
}
