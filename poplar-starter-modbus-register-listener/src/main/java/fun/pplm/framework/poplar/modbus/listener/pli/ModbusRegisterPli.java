package fun.pplm.framework.poplar.modbus.listener.pli;

import java.util.Collection;

import fun.pplm.framework.poplar.modbus.listener.bean.model.ModbusRegisterListenerModel;

/**
 * 
 * Poplar Listener Interface
 * modbus 寄存器侦听接口
 * @author OracleGao
 *
 */
public interface ModbusRegisterPli {
	/**
	 * 侦听器名称
	 * 默认取类名
	 * @return 侦听器名称
	 */
	public default String name() {
		return getClass().getCanonicalName();
	}
	
	/**
	 * 侦听时间间隔
	 * 单位毫秒
	 * 默认1000
	 * 侦听时间间隔非精确保证
	 * 当该侦听器被访问时，如果当前时间减去上次执行时间大于该值时，触发读取寄存器数据进行比较
	 * @return 时间间隔
	 */
	public default long interval() {
		return 1000L;
	}
	
	/**
	 * 侦听的寄存器model信息
	 * @return 侦听器models列表
	 */
	public Collection<ModbusRegisterListenerModel> models();
	
	/**
	 * 侦听到数据变化的回调
	 * @param model 数据变化的侦听器model
	 * @param preData 变化前的侦听数据
	 * @param data 寄存器当前数据
	 * @return 下次侦听数据，如果为空则按照keepRegData策略执行
	 */
	public int[] onDataChanged(ModbusRegisterListenerModel model, int[] preData, int[] data);
	
}
