package fun.pplm.framework.poplar.modbus.listener.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.common.config.ThreadPoolConfig;
/**
 * 
 * modbus 寄存器侦听器配置
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.modbus.register.listener")
public class ModbusRegisterListenerConfig {
	/**
	 * 每次扫描所有侦听器的时间间隔
	 * 单位毫秒
	 * 默认100
	 */
	private Long interval = 100L;
	/**
	 * 侦听服务线程池配置
	 */
	private ThreadPoolConfig listenThreadPool = new ThreadPoolConfig();

	public ModbusRegisterListenerConfig() {
		super();
	}

	public Long getInterval() {
		return interval;
	}

	public void setInterval(Long interval) {
		this.interval = interval;
	}

	public ThreadPoolConfig getListenThreadPool() {
		return listenThreadPool;
	}

	public void setListenThreadPool(ThreadPoolConfig listenThreadPool) {
		this.listenThreadPool = listenThreadPool;
	}
	
}
