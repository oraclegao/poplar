package fun.pplm.framework.poplar.json;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.ParameterizedType;
import java.net.URL;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;

import fun.pplm.framework.poplar.json.exception.JsonException;

import static fun.pplm.framework.poplar.json.config.ObjectMapperConfig.OBJECT_MAPPER;

/**
 * 
 * @author OracleGao
 *
 */
public class Json {
	/**
	 * 对象序列化
	 * String类型对象直接返回
	 * null对象返回null
	 * @param object 序列化对象
	 * @return 序列化的字符串
	 */
	public static String string(Object object) {
		if (object == null) {
			return null;
		}
		if(object instanceof CharSequence) {
			return object.toString();
		}
		try {
			return OBJECT_MAPPER.writeValueAsString(object);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(String content, TypeReference<T> valueTypeRef) {
		try {
			return OBJECT_MAPPER.readValue(content, valueTypeRef);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(String content, JavaType valueType) {
		try {
			return OBJECT_MAPPER.readValue(content, valueType);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(String content, Class<T> valueType) {
		try {
			return OBJECT_MAPPER.readValue(content, valueType);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(File src, Class<T> valueType) {
		try {
			return OBJECT_MAPPER.readValue(src, valueType);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(File src, TypeReference<T> valueTypeRef) {
		try {
			return OBJECT_MAPPER.readValue(src, valueTypeRef);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(File src, JavaType valueType) {
		try {
			return OBJECT_MAPPER.readValue(src, valueType);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(InputStream src, Class<T> valueType) {
		try {
			return OBJECT_MAPPER.readValue(src, valueType);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(InputStream src, TypeReference<T> valueTypeRef) {
		try {
			return OBJECT_MAPPER.readValue(src, valueTypeRef);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(InputStream src, JavaType valueType) {
		try {
			return OBJECT_MAPPER.readValue(src, valueType);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(DataInput src, Class<T> valueType) {
		try {
			return OBJECT_MAPPER.readValue(src, valueType);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(DataInput src, JavaType valueType) {
		try {
			return OBJECT_MAPPER.readValue(src, valueType);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(Reader src, Class<T> valueType) {
		try {
			return OBJECT_MAPPER.readValue(src, valueType);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(Reader src, TypeReference<T> valueTypeRef) {
		try {
			return OBJECT_MAPPER.readValue(src, valueTypeRef);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(Reader src, JavaType valueType) {
		try {
			return OBJECT_MAPPER.readValue(src, valueType);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(URL src, Class<T> valueType) {
		try {
			return OBJECT_MAPPER.readValue(src, valueType);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(URL src, TypeReference<T> valueTypeRef) {
		try {
			return OBJECT_MAPPER.readValue(src, valueTypeRef);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T bean(URL src, JavaType valueType) {
		try {
			return OBJECT_MAPPER.readValue(src, valueType);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static <T> T convert(Object fromValue, Class<T> toValueType) {
		return OBJECT_MAPPER.convertValue(fromValue, toValueType);
	}
	
	public static <T> T convert(Object fromValue, TypeReference<T> toValueTypeRef) {
		return OBJECT_MAPPER.convertValue(fromValue, toValueTypeRef);
	}
	
	public static <T> T convert(Object fromValue, JavaType toValueType) {
		return OBJECT_MAPPER.convertValue(fromValue, toValueType);
	}
	
	public static void write(File resultFile, Object value) {
		try {
			OBJECT_MAPPER.writeValue(resultFile, value);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static void write(OutputStream out, Object value) {
		try {
			OBJECT_MAPPER.writeValue(out, value);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static void write(DataOutput out, Object value) {
		try {
			OBJECT_MAPPER.writeValue(out, value);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	public static void write(Writer writer, Object value) {
		try {
			OBJECT_MAPPER.writeValue(writer, value);
		} catch (IOException e) {
			throw new JsonException(e);
		}
	}
	
	/**
	 * 运行时获取具有泛型的类内真实泛型的JavaType对象，用于 ObjectMapper 反射
	 * <pre>
	 * class A {
	 *     private String name;
	 * }
	 *
	 * class B&lt;T&gt; {
	 *     public void reflect() {
	 *         String content = "{\"name\":\"test\"}";
	 *         JavaType javaType = Json.getGenericJavaType(this, B.class, 0);
	 *         T t = objectMapper.readValue(content, javaType);
	 *     }
	 * }
	 *
	 * class C extends B&lt;A&gt; {
	 *     public static void main(String[] args) {
	 *         C c = new C();
	 *         c.reflect();
	 *     }
	 * }
	 * </pre>
	 * @param inst 实现泛型类的实例对象
	 * @param genericClass 声明泛型的类
	 * @param genericIdx 声明泛型的类中泛型索引，从0开始
	 * @return 实例中泛型的JavaType对象
	 */
	public static JavaType getGenericJavaType(Object inst, Class<?> genericClass, int genericIdx) {
		String typeName = getGenericTypeName(inst, genericClass, genericIdx);
		JavaType javaType = OBJECT_MAPPER.getTypeFactory().constructFromCanonical(typeName);
		return javaType;
	}
	
	/**
	 * 运行时获取具有泛型的类内真实泛型类的完整类型字符串
	 * @param inst 实现泛型类的实例对象
	 * @param genericClass 声明泛型的类
	 * @param genericIdx 声明泛型的类中泛型索引，从0开始
	 * @return 实例中泛型的类完整类型字符串
	 */
	public static String getGenericTypeName(Object inst, Class<?> genericClass, int genericIdx) {
		String name = genericClass.getCanonicalName();
		Class<?> clazz = inst.getClass();
		String temp = clazz.getSuperclass().getCanonicalName();
		while (!name.equals(temp)) {
			clazz = clazz.getSuperclass();
			temp = clazz.getSuperclass().getCanonicalName();
		}
		ParameterizedType type = (ParameterizedType)clazz.getGenericSuperclass();
		String typeName = type.getActualTypeArguments()[genericIdx].getTypeName();
		return typeName;
	}
	
}
