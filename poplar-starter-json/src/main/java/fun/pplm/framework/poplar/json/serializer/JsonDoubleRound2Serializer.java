package fun.pplm.framework.poplar.json.serializer;

import java.io.IOException;
import java.text.DecimalFormat;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * 
 * double类型四舍五入保留两位小数
 * <p>
 * usage: @JsonSerualize(using = JsonDoubleRound2Serializer.class)
 *
 * @author OracleGao
 * 
 */
public class JsonDoubleRound2Serializer extends JsonSerializer<Double> {

	//四舍五入保留两位小数
	private DecimalFormat decimalFormat = new DecimalFormat("#.##"); 
	
	@Override
	public void serialize(Double value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		 if(value != null) {
			 gen.writeString(decimalFormat.format(value));
	     }
	}

}
