package fun.pplm.framework.poplar.json.config;

import java.io.IOException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * 
 * @author OracleGao
 *
 */
@Configuration
public class ObjectMapperConfig {
	public final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	
	static {
		SimpleModule simpleModule = new SimpleModule();
		/**
		 * 序列换成json时,将所有的long变成string
		 * 因为js中得数字类型不能包含所有的java long值
		 * 不使用此转换器 long转json可能有精度问题
		 */
        simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
        simpleModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
        /**
         * MultipartFile对象序列化支持
         */
        simpleModule.addSerializer(MultipartFile.class, new JsonSerializer<MultipartFile>() {
			@Override
			public void serialize(MultipartFile value, JsonGenerator gen, SerializerProvider serializers)
					throws IOException {
				gen.writeStartObject();
				gen.writeStringField("name", value.getOriginalFilename());
				gen.writeStringField("contentType", value.getContentType());
                gen.writeNumberField("size", value.getSize());
				gen.writeEndObject();
			}
        });
        OBJECT_MAPPER.registerModule(simpleModule);
        /**
         * LocalDateTime序列化支持
         */
        OBJECT_MAPPER.registerModule(new JavaTimeModule());
        OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        OBJECT_MAPPER.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        
	}
	
	@Bean
    public ObjectMapper objectMapper() {
        return OBJECT_MAPPER;
    }
	
}
