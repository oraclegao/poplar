## 1.10.19
- Json.string()字符串类型参数直接返回的判断由String类型改为其父类接口CharSequence

## 1.9.18
- JsonConfig更名为ObjectMapperConfig

## 1.8.16
- 添加MultipartFile对象序列化支持

## 1.6.11
- 添加ObjectMapper常用方法静态代理，并将异常代理为runtime的JsonException

## 1.5.7
- 增加LocalDataTime序列化支持

## 1.4.6
### 优化Json.string()方法
- 入参为null直接返回null
- 入参字符串类型，直接返回字符串

### Json类添加泛型工具方法【fixed】
- 添加Json.getGenericJavaType()方法，获取泛型JavaType对象
- 添加Json.getGenericTypeName()方法，获取泛型类完整字符串名称
 