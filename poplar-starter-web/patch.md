## 1.8.17
- 新增会话白名单服务接口SessionWhitePathPsi公共依赖，允许其它web服务实现该接口为session拦截注入白名单url patterns

## 1.8.16
- 添加SwaggerSecurityPsi接口，给业务服务提供swagger全局安全配置实现接口
- HttpServletUtil类中增加getParamHeaderValue方法获取头或参数值；增加单独传递request参数的相关方法

## 1.8.15
- 将poplar-starter-web-audit中的Audit审计注解移动进来，解决web子模块接口审计编译依赖问题
- 新增HttpServletUtil工具类
- 将AppContext中http servlet request和resposne相关api移动到HttpServletUtil新增类中
- http响应码添加3 sign error签名错误
- 添加审计类型枚举

## 1.7.14
- AppContext增加获取客户端ip相关静态方法

## 1.6.11
- 添加登录失败响应码402
- 添加登录失败LoginException异常拦截器
- 优化AppContext.getRequestAttribute(),HttpServletRequest为空时不在抛异常，而是返回空
- 优化异常响应消息

## 1.6.10
- 优化异常处理ExceptionController
