package fun.pplm.framework.poplar.web.resp;

/**
 * 
 * @author OracleGao
 *
 */
public enum RespCode {
    OK(0, "ok"),
    ERROR(1, "error"),
    INVALID_PARAMETER(2, "invalid param"),
    SIGN_ERROR(3, "sign error"),
    UNAUTHORIZED(401, "unauthorized"),
	LOGIN_FAILED(402, "login failed"),
	UNREPEATABLE_ACCESS(403, "unrepeatable access");
	
	private final Integer value;
    private final String text;

    RespCode(Integer value, String text) {
        this.value = value;
        this.text = text;
    }

	public Integer getValue() {
		return value;
	}

	public String getText() {
		return text;
	}
   
}
