package fun.pplm.framework.poplar.web.exception;

/**
 * 
 * 不可能重复访问异常
 * @author OracleGao
 *
 */
public class UnrepeatableAccessException extends RuntimeException {

	private static final long serialVersionUID = -4040018191965342309L;

	public UnrepeatableAccessException() {
		super();
	}

	public UnrepeatableAccessException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UnrepeatableAccessException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnrepeatableAccessException(String message) {
		super(message);
	}

	public UnrepeatableAccessException(Throwable cause) {
		super(cause);
	}

}
