package fun.pplm.framework.poplar.web;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 
 * 应用上下文对象
 * @author OracleGao
 *
 */
@Component
public class AppContext implements ApplicationContextAware {

	private static ApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		AppContext.applicationContext = applicationContext;
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}
	
	public static <T> T getBean(Class<T> clazz) {
		return applicationContext.getBean(clazz);
	}
	
	public static <T> Collection<T> getBeans(Class<T> clazz) {
		Map<String, T> map = applicationContext.getBeansOfType(clazz);
		if (map != null) {
			return map.values();
		}
		return Collections.emptyList();
	}
	
}
