package fun.pplm.framework.poplar.web.session.psi;

/**
 * 
 * Poplar Service Interface
 * 会话路径白名单服务接口
 * @author OracleGao
 *
 */
public interface SessionWhitePathPsi extends WhitePathPsi {

}
