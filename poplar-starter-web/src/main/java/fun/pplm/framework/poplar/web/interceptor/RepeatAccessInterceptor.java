package fun.pplm.framework.poplar.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;

/**
 * 
 * 提交拦截器
 * @author OracleGao
 *
 */
public class RepeatAccessInterceptor implements HandlerInterceptor {
	
	/**
	 * 拦截方法
	 */
	private String[] interceptMethods;
	
	public RepeatAccessInterceptor(String[] interceptMethods) {
		super();
		this.interceptMethods = interceptMethods;
	}

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		//String method = request.getMethod();
		return true;
	}

	public String[] getInterceptMethods() {
		return interceptMethods;
	}

	public void setInterceptMethods(String[] interceptMethods) {
		this.interceptMethods = interceptMethods;
	}
	
}
