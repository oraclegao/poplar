package fun.pplm.framework.poplar.web.swagger.psi;

import java.util.List;

import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.service.contexts.SecurityContext;

/**
 * 
 * Poplar Service Interface
 * swagger security poplar服务接口
 * 主要用户swagger全局安全配置，比如token ApiKey对象
 * @author OracleGao
 *
 */
public interface SwaggerSecurityPsi {
	
	public List<SecurityScheme> getSecuritySchemes();
	
	public List<SecurityContext> getSecurityContexts();
	
}
