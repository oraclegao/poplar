package fun.pplm.framework.poplar.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.security.auth.login.LoginException;
import javax.security.sasl.AuthenticationException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import fun.pplm.framework.poplar.web.resp.Resp;
import fun.pplm.framework.poplar.web.resp.RespCode;

/**
 * 
 * 异常处理
 * @author OracleGao
 *
 */
@ConditionalOnClass(value = RestController.class)
@ConditionalOnMissingBean(ExceptionController.class)
@RestController
@ControllerAdvice
public class ExceptionController {
	private Logger logger = LoggerFactory.getLogger(ExceptionController.class);
	
	@PostConstruct
	private void init() {
		logger.debug(this.getClass().getSimpleName() + "注入成功");
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Resp<String> methodArgumentNotValidException(MethodArgumentNotValidException e) {
		logger.error(e.getMessage());
		List<FieldError> fieldErrors = e.getFieldErrors();
		String message = null;
		if (fieldErrors != null && fieldErrors.size() > 0) {
			message = fieldErrors.stream().map(fieldError -> fieldError.getField() + " " + fieldError.getDefaultMessage()).collect(Collectors.joining(", "));
		}
		if (StringUtils.isBlank(message)) {
			message = e.getMessage();
		}
		if (StringUtils.isBlank(message)) {
			message = RespCode.INVALID_PARAMETER.getText();
		}
		return Resp.error(RespCode.INVALID_PARAMETER.getValue(), message);
	}
	
	@ExceptionHandler(BindException.class)
	public Resp<String> bindException(BindException e) {
		List<ObjectError> errors = e.getAllErrors();
		String message = null;
		if (errors != null && errors.size() > 0) {
			message = errors.stream().filter(error -> error instanceof FieldError).map(error -> ((FieldError) error).getField() + " " + error.getDefaultMessage()).collect(Collectors.joining(", "));
		}
		if (StringUtils.isBlank(message)) {
			message = e.getMessage();
		}
		if (StringUtils.isBlank(message)) {
			message = RespCode.INVALID_PARAMETER.getText();
		}
		return Resp.error(RespCode.INVALID_PARAMETER.getValue(), message);
	} 
	
	@ExceptionHandler(AuthenticationException.class)
	public Resp<String> authenticationException(AuthenticationException e) {
		String message = e.getMessage();
		logger.error(message, e);
		if (StringUtils.isBlank(message)) {
			message = RespCode.UNAUTHORIZED.getText();
		}
		return Resp.error(RespCode.UNAUTHORIZED.getValue(), message);
	}
	
	@ExceptionHandler(LoginException.class)
	public Resp<String> loginException(LoginException e) {
		String message = e.getMessage();
		logger.error(message, e);
		if (StringUtils.isBlank(message)) {
			message = RespCode.LOGIN_FAILED.getText();
		}
		return Resp.error(RespCode.LOGIN_FAILED.getValue(), message);
	}
	
	@ExceptionHandler(Exception.class)
	public Resp<String> exception(Exception e) {
		logger.error(e.getMessage(), e);
		return Resp.error(e.getMessage());
	}
	
}
