package fun.pplm.framework.poplar.web.session.psi;

import java.util.List;

import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.util.pattern.PathPattern;

/**
 * 
 * Poplar Service Interface
 * url白名单服务接口
 * @author OracleGao
 *
 */
public interface WhitePathPsi {
	
	/**
	 * For pattern syntax see {@link PathPattern} when parsed patterns
	 * are {@link PathMatchConfigurer#setPatternParser enabled} or
	 * {@link AntPathMatcher} otherwise. The syntax is largely the same with
	 * {@link PathPattern} more tailored for web usage and more efficient.
	 * @return path patterns
	 */
	public List<String> whitePathPatterns();
	
}
