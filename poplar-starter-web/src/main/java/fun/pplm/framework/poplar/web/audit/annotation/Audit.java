package fun.pplm.framework.poplar.web.audit.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import fun.pplm.framework.poplar.web.audit.enums.AuditType;

/**
 * 
 * 审计注解
 * @author OracleGao
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Audit {
	/**
	 * 功能名称 该值会被注入AuditModel.apiName
	 * @return 功能名称
	 */
	String value();
	/**
	 * 自定义功能类型，用于分组，默认值"" 该值会被注入AuditModel.apiTypes
	 * @return 功能类型
	 */
	String[] types() default {};
	/**
	 * 审计类型，覆盖全局配置
	 * @return 审计类型
	 */
	AuditType[] auditTypes() default {};
	
}
