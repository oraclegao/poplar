package fun.pplm.framework.poplar.web.resp;

/**
 * 
 * @author OracleGao
 *
 */
public interface RespFilter {
	
	public void process(Object content);
	
}
