package fun.pplm.framework.poplar.web.resp;

import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fun.pplm.framework.poplar.common.page.PageQueryParam;
import fun.pplm.framework.poplar.common.page.Pagination;
import fun.pplm.framework.poplar.web.AppContext;

/**
 * 
 * @author OracleGao
 * @param <T> 应答泛型对象
 * 
 */
public class Resp<T> {
    public static final String CODE = "code";
    public static final String MESSAGE = "message";
    public static final String CONTENT = "content";

    @ApiModelProperty(value = "返回状态码", required = false, example = "0", position = 0)
    private Integer code;
    @ApiModelProperty(value = "返回信息", required = false, position = 1)
    private String message;

    private T content;
    
    public Resp() {
    	super();
    }

	private Resp(Integer code, String message, T content) {
    	super();
        this.code = code;
        this.message = message;
        if (content != null) {
        	for (RespFilter respFilter : getRespFilters()) {
        		respFilter.process(content);
        	}
        	this.content = content;
        }
    }
	
	public Resp(Integer code, String message) {
		this(code, message, null);
	}

	private Resp(RespCode respCode) {
    	this(respCode.getValue(), respCode.getText(), null);
    }
	
	private Resp(RespCode respCode, T content) {
    	this(respCode.getValue(), respCode.getText(), content);
    }

	private Collection<RespFilter> getRespFilters() {
		return AppContext.getBeans(RespFilter.class);
	}

    public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }

    public static <T> Resp<T> success() {
        return new Resp<T>(RespCode.OK);
    }

    public static <T> Resp<T> success(T object) {
        return new Resp<>(RespCode.OK, object);
    }
    
    public static <T> Resp<Pagination<T>> successPageable(Collection<T> data, long total, PageQueryParam pageQueryParam) {
    	return new Resp<Pagination<T>>(RespCode.OK, Pagination.create(data, total, pageQueryParam));
    }
    
    public static <D, S extends D> Resp<Pagination<D>> successPageable(Pagination<S> pagination, Class<D> clazz) {
		Pagination<D> target = new Pagination<>();
        target.setPageNum(pagination.getPageNum());
        target.setPageSize(pagination.getPageSize());
        target.setTotal(pagination.getTotal());
        target.setPages(pagination.getPages());
        List<D> list = new ArrayList<>();
        list.addAll(pagination.getData());
        target.setData(list);
        return new Resp<Pagination<D>>(RespCode.OK, target);
	}
    
    public static <D, S extends D> Resp<List<D>> success(List<S> vos, Class<D> clazz) {
        List<D> list = new ArrayList<>();
        list.addAll(vos);
        return new Resp<List<D>>(RespCode.OK, list);
	}
    
    public static <T> Resp<T> error(String message) {
        return new Resp<T>(RespCode.ERROR.getValue(), message);
    }

    public static <T> Resp<T> error(Integer code, String message) {
        return new Resp<T>(code, message);
    }

    public static <T> Resp<T> error(Integer code, String message, T object) {
        return new Resp<>(code, message, object);
    }
    
    public static <T> Resp<T> error(RespCode respCode) {
        return new Resp<>(respCode);
    }
    
    public static <T> Resp<T> error(RespCode respCode, T object) {
        return new Resp<>(respCode, object);
    }
    
}