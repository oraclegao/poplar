package fun.pplm.framework.poplar.web.audit.enums;

/**
 * 
 * 审计类型
 * @author OracleGao
 *
 */
public enum AuditType {
	/**
	 * 业务方法执行之前
	 */
	BEFORE, 
	/**
	 * 业务方法执行之后
	 */
	AFTER
	
}