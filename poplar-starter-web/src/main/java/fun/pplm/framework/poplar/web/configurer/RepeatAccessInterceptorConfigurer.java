package fun.pplm.framework.poplar.web.configurer;

import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 
 * 重复访问拦截器配置
 * @author OracleGao
 *
 */
//@Configuration
//@ConfigurationProperties(prefix = "poplar.web.repeatAccessInterceptor")
public class RepeatAccessInterceptorConfigurer implements WebMvcConfigurer {
	private static Logger logger = LoggerFactory.getLogger(RepeatAccessInterceptorConfigurer.class);

	/**
	 * 拦截方法
	 * 默认post
	 */
	private String[] interceptMethods = new String[] {"post"};
	/**
	 * 是否启用
	 * 默认启用
	 */
	private Boolean enabled = true;
    /**
     * 白名单路径
     */
    private List<String> whitePathPatterns;

	public RepeatAccessInterceptorConfigurer() {
		super();
	}

    @PostConstruct
    private void init() {
    	
    	logger.debug(this.getClass().getSimpleName() + "注入成功, config: {}", this);
    }
	
	public String[] getInterceptMethods() {
		return interceptMethods;
	}

	public void setInterceptMethods(String[] interceptMethods) {
		this.interceptMethods = interceptMethods;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public List<String> getWhitePathPatterns() {
		return whitePathPatterns;
	}

	public void setWhitePathPatterns(List<String> whitePathPatterns) {
		this.whitePathPatterns = whitePathPatterns;
	}
	
}
