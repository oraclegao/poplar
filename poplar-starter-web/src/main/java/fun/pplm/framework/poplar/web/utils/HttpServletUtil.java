package fun.pplm.framework.poplar.web.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 
 * http servlet 工具类
 * 提供HttpServletRequest和HttpServletResponse静态工具方法
 * @author OracleGao
 *
 */
public class HttpServletUtil {
	private static Logger logger = LoggerFactory.getLogger(HttpServletUtil.class);
	
	public static String getRequestUri() {
		HttpServletRequest request = getRequest();
		if (request != null) {
			return request.getRequestURI();
		}
		return null;
	}
	
	/**
	 * 获取当前会话请求头中的值
	 * @param key 请求头key
	 * @return 请求头值
	 */
	public static String getHeaderValue(String key) {
		HttpServletRequest request = getRequest();
		if (request != null) {
			return request.getHeader(key);
		}
		return null;
	}
	
	/**
	 * 获取当前会话url查询参数或header中的值
	 * url参数值如果是数组，取第一个元素
	 * url参数值优先于header中的值，如果url参数值是blank则尝试从header中取，如果header中也是blank则返回null
	 * @param key 参数key
	 * @return 参数值，取不到值返回null
	 */
	public static String getParamHeaderValue(String key) {
		HttpServletRequest request = getRequest();
		if (request == null) {
			return null;
		}
		return getParamHeaderValue(request, key);
	}
	
	/**
	 * 获取http request中url查询参数或header中的值
	 * url参数值如果是数组，取第一个元素
	 * url参数值优先于header中的值，如果url参数值是blank则尝试从header中取，如果header中也是blank则返回null
	 * @param request http请求对象
	 * @param key 参数key
	 * @return 参数值，取不到值返回null
	 */
	public static String getParamHeaderValue(HttpServletRequest request, String key) {
		String value = request.getParameter(key);
		if (StringUtils.isBlank(value)) {
			return request.getHeader(key);
		}
		return value;
	}
	
	/**
	 * 获取用户会话cookie中的值
	 * @param key cookie key
	 * @return cookie值
	 */
	public static String getCookieValue(String key) {
		HttpServletRequest request = getRequest();
		if (request == null) {
			return null;
		}
    	return getCookieValue(request, key);
	}
	
	/**
	 * 获取http request cookie中的值
	 * @param request http请求对象
	 * @param key cookie key
	 * @return cookie值,找不到key返回null
	 */
	public static String getCookieValue(HttpServletRequest request, String key) {
		Cookie[] cookies = request.getCookies();
    	if (cookies != null && cookies.length > 0) {
    	    for (Cookie cookie : cookies) {
    	        if (key.equals(cookie.getName())) {
    	            return cookie.getValue();
    	        }
    	    }
    	}
    	return null;
	}
	
	/**
	 * 
	 * 获取当前会话的请求对象实例
	 * @return http请求对象实例
	 * 
	 */
	public static HttpServletRequest getRequest() {
		ServletRequestAttributes servletRequestAttributes = getServletRequestAttributes();
		if (servletRequestAttributes != null) {
			return servletRequestAttributes.getRequest();
		}
		logger.warn("无法获取HttpServletRequest上下文对象");
		return null;
	}
	
	/**
	 * 设置当前http会话的请求对象属性
	 * @param name 属性名
	 * @param attr 属性对象
	 */
	public static void setRequestAttribute(String name, Object attr) {
		HttpServletRequest request = getRequest();
		if (request == null) {
			throw new RuntimeException("HttpServletRequest is null");
		}
		request.setAttribute(name, attr);
	}
	
	/**
	 * 获取当前http会话的请求对象属性
	 * @param name 属性名
	 * @return 属性对象
	 */
	public static Object getRequestAttribute(String name) {
		HttpServletRequest request = getRequest();
		if (request == null) {
			return null;
		}
		return request.getAttribute(name);
	}
	
	/**
	 * 
	 * 获取当前会话的应答对象实例
	 * @return http应答对象实例
	 * 
	 */
	public static HttpServletResponse getResponse() {
		ServletRequestAttributes servletRequestAttributes = getServletRequestAttributes();
		if (servletRequestAttributes != null) {
			return servletRequestAttributes.getResponse();
		}
		logger.warn("无法获取HttpServletResponse上下文对象");
		return null;
	}
	
	private static ServletRequestAttributes getServletRequestAttributes() {
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		if (requestAttributes == null) {
			return null;
		}
		if (requestAttributes instanceof ServletRequestAttributes) {
			return (ServletRequestAttributes)requestAttributes;
		}
		return null;
	}
	
	/**
	 * 获取客户端ip
	 * @param request http请求对象
	 * @return 客户端ip
	 */
	public static String getClientIp(HttpServletRequest request){
	    String ip = request.getHeader("X-Forwarded-For");
	    if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getHeader("Proxy-Client-IP");
	    }
	    if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getHeader("WL-Proxy-Client-IP");
	    }
	    if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getRemoteAddr();
	    }
	    // 如果 IP 是本地回环地址，则替换成 127.0.0.1
	    if ("0:0:0:0:0:0:0:1".equals(ip)) {
	    	ip = "127.0.0.1";
	    }
	    return ip;
	}
	
	/**
	 * 获取http会话客户端ip
	 * @return 客户端ip
	 */
	public static String getClientIp(){
		HttpServletRequest request = getRequest();
		if(request == null) {
			return null;
		}
	    return getClientIp(request);
	}
	
}
