# poplar-common

框架通用组件和工具类

## 配置项

### ratelimit validator
- poplar.validator.ratelimit.type: 限速类型，fix:固定窗口限速，slide:滑动窗口限速。默认slide
- poplar.validator.ratelimit.timeWindowMs: 限速时间窗口，单位毫秒，默认0，不开启
- poplar.validator.ratelimit.count: 限速数量
- poplar.validator.ratelimitkeyPrefix: redis key前缀，默认:poplar:validator:ratelimit:

### repeat validator
- poplar.validator.repeat.timeWindowSecond: 重复交验时间窗口，单位秒，默认0不开启
- poplar.validator.repeat.keyPrefix: 重复交验redis key前缀 默认"poplar.validator:repeat:"