## 1.11.22
- 添加AsciiUtil工具类

## 1.11.21
- 添加UrlUtil工具类，提供url相关工具api

## 1.10.19
- 添加SqlFilterUtil符合sql查询习惯的条件过滤工具
- 添加ArrayUtil数组工具
- BinaryUtil增加整型数组批量转二进制字符串方法；int2FormatBinary方法增加自定义4位分隔符参数
- 移除RedisService.setExpireObject和RedisService.setExpireStr
- 增加RedisService.setExpire方法统一替代RedisService.setExpireObject和RedisService.setExpireStr
- 完善RedisService.expire方法注释
- RedisService中涉及Json的方法移除IOException统一替换成继承于RuntimeException的JsonException
- RedisService中移除setStr，setObject和getStr，getObject，由新增的set和get方法统一实现

## 1.9.18
- LineWorker线程框架底层优化为信号机制
- BeanUtil.converts()增加过滤器参数
- 新增线程池配置类ThreadPoolConfig
- 新增线程池工具类ThreadPoolUtil

## 1.8.16
- LineWorker.finalized()和其实现子类函数名变更：finalize->finalized，避免重写废弃的Object.finalize()

## 1.8.15
- 修复bug: BeanUtil.convert成员属性不同类型自动跳过，而不再抛异常
- LineWorker, ConvertionQueue添加析构函数: 对象实例线程结束后销毁前的函数回调
- 优化队列框架类，完善队列容量上限功能和运行时容量功能
- 增加批处理线程框架类BatchLineWorker

## 1.7.14
- 优化消费队列，支持启动消费队列之前调整队列大小
- 修复消费队列log不显示服务名称问题

## 1.6.13
- 添加ZipUtil工具类

## 1.6.12
- 添加BinaryUtil二进制工具类
- 添加BeanUtil bean工具类

## 1.6.11
- 完成ByteUtil方法注释
- ByteUtil添加奇偶校验方法
- 增加消费队列无消费元素时的idle回调方法

## 1.6.10
- 添加uuid工具类
- 添加int整型枚举校验注释符并实现校验功能

## 1.5.7
### 线程框架和消费队列优化
- 线程框架增加 运行状态方法
- 消费队列offer方法增加消费线程状态检测，未启动消费线程时将输出警告日志
- 线程框架LineWorker添加@PreDestory销毁方法

## 1.4.6
### 增加线程框架和消费队列相关类
- 添加生产线工人线程框架
- 添加类型转换消费队列
- 添加json反序列化消费队列
- 添加消费队列
### 增加字节处理工具类
- 增加字节处理工具类ByteUtil

## 1.3.2
- 修复分页计算总页数bug

## 1.0.0
- springboot 上下文工具 AppContext
- objectMapper 默认配置
- beanValidator配置
- exception controller注入
- authexecption
- 雪花算法生成id
- jackson工具
- 分页支持
- restful api 响应封装
- redisService封装
- tree node 封装
- 对象反射工具和路径工具
- beanValidator
- Rsa加解密工具方法