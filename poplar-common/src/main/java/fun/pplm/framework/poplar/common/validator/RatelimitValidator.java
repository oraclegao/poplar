package fun.pplm.framework.poplar.common.validator;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.ZSetOperations;

import fun.pplm.framework.poplar.common.service.RedisService;
import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * 限速校验
 * 默认滑动窗口限速
 * @author OracleGao
 *
 */
@ConditionalOnProperty(name = "poplar.validator.ratelimit.timeWindowMs")
@ConfigurationProperties(prefix = "poplar.validator.ratelimit")
public class RatelimitValidator {
	private static Logger logger = LoggerFactory.getLogger(RatelimitValidator.class);
	
	/**
	 * 限速类型
	 * fix:固定窗口默认
	 * slide:滑动窗口
	 */
	private String type = "slide";
	
	/**
	 * 限速时间窗口，单位毫秒，默认1000, 0: 不开启
	 */
	private long timeWindowMs = 0;
	
	/**
	 * 限速数量,默认10
	 */
	private int count = 10;
	
	/**
	 * redis key前缀
	 */
	private String keyPrefix = "poplar:validator:ratelimit:";
	
	@Autowired
	private RedisService redisService;
	
	private ValueOperations<String, String> vo;
	private ZSetOperations<String, String> zso;
	
	@PostConstruct
	public void init() {
		switch (type) {
		case "slide":
			zso = redisService.opsForZSet();
			break;
		case "fix":
			vo = redisService.opsForValue();
			break;
		default:
			throw new RuntimeException("invalid type: " + type);
		}
		logger.debug(this.getClass().getSimpleName() + "注入成功, config: {}", this);
	}
	
	public boolean overlimit(Ratelimitable data) {
		if (timeWindowMs == 0) {
			return true;
		}
		switch (type) {
		case "slide":
			return overlimitSlide(data);
		case "fix":
			return overlimitFix(data);
		default:
			throw new RuntimeException("invalid type: " + type);
		}
	}
	
	private boolean overlimitFix(Ratelimitable data) {
		String key = this.keyPrefix + data.getLimitKey();
		if (vo.setIfAbsent(key, "1")) {
			this.redisService.expireMs(key, this.timeWindowMs);
		} else {
			if (vo.increment(key) > this.count) {
				return true;
			}
		}
		return false;
	}
	
	private boolean overlimitSlide(Ratelimitable data) {
		String key = keyPrefix + data.getLimitKey();
		long timeSlice = timeWindowMs;
		long currentAt = System.currentTimeMillis();
		zso.add(key, String.valueOf(currentAt), currentAt);
		zso.removeRangeByScore(key, 0, currentAt - timeSlice);
		long cnt = zso.zCard(key);
		redisService.expire(key, timeSlice);
		return cnt > count;
	}

	public static interface Ratelimitable {
		
		/**
		 * 
		 * 根据业务对象获取限速key
		 * @return 限速对象key
		 * 
		 */
		public String getLimitKey();
		
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getTimeWindowMs() {
		return timeWindowMs;
	}

	public void setTimeWindowMs(long timeWindowMs) {
		this.timeWindowMs = timeWindowMs;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	@Override
	public String toString() {
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("type", type);
		map.put("timeWindowMs", timeWindowMs);
		map.put("count", count);
		map.put("keyPrefix", keyPrefix);
		return Json.string(map);
	}
	
}
