package fun.pplm.framework.poplar.common.utils;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * 
 * RSA算法
 * @author OracleGao
 * 
 */
public class RsaUtil {
	private static Logger logger = LoggerFactory.getLogger(RsaUtil.class);

	private static final String CHARSET = "UTF-8";
	private static final String RSA_ALGORITHM = "RSA";

	/**
	 * 创建RSA秘钥对 
	 * @param keySize key长度
	 * @return rsa密钥对对象
	 */
	public static RsaKeyPair createKeyPair(int keySize) {
		if (keySize < 512) {
			keySize = 512;
		} else if (keySize > 1024) {
			keySize = 1024;
		}

		// 为RSA算法创建一个KeyPairGenerator对象
		KeyPairGenerator kpg;
		try {
			kpg = KeyPairGenerator.getInstance(RSA_ALGORITHM);
		} catch (NoSuchAlgorithmException e) {
			logger.error("获取RSA算法KeyPairGenerator对象发生错误", e);
			return null;
		}
		// 初始化KeyPairGenerator对象,密钥长度
		kpg.initialize(keySize);
		// 生成密匙对
		KeyPair keyPair = kpg.generateKeyPair();
		// 得到公钥
		Key publicKey = keyPair.getPublic();
		String publicKeyStr = Base64.encodeBase64String(publicKey.getEncoded());
		// 得到私钥
		Key privateKey = keyPair.getPrivate();
		String privateKeyStr = Base64.encodeBase64String(privateKey.getEncoded());
		return new RsaKeyPair(publicKeyStr, privateKeyStr);
	}

	/**
	 * 得到公钥
	 * @param publicKey 密钥字符串（经过base64编码）
	 * @throws Exception 异常信息
	 */
	private static RSAPublicKey getPublicKey(String publicKey) {

		if (null == publicKey || publicKey.isEmpty()) {
			return null;
		}

		// 通过X509编码的Key指令获得公钥对象
		KeyFactory keyFactory = null;
		try {
			keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
		} catch (NoSuchAlgorithmException e) {
			logger.error("获得公钥对象失败", e);
			return null;
		}
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(Base64.decodeBase64(publicKey));
		RSAPublicKey key = null;
		try {
			key = (RSAPublicKey) keyFactory.generatePublic(x509KeySpec);
		} catch (InvalidKeySpecException e) {
			logger.error("生成公钥失败", e);
			return null;
		}
		return key;
	}

	/**
	 * 得到私钥
	 * @param privateKey 密钥字符串（经过base64编码）
	 * @throws Exception 异常信息
	 */
	private static RSAPrivateKey getPrivateKey(String privateKey) {

		if (null == privateKey || privateKey.isEmpty()) {
			return null;
		}
		// 通过X509编码的Key指令获得公钥对象
		KeyFactory keyFactory = null;
		try {
			keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
		} catch (NoSuchAlgorithmException e) {
			logger.error("获得私钥对象失败", e);
			return null;
		}
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(Base64.decodeBase64(privateKey));
		RSAPrivateKey key = null;
		try {
			key = (RSAPrivateKey) keyFactory.generatePrivate(pkcs8KeySpec);
		} catch (InvalidKeySpecException e) {
			logger.error("生成私钥失败", e);
			return null;
		}
		return key;
	}

	/**
	 * 公钥加密
	 * @param data 待加密数据
	 * @param publicKey 公钥
	 * @return 加密字符串
	 */
	public static String publicEncrypt(String data, String publicKey) {
		if (null == data || data.isEmpty() || null == publicKey || publicKey.isEmpty()) {
			return null;
		}
		return publicEncrypt(data, getPublicKey(publicKey));
	}

	public static String publicEncrypt(String data, RSAPublicKey publicKey) {
		if (null == data || data.isEmpty() || null == publicKey) {
			return null;
		}
		try {
			Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			return Base64.encodeBase64String(rsaSplitCodec(cipher, Cipher.ENCRYPT_MODE, data.getBytes(CHARSET),
					publicKey.getModulus().bitLength()));
		} catch (Exception e) {
			logger.error("通过公钥加密发生错误", e);
			return null;
		}
	}

	/**
	 * 私钥解密
	 * @param data 待解密数据
	 * @param privateKey 私钥
	 * @return 加密数据
	 */
	public static String privateDecrypt(String data, String privateKey) {
		if (null == data || data.isEmpty() || null == privateKey || privateKey.isEmpty()) {
			return null;
		}
		return privateDecrypt(data, getPrivateKey(privateKey));
	}

	public static String privateDecrypt(String data, RSAPrivateKey privateKey) {
		if (null == data || data.isEmpty() || null == privateKey) {
			return null;
		}
		try {
			Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			return new String(rsaSplitCodec(cipher, Cipher.DECRYPT_MODE, Base64.decodeBase64(data),
					privateKey.getModulus().bitLength()), CHARSET);
		} catch (Exception e) {
			logger.error("通过私钥解密发生错误", e);
		}
		return null;
	}

	/**
	 * 私钥加密
	 * @param data 待加密数据
	 * @param privateKey 私钥
	 * @return 加密结果
	 */
	public static String privateEncrypt(String data, String privateKey) {
		if (null == data || data.isEmpty() || null == privateKey || privateKey.isEmpty()) {
			return null;
		}
		return privateEncrypt(data, getPrivateKey(privateKey));

	}

	public static String privateEncrypt(String data, RSAPrivateKey privateKey) {
		if (null == data || data.isEmpty() || null == privateKey) {
			return null;
		}
		try {
			Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, privateKey);
			return Base64.encodeBase64String(rsaSplitCodec(cipher, Cipher.ENCRYPT_MODE, data.getBytes(CHARSET),
					privateKey.getModulus().bitLength()));
		} catch (Exception e) {
			logger.error("通过私钥加密发生错误", e);
		}
		return null;
	}

	/**
	 * 公钥解密
	 * @param data 待解密数据
	 * @param publicKey 公钥
	 * @return 解密结果
	 */
	public static String publicDecrypt(String data, String publicKey) {
		if (null == data || data.isEmpty() || null == publicKey || publicKey.isEmpty()) {
			return null;
		}
		return publicDecrypt(data, getPublicKey(publicKey));
	}

	public static String publicDecrypt(String data, RSAPublicKey publicKey) {
		if (null == data || data.isEmpty() || null == publicKey) {
			return null;
		}
		try {
			Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, publicKey);
			return new String(rsaSplitCodec(cipher, Cipher.DECRYPT_MODE, Base64.decodeBase64(data),
					publicKey.getModulus().bitLength()), CHARSET);
		} catch (Exception e) {
			logger.error("通过公钥解密发生错误", e);
		}
		return null;
	}

	private static byte[] rsaSplitCodec(Cipher cipher, int opmode, byte[] datas, int keySize) {
		int maxBlock = 0;
		if (opmode == Cipher.DECRYPT_MODE) {
			maxBlock = keySize / 8;
		} else {
			maxBlock = keySize / 8 - 11;
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] buff;
		int i = 0;
		try {
			while (datas.length > offSet) {
				if (datas.length - offSet > maxBlock) {
					buff = cipher.doFinal(datas, offSet, maxBlock);
				} else {
					buff = cipher.doFinal(datas, offSet, datas.length - offSet);
				}
				out.write(buff, 0, buff.length);
				i++;
				offSet = i * maxBlock;
			}
		} catch (Exception e) {
			throw new RuntimeException("加解密阀值为[" + maxBlock + "]的数据时发生异常", e);
		}
		byte[] resultDatas = out.toByteArray();
		try {
			if (out != null) {
				out.close();
			}
		} catch (final IOException e) {
			logger.error("关闭流失败", e);
		}
		return resultDatas;
	}

	/**
	 * RSA秘钥对类
	 * @author OracleGao
	 */
	public static class RsaKeyPair {
		private String publicKey;
		private String privateKey;

		private RsaKeyPair(String publicKey, String privateKey) {
			super();
			this.publicKey = publicKey;
			this.privateKey = privateKey;
		}

		public String getPublicKey() {
			return publicKey;
		}

		public void setPublicKey(String publicKey) {
			this.publicKey = publicKey;
		}

		public String getPrivateKey() {
			return privateKey;
		}

		public void setPrivateKey(String privateKey) {
			this.privateKey = privateKey;
		}
	}

	public static void main(String[] args) {
//        RsaKeyModel keys = RSAUtil.createKeys(512);
//        String publicKey = keys.getPublicKey();
//        String privateKey = keys.getPrivateKey();
		String publicKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAI1BaO0H37kDWzPYF/gZb01kK9vtW2ZxvCpCL0K3pwo5yZkX8pfxHpMh900Z2B9VsvX2O5wrthSAyfUR/PPMWtUCAwEAAQ==";
//		String privateKey = "MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEAjUFo7QffuQNbM9gX+BlvTWQr2+1bZnG8KkIvQrenCjnJmRfyl/EekyH3TRnYH1Wy9fY7nCu2FIDJ9RH888xa1QIDAQABAkAVc8acoOKANZtHP108h5hYt4rVprGH2GQMQurshfmvWqeGmK8iF7LYFswzs8/A7wDjn12BJEuCXDGjRRXtg6PBAiEAyBRiSeHo67v2m5vnamnBP5W7cHuOfoRfgVRyeL7pkXECIQC0vC8lTwzHTpMvl/17tUBzaUbhB09X2gIPAC7uV1ztpQIhAImhzAL6zhjrEbb1RfljijXNMEUuGVVahcAXcS8yVMVhAiEAhpQaBw311fMMZkQm3Bb7BvU9chKfYitTnRfl2lPzPoECIAmYtaRBZ+Az6x28pDVCG5fIrjJvB34YFOQeo6KJtc8p";
//        System.out.println("公钥: \n\r" + publicKey);
//        System.out.println("私钥： \n\r" + privateKey);

//        System.out.println("公钥加密——私钥解密");
//        String str = "11站在大明门前守卫的禁卫军，事先没有接到\n" + "有关的命令，但看到大批盛装的官员来临，也就\n" + "以为确系举行大典，因而未加询问。进大明门即\n"
//                + "为皇城。文武百官看到端门午门之前气氛平静，\n" + "城楼上下也无朝会的迹象，既无几案，站队点名\n" + "的御史和御前侍卫“大汉将军”也不见踪影，不免\n"
//                + "心中揣测，互相询问：所谓午朝是否讹传？";
//        System.out.println("\r明文：\r\n" + str);
//        System.out.println("\r明文大小：\r\n" + str.getBytes().length);
//        String encodedData = RSAUtil.publicEncrypt(str, RSAUtil.getPublicKey(publicKey));
//        System.out.println("密文：\r\n" + encodedData);
//        String decodedData = RSAUtil.privateDecrypt(encodedData, RSAUtil.getPrivateKey(privateKey));
//        System.out.println("解密后文字: \r\n" + decodedData);
//
		String encodedData = "NLzwDR4f/ZnQZllRkAL88IatsJwTku62cEIT3QOuREYLmJ6Es2v4rw17i032qK56bOmIGDXGgqRfOJsomdEWvYVRZFXYfhf+obg9dSLjZcSsDJt7BeDklm7wCeDIO6WlUhYev7A4xCRSqNiL1D3I/jqNurqjagP6JOS4p9bWBKGEtT9EE2RZQWHHlJrhY6Z37rMOnzH+6ynsHJgw6hyZua8DqoXrq/tfDZIWPkYV5dSipITxFau62J3DE/G2+/8XP2b9kLbdzHm5+ho4tkgniDf04BgaLSnB/6dG9zBsjYh3amupnxw7PWY7E6siy2nbCXq1oLEkJnZqOrlbX36upDKcdAD3BwSCUUkonWFjHW79TdKtp2AQ6WjVFRV/lk5bh1SkVg19u7ps1CHggzu4jj1MdFFbkBHBuTtDZN57b3aFc519q8KiPwUlcxgb5k0ReyBa8h55cdQo73ZJUPhQB3Axsj3OOyqpf9cQs8BJE05REZxiHhYEandFUQK8MT4+CZ6ZelQYK+G+IU6LwoINJ3KsxGzjW+6dR065Axa2RwG4/8S/ZI3YWuaUWh5a3JUYr34PShWF9aGFdqgiRDle6VTGQVJ0E4s1ITPaIMGBRf19DTpaNmHQiMad2i12PBYOyViCrT6n2dM7gPGB61UKI1VCpLvXx4PwCwjn2uJJ4z4c3Ao6wxuqNjipZTubYoMVh02BWeW9A/XH9emll6sGEqdk+GQiiHoMlVVSyk4s1XmmzhN64BJCEtGZ0RqnSPbfFBb4Q7NoNREPuWI2SRZRJM0MoFZ2OuaDh8w+nXMFiXPsEcjya+YRWXB/tY6jFa6bw/Gpyym0d7fzfGqKo/vZD0LKylUlfW3Gm+AZg0JSiNCrN388Y89CrDgN+oq3XAb/n9EJa4IZO86bQQ7L/htm8Cja3Kti18v8UQWOW0cwGNQwkC45P9UgSqUFLampMBp8dDI5mpsljWCO1YfXOhEl05F71R3Yg1t3ZKBGzCHiWoCVuNNRrNjwGLSyHZYxnWRoFMKjqCxRR373BVbW4Xxl3UVxoWVTBnzti/U/o3/c+LVQaAju6Wr43n6yMTnKoYhqwWGKGUKK/Skf32ZVb11ARYsbR437dep008+PtHTfkkoT0Epetd65p9IONDb/m4KylDaGubS232MwpxS6NxLJXMe8uefKz3kOQopKb1MaKlJUycriyJ91QOmcvNSA4dAKwYJmUduCWnKcTCGwUkbk8UZSdMk4+J5icNmOwOhn+A05KVKO6IZi4C9ArGqNL8x0MIWG1MQYoLMswdCgFuOG/3xv9ZiwvHePY4J2xAyfDlCPtGf183Nr+qgyixAtTH2nORTFbPE4Z3i+QC70c8a6uhHE/hnfTlnEuEFbxq5NZLqxJjcDJ9K+fdour/ay6UxxGDcHsONbF79B4a25li8OcbIVgxwVSPzlaDszGQ6uEOojWcZu9PACtgfsyDoBabij/G2JNKVBcBTAXbliJHeue2ZE+bqNjkwBQ/+6ZiDRew4J31udIuL1f7iAZZhPaszDS5uPttRhW38sASKZgwx0ab71OKrC3IcYobqAvutEj5/q9RQ+gifRIxE8SGtQG3vXLw39dhFuzSHfHTF5QtAA2w==";
		System.out.println(RsaUtil.publicDecrypt(encodedData, publicKey));
//
	}

}
