package fun.pplm.framework.poplar.common.utils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * bean工具
 * @author OracleGao
 *
 */
public class BeanUtil {
	private static Logger logger = LoggerFactory.getLogger(BeanUtil.class);
	
	/**
	 * 集合类型转换，主要用于entity对象集合转vo对象结合等场景
	 * @param <R> 结果类型
	 * @param srcs 源对象集合
	 * @param destClass 转换类型，必须要有无参构造函数
	 * @return 转换后的对象集合
	 */
	public static <R> List<R> converts(Collection<?> srcs, Class<R> destClass) {
		return converts(srcs, destClass, null, null);
	}
	
	/**
	 * 集合类型转换，主要用于entity对象集合转vo对象结合等场景
	 * @param <R> 结果类型
	 * @param srcs 源对象集合
	 * @param destClass 转换类型，必须要有无参构造函数
	 * @param ignores srcs集合中的对象忽略的成员名称集合
	 * @return 转换后的对象集合
	 */
	public static <R> List<R> converts(Collection<?> srcs, Class<R> destClass, Collection<String> ignores) {
		return converts(srcs, destClass, ignores, null);
	}
	
	/**
	 * 集合类型转换，主要用于entity对象集合转vo对象结合等场景
	 * @param <R> 结果类型
	 * @param <S> 源类型
	 * @param srcs 源对象集合
	 * @param destClass 转换类型，必须要有无参构造函数
	 * @param filter 源对象集合过滤函数
	 * @return 转换后的对象集合
	 */
	public static <S, R> List<R> converts(Collection<S> srcs, Class<R> destClass, Predicate<? super S> filter) {
		return converts(srcs, destClass, null, filter);
	}
	
	/**
	 * 集合类型转换，主要用于entity对象集合转vo对象结合等场景
	 * @param <R> 结果类型
	 * @param <S> 源类型
	 * @param srcs 源对象集合
	 * @param destClass 转换类型，必须要有无参构造函数
	 * @param ignores srcs集合中的对象忽略的成员名称集合
	 * @param filter 源对象集合过滤函数
	 * @return 转换后的对象集合
	 */
	public static <S, R> List<R> converts(Collection<S> srcs, Class<R> destClass, Collection<String> ignores, Predicate<? super S> filter) {
		if (srcs == null) {
			return Collections.emptyList();
		}
		int size = srcs.size();
		if (size == 0) {
			return Collections.emptyList();
		}
		if (filter != null) {
			srcs = srcs.stream().filter(filter).collect(Collectors.toList());
			size = srcs.size();
			if (size == 0) {
				return Collections.emptyList();
			}
		}
		List<R> rs = new ArrayList<>(size);
		R r = null;
		try {
			for (Object obj : srcs) {
				r = newFrom(obj, destClass, ignores);
				rs.add(r);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return rs;
	}
	
	/**
	 * 集合类型转换，主要用于entity对象集合转vo对象结合等场景
	 * @param <R> 结果类型
	 * @param <S> 源类型
	 * @param srcs 源对象集合
	 * @param mapper mapper
	 * @return 转换后的对象集合
	 */
	public static <R, S> List<R> converts(Collection<S> srcs, Function<? super S, ? extends R> mapper) {
		return converts(srcs, mapper, null);
	}
	
	/**
	 * 集合类型转换，主要用于entity对象集合转vo对象结合等场景
	 * @param <R> 结果类型
	 * @param <S> 源类型
	 * @param srcs 源对象集合
	 * @param mapper mapper转换函数
	 * @param filter 原对象集合过滤函数
	 * @return 转换后的对象集合
	 */
	public static <R, S> List<R> converts(Collection<S> srcs, Function<? super S, ? extends R> mapper, Predicate<? super S> filter) {
		if(srcs == null || srcs.size() == 0) {
			return Collections.emptyList();
		}
		if (filter != null) {
			return srcs.stream().filter(filter).map(mapper).collect(Collectors.toList());
		}
		return srcs.stream().map(mapper).collect(Collectors.toList());
	}
	
	/**
	 * 创建bean对象，并将src对象类型转换成bean对象<br>
	 * 详见convert方法
	 * @param <R> 结果类型
	 * @param src 源对象
	 * @param destClass 目标对象类型
	 * @return 目标对象
	 */
	public static <R> R newFrom(Object src, Class<R> destClass) {
		return newFrom(src, destClass, null);
	}
	
	/**
	 * 创建bean对象，并将src对象类型转换成bean对象<br>
	 * 详见convert方法
	 * @param <R> 类型
	 * @param src 源对象
	 * @param destClass 目标对象类型
	 * @param ignores 忽略的成员名称集合
	 * @return 目标对象
	 */
	public static <R> R newFrom(Object src, Class<R> destClass, Collection<String> ignores) {
		try {
			R r = destClass.newInstance();
			convert(src, r, ignores);
			return r;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * bean对象类型转换，主要用于entity对象转vo等场景<br>
	 * 将原对象类型转换成目标对象类型<br>
	 * 将原对象所有具有getter的成员属赋值给目标对象所有具有setter的同名成员属性<br>
	 * 同名成员源对象调用get方法，目标对象调用对应的set方法<br>
	 * 不做源对象和目标对象成员类型检查<br>
	 * 不支持boolean类型的is的getter赋值<br>
	 * @param src 源对象
	 * @param dest 目标对象
	 */
	public static void convert(Object src, Object dest) {
		convert(src, dest, null);
	}
	
	/**
	 * bean对象类型转换，主要用于entity对象转vo等场景<br>
	 * 将原对象类型转换成目标对象类型<br>
	 * 将原对象所有具有getter的成员属赋值给目标对象所有具有setter的同名成员属性<br>
	 * 同名成员源对象调用get方法，目标对象调用对应的set方法<br>
	 * 不做源对象和目标对象成员类型检查<br>
	 * 不支持boolean类型的is的getter赋值<br>
	 * @param src 源对象
	 * @param dest 目标对象
	 * @param ignores 忽略的成员名称集合
	 */
	public static void convert(Object src, Object dest, Collection<String> ignores) {
		Class<?> srcClass = src.getClass();
		String methodName = null;
		String fieldName = null;
		List<String> setMethodNames = new ArrayList<>();
		for (Method method : srcClass.getMethods()) {
			methodName = method.getName();
			if (methodName.startsWith("get") && methodName.length() > 3) {
				fieldName = StringUtils.uncapitalize(methodName.substring(3));
				if (ignores != null && ignores.size() > 0) {
					if (ignores.contains(fieldName)) {
						continue;
					}
				}
				Class<?> returnType = method.getReturnType();
				if (Map.class.isAssignableFrom(returnType) || Collection.class.isAssignableFrom(returnType)) {
					continue; //Map类和Collection类不作处理
				}
				setMethodNames.add("s" + methodName.substring(1));
			}
		}
		if (setMethodNames.size() > 0) {
			Class<?> destClass = dest.getClass();
			Method getMethod = null;
			String getMethodName = null;
			Object value = null;
			for (Method method : destClass.getMethods()) {
				methodName = method.getName();
				if (methodName.startsWith("set") && methodName.length() > 3) {
					if (setMethodNames.contains(methodName)) {
						getMethodName = "g" + methodName.substring(1);
						try {
							getMethod = srcClass.getMethod(getMethodName);
							value = getMethod.invoke(src);
							if (value != null) {
								method.invoke(dest, value);
							}
						} catch(IllegalArgumentException e) {
							logger.debug("同名成员{}类型不匹配跳过赋值。", StringUtils.uncapitalize(methodName.substring(3)));
						} catch (Exception e) {
							throw new RuntimeException(e);
						}
					}
				}
			}
		}
	}

}
