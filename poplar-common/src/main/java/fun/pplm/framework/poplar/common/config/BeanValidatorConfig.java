package fun.pplm.framework.poplar.common.config;

import javax.annotation.PostConstruct;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;

import org.hibernate.validator.messageinterpolation.ParameterMessageInterpolator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.common.validator.BeanValidator;

/**
 * 
 * @author OracleGao
 *
 */
@Configuration
@ConditionalOnMissingBean(BeanValidator.class)
public class BeanValidatorConfig {
	private static Logger logger = LoggerFactory.getLogger(BeanValidatorConfig.class);
	
    @PostConstruct
    private void init() {
    	logger.debug(this.getClass().getSimpleName() + "注入成功");
    }
	
	@Bean
	public BeanValidator beanValidator() {
		ValidatorFactory factory = Validation.byDefaultProvider()
                .configure()
                .messageInterpolator(new ParameterMessageInterpolator())
                .buildValidatorFactory();
		BeanValidator beanValidator = new BeanValidator(factory.getValidator());
		logger.debug(beanValidator.getClass().getSimpleName() + "注入成功");
		return beanValidator;
	}
	
}
