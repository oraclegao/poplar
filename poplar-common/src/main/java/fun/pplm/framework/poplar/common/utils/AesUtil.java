package fun.pplm.framework.poplar.common.utils;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * AES加解密
 * @author OracleGao
 *
 */
public class AesUtil {
	private static Logger logger = LoggerFactory.getLogger(AesUtil.class);
	
	private static final String KEY_ALGORITHM = "AES";
	private static final String CHARSET = "UTF-8";
	private static final String DEFAULT_PASSWORD = "poplar";
	private static final String DEFAULT_CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";
	private static final String ALGORITHM = "SHA1PRNG";
	private static final int DEFAULT_KEYSIZE = 256;

	private static Cipher creationCipher(int cipherType, String password, int keySize) {
		if (cipherType != Cipher.ENCRYPT_MODE && cipherType != Cipher.DECRYPT_MODE) {
			return null;
		}
		if (null == password || password.isEmpty()) {
			password = DEFAULT_PASSWORD;
		}
		if (keySize != 128 && keySize != 192 && keySize != 256) {
			keySize = DEFAULT_KEYSIZE;
		}
		try {
			KeyGenerator kgen = KeyGenerator.getInstance(KEY_ALGORITHM);
			kgen.init(keySize, new SecureRandom(password.getBytes(CHARSET)));
			SecureRandom random = SecureRandom.getInstance(ALGORITHM);
			random.setSeed(password.getBytes(CHARSET));
			kgen.init(DEFAULT_KEYSIZE, random);
			SecretKey secretKey = kgen.generateKey();
			byte[] enCodeFormat = secretKey.getEncoded();
			SecretKeySpec key = new SecretKeySpec(enCodeFormat, KEY_ALGORITHM);
			Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
			cipher.init(cipherType, key);
			return cipher;
		} catch (Exception e) {
			logger.error("创建密码器失败", e);
		}
		return null;
	}

	/**
	 * AES加密字符串
	 * @param content  需要被加密的字符串
	 * @param password 加密需要的密码
	 * @param keySize  key的长度
	 * @return 密文
	 */
	public static String encrypt(String content, String password, int keySize) {
		if (null == content || content.isEmpty()) {
			return null;
		}
		Cipher cipher = creationCipher(Cipher.ENCRYPT_MODE, password, keySize);
		if (null == cipher) {
			return null;
		}
		try {
			byte[] byteContent = content.getBytes(CHARSET);
			byte[] result = cipher.doFinal(byteContent);
			return Base64.encodeBase64String(result);
		} catch (Exception e) {
			logger.error("AES加密失败", e);
		}
		return null;
	}

	public static String encrypt(String content) {
		return encrypt(content, null, 0);
	}

	public static String encrypt(String content, String password) {
		return encrypt(content, password, 0);
	}

	public static String encrypt(String content, int keySize) {
		return encrypt(content, null, keySize);
	}

	/**
	 * 解密
	 * @param content  AES加密过过的内容
	 * @param password 加密时的密码
	 * @param keySize  key的长度
	 * @return 明文
	 */
	public static String decrypt(byte[] content, String password, int keySize) {
		if (null == content || content.length == 0) {
			return null;
		}
		try {
			KeyGenerator kgen = KeyGenerator.getInstance(KEY_ALGORITHM);
			SecureRandom random = SecureRandom.getInstance(ALGORITHM);
			random.setSeed(password.getBytes(CHARSET));
			kgen.init(256, random);
			SecretKey secretKey = kgen.generateKey();
			byte[] enCodeFormat = secretKey.getEncoded();
			SecretKeySpec key = new SecretKeySpec(enCodeFormat, KEY_ALGORITHM);
			Cipher cipher = creationCipher(Cipher.DECRYPT_MODE, password, keySize);
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] result = cipher.doFinal(content);
			return new String(result);
		} catch (Exception e) {
			logger.error("AES解密失败", e);
		}
		return null;
	}

	public static String decrypt(byte[] content) {
		return decrypt(content, null, 0);
	}

	public static String decrypt(byte[] content, String password) {
		return decrypt(content, password, 0);
	}

	public static String decrypt(byte[] content, int keySize) {
		return decrypt(content, null, keySize);
	}

	public static String decrypt(String content, String password, int keySize) {
		if (null == content || content.isEmpty()) {
			return null;
		}
		return decrypt(Base64.decodeBase64(content), password, keySize);
	}

	public static String decrypt(String content) {
		return decrypt(content, null, 0);
	}

	public static String decrypt(String content, String password) {
		return decrypt(content, password, 0);
	}

	public static String decrypt(String content, int keySize) {
		return decrypt(content, null, keySize);
	}
	
	public static void main(String[] args) {
		String content = "{\"productNumber\":\"2210091100109701\",\"formula\":\"C080J070N035P200\"}";
		//生成机构token时，为送检数据生成的临时加密密码
		String key= "20221209";
		String data = encrypt(content, key);
		System.out.println("encrypt: " + data);
		System.out.println("decrypt: " + decrypt(data, key));
	}
	
}
