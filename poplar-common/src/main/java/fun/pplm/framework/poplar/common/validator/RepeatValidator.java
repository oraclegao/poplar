package fun.pplm.framework.poplar.common.validator;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;

import fun.pplm.framework.poplar.common.service.RedisService;
import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * 数据重复交验
 * 依赖redis做数据缓存，支持分布式重复交验
 * @author OracleGao
 *
 */
@ConditionalOnProperty(name = "poplar.validator.repeat.timeWindowSecond")
@ConfigurationProperties(prefix = "poplar.validator.repeat")
public class RepeatValidator {
	private static Logger logger = LoggerFactory.getLogger(RepeatValidator.class);
	
	/**
	 * 重复校验时间窗口，单位秒，0表示不开启校验
	 */
	private long timeWindowSecond = 0;
	
	/**
	 * redis key前缀
	 */
	private String keyPrefix = "poplar:validator:repeat:";
	
	@Autowired
	private RedisService redisService;
	
	@PostConstruct
	private void init() {
		logger.debug(this.getClass().getSimpleName() + "注入成功, config: {}", this);
	}
	
	/**
	 * 
	 * @param data 校验重复的数据
	 * @return true 重复，false 不重复
	 * 
	 */
	public boolean valid(RepeatCheckable data) {
		if (timeWindowSecond == 0) {
			return false;
		}
		if (data == null) {
			throw new NullPointerException("value of RepeatCheckable is null");
		}
		String unique = data.unique();
		if (StringUtils.isBlank(unique)) {
			throw new RuntimeException("value of RepeatCheckable.unique() is blank");
		}
		String key = keyPrefix + unique;
		if (redisService.exists(key)) {
			redisService.expire(key, timeWindowSecond);
			return true;
		} else {
			redisService.setExpire(key, unique, timeWindowSecond);
		}
		return false;
	}
	
	@Override
	public String toString() {
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("timeWindowSecond", timeWindowSecond);
		map.put("keyPrefix", keyPrefix);
		return Json.string(map);
	}
	
	/**
	 * 
	 * 可被重复交验的接口
	 * @author OracleGao
	 *
	 */
	public static interface RepeatCheckable {
		
		/**
		 * 
		 * @return 返回判断唯一的字符串
		 * 
		 */
		public String unique();
	}
	
}
