package fun.pplm.framework.poplar.common.utils;

/**
 * 
 * 二进制工具
 * @author OracleGao
 *
 */
public class BinaryUtil {
	
	public static String int2FormatBinary(int value) {
		return int2FormatBinary(value, null);
	}
	
	/**
	 * 整型转换成为格式化的32位二进制字符串，主要用于可读日志输出
	 * 高位不足4位用0补齐
	 * 0000_0000_0000_0000_0000_0000_0000_0000
	 * @param value 整型值
	 * @param separator4 4位分割符替换“_”
	 * @return 二进制格式字符串
	 */
	public static String int2FormatBinary(int value, String separator4) {
		if(separator4 == null) {
			separator4 = " ";
		}
		String binary = Integer.toBinaryString(value);
		int len = binary.length();

		StringBuilder stringBuilder = new StringBuilder();
		int i = 32;
		while(i-- - len > 0) {
			stringBuilder.append("0");
		}
		stringBuilder.append(binary);
		stringBuilder.insert(4, separator4);
		stringBuilder.insert(9, separator4);
		stringBuilder.insert(14, separator4);
		stringBuilder.insert(19, separator4);
		stringBuilder.insert(24, separator4);
		stringBuilder.insert(29, separator4);
		stringBuilder.insert(34, separator4);
		return stringBuilder.toString();
	}
	
	/**
	 * 整型转换成为格式化的32位二进制字符串，主要用于可读日志输出
	 * 高位不足4位用0补齐
	 * 00000000000000000000000000000000
	 * @param value 整型值
	 * @return 二进制格式字符串
	 */
	public static String i2B(int value) {
		return int2FormatBinary(value, "");
	}
	
	public static String[] i2B(int[] values) {
		String[] binaries = new String[values.length];
		for (int i = 0; i < values.length; i++) {
			binaries[i] = i2B(values[i]);
		}
		return binaries;
	}
	
	public static String[] i2B(Integer[] values) {
		String[] binaries = new String[values.length];
		for (int i = 0; i < values.length; i++) {
			binaries[i] = i2B(values[i]);
		}
		return binaries;
	}
	
	/**
	 * 整型转换成为格式化的32位二进制字符串，主要用于可读日志输出
	 * 每4位用空格分隔，高位不足4位用0补齐
	 * 0000 0000 0000 0000 0000 0000 0000 0000
	 * @param value 整型值
	 * @return 二进制格式字符串
	 */
	public static String i2FB(int value) {
		return int2FormatBinary(value, null);
	}
	
	public static String[] i2FB(int[] values) {
		String[] binaries = new String[values.length];
		for (int i = 0; i < values.length; i++) {
			binaries[i] = i2FB(values[i]);
		}
		return binaries;
	}
	
	public static String[] i2FB(Integer[] values) {
		String[] binaries = new String[values.length];
		for (int i = 0; i < values.length; i++) {
			binaries[i] = i2FB(values[i]);
		}
		return binaries;
	}
	
	/**
	 * 左移填充1
	 * @param value 待左移的数
	 * @param bitCount 移动位数
	 * @return 结果
	 */
	public static int shiftLeftFill1(int value, int bitCount) {
		if (bitCount < 0) {
			throw new IllegalArgumentException("bitCount must be positive");
		} else if (bitCount == 0) {
			return value;
		} else if (bitCount >= 32) {
			return -1;
		}
		for (int i = 0; i < bitCount; i++) {
			value = (value << 1) | 1;
		}
		return value;
	}
	
	/**
	 * 将0左移填充1
	 * @param bitCount 移动位数
	 * @return 结果
	 */
	public static int shiftLeftFill1(int bitCount) {
		return shiftLeftFill1(0, bitCount);
	}
	
	public static void main(String[] args) {
		int v = shiftLeftFill1(0, 31);
		System.out.println(i2FB(v));
	}
	
}
