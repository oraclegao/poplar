package fun.pplm.framework.poplar.common.runnable;

import javax.annotation.PreDestroy;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 生产线工人线程框架
 * 对生产线工人按照一定流程反复完成类似任务这种场景的抽象线程框架
 * @author OracleGao
 *
 */
public abstract class LineWorker implements Runnable {
	private static Logger logger = LoggerFactory.getLogger(LineWorker.class);
	
	protected volatile boolean runFlag = false;
	
	/**
	 * 名称
	 * 默认值运行时类名
	 */
	protected volatile String name;
	
	/**
	 * 任务间歇的时间
	 * 线程启动之前和线程执行中可修改，并在接下来的间隔生效
	 * 单位毫秒，默认1000L
	 */
	protected volatile long interval = 1000L;
	
	private Object signal = new Object();
	
	public LineWorker() {
		super();
	}

	public LineWorker(String name) {
		super();
		this.name = name;
	}

	public LineWorker(long interval) {
		super();
		this.interval = interval;
	}

	public LineWorker(String name, long interval) {
		super();
		this.name = name;
		this.interval = interval;
	}
	
	/**
	 * 启动
	 */
	public synchronized void startup() {
		if (!runFlag) {
			if (StringUtils.isBlank(this.name)) {
				this.name = this.getClass().getSimpleName();
			}
			new Thread(this, name).start();
			this.runFlag = true;
			logger.debug("{}: 启动线程", name);
		}
	}
	
	/**
	 * 停止
	 */
	public synchronized void shutdown() {
		if(runFlag) {
			logger.debug("{}: 即将结束线程", name);
			runFlag = false;
            synchronized (signal) {
                signal.notifyAll();
            }
		}
	}
	
	@Override
	public final void run() {
		while(true) {
			if(!runFlag) {
				logger.debug("{}: 结束线程", name);
				break;
			}
			try {
				long startTime = System.currentTimeMillis();
				execute();
				long timeToWait = interval - (System.currentTimeMillis() - startTime);
				if (timeToWait > 0) {
		            // 等待时间间隔的到达
		            synchronized (signal) {
						if (!runFlag) {
							break;
						}
		                signal.wait(timeToWait);
		            }
		        }
			} catch (Exception e) {
				logger.error(name + "异常: " + e.getMessage(), e);
			}
		}
	}
	
	protected abstract void execute();

	public boolean running() {
		return runFlag;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public long getInterval() {
		return interval;
	}

	public void setInterval(long interval) {
		this.interval = interval;
	}

	/**
	 * 析构函数
	 * 线程停止后实例对象销毁前调用
	 */
	protected void finalized() {}
	
	@PreDestroy
	protected void destory() {
		if (runFlag) {
			shutdown();
		}
		finalized();
	}
	
}
