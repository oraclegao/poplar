package fun.pplm.framework.poplar.common.validator;

import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;

/**
 * 
 * bean实体属性校验，使用spring validator
 * @author OracleGao
 * create 2022年7月3日
 *
 */
public class BeanValidator {
	private Validator validator;
	
	public BeanValidator(Validator validator) {
		super();
		this.validator = validator;
	}
	
	public void validate(Object bean) throws ValidationException {
		Set<ConstraintViolation<Object>> cvSet = validator.validate(bean);
		if (cvSet.size() > 0) {
			String message = cvSet.stream().map(cv->cv.getMessage()).collect(Collectors.joining(","));
			throw new ValidationException(message);
		}
	}
	
}
