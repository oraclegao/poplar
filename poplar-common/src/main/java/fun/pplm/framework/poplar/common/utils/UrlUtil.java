package fun.pplm.framework.poplar.common.utils;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * url工具类
 * @author OracleGao
 *
 */
public class UrlUtil {
	/**
	 * 获取按照字典排序的url参数字符串
	 * 对于参数是数组的情况，使用a=v1&amp;a=v2&amp;...的形式组装
	 * @param paramMap 参数map, value类型只能是string,string[]或Collection&lt;String&gt;，忽略map值是null的参数
     * @return 排好序的参数名值字符串
	 */
    public static String getLexSortedParamStr(Map<String, ? extends Object> paramMap) {
    	Set<String> keySet = paramMap.keySet();
    	List<String> keys = new ArrayList<>(keySet);
    	Collections.sort(keys);
    	StringBuilder stringBuilder = new StringBuilder();
    	Object obj = null;
    	String[] values = null;
    	Collection<?> vals = null;
    	String val = null;
        for (String key : keys) {
        	obj = paramMap.get(key);
        	if (obj != null) {
        		if (obj instanceof String) {
        			stringBuilder.append(key).append("=").append((String)obj).append("&");
        		} else if (obj instanceof String[]) {
            		values = (String[]) obj;
            		if (values.length > 0) {
                		for (String value : values) {
                			if (value != null) {
                				stringBuilder.append(key).append("=").append(value).append("&");
                			}
                		}
                	}
            	} else if (obj instanceof Collection<?>) {
            		vals = (Collection<?>) obj;
            		if (vals.size() > 0) {
            			for (Object object : vals) {
                			if (object != null) {
                				val = object.toString();
	                			if(val != null) {
	                				stringBuilder.append(key).append("=").append(val).append("&");
	                			}
                			}
                		}
            		}
            	}
        	}
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        String str = stringBuilder.toString();
        return str;
    }
    
    /**
     * 解析url查询参数
     * @param url url字符串
     * @return 查询参数map
     */
    public static Map<String, List<String>> parseQueryParams(String url) {
    	URI uri = URI.create(url);
    	Map<String, List<String>> paramMap = new HashMap<>();
    	String queryParams = uri.getQuery();
    	String[] keyValue = null;
    	List<String> values = null;
    	for (String param : queryParams.split("&")) {
    		keyValue = param.split("=", 2);
    		if (StringUtils.isNotBlank(keyValue[0])) {
    			if (paramMap.containsKey(keyValue[0])) {
    				values = paramMap.get(keyValue[0]);    				
    			} else {
    				values = new ArrayList<>();
    				paramMap.put(keyValue[0], values);
    			}
    			values.add(keyValue[1]);
    		}
    	}
    	return paramMap;
    }
    
}
