package fun.pplm.framework.poplar.common.utils;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fun.pplm.framework.poplar.common.config.ThreadPoolConfig;

/**
 * 
 * 线程池工具类
 * @author OracleGao
 *
 */
public class ThreadPoolUtil {
	private static Logger logger = LoggerFactory.getLogger(ThreadPoolUtil.class);

	public static ThreadPoolExecutor newThreadPool(ThreadPoolConfig config) {
		logger.debug("创建线程池开始: {}", config);
		ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(config.getCorePoolSize(),
				config.getMaximumPoolSize(), 
				config.getKeepAliveTimeSecond(), 
				TimeUnit.SECONDS,
				new ArrayBlockingQueue<>(config.getQueueCapacity()), 
				config.getRejectedPolicy());
		logger.debug("创建线程池成功");
		return threadPoolExecutor;
	}

	public static void destoryThreadPool(ExecutorService executorService, ThreadPoolConfig config) {
		if (executorService != null) {
			executorService.shutdown();
			int timeoutCount = 0;
			while (!executorService.isTerminated()) {
				try {
					Thread.sleep(config.getDestoryCountDownTimeout());
					if (++timeoutCount >= config.getDestoryCountDown()) {
						break;
					}
				} catch (InterruptedException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
	}

}
