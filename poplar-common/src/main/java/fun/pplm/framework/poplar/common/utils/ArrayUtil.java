package fun.pplm.framework.poplar.common.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 数组工具类
 * @author OracleGao
 *
 */
public class ArrayUtil {
	/**
	 * 二维数组按行展开成一维列表
	 * @param <T> 元素类型
	 * @param data2D 二维数组数据
	 * @return 按行展开后的列表
	 */
	public static <T> List<T> expandedByRow(T[][] data2D) {
		List<T> data = new ArrayList<>();
		for (T[] values : data2D) {
			for (T value : values) {
				data.add(value);
			}
		}
		return data;
	}
	
	/**
	 * 二维int数组按行展开成一维int数组
	 * @param data2D 二维int数组
	 * @return 按行展开后的一维int数组
	 */
	public static int[] expandedByRow(int[][] data2D) {
		List<Integer> dataList = new ArrayList<>();
		for (int[] values : data2D) {
			for (int value: values) {
				dataList.add(value);
			}
		}
		int length = dataList.size();
		int[] data = new int[length];
		for (int i = 0; i < length; i++) {
			data[i] = dataList.get(i);
		}
		return data;
	}
	
	/**
	 * 二维数组按列展开成一维列表
	 * @param <T> 元素类型
	 * @param data2D 二维数组数据
	 * @return 按列展开后的列表
	 */
	public static <T> List<T> expandedByCol(T[][] data2D) {
		int maxLength = 0;
		for (int i = 0; i < data2D.length; i++) {
			if (data2D[i].length > maxLength) {
				maxLength = data2D[i].length;
			}
		}
		List<T> data = new ArrayList<>();
		for (int j = 0; j < maxLength; j++) {
			for (int i = 0; i < data2D.length; i++) {
				if(j < data2D[i].length) {
					data.add(data2D[i][j]);
				}
			}
		}
		return data;
	}

}
