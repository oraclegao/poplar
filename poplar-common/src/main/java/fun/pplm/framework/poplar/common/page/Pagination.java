package fun.pplm.framework.poplar.common.page;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Collection;

/**
 * 
 * @author OracleGao
 *
 * @param <T> 分页对象泛型 
 * 
 */
@ApiModel(value = "pagination", description = "分页实体")
public class Pagination<T> {
    @ApiModelProperty(value = "页码",  position = 0)
    private int pageNum;
    @ApiModelProperty(value = "每页记录数", position = 1)
    private int pageSize;
    @ApiModelProperty(value = "总记录数", position = 2)
    private long total;
    @ApiModelProperty(value = "总页数", position = 3)
    private int pages;
    @ApiModelProperty(value = "返回对象", position = 4)
    private Collection<T> data;

    public Pagination() {
		super();
	}

    private Pagination(Collection<T> data, long total, PageQueryParam pageQueryParam) {
    	super();
    	this.pageNum = pageQueryParam.getPageNum();
    	this.pageSize = data.size();
    	this.total = total;
    	this.pages = (int)Math.ceil(total * 1.0 / pageQueryParam.getPageSize());
    	this.data = data;
    }
    
    private Pagination(int pageNum, int pageSize, long total, int pages, Collection<T> data) {
		super();
		this.pageNum = pageNum;
		this.pageSize = pageSize;
		this.total = total;
		this.pages = pages;
		this.data = data;
	}
    
	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public Collection<T> getData() {
		return data;
	}

	public void setData(Collection<T> data) {
		this.data = data;
	}

	public static <T> Pagination<T> create(Collection<T> data, long total, PageQueryParam pageQueryParam) {
		return new Pagination<T>(data, total, pageQueryParam);
	}
	
	public static <T> Pagination<T> create(int pageNum, int pageSize, long total, int pages, Collection<T> data) {
		return new Pagination<T>(pageNum, pageSize, total, pages, data);
	}
	
}