package fun.pplm.framework.poplar.common.validator.handler;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import fun.pplm.framework.poplar.common.validator.annotation.Item;

/**
 * 
 * 字符串枚举校验句柄类
 * @author OracleGao
 *
 */
public class ItemValidatorHandler implements ConstraintValidator<Item, CharSequence> {

	private String[] values;
	private boolean caseSensitive;

    @Override
    public void initialize(Item item) {
    	values = item.value();
    	caseSensitive = item.caseSensitive();
    }
	
	@Override
	public boolean isValid(CharSequence cs, ConstraintValidatorContext context) {
		if (cs == null || cs.length() == 0) {
			return true;
		}
		String value = cs.toString();
		if (caseSensitive) {
			for (String val : this.values) {
				if (val.equals(value)) {
					return true;
				}
			}
		} else {
			for (String val : this.values) {
				if (val.equalsIgnoreCase(value)) {
					return true;
				}
			}
		}
		return false;
	}

}
