package fun.pplm.framework.poplar.common.utils;

import java.nio.file.FileSystems;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.List;

/**
 * 
 * @author XuQiang, OracleGao
 * 
 */
public class PathUtil {
    /**
     * 获取路径分段值
     * @param path 完整路径
     * @param num 第几分段
     * @return 分段字符串
     */
    public static String segment(String path, int num) {
        return path.split("/")[num];
    }

    /**
     * 路径分段中是否存在某个字符串
     * @param path 路径
     * @param key 匹配的字符串
     * @return 是否存在
     */
    public static boolean exist(String path, String key) {
        String[] paths = path.split("/");
        for (String p : paths) {
            if (p.equals(key)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 多规则匹配
     * @param rules 规则集合
     * @param path 路径
     * @return 是否匹配
     */
    public static boolean matches(List<String> rules, String path) {
        for(String rule : rules){
            if (matches(rule, path)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 路径匹配
     * @param rule 规则
     * @param path 路径
     * @return 是否匹配
     */
    public static boolean matches(String rule, String path) {
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:" + rule);
        return matcher.matches(Paths.get(path));
    }

    /**
     * 当前path是否需要校验
     * @param path 判断的路径
     * @param ignoreRules 忽略的规则路径集合
     * @return  当前path是否需要校验
     */
    public static boolean checkPath(String path, List<String> ignoreRules) {
        //判断是否忽略
        if (ignoreRules.size() > 0 && PathUtil.matches(ignoreRules, path)) {
            //忽略路径
            return false;
        } else {
            //需要校验
            return true;
        }
    }
    
}
