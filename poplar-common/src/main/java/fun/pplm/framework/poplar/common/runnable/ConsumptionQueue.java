package fun.pplm.framework.poplar.common.runnable;

/**
 * 
 * 消费队列
 * 
 * @author OracleGao
 *
 * @param <E> 队列元素
 */
public abstract class ConsumptionQueue<E> extends ConvertionQueue<E, E> {

	
	public ConsumptionQueue() {
		super();
	}

	public ConsumptionQueue(long interval) {
		super(interval);
	}

	public ConsumptionQueue(String name, long interval) {
		super(name, interval);
	}

	public ConsumptionQueue(String name) {
		super(name);
	}

	public ConsumptionQueue(int queueSize, long interval) {
		super(queueSize, interval);
	}

	public ConsumptionQueue(int queueSize, String name, long interval) {
		super(queueSize, name, interval);
	}

	public ConsumptionQueue(int queueSize, String name) {
		super(queueSize, name);
	}

	public ConsumptionQueue(int queueSize) {
		super(queueSize);
	}

	@Override
	protected E getConsumed(E e) throws Exception {
		return e;
	}
	
}
