package fun.pplm.framework.poplar.common.validator.handler;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import fun.pplm.framework.poplar.common.validator.annotation.IntItem;

/**
 * 
 * 整型枚举校验句柄类
 * @author OracleGao
 *
 */
public class IntItemValidatorHandler implements ConstraintValidator<IntItem, Integer> {
	private int[] values;
	
    @Override
    public void initialize(IntItem intItem) {
    	values = intItem.value();
    }

	@Override
	public boolean isValid(Integer value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		for (int v : values) {
			if (v == value) {
				return true;
			}
		}
		return false;
	}

}
