package fun.pplm.framework.poplar.common.validator.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import fun.pplm.framework.poplar.common.validator.handler.ItemValidatorHandler;

/**
 * 
 * 字符串枚举校验注释符
 * @author OracleGao
 *
 */
@Documented
@Constraint(validatedBy = {ItemValidatorHandler.class })
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
public @interface Item {
	
	/**
	 * 字符串枚举校验值
	 * @return 空字符串数组
	 */
	String[] value() default {};
	
	/**
	 * 比对的字符串枚举值是否大小写敏感
	 * 默认true敏感
	 * @return true敏感，false不敏感
	 */
	boolean caseSensitive() default true;
	
	String message() default "value not in item values";
	
	Class<?>[] groups() default {};
	
	Class<? extends Payload>[] payload() default {};

}
