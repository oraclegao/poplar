package fun.pplm.framework.poplar.common.runnable;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * 类型转换消费队列，结合生产线工人框架的LinkedBlockingQueue的实现
 * O类型会被转成P类型存入队列，P类型对象会被消费
 * 队列空就会空闲，队列中有数据会在空闲间隔后消费
 * 
 * @author OracleGao
 *
 * @param <O> offer类型 进入队列类型
 * @param <P> polling类型 消费类型
 */
public abstract class ConvertionQueue<O, P> extends LineWorker {
	private static Logger logger = LoggerFactory.getLogger(ConvertionQueue.class);
	
	/**
	 * 队列元素数量上限
	 */
	public static final int QUEUE_MAX_SIZE_DEFAULT = 100;
	
	/**
	 * 消费队列元素数量上限
	 */
	private int queueMaxSize;
	
	/**
	 * 消费队列
	 */
	protected volatile BlockingQueue<P> queue;
	
	public ConvertionQueue() {
		super();
		this.queueMaxSize = QUEUE_MAX_SIZE_DEFAULT;
		this.queue = new LinkedBlockingQueue<>(queueMaxSize);
	}
	
	public ConvertionQueue(long interval) {
		super(interval);
	}

	public ConvertionQueue(String name, long interval) {
		super(name, interval);
	}

	public ConvertionQueue(String name) {
		super(name);
	}

	public ConvertionQueue(int queueMaxSize) {
		super();
		this.queueMaxSize = queueMaxSize;
		this.queue = new LinkedBlockingQueue<>(queueMaxSize);
	}

	public ConvertionQueue(int queueMaxSize, long interval) {
		super(interval);
		this.queueMaxSize = queueMaxSize;
		this.queue = new LinkedBlockingQueue<>(queueMaxSize);
	}

	public ConvertionQueue(int queueMaxSize, String name, long interval) {
		super(name, interval);
		this.queueMaxSize = queueMaxSize;
		this.queue = new LinkedBlockingQueue<>(queueMaxSize);
	}

	public ConvertionQueue(int queueMaxSize, String name) {
		super(name);
		this.queueMaxSize = queueMaxSize;
		this.queue = new LinkedBlockingQueue<>(queueMaxSize);
	}

	/**
	 * 启动
	 */
	public synchronized void startup() {
		//启动时设置的队列上限与当前队列上限不一致时，重新创建队列。在消费线程运行过程中队列大小不可变。
		if (queue.size() + queue.remainingCapacity() != queueMaxSize) {
			BlockingQueue<P> temp = queue;
			queue = new LinkedBlockingQueue<>(queueMaxSize);
			P p = null;
			int i = 0;
			while((p = temp.poll()) != null) {
				if (i < queueMaxSize) {
					queue.offer(p);
				} else {
					logger.warn("队列已满，忽略超过长度的元素: {}", Json.string(p));
				}
				i++;
			}
		}
		super.startup();
	}
	
	@Override
	protected final void execute() {
		P p = queue.poll();
		if (p != null) {
			logger.debug("{}: 取到新元素, 开始消费", name);
			consume(p);
			logger.debug("{}: 消费完成", name);
		} else {
			logger.debug("{}: 没有新元素, 调用idle", name);
			idle();
			logger.debug("{}: idle完成", name);
		}
	}
	
	public boolean offer(O o) throws Exception {
		if (!super.runFlag) {
			logger.warn("{}: 消费线程没有启动, 元素将堆积在队列中");
		}
		return queue.offer(getConsumed(o));
	}
	
	public boolean offer(O o, long timeout, TimeUnit timeUnit) throws Exception {
		if (!super.runFlag) {
			logger.warn("{}: 消费线程没有启动, 元素将堆积在队列中", name);
		}
		return queue.offer(getConsumed(o), timeout, timeUnit);
	}
	
	public int getQueueMaxSize() {
		return queueMaxSize;
	}

	public void setQueueMaxSize(int queueMaxSize) {
		this.queueMaxSize = queueMaxSize;
	}

	/**
	 * 队列当前元素数量
	 * @return 队列当前元素数量
	 */
	public int getQueueSize() {
		return queue.size();
	}
	
	/**
	 * 队列剩余元素容量
	 * @return 队列剩余容量数量
	 */
	public int remainingCapacity() {
		return queue.remainingCapacity();
	}
	
	protected abstract P getConsumed(O o) throws Exception;
	
	protected abstract void consume(P p);
	
	/**
	 * 无元素消费时的idle回调方法
	 */
	protected void idle() {}
	
	@Override
	protected void finalized() {
		if (queue.size() > 0) {
			finalized(queue);
		}
	}
	
	/**
	 * 析构函数
	 * 线程结束后，对象销毁前处理队列中的元素
	 * @param queue 待处理队列
	 */
	protected void finalized(BlockingQueue<P> queue) {}
	
}
