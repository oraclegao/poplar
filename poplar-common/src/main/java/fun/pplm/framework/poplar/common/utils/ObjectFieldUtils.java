package fun.pplm.framework.poplar.common.utils;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ReflectionUtils;

/**
 * 
 * 反射管理对象成员属性
 * @author OracleGao
 *
 */
public class ObjectFieldUtils {
	
	public static final String FIELD_TYPE_COLLECTION = "[]";
	
	/**
	 * @param object 要设置值的对象
	 * @param fieldName 成员名称，支持多级结构 c1f1.c2f1, 支持集合设置值 c1fs.[].c2f1, [].c2f1
	 * @param value 设置的值
	 */
	public static void setFieldValue(Object object, String fieldName, Object value) {
		if (object == null) {
			throw new NullPointerException("object null");
		}
		if (StringUtils.isBlank(fieldName)) {
			throw new RuntimeException("fieldName blank");
		}
		String[] fieldNames = fieldName.split("\\.");
		LinkedList<String> fieldNameQueue = new LinkedList<>();
		for (String fn : fieldNames) {
			if (StringUtils.isBlank(fn)) {
				throw new RuntimeException("invalid fieldName: " + fieldName);
			}
			fieldNameQueue.add(fn);
		}
		setFieldValue(object, fieldNameQueue, value);
	}
	
	public static void setFieldValue(Object object, LinkedList<String> fieldNameQueue, Object value) {
		if (fieldNameQueue == null) {
			throw new NullPointerException("fieldNameQueue null");
		}
		if (fieldNameQueue.size() < 1) {
			throw new RuntimeException("fieldNameQueue empty");
		}
		String fieldName = fieldNameQueue.removeFirst();
		if (FIELD_TYPE_COLLECTION.equals(fieldName)) {
			Collection<?> collection = null;
			if (object.getClass().isArray()) {
				collection = Arrays.asList(object);
			} else if (object instanceof Collection) {
				collection = Collection.class.cast(object);
			}
			if (fieldNameQueue.isEmpty()) {
				throw new RuntimeException("invalid fieldName");
			}
			for (Object obj : collection) {
				setFieldValue(obj, new LinkedList<String>(fieldNameQueue), value);
			}
		} else {
			if (fieldNameQueue.isEmpty()) {
				Field field = ReflectionUtils.findField(object.getClass(), fieldName);
				ReflectionUtils.makeAccessible(field);
				ReflectionUtils.setField(field, object, value);
			} else {
				Object obj = getFieldValue(object, fieldName);
				setFieldValue(obj, fieldNameQueue, value);
			}
		}
	}
	
	public static Object getFieldValue(Object object, String fieldName) {
		Field field = ReflectionUtils.findField(object.getClass(), fieldName);
		if (field != null) {
			ReflectionUtils.makeAccessible(field);
			return ReflectionUtils.getField(field, object);
		}
		return null;
	}

	@SuppressWarnings("unused")
	private static Field getField(Object object, String fieldName) {
		if (object == null) {
			throw new NullPointerException("object null");
		}
		if (StringUtils.isBlank(fieldName)) {
			throw new RuntimeException("fieldName blank");
		}
		String[] fieldNames = fieldName.split("\\.");
		LinkedList<String> fieldNameQueue = new LinkedList<>();
		for (String fn : fieldNames) {
			if (StringUtils.isBlank(fn)) {
				throw new RuntimeException("invalid fieldName: " + fieldName);
			}
			fieldNameQueue.add(fn);
		}
		return getField(object, fieldNameQueue);
	}
	
	private static Field getField(Object object, LinkedList<String> fieldNameQueue) {
		if (fieldNameQueue == null) {
			throw new NullPointerException("fieldNameQueue null");
		}
		if (fieldNameQueue.size() < 1) {
			throw new RuntimeException("fieldNameQueue empty");
		}
		String fieldName = fieldNameQueue.removeFirst();
		if (FIELD_TYPE_COLLECTION.equals(fieldName)) {
		}
		return null;
	}
	
}
