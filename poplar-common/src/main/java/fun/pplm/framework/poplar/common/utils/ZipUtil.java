package fun.pplm.framework.poplar.common.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;

/**
 * 
 * zip工具类
 * @author OracleGao
 *
 */
public class ZipUtil {

	/**
	 * 替换zip压缩包流中的文件
	 * @param zipInputStream zip输入流
	 * @param replacedMap 文件替换map 压缩包文件文件uri-&gt;替换文件输入流
	 * @return zip输出字节数组流
	 * @throws IOException io异常
	 */
	public static ByteArrayOutputStream zipInsideFileReplace(ZipInputStream zipInputStream, Map<String, InputStream> replacedMap) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try (
        	ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream);
        ) {
        	zipInsideFileReplace(zipInputStream, zipOutputStream, replacedMap);
			zipOutputStream.finish();
		}
        return byteArrayOutputStream;
	}
	
	/**
	 * 替换zip压缩包流中的文件
	 * @param zipInputStream zip输入流
	 * @param zipOutputStream zip输出流
	 * @param replacedMap 文件替换map 压缩包文件文件uri-&gt;替换文件输入流
	 * @throws IOException io异常
	 */
	public static void zipInsideFileReplace(ZipInputStream zipInputStream, ZipOutputStream zipOutputStream, Map<String, InputStream> replacedMap) throws IOException {
        try {
			ZipEntry zipEntry = null;
			String zipEntryName = null;
			while((zipEntry = zipInputStream.getNextEntry()) != null) {
                zipEntryName = zipEntry.getName();
                if (!zipEntry.isDirectory() && replacedMap.containsKey(zipEntryName)) {
                	zipOutputStream.putNextEntry(new ZipEntry(zipEntryName));
                	IOUtils.copy(replacedMap.get(zipEntryName), zipOutputStream);
                	zipOutputStream.closeEntry();
                } else {
                	zipOutputStream.putNextEntry(new ZipEntry(zipEntryName));
                	IOUtils.copy(zipInputStream, zipOutputStream);
                    zipOutputStream.closeEntry();
                }
                zipInputStream.closeEntry();
			}
		} finally {
			zipOutputStream.flush();
		}
	}
	
}
