package fun.pplm.framework.poplar.common.runnable;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * json字符串反序列化消费队列
 * 
 * @author OracleGao
 *
 * @param <E> 泛型
 */
public abstract class JsonConvertionQueue<E> extends ConvertionQueue<String, E> {

	@Autowired
	private ObjectMapper objectMapper;
	
	public JsonConvertionQueue(int queueSize, long interval) {
		super(queueSize, interval);
	}

	public JsonConvertionQueue(int queueSize, String name, long interval) {
		super(queueSize, name, interval);
	}

	public JsonConvertionQueue(int queueSize, String name) {
		super(queueSize, name);
	}

	public JsonConvertionQueue(int queueSize) {
		super(queueSize);
	}

	@SuppressWarnings("unchecked")
	protected E getConsumed(String json) throws IOException {
		String typeName = Json.getGenericTypeName(this, JsonConvertionQueue.class, 0);
		E e = null;
		if ("java.lang.String".equals(typeName)) {
			e = (E)json;
		} else {
			JavaType javaType = objectMapper.getTypeFactory().constructFromCanonical(typeName);
			e = objectMapper.readValue(json, javaType);
		}
		return e;
	}
	
}
