package fun.pplm.framework.poplar.common.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.redis.core.ClusterOperations;
import org.springframework.data.redis.core.GeoOperations;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.ZSetOperations;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;

import fun.pplm.framework.poplar.json.Json;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

/**
 * 
 * @author OracleGao
 *
 */
@ConditionalOnProperty("spring.redis.host")
public class RedisService {
	private static Logger logger = LoggerFactory.getLogger(RedisService.class);
	
	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	
	@PostConstruct
	private void init() {
		logger.debug(this.getClass().getSimpleName() + "注入成功");
	}

	public void set(String key, Object value) {
		stringRedisTemplate.opsForValue().set(key, Json.string(value));
	}

	public String get(String key) {
		return stringRedisTemplate.opsForValue().get(key);
	}

	public <T> T get(String key, Class<T> clazz) {
		String value = stringRedisTemplate.opsForValue().get(key);
		if (value == null) {
			return null;
		}
		return Json.bean(value, clazz);
	}

	public <T> T get(String key, JavaType javaType) {
		String value = stringRedisTemplate.opsForValue().get(key);
		if (value == null) {
			return null;
		}
		return Json.bean(value, javaType);
	}

	public <T> T get(String key, TypeReference<T> typeReference) {
		String value = stringRedisTemplate.opsForValue().get(key);
		if (value == null) {
			return null;
		}
		return Json.bean(value, typeReference);
	}

	public <T> T getHashObject(String key, String hashKey, Class<T> clazz) {
		String value = getHashObject(key, hashKey);
		if (value == null) {
			return null;
		}
		return Json.bean(value, clazz);
	}

	public <T> T getHashObject(String key, String hashKey, JavaType javaType) {
		String value = getHashObject(key, hashKey);
		if (value == null) {
			return null;
		}
		return Json.bean(value, javaType);
	}

	public <T> T getHashObject(String key, String hashKey, TypeReference<T> typeReference) {
		String value = getHashObject(key, hashKey);
		if (value == null) {
			return null;
		}
		return Json.bean(value, typeReference);
	}

	public String getHashObject(String key, String hashKey) {
		Object value = stringRedisTemplate.opsForHash().get(key, hashKey);
		if (value == null) {
			return null;
		}
		return value.toString();
	}

	/**
	 * 秒超时
	 * key不存在时设置会失败，返回false
	 * @param key redis key
	 * @param timeout 过期时间 单位秒
	 * @return 是否设置成功
	 */
	public boolean expire(String key, long timeout) {
		return expire(key, timeout, TimeUnit.SECONDS);
	}
	
	/**
	 * 超时
	 * key不存在时设置会失败，返回false
	 * @param key redis key
	 * @param timeout 过期时间
	 * @param timeUnit 过期时间单位
	 * @return 是否设置成功
	 */
	public boolean expire(String key, long timeout, TimeUnit timeUnit) {
		return stringRedisTemplate.expire(key, timeout, timeUnit);
	}
	
	/**
	 * 毫秒超时
	 * key不存时设置会失败，返回false
	 * @param key redis key
	 * @param timeout 过期时间 单位毫秒
	 * @return 是否设置成功
	 */
	public boolean expireMs(String key, long timeout) {
		return stringRedisTemplate.expire(key, timeout, TimeUnit.MILLISECONDS);
	}

	public boolean exists(String key) {
		return stringRedisTemplate.hasKey(key);
	}

	/**
	 * 创建值为空字符串的key并设置过期时间
	 * 如果key已经存在则更新过期时间
	 * @param key key
	 * @param expireTime 过期时间，单位秒
	 */
	public void setExpire(final String key, long expireTime) {
		if (!expire(key, expireTime)) {
			setExpire(key, "", expireTime);
		}
	}
	
	/**
	 * 设置具有过期时间的key
	 * 如果key不存在则创建key/value使用空字符串
	 * 如果key已经存在则更新过期时间
	 * @param key key
	 * @param expireTime 过期时间
	 * @param timeUnit 过期时间单位
	 */
	public void setExpire(final String key, long expireTime, TimeUnit timeUnit) {
		if (!expire(key, expireTime, timeUnit)) {
			setExpire(key, "", expireTime, timeUnit);
		}
	}
	
	/**
	 * 设置具有过期时间的key/value对
	 * @param key key
	 * @param value value，如果是String类型，则不进行json序列化，否则先进行json序列化然后写入.详见Json.string()
	 * @param expireTime 过期时间，单位秒
	 */
	public void setExpire(final String key, Object value, long expireTime) {
		setExpire(key, value, expireTime, TimeUnit.SECONDS);
	}
	
	/**
	 * 设置具有过期时间的key/value对
	 * @param key key
	 * @param value value，如果是String类型，则不进行json序列化，否则先进行json序列化然后写入.详见Json.string()
	 * @param expireTime 过期时间
	 * @param timeUnit 过期时间单位
	 */
	public void setExpire(final String key, Object value, long expireTime, TimeUnit timeUnit) {
		stringRedisTemplate.opsForValue().set(key, Json.string(value));
		stringRedisTemplate.expire(key, expireTime, timeUnit);
	}

	/**
	 * 
	 * 批量删除对应的value
	 * @param keys 批量删除的keys
	 * 
	 */
	public void remove(final String... keys) {
		for (String key : keys) {
			remove(key);
		}
	}

	/**
	 * 
	 * 批量删除key
	 * @param pattern 删除key的模式字符串
	 * 
	 */
	public void removePattern(final String pattern) {
		Set<String> keys = stringRedisTemplate.keys(pattern);
		if (keys.size() > 0)
			stringRedisTemplate.delete(keys);
	}

	/**
	 * 
	 * 删除对应的value
	 * @param key redis key
	 * 
	 */
	public void remove(final String key) {
		if (exists(key)) {
			stringRedisTemplate.delete(key);
		}
	}

	public ValueOperations<String, String> opsForValue() {
		return stringRedisTemplate.opsForValue();
	}

	public SetOperations<String, String> opsForSet() {
		return stringRedisTemplate.opsForSet();
	}

	public ZSetOperations<String, String> opsForZSet() {
		return stringRedisTemplate.opsForZSet();
	}

	public ListOperations<String, String> opsForList () {
		return stringRedisTemplate.opsForList();
	}
	
	public HashOperations<String, Object, Object> opsForHash() {
		return stringRedisTemplate.opsForHash();
	}
	
	public GeoOperations<String, String> opsForGeo() {
		return stringRedisTemplate.opsForGeo();
	}
	
	public ClusterOperations<String, String> opsForCluster() {
		return stringRedisTemplate.opsForCluster();
	}
	
	public StringRedisTemplate getStringRedisTemplate() {
		return stringRedisTemplate;
	}

	public void setStringRedisTemplate(StringRedisTemplate stringRedisTemplate) {
		this.stringRedisTemplate = stringRedisTemplate;
	}

}
