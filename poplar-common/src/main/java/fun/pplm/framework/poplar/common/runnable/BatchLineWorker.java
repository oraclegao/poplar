package fun.pplm.framework.poplar.common.runnable;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 批处理生产线工人
 * @author OracleGao
 *
 */
public abstract class BatchLineWorker<T> extends LineWorker {
	private static Logger logger = LoggerFactory.getLogger(BatchLineWorker.class);
	
	protected volatile CopyOnWriteArrayList<T> buffer = new CopyOnWriteArrayList<>();
	
	public BatchLineWorker() {
		super();
	}

	public BatchLineWorker(long interval) {
		super(interval);
	}

	public BatchLineWorker(String name, long interval) {
		super(name, interval);
	}

	public BatchLineWorker(String name) {
		super(name);
	}

	@Override
	protected void execute() {
		if (buffer.size() > 0) {
			logger.debug("{}: 缓存存在数据, 开始批处理", name);
			CopyOnWriteArrayList<T> data = buffer;
			buffer = new CopyOnWriteArrayList<>();
			batchProcess(data);
			logger.debug("{}: 批处理完成", name);
		} else {
			logger.debug("{}: 缓存数据为空, 调用idle", name);
			idle();
			logger.debug("{}: idle完成", name);
		}
	}
	
	/**
	 * 数据批处理回调方法
	 * @param data 数据
	 */
	protected abstract void batchProcess(List<T> data);
	
	@Override
	protected void finalized() {
		if (buffer!= null && buffer.size() > 0) {
			batchProcess(buffer);
		}
	}
	
	/**
	 * 无数据时的idle回调方法
	 */
	protected void idle() {}
	
	public void add(T e) {
		if (!super.runFlag) {
			logger.warn("{}: 批处理线程没有启动, 数据将堆积在缓存中");
		}
		buffer.add(e);
	}
	
	public void addAll(Collection<? extends T> es) {
		if (!super.runFlag) {
			logger.warn("{}: 批处理线程没有启动, 数据将堆积在缓存中");
		}
		buffer.addAll(es);
	}

	/**
	 * 当前缓存数据量
	 * @return 当前缓存中元素数量
	 */
	public int bufferSize() {
		return buffer.size();
	}
	
}
