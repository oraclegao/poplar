package fun.pplm.framework.poplar.common.utils;

import java.util.UUID;

/**
 * 
 * Uuid工具类
 * @author OracleGao
 *
 */
public class Uuid {
	
	/**
	 * 32位uuid 移除-号
	 * @return 32位uuid
	 */
	public static String uuid32() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
	/**
	 * 标准uuid 36位
	 * @return uuid
	 */
	public static String uuid() {
		return UUID.randomUUID().toString();
	}
	
}
