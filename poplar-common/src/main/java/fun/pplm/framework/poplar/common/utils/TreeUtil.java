package fun.pplm.framework.poplar.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 
 * @author ZhaoYujie, OracleGao
 * description: 树状结构工具类
 * created: 2020-12-23 18:21
 * 
 */
public class TreeUtil {
	private static Logger logger = LoggerFactory.getLogger(TreeUtil.class);
	
	/**
	 * 封装集合成树状结构（顶级节点的父id默认为0）,默认对象标识为id，父标识为pid，子节点集合是children
	 * @param <T> 树节点对象泛型
	 * @param nodes 原始对象集合
	 * @return 组装树状结构后的集合
	 */
	public static <T> List<T> buildTree(List<T> nodes) {
		return buildTree(nodes, "id", "pid", "children");
	}

	/**
	 * 封装集合成树状结构（不需要从顶级节点计算）
	 * @param nodes             原始对象集合
	 * @param idFieldName       对象属性ID名称
	 * @param pidFieldName      对象属性父ID名称
	 * @param childrenFieldName 子集合属性名称
	 * @param <T> 树节点对象泛型
	 * @return 组装树状结构后的集合
	 */
	public static <T> List<T> buildTree(List<T> nodes, String idFieldName, String pidFieldName,
			String childrenFieldName) {
		Map<String, List<T>> children = nodes.stream().collect(Collectors.groupingBy(node -> getFieldValueByFieldName(node, pidFieldName)));
		nodes.forEach(node -> {
			try {
				MethodUtils.invokeMethod(node, "set" + StringUtils.capitalize(childrenFieldName), children.get(getFieldValueByFieldName(node, idFieldName)));
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		});
		List<String> idFieldNameList = nodes.stream().map(node -> getFieldValueByFieldName(node, idFieldName)).collect(Collectors.toList());
		return nodes.stream().filter(node -> !idFieldNameList.contains(getFieldValueByFieldName(node, pidFieldName))).collect(Collectors.toList());
	}
	
	public static String getFieldValueByFieldName(Object object, String fieldName) {
		try {
			Field field = object.getClass().getDeclaredField(fieldName);
			// 对private的属性的访问
			field.setAccessible(true);
			return String.valueOf(field.get(object));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	/**
	 * @author OracleGao
	 * @param <T> 树节点泛型
	 */
	public static interface TreeNode<T extends TreeNode<T>> {
		public List<T> getChildren();
		public void setChildren(List<T> list);
	}
	
}
