package fun.pplm.framework.poplar.common.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * ascii工具类
 * @author OracleGao
 *
 */
public class AsciiUtil {

	/**
	 * 字符串转ascii整型数组
	 * @param str 字符串
	 * @return 整型数组
	 */
	public static List<Integer> str2AsciiInts(String str) {
		byte[] bytes = str.getBytes();
		List<Integer> data = new ArrayList<Integer>(bytes.length);
		for (Byte b : bytes) {
			data.add(b.intValue());
		}
		return data;
	}
	
}
