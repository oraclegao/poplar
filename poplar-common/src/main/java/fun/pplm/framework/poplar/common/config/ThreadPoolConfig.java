package fun.pplm.framework.poplar.common.config;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * 线程池配置
 * @author OracleGao
 *
 */
public class ThreadPoolConfig {
    /**
     * 线程池核心(常驻)线程数
     * 默认2如果cpu核数大于1，否则1 
     */
	private int corePoolSize = Runtime.getRuntime().availableProcessors() > 1 ? 2 : 1;
    /**
     * 线程池最大线程数
     * 默认cpu核数, Runtime.getRuntime().availableProcessors()
     */
    private int maximumPoolSize = Runtime.getRuntime().availableProcessors();
    /**
     * 非核心线程存活时间
     * 单位秒
     * 默认60
     */
    private long keepAliveTimeSecond = 60;
    /**
     * 线程池队列大小
     * 默认cpu核数 * 8, Runtime.getRuntime().availableProcessors() * 8
     */
    private int queueCapacity = Runtime.getRuntime().availableProcessors() * 8;
    /**
     * 拒绝策略
     * 默认java.util.concurrent.ThreadPoolExecutor$AbortPolicy
     */
    private RejectedExecutionHandler rejectedPolicy = new ThreadPoolExecutor.AbortPolicy();
    /**
     * 线程池销毁timeout次数
     * 默认5
     */
    private Integer destoryCountDown = 5;
    /**
     * 线程池销毁timeout时长
     * 单位毫秒
     * 默认1000L
     */
    private Long destoryCountDownTimeout = 1000L;
    
	public ThreadPoolConfig() {
		super();
	}

	public int getCorePoolSize() {
		return corePoolSize;
	}

	public void setCorePoolSize(int corePoolSize) {
		this.corePoolSize = corePoolSize;
	}

	public int getMaximumPoolSize() {
		return maximumPoolSize;
	}

	public void setMaximumPoolSize(int maximumPoolSize) {
		this.maximumPoolSize = maximumPoolSize;
	}

	public long getKeepAliveTimeSecond() {
		return keepAliveTimeSecond;
	}

	public void setKeepAliveTimeSecond(long keepAliveTimeSecond) {
		this.keepAliveTimeSecond = keepAliveTimeSecond;
	}

	public int getQueueCapacity() {
		return queueCapacity;
	}

	public void setQueueCapacity(int queueCapacity) {
		this.queueCapacity = queueCapacity;
	}

	public RejectedExecutionHandler getRejectedPolicy() {
		return rejectedPolicy;
	}

	public void setRejectedPolicy(RejectedExecutionHandler rejectedPolicy) {
		this.rejectedPolicy = rejectedPolicy;
	}

	public Integer getDestoryCountDown() {
		return destoryCountDown;
	}

	public void setDestoryCountDown(Integer destoryCountDown) {
		this.destoryCountDown = destoryCountDown;
	}

	public Long getDestoryCountDownTimeout() {
		return destoryCountDownTimeout;
	}

	public void setDestoryCountDownTimeout(Long destoryCountDownTimeout) {
		this.destoryCountDownTimeout = destoryCountDownTimeout;
	}
    
	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("corePoolSize", corePoolSize);
		map.put("maximumPoolSize", maximumPoolSize);
		map.put("keepAliveTimeSecond", keepAliveTimeSecond);
		map.put("queueCapacity", queueCapacity);
		map.put("rejectedPolicy", rejectedPolicy.getClass().getCanonicalName());
		map.put("destoryCountDown", destoryCountDown);
		map.put("destoryCountDownTimeout", destoryCountDownTimeout);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}

}
