package fun.pplm.framework.poplar.common.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * 字节处理工具
 * @author OracleGao
 *
 */
public class ByteUtil {
	/**
	 * CRC-16/CCITT-FALSE
	 */
	public static final int CRC16_POLYNOM_DEFAULT = 0xC86C;

	/**
	 * 计算 CRC16 校验码
	 * 多项式值使用 0xC86C
	 * @param data 字节数组
	 * @return 校验码
	 */
	public static int crc16(byte[] data) {
	    return crc16(data, CRC16_POLYNOM_DEFAULT);
	}
	
	/**
	 * 计算 CRC16 校验码
	 * @param data 字节数组
	 * @param polynom 多项式值
	 * @return 校验码
	 */
	public static int crc16(byte[] data, int polynom) {
	    int crc = 0xFFFF;
	    for (byte b : data) {
	        crc = crc ^ ((int) b << 8);
	        for (int j = 0; j < 8; ++j) {
	            if ((crc & 0x8000) != 0)
	                crc = (crc << 1) ^ polynom;
	            else
	                crc = (crc << 1);
	        }
	    }
	    return crc;
	}

	/**
	 * 验证 CRC16 校验码是否正确
	 * 多项式值使用 0xC86C
	 * @param data 字节数组
	 * @return true正确，false错误
	 */
	public static boolean crc16Verify(byte[] data) {
	    return crc16Verify(data, CRC16_POLYNOM_DEFAULT);
	}

	/**
	 * 验证 CRC16 校验码是否正确
	 * @param data 字节数组
	 * @param polynom 多项式值
	 * @return true正确，false错误
	 */
	public static boolean crc16Verify(byte[] data, int polynom) {
	    return crc16(data, polynom) == 0x0;
	}
	
	/**
	 * 在字节数组末尾添加 CRC16 校验码
	 * 多项式值使用 0xC86C
	 * @param data 字节数组
	 * @return 结尾添加CRC16校验码的字节数组
	 */
	public static byte[] crc16Append(byte[] data) {
	    return crc16Append(data, CRC16_POLYNOM_DEFAULT);
	}
	
	/**
	 * 在自己数组末尾添加 CRC16 校验码
	 * @param data 自己数组
	 * @param polynom 多项式值
	 * @return 字节数组
	 */
	public static byte[] crc16Append(byte[] data, int polynom) {
	    int crc = crc16(data, polynom);
	    byte[] result = Arrays.copyOf(data, data.length + 2);
	    result[result.length - 2] = (byte) ((crc >> 8) & 0xFF);
	    result[result.length - 1] = (byte) (crc & 0xFF);
	    return result;
	}
	
	/**
	 * 删除字节数组末尾的 CRC16 校验码
	 * @param data 待删除字节数组
	 * @return 字节数组
	 */
	public static byte[] crc16Delete(byte[] data) {
		return Arrays.copyOf(data, data.length - 2);
	}
	
	/**
	 * 整型转4字节数组
	 * @param val 整型值
	 * @return 4字节数组
	 */
	public static byte[] int2Bytes(int val) {  
        byte[] bytes = new byte[4];  
        bytes[0] = (byte) (val & 0xff);  
        bytes[1] = (byte) ((val >> 8) & 0xff);  
        bytes[2] = (byte) ((val >> 16) & 0xff);  
        bytes[3] = (byte) (val >>> 24);  
        return bytes;  
    }

	/**
	 * 字节转hex字符串，主要用户可读日志输出
	 * @param b 字节
	 * @return hex字符串
	 */
	public static String byte2Hex(byte b) {
		return String.format("%02X", b);
	}
	
	/**
	 * 字节数组转hex字符串，主要用于可读的日志输出
	 * @param bytes 字节数组
	 * @return hex字符串
	 */
	public static String bytes2Hexs(byte[] bytes) {
		return bytes2Hexs(bytes, 0, bytes.length);
	}
	
	/**
	 * 字节数组转hex字符串，主要用于可读的日志输出
	 * @param bytes 字节数组
	 * @param offset 字节数组头部偏移量
	 * @param count 字节数量
	 * @return hex字符串
	 */
	public static String bytes2Hexs(byte[] bytes, int offset, int count) {
		List<String> results = new ArrayList<>();
		for (int i = offset; i < offset + count; i++) {
			results.add(String.format("%02X", bytes[i]));
		}
		return Json.string(results);
	}
	
	/**
	 * 整型转字节数组再转hex字符串，主要用于可读的日志输出
	 * @param val 整型值
	 * @return hex字符串
	 */
	public static String int2Hexs(int val) {
		return bytes2Hexs(int2Bytes(val));
	}
	
	/**
	 * 源字节数组拷贝到目标字节数组
	 * @param src 源字节数组
	 * @param srcBeginIdx 源字节数组开始索引
	 * @param srcEndIdx 源字节数组结束索引
	 * @param dest 目标字节数组, 目标字节数组要确保空间足够，否则会抛出数据越界异常
	 * @param destBeginIdx 目标字节数组开始索引
	 * @return 拷贝的长度
	 */
	public int arrayCopy(byte[] src, int srcBeginIdx, int srcEndIdx, byte[] dest, int destBeginIdx) {
		for (int i = 0; i <= srcEndIdx - srcBeginIdx; i++) {
			dest[destBeginIdx + i] = src[srcBeginIdx + i];
		}
		return srcEndIdx - srcBeginIdx + 1;
	}
	
	/**
	 * 计算字节数组奇偶校验值
	 * @param data 字节数组
	 * @return 奇偶校验值
	 */
	public static byte parity(byte[] data) {
		return parity(data, 0, data.length);
	}
	
	/**
	 * 计算字节数组奇偶校验值
	 * @param data 字节数组
	 * @param offset 字节数组头部偏移量
	 * @param count 字节数量
	 * @return 奇偶校验值
	 */
	public static byte parity(byte[] data, int offset, int count) {
		byte parity = data[offset];
		for (int i = offset + 1; i < offset + count; i++) {
			parity ^= data[i];
		}
		return parity;
	}
	
	/**
	 * 计算字节数组奇偶校验值并添加到字节数组之后
	 * @param data 字节数组
	 * @return 结尾添加奇偶校验值的字节数组
	 */
	public static byte[] parityAppend(byte[] data) {
		byte parity = parity(data);
		byte[] result = Arrays.copyOf(data, data.length + 1);
		result[result.length - 1] = parity;
		return result;
	}
	
	/**
	 * 校验字节数组末尾奇偶校验值
	 * @param data 末尾是奇偶校验值的字节数组
	 * @return true正确，false错误
	 */
	public static boolean parityVerify(byte[] data) {
		return parity(data, 0, data.length - 1) == data[data.length - 1];
	}
	
	/**
	 * 校验字节数组奇偶校验值
	 * @param data 字节数组
	 * @param parity 奇偶校验值
	 * @return true正确，false错误
	 */
	public static boolean parityVerify(byte[] data, byte parity) {
		return parity(data) == parity;
	}
	
	/**
	 * 校验字节数组奇偶校验值
	 * @param data 字节数组
	 * @param offset 字节数组头部偏移量
	 * @param count 字节数量
	 * @param parity 奇偶校验值
	 * @return true正确，false错误
	 */
	public static boolean parityVerify(byte[] data, int offset, int count, byte parity) {
		return parity(data, offset, count) == parity;
	}
	
}
