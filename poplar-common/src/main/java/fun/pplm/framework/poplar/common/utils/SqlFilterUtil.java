package fun.pplm.framework.poplar.common.utils;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * sql条件过滤工具
 * 符合sql查询习惯
 * @author OracleGao
 *
 */
public class SqlFilterUtil {
	/**
	 * 等于
	 * 符合sql查询习惯，条件为空时返回true; 值为空时返回false
	 * @param <T> 比较类型
	 * @param value 值
	 * @param condition 条件
	 * @return value == condition
	 */
	public static <T extends Number & Comparable<? super T>> boolean eq(T value, T condition) {
		if (condition == null) {
			return true;
		}
		if (value == null) {
			return false;
		}
		return value.equals(condition); 
	}
	
	/**
	 * 不等于
	 * 符合sql查询习惯，条件为空时返回true; 值为空时返回false
	 * @param <T> 比较类型
	 * @param value 值
	 * @param condition 条件
	 * @return value != condition
	 */
	public static <T extends Number & Comparable<? super T>> boolean ne(T value, T condition) {
		if (condition == null) {
			return true;
		}
		if (value == null) {
			return false;
		}
		return !value.equals(condition); 
	}
	
	/**
	 * 大于
	 * 符合sql查询习惯，条件为空时返回true; 值为空时返回false
	 * @param <T> 比较类型
	 * @param value 值
	 * @param condition 条件
	 * @return value &gt; condition
	 */
	public static <T extends Number & Comparable<? super T>> boolean gt(T value, T condition) {
		if (condition == null) {
			return true;
		}
		if (value == null) {
			return false;
		}
		return value.compareTo(condition) > 0;
	}
	
	/**
	 * 大于等于
	 * 符合sql查询习惯，条件为空时返回true; 值为空时返回false
	 * @param <T> 比较类型
	 * @param value 值
	 * @param condition 条件
	 * @return value &gt;= condition
	 */
	public static <T extends Number & Comparable<? super T>> boolean ge(T value, T condition) {
		if (condition == null) {
			return true;
		}
		if (value == null) {
			return false;
		}
		return value.compareTo(condition) >= 0;
	}
	
	/**
	 * 小于 
	 * 符合sql查询习惯，条件为空时返回true; 值为空时返回false
	 * @param <T> 比较类型
	 * @param value 值
	 * @param condition 条件
	 * @return value &lt; condition
	 */
	public static <T extends Number & Comparable<? super T>> boolean lt(T value, T condition) {
		if (condition == null) {
			return true;
		}
		if (value == null) {
			return false;
		}
		return value.compareTo(condition) < 0;
	}
	
	/**
	 * 小于等于
	 * 符合sql查询习惯，条件为空时返回true; 值为空时返回false
	 * @param <T> 比较类型
	 * @param value 值
	 * @param condition 条件
	 * @return value &lt;= condition
	 */
	public static <T extends Number & Comparable<? super T>> boolean le(T value, T condition) {
		if (condition == null) {
			return true;
		}
		if (value == null) {
			return false;
		}
		return value.compareTo(condition) <= 0;
	}
	
	/**
	 * is null
	 * @param value 值
	 * @return value == null
	 */
	public static boolean isNull(Object value) {
		return value == null;
	}
	
	/**
	 * is not null
	 * @param value 值
	 * @return value != null
	 */
	public static boolean isNotNull(Object value) {
		return value != null;
	}
	
	/**
	 * is blank
	 * @param value 值
	 * @return StringUtils.isBlank(value)
	 */
	public static boolean isBlank(CharSequence value) {
		return StringUtils.isBlank(value);
	}
	
	/**
	 * is not blank
	 * @param value 值
	 * @return StringUtils.isNotBlank(value)
	 */
	public static boolean isNotBlank(CharSequence value) {
		return StringUtils.isNotBlank(value);
	}
	
	/**
	 * like '%值%'，全匹配模糊查询
	 * 符合sql查询习惯，条件为空时返回true; 值为空时返回false
	 * @param value 值
	 * @param condition 条件
	 * @return 匹配结果
	 */
	public static boolean like(CharSequence value, CharSequence condition) {
		if (StringUtils.isBlank(condition)) {
			return true;
		}
		if (StringUtils.isBlank(value)) {
			return false;
		}
		return value.toString().contains(condition);
	}
	
	/**
	 * like '%值'，左匹配模糊查询
	 * 符合sql查询习惯，条件为空时返回true; 值为空时返回false
	 * @param value 值
	 * @param condition 条件
	 * @return 匹配结果
	 */
	public static boolean likeLeft(CharSequence value, CharSequence condition) {
		if (StringUtils.isBlank(condition)) {
			return true;
		}
		if (StringUtils.isBlank(value)) {
			return false;
		}
		return value.toString().endsWith(condition.toString());
	}
	
	/**
	 * like '值%'，右匹配模糊查询
	 * 符合sql查询习惯，条件为空时返回true; 值为空时返回false
	 * @param value 值
	 * @param condition 条件
	 * @return 匹配结果
	 */
	public static boolean likeRight(CharSequence value, CharSequence condition) {
		if (StringUtils.isBlank(condition)) {
			return true;
		}
		if (StringUtils.isBlank(value)) {
			return false;
		}
		return value.toString().startsWith(condition.toString());
	}
	
	/**
	 * in 集合
	 * 符合sql查询习惯，条件为空时返回true; 值为空时返回false
	 * @param <T> 比较类型
	 * @param value 值
	 * @param condition 条件
	 * @return 匹配结果
	 */
	public static <T> boolean in(T value, Collection<T> condition) {
		if (condition == null || condition.isEmpty()) {
			return true;
		}
		if (value == null) {
			return false;
		}
		return condition.contains(value);
	}
	
	/**
	 * not in 集合
	 * 符合sql查询习惯，条件为空时返回true; 值为空时返回false
	 * @param <T> 比较类型
	 * @param value 值
	 * @param condition 条件
	 * @return 匹配结果
	 */
	public static <T> boolean notIn(T value, Collection<T> condition) {
		if (condition == null || condition.isEmpty()) {
			return true;
		}
		if (value == null) {
			return false;
		}
		return !condition.contains(value);
	}

}
