package fun.pplm.framework.poplar.common.page;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author OracleGao
 *
 */
@ApiModel(value = "分页查询参数")
public class PageQueryParam {
	@ApiModelProperty(name= "pageNum", value = "页号", required = false,  example = "1", dataType = "integer")
	private Integer pageNum = 1;
	@ApiModelProperty(name= "pageSize", value = "每页记录数", required = false, example = "10", dataType = "integer")
	private Integer pageSize = 10;

	public PageQueryParam() {
		super();
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	
}