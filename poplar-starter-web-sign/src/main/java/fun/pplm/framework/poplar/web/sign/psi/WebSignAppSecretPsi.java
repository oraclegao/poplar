package fun.pplm.framework.poplar.web.sign.psi;

/**
 * 
 * Poplar Service Interface
 * 签名密钥服务接口
 * @author OracleGao
 *
 */
public interface WebSignAppSecretPsi {
	/**
	 * 获取应用密钥
	 * @param appKey 应用key
	 * @return 应用密钥字符串
	 */
	public String getAppSecret(String appKey);
	
}
