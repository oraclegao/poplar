package fun.pplm.framework.poplar.web.sign.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.AntPathMatcher;

import fun.pplm.framework.poplar.common.utils.UrlUtil;
import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.web.resp.Resp;
import fun.pplm.framework.poplar.web.resp.RespCode;
import fun.pplm.framework.poplar.web.sign.config.WebSignConfig;
import fun.pplm.framework.poplar.web.sign.psi.WebSignAppSecretPsi;
import fun.pplm.framework.poplar.web.utils.HttpServletUtil;

/**
 * 
 * 接口签名过滤器
 * appKey和sign属性可以放在http请求头中也可以放在请求参数中
 * sign属性不参与签名计算
 * appKey如果放在http请求头中则不参与签名计算，如果放在参数中则必须参与签名计算
 * timestamp必须放到求情参数中，并且参与签名计算
 * @author OracleGao
 *
 */
@WebFilter(displayName = "signFilter", urlPatterns = "/*")
public class WebSignFilter implements Filter {
	private static Logger logger = LoggerFactory.getLogger(WebSignFilter.class);
	
	@Autowired
	private WebSignConfig webSignConfig;
	
	@Autowired
	private WebSignAppSecretPsi webSignAppSecretPsi;
	
	private AntPathMatcher antPathMatcher = new AntPathMatcher();
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		String requestURI = httpServletRequest.getRequestURI();
		List<String> whitePathPatterns = webSignConfig.getWhitePathPatterns();
		if(whitePathPatterns != null && whitePathPatterns.size() > 0) {
			for (String whitePathPattern : whitePathPatterns) {
				if (antPathMatcher.matchStart(whitePathPattern, requestURI)) {
					chain.doFilter(request, response);
					return;
				}
			}
		}
		String appKeyName = webSignConfig.getAppKeyName();
		String appKey = HttpServletUtil.getParamHeaderValue(httpServletRequest, appKeyName);
		if(StringUtils.isBlank(appKey)) {
			exception(appKeyName + " must not be blank", httpServletResponse);
			return;
		}
		String timestampName = webSignConfig.getTimestampName();
		String timestampValue = null;
		Long timestamp = null;
		try {
			timestampValue = HttpServletUtil.getParamHeaderValue(httpServletRequest, timestampName);
			if (StringUtils.isBlank(timestampValue)) {
				exception(timestampName + " must not be blank", httpServletResponse);
				return;
			}
			timestamp = Long.parseLong(timestampValue);
		} catch(Exception e) {
			exception("invalid " + timestampName, httpServletResponse);
			return;
		}
		String signName = webSignConfig.getSignName();
		String sign = HttpServletUtil.getParamHeaderValue(httpServletRequest, signName);
		if(StringUtils.isBlank(sign)) {
			exception(signName + " must not be blank", httpServletResponse);
			return;
		}
		long duration = Math.abs((System.currentTimeMillis() - timestamp) / 1000);
		if (duration > webSignConfig.getTimeoutSecond()) {
			exception("signature expired", httpServletResponse);
			return;
		}
		
		Map<String, Object> paramMap = new HashMap<>();
		Map<String, String[]> requestParameterMap = httpServletRequest.getParameterMap();
		requestParameterMap.keySet().stream()
			.filter(key -> !key.equals(appKeyName) && !key.equals(timestampName) && !key.equals(signName))
			.forEach(key -> paramMap.put(key, requestParameterMap.get(key)));
		paramMap.put(appKeyName, appKey);
		paramMap.put(timestampName, timestampValue);
		
		if (!validateSignature(appKey, sign, paramMap)) {
			exception("invalid signature", httpServletResponse);
			return;
		} 
		chain.doFilter(request, response);
	}
	
    /**
     * 验证签名
     * @param appKey 应用key
     * @param sign 签名
     * @Param paramMap 参数map
     * @return 是否验证通过
     */
    private boolean validateSignature(String appKey, String sign, Map<String, Object> paramMap) {
    	String appSecret = webSignAppSecretPsi.getAppSecret(appKey);
    	if (StringUtils.isBlank(appSecret)) {
    		logger.error("appSecret must not be blank");
    		return false;
    	}
    	HmacUtils hmacUtils = new HmacUtils(HmacAlgorithms.HMAC_SHA_1, appSecret);
    	String paramStr = UrlUtil.getLexSortedParamStr(paramMap);
    	String validSign = hmacUtils.hmacHex(paramStr);
    	if (sign.equals(validSign)) {
    		return true;
    	}
    	return false;
    }
    
    private void exception(String message, HttpServletResponse httpServletResponse) {
    	Resp<Void> resp = Resp.error(RespCode.SIGN_ERROR.getValue(), RespCode.SIGN_ERROR.getText() + ": " + message);
    	httpServletResponse.setStatus(HttpStatus.OK.value());
    	httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
    	try {
			IOUtils.write(Json.string(resp), httpServletResponse.getOutputStream(), "utf-8");
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
    }

}
