package fun.pplm.framework.poplar.web.sign.controller;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fun.pplm.framework.poplar.web.audit.annotation.Audit;
import fun.pplm.framework.poplar.web.resp.Resp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import static fun.pplm.framework.poplar.web.sign.utils.WebSignConstant.WEB_AUDIT_TYPE_WEB_SIGN_VERIFY;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * web签名服务
 * @author OracleGao
 *
 */
@ConditionalOnProperty(prefix = "poplar.web.sign", name = "apiEnabled", havingValue = "true")
@Api(value = "webSignController", tags = "web签名服务")
@Validated
@RestController
@RequestMapping(path = "/sign", produces = MediaType.APPLICATION_JSON_VALUE)
public class WebSignController {
	
	@Audit(value = "验证签名", types = WEB_AUDIT_TYPE_WEB_SIGN_VERIFY)
	@ApiOperation(value = "验证签名", notes = "验证签名")
	@GetMapping("/verify")
	public Resp<Map<String, String[]>> verify(HttpServletRequest request) {
		return Resp.success(request.getParameterMap());
	}
	
}
