package fun.pplm.framework.poplar.web.sign.utils;

/**
 * 
 * web签名常量
 * @author OracleGao
 *
 */
public class WebSignConstant {
	/**
	 * web审计类型 签名验证
	 */
	public static final String WEB_AUDIT_TYPE_WEB_SIGN_VERIFY = "poplar-web-sign-verify";
	
}
