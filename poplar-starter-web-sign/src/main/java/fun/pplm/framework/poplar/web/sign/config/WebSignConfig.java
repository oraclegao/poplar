package fun.pplm.framework.poplar.web.sign.config;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.web.session.psi.WebSignWhitePathPsi;

/**
 * 
 * web应用签名配置
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.web.sign")
public class WebSignConfig {
	private static Logger logger = LoggerFactory.getLogger(WebSignConfig.class);
	
	/**
	 * app key属性名称
	 * 默认appKey
	 */
	private String appKeyName = "appKey";
	/**
	 * timestamp属性名称
	 * 默认timestamp
	 */
	private String timestampName = "timestamp";
	/**
	 * sign属性名称
	 * 默认sign
	 */
	private String signName = "sign";
	/**
	 * 白名单路径模式
	 */
	private List<String> whitePathPatterns = new ArrayList<>();
	/**
	 * 签名超时时间
	 * 单位秒
	 * 默认300
	 */
	private Long timeoutSecond = 300L;
	
	/**
	 * 是否开启http restful api
	 * 默认false不开启
	 */
	private Boolean apiEnabled = false;

	@Autowired(required = false)
	private List<WebSignWhitePathPsi> webSignWhitePathServices = new ArrayList<>();
	
	public WebSignConfig() {
		super();
	}

	@PostConstruct
	protected void init() {
    	if (!webSignWhitePathServices.isEmpty()) {
    		for (WebSignWhitePathPsi webSignWhitePathService : webSignWhitePathServices) {
    			whitePathPatterns.addAll(webSignWhitePathService.whitePathPatterns());
    		}
    	}
    	whitePathPatterns.add("/favicon.ico");
    	whitePathPatterns.add("/error");
    	logger.debug(this.getClass().getSimpleName() + "注入成功, config: {}", this);
	}
	
	public String getAppKeyName() {
		return appKeyName;
	}

	public void setAppKeyName(String appKeyName) {
		this.appKeyName = appKeyName;
	}

	public String getTimestampName() {
		return timestampName;
	}

	public void setTimestampName(String timestampName) {
		this.timestampName = timestampName;
	}

	public String getSignName() {
		return signName;
	}

	public void setSignName(String signName) {
		this.signName = signName;
	}

	public List<String> getWhitePathPatterns() {
		return whitePathPatterns;
	}

	public void setWhitePathPatterns(List<String> whitePathPatterns) {
		this.whitePathPatterns = whitePathPatterns;
	}

	public Long getTimeoutSecond() {
		return timeoutSecond;
	}

	public void setTimeoutSecond(Long timeoutSecond) {
		this.timeoutSecond = timeoutSecond;
	}
	
	public Boolean getApiEnabled() {
		return apiEnabled;
	}

	public void setApiEnabled(Boolean apiEnabled) {
		this.apiEnabled = apiEnabled;
	}

	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("appKeyName", appKeyName);
		map.put("timestampName", timestampName);
		map.put("signName", signName);
		map.put("whitePathPatterns", whitePathPatterns);
		map.put("timeoutSecond", timeoutSecond);
		map.put("apiEnabled", apiEnabled);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
	
}
