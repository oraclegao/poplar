## 1.11.21
- 修复WebSignConfig.whitePathPatterns空指针异常

## 1.8.17
- 增加url白名单回调服务注入，允许其他服务注入白名单url patterns
- 优化url拦截器配置，将/error和/favicon.ico作为默认会话白名单项

## 1.8.16
- 优化签名校验异常信息内容
- 服务接口类后缀更名为Psi: SignAppSecretService -> SignAppSecretPsi

## 1.8.15
- 新增接口签名starter
