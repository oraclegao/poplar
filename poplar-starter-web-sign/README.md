# poplar-starter-web-sign

## 签名规则
- 使用HMAC_SHA_1签名算法，对按照字典序排序的url中所有的query参数组成的字符串利用appSecret进行签名，将签名结果字符串放到查询参数中请求接口。 
- 对于post，put等情况用法与get相同，只是http request body不参与签名

### 举例
- 对该接口进行签名：/sign/verify?foo=hello&bar=world
- appKey=wtf15d9031
- appSecret=af13c41bf73f4ebbb9e7782cd5d2db1d
- timestamp系统长整型时间戳，单位毫秒，与服务端时间前后相差不超过300秒
1. 组装签名字符串，按照字典序对参数排序，组装结果：appKey=wtf15d9031&bar=world&foo=hello&timestamp=1703644048186
2. 使用HMAC_SHA_1签名算法对签名字符串签名:af13c41bf73f4ebbb9e7782cd5d2db1d。以下是java代码示例（签名结果由于timestamp不一样会有所不同）
``` java
// java示例
import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;

public class WebSignDemo {

	public static void main(String[] args) {
		String appKey = "wtf15d9031";
		String appSecret = "af13c41bf73f4ebbb9e7782cd5d2db1d";
		//按字典序对参数排好序
		String param="appKey=" + appKey + "&bar=world&foo=hello&timestamp=" + System.currentTimeMillis();
		HmacUtils hmacUtils = new HmacUtils(HmacAlgorithms.HMAC_SHA_1, appSecret);
		String sign = hmacUtils.hmacHex(param);
		System.out.println(param);
		System.out.println(sign);
	}
	
}
```

``` python
## python示例
import hmac
import hashlib
import time

app_key = 'wtf15d9031'
app_secret = b'af13c41bf73f4ebbb9e7782cd5d2db1d'
timestamp = str(int(time.time()) * 1000)
param = 'appKey=' + app_key + '&bar=world&foo=hello&timestamp=' + timestamp

# 计算签名
digest = hmac.new(app_secret, param.encode(), hashlib.sha1).digest()
# 将签名转换为十六进制字符串
digest_hex = digest.hex()
print(param)
print(digest_hex)
```

3. 使用签名访问url
``` shell
curl --request GET 'http://localhost:8080/sign/verify?foo=hello&bar=world&appKey=wtf15d9031timestamp=1703644048186&sign=0c304b7f21f6879867545b232b3ed73dafdbca1e'
```

4. 其中appKey，timestamp和sign签名相关参数也可以部分或全部放在header中，但是需要参与签名计算。查询参数值优先级更高会覆盖header中的值。以下签名访问也是可以的
``` shell
curl --request GET --header 'appKey: wtf15d9031' --header 'timestamp: 1703644048186' --header 'sign: 0c304b7f21f6879867545b232b3ed73dafdbca1e'  http://localhost:8080/sign/verify?foo=hello&bar=world'
```
``` shell
curl --request GET --header 'appKey: wtf15d9031' --header 'sign: 0c304b7f21f6879867545b232b3ed73dafdbca1e'  http://localhost:8080/sign/verify?foo=hello&bar=world&timestamp=1703644048186'
```
5. 对于参数是数组情况，签名字符串参数按照从左至右方向排序，比如/sign/verify?foo=hello&bar=world&bar=2023,签名字符串为：appKey=wtf15d9031&bar=world&bar=2023&foo=hello&timestamp=1703644048186