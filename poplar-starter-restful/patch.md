## 1.11.21
- 添加RestfulPsi服务接口支持多态扩展，代理模式或装饰模式的实现
- 改造RestfulService实现RestfulPsi服务接口
- 添加restful调用重试功能，通过配置文件配置重试

## 1.8.17
- RestfulService中添加支持HttpHeaders参数的get重载方法
- RestfulService中get方法手动绕过WAF支持自动重定向
- 增加配置项httpsProtocol默认TLSv1.2，用于创建默认的SimpleClientHttpRequestFactory中SSLContext
- 规范接口函数参数顺序，jsonPost添加body, header为null情况支持

## 1.4.6
- 增加http请求原始response body的debug级别日志输出
