package fun.pplm.framework.poplar.restful.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;

import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.restful.bean.model.RetryConfigModel;
import fun.pplm.framework.poplar.restful.config.RestfulConfig;
import fun.pplm.framework.poplar.restful.psi.RestfulPsi;
import fun.pplm.framework.poplar.restful.resp.RestfulResp;

/**
 * 
 * restful服务
 * 封装resttemplate，使用ObjectMapper转换json
 * 实现get，form post和json body post
 * @author OracleGao
 *
 */
@Service
public class RestfulService implements RestfulPsi {
	private Logger logger = LoggerFactory.getLogger(RestfulService.class);
	
	public static final String CLASS_NAME_STRING = "java.lang.String";
	public static final String CLASS_NAME_VOID = "java.lang.Void";
	
	@Autowired
	private RestfulConfig restfulConfig;
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public <T> RestfulResp<T> get(String url, HttpHeaders headers, Class<T> clazz) {
		ResponseEntity<String> responseEntity = get(url, headers);
		RestfulResp<T> resp = new RestfulResp<>(responseEntity);
		String data = responseEntity.getBody();
		logger.debug("http get resp, url: {}, resBody: {}", url, data);
		resp.setBody(getBody(data, clazz));
		return resp;
	}
	
	@Override
	public <T> RestfulResp<T> get(String url, HttpHeaders headers, TypeReference<T> typeReference) {
		ResponseEntity<String> responseEntity = get(url, headers);
		RestfulResp<T> resp = new RestfulResp<>(responseEntity);
		String data = responseEntity.getBody();
		logger.debug("http get resp, url: {}, resBody: {}", url, data);
		resp.setBody(getBody(data, typeReference));
		return resp;
	}
	
	private ResponseEntity<String> get(String url, HttpHeaders headers) {
		final HttpEntity<?> requestEntity = getHttpEntity(headers);
		RetryConfigModel retryConfigModel = restfulConfig.getRetryConfig(url, "get");
		ResponseEntity<String> responseEntity = null;
		if (retryConfigModel == null) {
			responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
			//手动绕过WAF（Web Application Firewall）支持url重定向
			if (responseEntity.getStatusCode() == HttpStatus.FOUND) {
			    String redirectUrl = responseEntity.getHeaders().getLocation().toString();
			    responseEntity = restTemplate.exchange(redirectUrl, HttpMethod.GET, requestEntity, String.class);
			}
		} else {
			responseEntity = retryCall(retryConfigModel, () -> {
				ResponseEntity<String> respEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
				//手动绕过WAF（Web Application Firewall）支持url重定向
				if (respEntity.getStatusCode() == HttpStatus.FOUND) {
				    String redirectUrl = respEntity.getHeaders().getLocation().toString();
				    respEntity = restTemplate.exchange(redirectUrl, HttpMethod.GET, requestEntity, String.class);
				}
				return respEntity;
			});
		}
		return responseEntity;
	}
	
	private HttpEntity<?> getHttpEntity(HttpHeaders headers) {
		HttpEntity<?> requestEntity = HttpEntity.EMPTY;
		if (headers != null) {
			requestEntity = new HttpEntity<>(headers);
		}
		return requestEntity;
	}
	
	/**
	 * form方式发送请求数据，json格式接收响应数据
	 * @param <T> 响应体(response body)对象类型
	 * @param url url
	 * @param formMap form数据map
	 * @param clazz 响应体(response body)对象类型，用于objectMapper json字符串反射对象
	 * @param headers 请求头(request headers)
	 * @return 响应结果
	 */
	@Override
	public <T> RestfulResp<T> formPost(String url, MultiValueMap<?, ?> formMap, HttpHeaders headers, Class<T> clazz) {
		ResponseEntity<String> responseEntity = formPost(url, formMap, headers);
		RestfulResp<T> resp = new RestfulResp<>(responseEntity);
		String data = responseEntity.getBody();
		logger.debug("http post form resp, url: {}, formMap: {}, reqHeaders: {}, resBody: {}", url, Json.string(formMap), Json.string(headers), data);
		resp.setBody(getBody(data, clazz));
		return resp;
	}
	
	/**
	 * form方式发送请求数据，json格式接收响应数据
	 * @param <T> 响应体(response body)对象类型
	 * @param url url
	 * @param formMap form数据map
	 * @param typeReference 响应体(response body)对象泛型，用于objectMapper json字符串反射对象
	 * @param headers 请求头(request headers)
	 * @return 响应结果
	 */
	@Override
	public <T> RestfulResp<T> formPost(String url, MultiValueMap<?, ?> formMap, HttpHeaders headers, TypeReference<T> typeReference) {
		ResponseEntity<String> responseEntity = formPost(url, formMap, headers);
		RestfulResp<T> resp = new RestfulResp<>(responseEntity);
		String data = responseEntity.getBody();
		logger.debug("http post form resp, url: {}, form: {}, reqHeaders: {}, resbody: {}", url, Json.string(formMap), Json.string(headers), data);
		resp.setBody(getBody(data, typeReference));
		return resp;
	}
	
	protected ResponseEntity<String> formPost(String url, MultiValueMap<?, ?> formMap, HttpHeaders headers) {
		if (headers == null) {
			headers = new HttpHeaders();
		}
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		final HttpEntity<MultiValueMap<?, ?>> requestBody = new HttpEntity<>(formMap, headers);
		RetryConfigModel retryConfigModel = restfulConfig.getRetryConfig(url, "post");
		ResponseEntity<String> responseEntity = null;
		if (retryConfigModel == null) {
			responseEntity = restTemplate.postForEntity(url, requestBody, String.class);
		} else {
		    responseEntity = retryCall(retryConfigModel, () -> restTemplate.postForEntity(url, requestBody, String.class));
		}
		return responseEntity;
	}
	
	/**
	 * json格式发送和接收数据
	 * @param <T> 响应体(response body)对象类型
	 * @param url url
	 * @param requestBody 请求体(request body)，会被转换成json字符串
	 * @param clazz 响应体(response body)对象类型，用于objectMapper json字符串反射对象
	 * @param headers 请求头(request headers)
	 * @return 响应结果
	 */
	@Override
	public <T> RestfulResp<T> jsonPost(String url, Object requestBody, HttpHeaders headers, Class<T> clazz) {
		ResponseEntity<String> responseEntity = jsonPost(url, requestBody, headers);
		RestfulResp<T> resp = new RestfulResp<>(responseEntity);
		String data = responseEntity.getBody();
		logger.debug("http post json resp, url: {}, reqBody: {}, reqHeaders: {}, resBody: {}", url, Json.string(requestBody), Json.string(headers), data);
		resp.setBody(getBody(data, clazz));
		return resp;
	}
	
	/**
	 * json格式发送和接收数据
	 * @param <T> 响应体(response body)对象类型
	 * @param url url
	 * @param requestBody 请求体(request body)，会被转换成json字符串
	 * @param typeReference 响应体(response body)对象泛型，用于objectMapper json字符串反射对象
	 * @param headers 请求头(request headers)
	 * @return 响应结果
	 */
	@Override
	public <T> RestfulResp<T> jsonPost(String url, Object requestBody, HttpHeaders headers, TypeReference<T> typeReference) {
		ResponseEntity<String> responseEntity = jsonPost(url, requestBody, headers);
		RestfulResp<T> resp = new RestfulResp<>(responseEntity);
		String data = responseEntity.getBody();
		logger.debug("http post json resp, url: {}, reqBody: {}, reqHeaders: {}, resBody: {}", url, Json.string(requestBody), Json.string(headers), data);
		resp.setBody(getBody(data, typeReference));
		return resp;
	}
	
	private ResponseEntity<String> jsonPost(String url, Object requestBody, HttpHeaders headers) {
		final HttpEntity<String> httpEntity = getHttpEntity(requestBody, headers);
		RetryConfigModel retryConfigModel = restfulConfig.getRetryConfig(url, "post");
		ResponseEntity<String> responseEntity = null;
		if (retryConfigModel == null) {
			responseEntity = restTemplate.postForEntity(url, httpEntity, String.class);
		} else {
			responseEntity = retryCall(retryConfigModel, () -> restTemplate.postForEntity(url, httpEntity, String.class));
		}
		return responseEntity;
	}
	
	private HttpEntity<String> getHttpEntity(Object requestBody, HttpHeaders headers) {
		if (headers == null) {
			headers = new HttpHeaders();
		}
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> httpEntity = null;
		if (requestBody != null) {
			String jsonBody = null;
			if (requestBody instanceof CharSequence) {
				jsonBody = requestBody.toString();
			} else {
				jsonBody = Json.string(requestBody);
			}
			httpEntity = new HttpEntity<>(jsonBody, headers);
		} else {
			httpEntity = new HttpEntity<>(headers);
		}
		return httpEntity;
	}
	
	@SuppressWarnings("unchecked")
	private <T> T getBody(String data, TypeReference<T> typeReference) {
		String className = typeReference.getType().getTypeName();
		if (CLASS_NAME_VOID.equals(className)) {
			return null;
		}
		if (CLASS_NAME_STRING.equals(className)) {
			return (T) data;
		}
		if (StringUtils.isBlank(data)) {
			return null;
		}
		return Json.bean(data, typeReference);
	}
	
	@SuppressWarnings("unchecked")
	private <T> T getBody(String data, Class<T> clazz) {
		String className = clazz.getName();
		if (CLASS_NAME_VOID.equals(className)) {
			return null;
		}
		if (CLASS_NAME_STRING.equals(className)) {
			return (T) data;
		}
		if (StringUtils.isBlank(data)) {
			return null;
		}
		return Json.bean(data, clazz);
	}
	
	private ResponseEntity<String> retryCall(RetryConfigModel retryConfigModel, RertryFunction function) {
		int times = retryConfigModel.getTimes();
		long interval = retryConfigModel.getInterval();
		int retries = 0;
		RestClientException exception = null;
		do {
			try {
				return function.call();
			} catch(RestClientException e) {
				exception = e;
				logger.warn("restful调用失败，等待{}毫秒重试...", interval);
				try {
					Thread.sleep(interval);
				} catch (InterruptedException e1) {
					logger.error(e.getMessage(), e);
				}
			}
		} while(++retries < times);
		logger.warn("restful调用失败，超过重试次数{}", times);
		throw exception;
	}

	@FunctionalInterface
	public interface RertryFunction {
		ResponseEntity<String> call();
	}
	
}
