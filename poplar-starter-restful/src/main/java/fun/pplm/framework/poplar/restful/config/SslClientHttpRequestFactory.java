package fun.pplm.framework.poplar.restful.config;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.http.client.SimpleClientHttpRequestFactory;

/**
 * 
 * SimpleClientHttpRequestFactory with default ssl https support
 * @author OracleGao
 *
 */
@ConditionalOnMissingBean(SimpleClientHttpRequestFactory.class)
public class SslClientHttpRequestFactory extends SimpleClientHttpRequestFactory {
	@Autowired
	private RestfulConfig restfulConfig;
	
	@Override
	protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
		if (connection instanceof HttpsURLConnection) {
			prepareHttpsConnection((HttpsURLConnection) connection);
		}
		super.prepareConnection(connection, httpMethod);
	}

	private void prepareHttpsConnection(HttpsURLConnection connection) {
		connection.setHostnameVerifier(new HostnameVerifier() {
			@Override
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		});
		try {
			connection.setSSLSocketFactory(createSslSocketFactory());
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private SSLSocketFactory createSslSocketFactory() throws Exception {
		SSLContext sslContext = SSLContext.getInstance(restfulConfig.getHttpsProtocol());
		sslContext.init(null, new TrustManager[] { new X509TrustManager() {
			@Override
			public void checkClientTrusted(X509Certificate[] chain, String authType)
					throws CertificateException {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] chain, String authType)
					throws CertificateException {
			}

			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}
		} }, new SecureRandom());
		return sslContext.getSocketFactory();
	}

}
