package fun.pplm.framework.poplar.restful.psi;

import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.core.type.TypeReference;

import fun.pplm.framework.poplar.restful.resp.RestfulResp;

/**
 * 
 * poplar service interface
 * restful服务接口
 * @author OracleGao
 *
 */
public interface RestfulPsi {
	public default <T> RestfulResp<T> get(String url, Class<T> clazz) {
		return get(url, null, clazz);
	}
	
	public default <T> RestfulResp<T> get(String url, TypeReference<T> typeReference) {
		return get(url, null, typeReference);
	}
	
	public <T> RestfulResp<T> get(String url, HttpHeaders headers, Class<T> clazz);
	
	public <T> RestfulResp<T> get(String url, HttpHeaders headers, TypeReference<T> typeReference);
	
	public default <T> RestfulResp<T> formPost(String url, MultiValueMap<?, ?> formMap, Class<T> clazz) {
		return formPost(url, formMap, null, clazz);
	}
	
	public default <T> RestfulResp<T> formPost(String url, MultiValueMap<?, ?> formMap, TypeReference<T> typeReference) {
		return formPost(url, formMap, null, typeReference);
	}
	
	/**
	 * form方式发送请求数据，json格式接收响应数据
	 * @param <T> 响应体(response body)对象类型
	 * @param url url
	 * @param formMap form数据map
	 * @param clazz 响应体(response body)对象类型，用于objectMapper json字符串反射对象
	 * @param headers 请求头(request headers)
	 * @return 响应结果
	 */
	public <T> RestfulResp<T> formPost(String url, MultiValueMap<?, ?> formMap, HttpHeaders headers, Class<T> clazz);
	
	/**
	 * form方式发送请求数据，json格式接收响应数据
	 * @param <T> 响应体(response body)对象类型
	 * @param url url
	 * @param formMap form数据map
	 * @param typeReference 响应体(response body)对象泛型，用于objectMapper json字符串反射对象
	 * @param headers 请求头(request headers)
	 * @return 响应结果
	 */
	public <T> RestfulResp<T> formPost(String url, MultiValueMap<?, ?> formMap, HttpHeaders headers, TypeReference<T> typeReference);
	
	public default <T> RestfulResp<T> jsonPost(String url, Class<T> clazz) {
		return jsonPost(url, null, null, clazz);
	}
	
	public default <T> RestfulResp<T> jsonPost(String url, TypeReference<T> typeReference) {
		return jsonPost(url, null, null, typeReference);
	}
	
	public default <T> RestfulResp<T> jsonPost(String url, Object requestBody, Class<T> clazz) {
		return jsonPost(url, requestBody, null, clazz);
	}
	
	public default <T> RestfulResp<T> jsonPost(String url, Object requestBody, TypeReference<T> typeReference) {
		return jsonPost(url, requestBody, null, typeReference);
	}
	
	public default <T> RestfulResp<T> jsonPost(String url, HttpHeaders headers, Class<T> clazz) {
		return jsonPost(url, null, headers, clazz);
	}
	
	public default <T> RestfulResp<T> jsonPost(String url, HttpHeaders headers, TypeReference<T> typeReference) {
		return jsonPost(url, null, headers, typeReference);
	}
	
	/**
	 * json格式发送和接收数据
	 * @param <T> 响应体(response body)对象类型
	 * @param url url
	 * @param requestBody 请求体(request body)，会被转换成json字符串
	 * @param clazz 响应体(response body)对象类型，用于objectMapper json字符串反射对象
	 * @param headers 请求头(request headers)
	 * @return 响应结果
	 */
	public <T> RestfulResp<T> jsonPost(String url, Object requestBody, HttpHeaders headers, Class<T> clazz);
	
	/**
	 * json格式发送和接收数据
	 * @param <T> 响应体(response body)对象类型
	 * @param url url
	 * @param requestBody 请求体(request body)，会被转换成json字符串
	 * @param typeReference 响应体(response body)对象泛型，用于objectMapper json字符串反射对象
	 * @param headers 请求头(request headers)
	 * @return 响应结果
	 */
	public <T> RestfulResp<T> jsonPost(String url, Object requestBody, HttpHeaders headers, TypeReference<T> typeReference);
	
}
