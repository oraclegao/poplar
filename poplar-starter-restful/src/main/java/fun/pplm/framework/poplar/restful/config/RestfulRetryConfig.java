package fun.pplm.framework.poplar.restful.config;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import fun.pplm.framework.poplar.common.validator.annotation.Item;

/**
 * 
 * 重试配置
 * 拦截org.springframework.web.client.RestClientException异常实现重试逻辑
 * @author OracleGao
 *
 */
public class RestfulRetryConfig {
	/**
	 * 重试次数
	 * 默认3
	 * 最小值1
	 */
	@Min(value = 1)
	private Integer times = 3;
	/**
	 * 重试时间间隔
	 * 单位毫秒
	 * 默认 15000 15秒
	 */
	@Min(value = 0)
	private Long interval = 15000L;
	/**
	 * url正则匹配模式
	 */
	@NotEmpty
	private List<String> urlPatterns = new ArrayList<>();
	/**
	 * 调用方法
	 * 默认* 全部
	 * 目前只支持get和post，忽略大小写
	 */
	@Item(value = {"get", "post", "*"}, caseSensitive = false)
	private String method = "*";
	
	public RestfulRetryConfig() {
		super();
	}

	public Integer getTimes() {
		return times;
	}

	public void setTimes(Integer times) {
		this.times = times;
	}

	public Long getInterval() {
		return interval;
	}

	public void setInterval(Long interval) {
		this.interval = interval;
	}

	public List<String> getUrlPatterns() {
		return urlPatterns;
	}

	public void setUrlPatterns(List<String> urlPatterns) {
		this.urlPatterns = urlPatterns;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
	
}
