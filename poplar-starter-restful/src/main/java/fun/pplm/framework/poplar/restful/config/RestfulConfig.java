package fun.pplm.framework.poplar.restful.config;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.client.RestTemplate;

import fun.pplm.framework.poplar.common.validator.BeanValidator;
import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.restful.bean.model.RetryConfigModel;

/**
 * 
 * restful服务配置
 * 
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.restful")
public class RestfulConfig {
	private static Logger logger = LoggerFactory.getLogger(RestfulConfig.class);
	
	private Integer connectTimeout = 15000;
	private Integer readTimeout = 5000;
	/**
	 * 重试配置
	 * 拦截org.springframework.web.client.RestClientException异常实现重试逻辑
	 */
	private List<RestfulRetryConfig> retries;
	/**
	 * https 协议
	 * 用于创建默认的SimpleClientHttpRequestFactory中SSLContext
	 * 默认TLSv1.2
	 * 可选协议名称：
	 * SSLv3：SSL 3.0 版本
     * TLSv1：TLS 1.0 版本 Deprecated
     * TLSv1.1：TLS 1.1 版本 Deprecated
     * TLSv1.2：TLS 1.2 版本
     * TLSv1.3：TLS 1.3 版本（Java 11 及以上版本支持）
	 */
	private String httpsProtocol = "TLSv1.2";
	
	@Autowired
	private SimpleClientHttpRequestFactory factory;
	
	@Autowired
	private BeanValidator beanValidator;
	
	private Map<String, RetryConfigModel> retryConfigGetMap = new HashMap<>();
	private Map<String, RetryConfigModel> retryConfigPostMap = new HashMap<>();
	
	private AntPathMatcher antPathMatcher = new AntPathMatcher();
	
	public RestfulConfig() {
		super();
	}
	
	@PostConstruct
	protected void init() {
		if (retries != null) {
			RetryConfigModel model = null;
			String method = null;
			List<String> urlPatterns = null;
			for (RestfulRetryConfig restfulRetryConfig : retries) {
				beanValidator.validate(restfulRetryConfig);
				method = restfulRetryConfig.getMethod().toLowerCase();
				urlPatterns = restfulRetryConfig.getUrlPatterns();
				model = new RetryConfigModel();
				model.setInterval(restfulRetryConfig.getInterval());
				model.setTimes(restfulRetryConfig.getTimes());
				if ("*".equals(method)) {
					for (String urlPattern : urlPatterns) {
						retryConfigGetMap.put(urlPattern, model);
						retryConfigPostMap.put(urlPattern, model);
					}
				} else if ("get".equals(method)) {
					for (String urlPattern : urlPatterns) {
						retryConfigGetMap.put(urlPattern, model);
					}
				} else if ("post".equals(method)) {
					for (String urlPattern : urlPatterns) {
						retryConfigPostMap.put(urlPattern, model);
					}
				}
			}
		}
		logger.debug(this.getClass().getSimpleName() + "注入成功, config: {}", this);
	}
	
	public RetryConfigModel getRetryConfig(String url, String method) {
		if (retries == null) {
			return null;
		}
		if ("get".equalsIgnoreCase(method)) {
			for (String urlPattern : retryConfigGetMap.keySet()) {
				if (antPathMatcher.match(urlPattern, url)) {
					return retryConfigGetMap.get(urlPattern);
				}
			}
		} else if ("post".equalsIgnoreCase(method)) {
			for (String urlPattern : retryConfigPostMap.keySet()) {
				if (antPathMatcher.match(urlPattern, url)) {
					return retryConfigPostMap.get(urlPattern);
				}
			}
		}
		return null;
	}

	public Integer getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(Integer connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public Integer getReadTimeout() {
		return readTimeout;
	}

	public void setReadTimeout(Integer readTimeout) {
		this.readTimeout = readTimeout;
	}

	public List<RestfulRetryConfig> getRetries() {
		return retries;
	}

	public void setRetries(List<RestfulRetryConfig> retries) {
		this.retries = retries;
	}

	public String getHttpsProtocol() {
		return httpsProtocol;
	}

	public void setHttpsProtocol(String httpsProtocol) {
		this.httpsProtocol = httpsProtocol;
	}

	@Bean
	public RestTemplate restTemplate() {
		factory.setConnectTimeout(connectTimeout);
		factory.setReadTimeout(readTimeout);
		return new RestTemplate(factory);
	}
	
	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("connectTimeout", connectTimeout);
		map.put("readTimeout", readTimeout);
		map.put("retries", retries);
		map.put("httpsProtocol", httpsProtocol);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
	
}
