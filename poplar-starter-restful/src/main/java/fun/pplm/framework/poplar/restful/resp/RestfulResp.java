package fun.pplm.framework.poplar.restful.resp;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * restful响应信息
 * @author OracleGao
 *
 * @param <T> 响应对象泛型
 * 
 */
public class RestfulResp<T> {
	private HttpStatus status;
	private HttpHeaders headers;
	private T body;

	public RestfulResp() {
		super();
	}

	public RestfulResp(ResponseEntity<?> responseEntity) {
		super();
		this.status = responseEntity.getStatusCode();
		this.headers = responseEntity.getHeaders();
	}
	
	public Integer getStatusCode() {
		return status.value();
	}
	
	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public HttpHeaders getHeaders() {
		return headers;
	}

	public void setHeaders(HttpHeaders headers) {
		this.headers = headers;
	}

	public T getBody() {
		return body;
	}

	public void setBody(T body) {
		this.body = body;
	}

	@Override
	public String toString() {
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("status", status);
		map.put("statusCode", getStatusCode());
		map.put("body", body);
		map.put("headers", headers);
		return Json.string(map);
	}
	
}
