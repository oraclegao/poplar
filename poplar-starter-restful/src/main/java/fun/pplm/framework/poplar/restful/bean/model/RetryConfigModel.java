package fun.pplm.framework.poplar.restful.bean.model;

/**
 * 
 * 重试配置
 * @author OracleGao
 *
 */
public class RetryConfigModel {
	/**
	 * 重试次数
	 * 默认3
	 */
	private Integer times = 3;
	/**
	 * 重试时间间隔
	 * 单位毫秒
	 * 默认 15000 15秒
	 */
	private Long interval = 15000L;
	
	public RetryConfigModel() {
		super();
	}

	public Integer getTimes() {
		return times;
	}

	public void setTimes(Integer times) {
		this.times = times;
	}

	public Long getInterval() {
		return interval;
	}

	public void setInterval(Long interval) {
		this.interval = interval;
	}

}
