package fun.pplm.framework.poplar.event.hub.model;

import com.fasterxml.jackson.core.type.TypeReference;

import static fun.pplm.framework.poplar.json.config.ObjectMapperConfig.OBJECT_MAPPER;

/**
 * 
 * 事件
 * @author OracleGao
 *
 */
public class Event {
	/**
	 * 事件id
	 */
	private String id;
	/**
	 * 事件类型
	 */
	private Integer type;
	/**
	 * 事件内容
	 */
	private Object content;
	/**
	 * 事件优先级
	 * 高到低1 ~ 9
	 * 默认5
	 */
	private int priority = 5;
	
	public Event() {
		super();
	}

	public <T> T getContent(Class<T> clazz) {
		return OBJECT_MAPPER.convertValue(content, clazz);
	}
	
	public <T> T getContent(TypeReference<T> typeReference) {
		return OBJECT_MAPPER.convertValue(content, typeReference);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Object getContent() {
		return content;
	}

	public void setContent(Object content) {
		this.content = content;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
	
}
