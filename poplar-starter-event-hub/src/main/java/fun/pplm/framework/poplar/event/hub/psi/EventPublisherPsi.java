package fun.pplm.framework.poplar.event.hub.psi;

import fun.pplm.framework.poplar.event.hub.model.Event;

/**
 * 
 * 事件发布者服务接口
 * @author OracleGao
 *
 */
public interface EventPublisherPsi {
	/**
	 * 发布事件
	 * 优先级默认5
	 * @param type 事件类型
	 * @return 成功: 优先级是5的事件对象，失败: null
	 */
	public Event publish(int type);
	
	/**
	 * 发布事件
	 * 优先级默认5
	 * @param type 事件类型
	 * @param content 事件内容
	 * @return 成功: 优先级是5的事件对象，失败: null
	 */
	public Event publish(int type, Object content);
	
	/**
	 * 发布事件
	 * @param type 事件类型
	 * @param content 事件内容
	 * @param priority 优先级 高到低1 ~ 9
	 * @return 成功: 事件对象，失败: null
	 */
	public Event publish(int type, Object content, int priority);

	/**
	 * 发布事件
	 * @param event 事件, 如果id是blank,发送成功时会自动生成id
	 * @return 事件id true成功， false失败
	 */
	public boolean publish(Event event);
	
}
