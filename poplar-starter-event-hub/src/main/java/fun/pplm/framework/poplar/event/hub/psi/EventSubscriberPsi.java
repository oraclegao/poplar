package fun.pplm.framework.poplar.event.hub.psi;

import fun.pplm.framework.poplar.event.hub.model.Event;

/**
 * 
 * 事件订阅者服务接口
 * @author OracleGao
 *
 */
public interface EventSubscriberPsi {
	/**
	 * 回调函数，EventHub获取订阅的事件类型
	 * @return 事件类型
	 */
	public int[] subTypes();
	/**
	 * 事件到达
	 * @param event 事件
	 */
	public void arrived(Event event);

}
