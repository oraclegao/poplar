package fun.pplm.framework.poplar.event.hub.config;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.common.config.ThreadPoolConfig;
import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * 事件配置
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.event.hub")
@ConditionalOnExpression("${poplar.event.hub.enabled:true}")
public class EventHubConfig {
	private Boolean enabled = true;
	/**
	 * 自动初始化
	 * 默认true
	 */
	private Boolean autoInit = true;
	/**
	 * 容量
	 * 默认cpu核数*16
	 */
	private Integer capacity = Runtime.getRuntime().availableProcessors() * 16;
	/**
	 * 无元素时的阻塞间隔
	 * 单位毫秒
	 * 默认1000
	 */
	private Long blockInterval = 1000L;
	/**
	 * 订阅服务线程池配置
	 */
	private ThreadPoolConfig subThreadPool = new ThreadPoolConfig();
	
	public EventHubConfig() {
		super();
	}
	
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getAutoInit() {
		return autoInit;
	}

	public void setAutoInit(Boolean autoInit) {
		this.autoInit = autoInit;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public Long getBlockInterval() {
		return blockInterval;
	}

	public void setBlockInterval(Long blockInterval) {
		this.blockInterval = blockInterval;
	}

	public ThreadPoolConfig getSubThreadPool() {
		return subThreadPool;
	}

	public void setSubThreadPool(ThreadPoolConfig subThreadPool) {
		this.subThreadPool = subThreadPool;
	}

	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("enabled", enabled);
		map.put("autoInit", autoInit);
		map.put("capacity", capacity);
		map.put("blockInterval", blockInterval);
		map.put("subThreadPool", subThreadPool);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
	
}
