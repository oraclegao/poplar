package fun.pplm.framework.poplar.event.hub.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Predicate;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Service;

import fun.pplm.framework.poplar.common.runnable.LineWorker;
import fun.pplm.framework.poplar.common.utils.ThreadPoolUtil;
import fun.pplm.framework.poplar.common.utils.Uuid;
import fun.pplm.framework.poplar.event.hub.config.EventHubConfig;
import fun.pplm.framework.poplar.event.hub.model.Event;
import fun.pplm.framework.poplar.event.hub.psi.EventPublisherPsi;
import fun.pplm.framework.poplar.event.hub.psi.EventSubscriberPsi;
import fun.pplm.framework.poplar.json.Json;

/**
 * 
 * 事件中心服务
 * @author OracleGao
 *
 */
@Service
@ConditionalOnExpression("${poplar.event.hub.enabled:true}")
public class EventHubService extends LineWorker implements EventPublisherPsi {
	private static Logger logger = LoggerFactory.getLogger(EventHubService.class);
	
	@Autowired(required = false)
	private EventHubConfig eventHubConfig;
	
	@Autowired(required = false)
	private volatile List<EventSubscriberPsi> eventSubscribers = new CopyOnWriteArrayList<>();
	
	private BlockingQueue<Event> blockingQueue;

	private ExecutorService subscriberExecuterService;
	
	private final ReentrantLock lock = new ReentrantLock();
	
	public EventHubService() {
		super(0L);
	}
	
	@PostConstruct
	protected void autoInit() {
		if (eventHubConfig.getAutoInit()) {
			init(eventHubConfig);
		}
	}
	
	public void init(EventHubConfig eventHubConfig) {
		logger.debug("初始化事件中心服务开始...");
		this.eventHubConfig = eventHubConfig;
		blockingQueue = new PriorityBlockingQueue<>(eventHubConfig.getCapacity(), new EventPriorityComparator());
		startup();
		logger.debug("初始化事件中心服务完成");
	}
	
	@Override
	public synchronized void startup() {
		if (!running()) {
			subscriberExecuterService = ThreadPoolUtil.newThreadPool(eventHubConfig.getSubThreadPool());
			super.startup();
		}
	}
	
	@Override
	public synchronized void shutdown() {
		if (running()) {
			ThreadPoolUtil.destoryThreadPool(subscriberExecuterService, eventHubConfig.getSubThreadPool());
			super.shutdown();
		}
	}

	@Override
	public Event publish(int type) {
		return publish(type, null);
	}
	
	@Override
	public Event publish(int type, Object content) {
		Event event = new Event();
		event.setType(type);
		event.setContent(content);
		if(publish(event)) {
			return event;
		}
		return null;
	}

	@Override
	public Event publish(int type, Object content, int priority) {
		Event event = new Event();
		event.setType(type);
		event.setContent(content);
		event.setPriority(priority);
		if(publish(event)) {
			return event;
		}
		return null;
	}

	@Override
	public boolean publish(Event event) {
		if (!running()) {
			throw new RuntimeException("事件中心服务没启动");
		}
		final ReentrantLock lock = this.lock;
		lock.lock();
		try {
			if (StringUtils.isBlank(event.getId())) {
				event.setId(Uuid.uuid32());
			}
			if (blockingQueue.size() >= eventHubConfig.getCapacity()) {
				logger.warn("事件队列已满，无法发布新的事件, event: {}", Json.string(event));
				return false;
			}
			if (blockingQueue.offer(event)) {
				logger.debug("事件发布成功, event: {}", Json.string(event));
				return true;
			}
			return false;
		} finally {
			lock.unlock();
		}
	}
	
	@Override
	protected void execute() {
		try {
			Event event = blockingQueue.poll(1000L, TimeUnit.MILLISECONDS);
			if (event != null) {
				logger.debug("事件到达，处理订阅, event: {}", Json.string(event));
				for (EventSubscriberPsi eventSubscriber : eventSubscribers) {
					int[] subTypes = eventSubscriber.subTypes();
					if (subTypes != null && subTypes.length > 0) {
						if(Arrays.stream(eventSubscriber.subTypes()).anyMatch(event.getType()::equals)) {
							subscriberExecuterService.submit(() -> {
								eventSubscriber.arrived(event);
							});
						}
					} else {
						logger.warn("事件订阅者订阅的事件类型为空, class: {}", eventSubscriber.getClass().getCanonicalName());
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void addSubscriber(EventSubscriberPsi eventSubscriber) {
		eventSubscribers.add(eventSubscriber);
	}
	
	public void addSubscribers(Collection<EventSubscriberPsi> eventSubscribers) {
		eventSubscribers.addAll(eventSubscribers);
	}
	
	public void removeSubscriber(EventSubscriberPsi eventSubscriber) {
		eventSubscribers.remove(eventSubscriber);
	}
	
	public void removeSubscriber(int index) {
		eventSubscribers.remove(index);
	}
	
	public void removeIf(Predicate<? super EventSubscriberPsi> filter) {
		eventSubscribers.removeIf(filter);
	}
	
	public void removeAll(Collection<EventSubscriberPsi> eventSubscribers) {
		eventSubscribers.removeAll(eventSubscribers);
	}
	
	public int size() {
		return blockingQueue.size();
	}

}
