package fun.pplm.framework.poplar.event.hub.service;

import java.util.Comparator;

import fun.pplm.framework.poplar.event.hub.model.Event;

/**
 * 
 * 事件优先级比较
 * @author OracleGao
 *
 */
public class EventPriorityComparator implements Comparator<Event> {
	@Override
	public int compare(Event e1, Event e2) {
		return e1.getPriority() - e2.getPriority();
	}
}
