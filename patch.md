## 1.11.22
### poplar-common
- 添加AsciiUtil工具类

### poplar-starter-mqtt
- 完善MqttService初始化日志

### poplar-starter-scanner
- 扫码枪服务初始化推迟到spring服务启动之后

### poplar-starter-web-file
- 修复文件下载Content-Disposition http响应头中文件名中存在特殊符号bug

## 1.11.21
- 新增poplar-starter-restful-sign starter 
- 使用classgraph替换reflections

### poplar-common
- 添加UrlUtil工具类，提供url相关工具api

### poplar-common-mybatis
- JsonBaseTypeHandler修复反序列化允许列值为空字符串的情况

### poplar-common-session
- 使用classgraph替换reflections，扫描SessionPii实现类
- SessionService增加createSession(SessionPii session, boolean setCookieFlag)方法实现创建session时设置cookie
- SessionHttpService增加setCookieToken()相关方法，将token设置到http接口response的cookie中

### poplar-starter-mqtt
- 增加初始连接mqtt服务器重试机制

### poplar-starter-restful
- 添加RestfulPsi服务接口支持多态扩展，代理模式或装饰模式的实现
- 改造RestfulService实现RestfulPsi服务接口
- 添加restful调用重试功能，通过配置文件配置重试

### poplar-starter-restful-sign
- 实现web sign签名规则的带有签名功能的restful服务

### poplar-starter-session-jwt
- 增加createSession(SessionPii session, boolean setCookieFlag)方法实现创建session时设置cookie

### poplar-starter-session-token
- 增加createSession(SessionPii session, boolean setCookieFlag)方法实现创建session时设置cookie

### poplar-starter-web-file
- 默认文件后缀过滤器增加从MultipartFile.getContentType()获取类型并判断白名单的逻辑
- 增加上传文件计算md5值功能，可在配置文件中开启
- 完善文件下载content-type属性，默认从文件对象获取

### poplar-starter-web-sign
- 修复WebSignConfig.whitePathPatterns空指针异常

## 1.10.20

### poplar-starter-web-file
- 增加文件上传后缀白名单功能
- 修复文件apiEnabled配置参数可以正确启用api

## 1.10.19
- 新增poplar-common-modbus模块
- 新增poplar-starter-modbus-slave-tcp starter
- 新增com.github.ulisesbocchio.jasypt-spring-boot-starter:3.0.5依赖
- 新增poplar-starter-modbus-register-listener starter

### poplar-common
- 添加SqlFilterUtil符合sql查询习惯的条件过滤工具
- 添加ArrayUtil数组工具
- BinaryUtil增加整型数组批量转二进制字符串方法；int2FormatBinary方法增加自定义4位分隔符参数
- 移除RedisService.setExpireObject和RedisService.setExpireStr
- 增加RedisService.setExpire方法统一替代RedisService.setExpireObject和RedisService.setExpireStr
- 完善RedisService.expire方法注释
- RedisService中涉及Json的方法移除IOException统一替换成继承于RuntimeException的JsonException
- RedisService中移除setStr，setObject和getStr，getObject，由新增的set和get方法统一实现

### poplar-common-modbus
- 新增common模块
- 提供http api访问寄存器管理数据和数据
- 抽取并管理poplar-starter-modbus-tcp和poplar-starter-modbus-slave-tcp公共依赖
- 抽取poplar-starter-modbus-tcp和poplar-starter-modbus-slave-tcp公共依赖
- ModbusUtil.initData更名为initWords使语义更加准确
- ModbusUtil修复负数字转二进制字符串异常问题
- ModbusUtil添加字数组批量转换格式化二进制字符串工具方法
- ModbusUtil添加字转十六进制字符串相关工具方法
- ModbusUtil添加now方法返回时间戳字数组
- ModbusTcpPsa读取寄存器数据方法返回值统一调整为int[]或int类型
- ModbusUtil工具方法返回值统一调整为int[]或int类型，并增加int或int[]型入参重载方法

### poplar-starter-json
- Json.string()字符串类型参数直接返回的判断由String类型改为其父类接口CharSequence

### poplar-starter-modbus-register-listener
- 新增poplar-starter-modbus-register-listener modbus寄存器侦听starter模块，用于侦听寄存器数据变化触发回调服务
- 依赖poplar-starter-modbus-tcp或poplar-starter-modbus-slave-tcp starter提供对其modbus寄存器的侦听服务

### poplar-starter-modbus-slave-tcp
- 新增starter模块
- 用于模拟modbus从站服务

### poplar-starter-modbus-tcp
- 移除ModbusRegisterConfigPsi，变更为ModbusRegisterPsi并放入poplar-common-modbus
- 移除ModbusRegisterModel，ModbusTcpOnReadyEvent放入poplar-common-modbus,并更换包名
- ModbusUtil.initData更名为initWords使语义更加准确
- 移除ModbusUtil，放入poplar-common-modbus
- 配置项添加useRtuOverTcp开关选项，优化并新增配置项注释
- 增加写保持寄存器字值越界告警日志
- 移除数据初始化侦听接口ModbusDataInitPsi，可以通过侦听事件ModbusTcpOnReadyEvent实现
- 移除生命周期侦听接口ModbusLifecycleListener
- 移除ModbusReadRegParam，ModbusInitParam，ModbusWriteHrParam类和相关方法
- ModbusTcpService重构，继承poplar-common-modbus模块的ModbusTcpPsa抽象类
- ModbusTcpConfig更名ModbusMasterTcpConfig并继承poplar-common-modbus模块的ModbusTcpConfig

### poplar-starter-player-audio
- 支持系统默认音响热插拔

### poplar-starter-session-token
- 使用RedisService新的api

### poplar-starter-web-file
- 优化po，qo，vo类的swagger文档说明

### poplar-starter-web-smscode
- 优化po类的swagger文档说明
- 能够正确处理相同号码短信发送时间间隔
- 使用RedisService新的api

## 1.9.18
- 新增音频播放器starter poplar-starter-player-audio
- 新增事件中心starter poplar-starter-event-hub

### poplar-common
- LineWorker线程框架底层优化为信号机制
- BeanUtil.converts()增加过滤器参数

### poplar-starter-event-hub
- 事件中心服务，提供带有优先级的事件发布订阅框架服务
- 订阅服务使用线程城池并发订阅服务

### poplar-starter-player-audio
- 音频播放器starter
- 支持java源生音频格式播放，包括wav和mp3两个音频格式
- 仅在win10系统调试，其它系统未经过调试

## 1.8.17

### poplar-common-session
- 增加session白名单回调服务注入，允许其他服务注入白名单url patterns
- 优化session拦截器配置，将/error和/favicon.ico作为默认会话白名单项

### poplar-starter-restful
- RestfulService中添加支持HttpHeaders参数的get重载方法
- RestfulService中get方法手动绕过WAF支持自动重定向
- 增加配置项httpsProtocol默认TLSv1.2，用于创建默认的SimpleClientHttpRequestFactory中SSLContext
- 规范接口函数参数顺序，jsonPost添加body, header为null情况支持

### poplar-starter-web
- 新增会话白名单服务接口SessionWhitePathPsi公共依赖，允许其它web服务实现该接口为session拦截注入白名单url patterns

### poplar-starter-web-file
- 优化PdfConvertService注入逻辑，当禁用pdf功能时不注入。

### poplar-starter-web-sign
- 增加url白名单回调服务注入，允许其他服务注入白名单url patterns
- 优化url拦截器配置，将/error和/favicon.ico作为默认会话白名单项

### poplar-starter-web-swagger
- 添加文档的授权和鉴权支持（仅swagger html和api，不含业务接口）
- 新增SwaggerSessionWhitePathService服务，把swagger相关url作为白名单注入session拦截服务，使session不拦截swagger相关服务

## 1.8.16
- poplar-starter-swagger模块名称更名为poplar-starter-web-swagger
- 规范业务实现的服务接口，标识接口，抽象类，侦听接口，侦听抽象类名后缀。服务接口类名后缀：Psi (Poplar Service Interface)；标识接口类名后缀： Pii (Poplar Identification Interface)；抽象类类名后缀：Psa (Poplar Service Abstract)；侦听器接口类名后缀：Pli (Poplar Listener Interface)；侦听器抽象类类名后缀：Pla (Poplar Listener Abstract)。

### poplar-common
- LineWorker.finalized()和其实现子类函数名变更：finalize->finalized，避免重写废弃的Object.finalize()

### poplar-common-serialport
- 抽象类类名后缀更名为Psa: SerialPortService -> SerialPortPsa
- 侦听接口类名后缀更名为Pli: SerialPortEventListener -> SerialPortEventPli
- 规范串口监听服务与串口服务映射获取名称回调函数的函数名：serialPortNamesListened -> serialPortNames
- 添加初始化串口服务失败的监听回调函数，并且初始化失败不再直接抛异常，而是允许业务服务通过实现SerialPortConnectPli接口，处理串口初始化失败的情况

### poplar-common-session
- 标识接口类后缀更名为Pii: Session -> SessionPii
- 接口类类名后缀更名为Psi，并优化类名SessionValidator -> SessionValidatePsi

### poplar-starter-json
- 添加MultipartFile对象序列化支持

### poplar-starter-modbus-tcp
- 服务接口类后缀更名为Psi，并优化类名: ModbusRegisterLoader -> ModbusRegisterConfigPsi
- 服务接口类后缀更名为Psi，并优化类名: ModbusDataInitializer -> ModbusDataInitPsi
- 侦听接口类后缀更名为Pli: ModbusLifecycleListener -> ModbusLifecyclePli

### poplar-starter-mqtt
- 服务接口类后缀更名为Pli,并优化类名: MqttConfigInterceptor -> MqttConfigPsi

### poplar-starter-scanner
- 侦听接口类后缀更名为Pli: ScannerListener -> ScannerPli

### poplar-starter-web
- 添加SwaggerSecurityPsi接口，给业务服务提供swagger全局安全配置实现接口
- HttpServletUtil类中增加getParamHeaderValue方法获取头或参数值；增加单独传递request参数的相关方法

### poplar-starter-web-audit
- 抽象类后缀更名为Psa: AuditBatchService -> AuditBatchPsa

### poplar-starter-web-file
- pdf转换controller中api通过配置项进行启用（整合）和停用（不整合）
- 添加api开关配置项apiEnabled，默认false停用（不整合）
- 修复文件上传支持swagger文档
- 添加删除文件相关api和接口
- 服务接口类后缀更名为Psi: WebFileMetaDataService -> WebFileMetaDataPsi
- 侦听接口类后缀更名为Pli: ZipPackageTaskListener -> ZipPackageTaskPli

### poplar-starter-web-sign
- 优化签名校验异常信息内容
- 服务接口类后缀更名为Psi: SignAppSecretService -> SignAppSecretPsi

### poplar-starter-web-smscode
- 服务接口类后缀更名为Psi，并根据业务优化类名: SmsCodeSender -> SmsCodeSendPsi
- 服务接口类后缀更名为Psi，并根据业务优化类名: SmsCodeGenerator -> SmsCodeGeneratePsi
- 优化类名: DefaultSmsCodeGenerator -> DefaultSmsCodeGenerateService

### poplar-starter-web-swagger(原poplar-starter-swagger)
- 模块名称更名为poplar-starter-web-swagger
- security相关对象由SwaggerSecurityPsi实现类提供

## 1.8.15
### poplar-common
- 修复bug: BeanUtil.convert成员属性不同类型自动跳过，而不再抛异常
- LineWorker, ConvertionQueue添加析构函数: 对象实例线程结束后销毁前的函数回调
- 优化队列框架类，完善队列容量上限功能和运行时容量功能
- 增加批处理线程框架类BatchLineWorker

### poplar-starter-web
- 将poplar-starter-web-audit中的Audit审计注解移动进来，解决web子模块接口审计编译依赖问题
- 新增HttpServletUtil工具类
- 将AppContext中http servlet request和resposne相关api移动到HttpServletUtil新增类中
- http响应码添加3 sign error签名错误
- 添加审计类型枚举

### poplar-starter-web-audit
- 将Audit注解移动到poplar-starter-web上层依赖中
- 使用HttpServletUtil替换AppContext
- 增加审计类型和相关实现，支持业务方法调用前的审计功能
- 增加审计启用停用开关配置

### poplar-starter-web-file
- 接口添加采集审计数据功能

### poplar-starter-web-sign
- 新增接口签名starter

### poplar-starter-web-smscode
- 接口添加采集审计数据功能

## 1.7.14
### poplar-common
- 优化消费队列，支持启动消费队列之前调整队列大小
- 修复消费队列log不显示服务名称问题

### poplar-starter-web
- AppContext增加获取客户端ip相关静态方法

### poplar-starter-web-audit
- 新增web审计starter

## 1.6.13
### poplar-common
- 添加ZipUtil工具类

### poplar-starter-web-file 
- PdfConvertService增加新的转换方法
- WebFileService增加新的获取文件资源对象方法

## 1.6.12
### poplar-common
- 添加BinaryUtil二进制工具类
- 添加BeanUtil bean工具类

### poplar-common-mybatis
- 添加DatetimeTypeHandler和DateTypeHandler

### poplar-starter-modbus-tcp
- 优化读取输入寄存器log
- 修复ModbusTcpSerice自动注入bug
- ModbusUtil工具类添加整型拆分成字的工具方法
- ModbusUtil工具类添加高低位字合并整型值工具方法
- 添加regCode大写敏感配置和相应的功能实现，默认大小写不敏感

## 1.6.11
### poplar-common
- 完成ByteUtil方法注释
- ByteUtil添加奇偶校验方法
- 增加消费队列无消费元素时的idle回调方法

### poplar-common-serialport
- 串口业务侦听接口回调函数重命名，避免与串口回调函数重名，导致内部函数回调重写冲突

### poplar-common-session
- 增加会话更新接口
- 完善会话清理接口

### poplar-starter-json
- 添加ObjectMapper常用方法静态代理，并将异常代理为runtime的JsonException

### poplar-starter-scanner
- 串口框架调整回调函数名称

### poplar-starter-session-jwt
- 增加会话更新接口实现(不支持会话更新)
- 完善会话清理接口实现(不支持会话清理)

### poplar-starter-session-token
- 增加会话更新接口实现
- 完善会话清理接口实现

### poplar-starter-web
- 添加登录失败响应码402
- 添加登录失败LoginException异常拦截器
- 优化AppContext.getRequestAttribute(),HttpServletRequest为空时不在抛异常，而是返回空
- 优化异常响应消息

## 1.6.10
### poplar-common
- 添加整型校验注释符和校验功能
- 添加uuid帮助类

### poplar-common-mybatis
- 修复bug
- 为mybaitis plus JacksonTypeHandler自动注入框架ObjectMapper对象

### poplar-common-session
- 优化getSession方法

### poplar-starter-code-aes
- 添加AES对称加密服务starter，提供对象json序列化和反序列化的对称加密服务

### poplar-starter-elasticsearch
- 暂时移除，不再随版本更新

### poplar-starter-web
- 优化异常处理ExceptionController

### poplar-starter-web-smscode
- 添加短信验证码服务starter

### poplar-starter-web-file
- 添加web服务的文件上传下载和pdf转换服务starter

## 1.5.9
### poplar-common-session
- 优化框架逻辑

### poplar-common-mybatis
- 集成用户会话功能，优化基础实体类属性自动注入用户会话中的属性
- 添加PageQo对象支持mybatis plus分页查询

## 1.5.8
### poplar-starter-swagger
- 优化配置项basePackage变更为basePackages支持多个基本包配置

### poplar-common-session
- 增加无法获取token值时异常提示信息

## 1.5.7
### poplar-common-mybatis
- 增加mybatis plus基础实体类
- 增加基础实体类属性自动注入

### poplar-starter-json
- 增加LocalDateTime序列化支持

### poplar-starter-scanner
- 新增串口扫码枪starter

### poplar-common-serialport
- [优化并修复bug](poplar-common-serialport/patch.md)

### poplar-starter-modbus-tcp
- [优化并修复bug](poplar-starter-modbus-tcp/patch.md)

## 1.4.6
- 修复公共依赖commons-beanutils版本信息错误

### poplar-common-serialport 【new】
- 新增串口服务框架

### poplar-modbus-tcp-starter
- 优化ModbusTcpService实例化和注入方式
- 添加工具类

### poplar-common
- 增加线程框架和消费队列相关类
- 添加字节处理工具类

### poplar-starter-json
- 优化Json.string()方法
- Json类添加泛型工具方法

### poplar-starter-restful
- 增加http请求原始response body的debug级别日志输出

## 1.3.5
- 添加公共依赖管理apache commons-beanutils-1.9.4

### poplar-starter-modbus-tcp
- 增加初始化连接重试机制
- 增加寄存器动态地址管理
- 增加保持寄存器写入数据int类型

## 1.3.4
### poplar-starter-modbus-tcp
- 完善方法参数校验
- 增加初始化后的写入方法和读取回调方法
- 增加参数对象的写入和读取方法
- 增加ModbusTcpService生命周期侦听接口
- 增加发送onReady的springboot事件

## 1.3.3
### poplar-starter-modbus-tcp
- 重构ModbusTcpService类，规范方法命名，支持读取有符号数

## 1.3.2
### poplar-common
- 修复分页计算总页数bug

## 1.3.1
### poplar-starter-modbus-tcp
- 添加poplar-starter-modbus-tcp 支持modbus tcp服务

## 1.0.0
- java 版本 11
- spring boot 版本 2.5.14
- spring cloud 版本 2020.0.6
- alibaba cloud 版本 2021.0.1.0
- 其余组件版本详见pom.xml
- poplar-common 版本 1.0.0
- poplar-mybatis-common 版本1.0.0
- poplar-json-common 版本1.0.0
- poplar-swagger-starter 版本1.0.0
- poplar-session-token-starter 版本1.0.0
- poplar-elasticsearch-starter 版本1.0.0
- poplar-mqtt-starter 版本1.0.0