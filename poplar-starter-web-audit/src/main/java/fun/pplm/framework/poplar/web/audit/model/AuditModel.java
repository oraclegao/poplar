package fun.pplm.framework.poplar.web.audit.model;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import fun.pplm.framework.poplar.common.session.pii.SessionPii;
import fun.pplm.framework.poplar.web.audit.enums.AuditType;

/**
 * 
 * 审计model
 * @author OracleGao
 *
 */
public class AuditModel {
	/**
	 * 用户会话信息
	 */
	private SessionPii session;
	/**
	 * api名称
	 * 来源Audit.value注解值
	 */
	private String apiName;
	/**
	 * api类型
	 * 来源Audit.types注解值
	 */
	private String[] apiTypes;
	/**
	 * 客户端ip
	 */
	private String clientIp;
	/**
	 * api url
	 */
	private String apiUrl;
	/**
	 * api http方法
	 * GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
	 */
	private String apiMethod;
	/**
	 * userAgent
	 */
	private String userAgent;
	/**
	 * 请求参数
	 */
	private Object[] args;
	/**
	 * 响应结果
	 */
	private Object result;
	/**
	 * 时间戳
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
	private LocalDateTime timestamp;
	/**
	 * 审计类型
	 */
	private AuditType auditType;
	
	public AuditModel() {
		super();
	}

	public SessionPii getSession() {
		return session;
	}

	public void setSession(SessionPii session) {
		this.session = session;
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String[] getApiTypes() {
		return apiTypes;
	}

	public void setApiTypes(String[] apiTypes) {
		this.apiTypes = apiTypes;
	}

	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getApiUrl() {
		return apiUrl;
	}

	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}

	public String getApiMethod() {
		return apiMethod;
	}

	public void setApiMethod(String apiMethod) {
		this.apiMethod = apiMethod;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public Object[] getArgs() {
		return args;
	}

	public void setArgs(Object[] args) {
		this.args = args;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public AuditType getAuditType() {
		return auditType;
	}

	public void setAuditType(AuditType auditType) {
		this.auditType = auditType;
	}
	
}
