package fun.pplm.framework.poplar.web.audit.service;

import java.time.LocalDateTime;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fun.pplm.framework.poplar.common.session.pii.SessionPii;
import fun.pplm.framework.poplar.common.session.service.SessionHttpService;
import fun.pplm.framework.poplar.web.audit.annotation.Audit;
import fun.pplm.framework.poplar.web.audit.config.AuditConfig;
import fun.pplm.framework.poplar.web.audit.enums.AuditType;
import fun.pplm.framework.poplar.web.audit.model.AuditModel;
import fun.pplm.framework.poplar.web.audit.psa.AuditBatchPsa;
import fun.pplm.framework.poplar.web.utils.HttpServletUtil;

/**
 * 
 * 审计切面
 * @author OracleGao
 *
 */
@Aspect
@Service
public class AuditAspectService {
	private static Logger logger = LoggerFactory.getLogger(AuditAspectService.class);
	
	@Autowired
	private AuditConfig auditConfig;
	
	@Autowired(required = false)
	private SessionHttpService sessionHttpService;
	@Autowired
	private AuditBatchPsa auditBatchService;
	
	@Before("@annotation(audit)")
	public void auditBeforeOperation(JoinPoint joinPoint, Audit audit) {
		if(!auditConfig.getEnabled()) {
			return;
		}
		AuditType[] auditTypes = audit.auditTypes();
		if (auditTypes == null || auditTypes.length == 0) {
			auditTypes = auditConfig.getAuditTypes();
		}
		if (Arrays.binarySearch(auditTypes, AuditType.BEFORE) >= 0) {
			AuditModel auditModel = new AuditModel();
			auditModel.setApiName(audit.value());
			auditModel.setApiTypes(audit.types());
			if(sessionHttpService != null) {
				auditModel.setSession(sessionHttpService.getSession(SessionPii.concreteSessionClass));
			}
			auditModel.setArgs(joinPoint.getArgs());
			auditModel.setTimestamp(LocalDateTime.now());
			HttpServletRequest request = HttpServletUtil.getRequest();
			if (request != null) {
				auditModel.setApiUrl(request.getRequestURI());
				auditModel.setApiMethod(request.getMethod());
				auditModel.setClientIp(HttpServletUtil.getClientIp(request));
				auditModel.setUserAgent(request.getHeader("User-Agent"));
			}
			auditModel.setAuditType(AuditType.BEFORE);
			try {
				auditBatchService.add(auditModel);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}
	
	@AfterReturning(pointcut = "@annotation(audit)", returning = "result")
    public void auditAfterOperation(JoinPoint joinPoint, Audit audit, Object result) {
		if(!auditConfig.getEnabled()) {
			return;
		}
		AuditType[] auditTypes = audit.auditTypes();
		if (auditTypes == null || auditTypes.length == 0) {
			auditTypes = auditConfig.getAuditTypes();
		}
		if (Arrays.binarySearch(auditTypes, AuditType.AFTER) >= 0) {
			AuditModel auditModel = new AuditModel();
			auditModel.setApiName(audit.value());
			auditModel.setApiTypes(audit.types());
			if(sessionHttpService != null) {
				auditModel.setSession(sessionHttpService.getSession(SessionPii.concreteSessionClass));
			}
			auditModel.setArgs(joinPoint.getArgs());
			auditModel.setResult(result);
			auditModel.setTimestamp(LocalDateTime.now());
			HttpServletRequest request = HttpServletUtil.getRequest();
			if (request != null) {
				auditModel.setApiUrl(request.getRequestURI());
				auditModel.setApiMethod(request.getMethod());
				auditModel.setClientIp(HttpServletUtil.getClientIp(request));
				auditModel.setUserAgent(request.getHeader("User-Agent"));
			}
			auditModel.setAuditType(AuditType.AFTER);
			try {
				auditBatchService.add(auditModel);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
    }
    
}
