package fun.pplm.framework.poplar.web.audit.config;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.web.audit.enums.AuditType;

/**
 * 
 * 审计配置
 * 
 * @author OracleGao
 *
 */
@Configuration
@ConfigurationProperties(prefix = "poplar.web.audit")
public class AuditConfig {
	/**
	 * 启用审计服务
	 * 默认true启用
	 */
	private Boolean enabled = true;
	/**
	 * 审计类型 默认AFTER: 业务方法执行之后
	 */
	private AuditType[] auditTypes = new AuditType[] { AuditType.AFTER };
	/**
	 * 批处理服务时间间隔
	 * 单位毫秒
	 * 默认3000
	 */
	private Long batchInterval = 3000L;

	public AuditConfig() {
		super();
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public AuditType[] getAuditTypes() {
		return auditTypes;
	}

	public void setAuditTypes(AuditType[] auditTypes) {
		this.auditTypes = auditTypes;
	}

	public Long getBatchInterval() {
		return batchInterval;
	}

	public void setBatchInterval(Long batchInterval) {
		this.batchInterval = batchInterval;
	}

	protected Map<String, Object> memberMap() {
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("enabled", enabled);
		map.put("auditTypes", auditTypes);
		map.put("batchInterval", batchInterval);
		return map;
	}

	@Override
	public String toString() {
		return Json.string(memberMap());
	}

}
