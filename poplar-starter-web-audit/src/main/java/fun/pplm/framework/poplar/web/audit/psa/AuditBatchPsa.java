package fun.pplm.framework.poplar.web.audit.psa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;

import fun.pplm.framework.poplar.common.runnable.BatchLineWorker;
import fun.pplm.framework.poplar.web.audit.config.AuditConfig;
import fun.pplm.framework.poplar.web.audit.model.AuditModel;

/**
 * Poplar Service Abstract
 * 审计数据批处理服务抽象
 * @author OracleGao
 *
 */
public abstract class AuditBatchPsa extends BatchLineWorker<AuditModel> {
	private static Logger logger = LoggerFactory.getLogger(AuditBatchPsa.class);
	
	@Autowired
	private AuditConfig auditConfig;
	
	@EventListener(ApplicationStartedEvent.class)
	protected void init() {
		if(!auditConfig.getEnabled()) {
			logger.debug("审计服务没有开启，需要在配置文件中开启");
			return;
		}
		logger.debug("初始化审计数据批处理服务...");
		this.setInterval(auditConfig.getBatchInterval());
		super.startup();
		logger.debug("初始化审计数据批处理服务完成");
	}
	
}
