package fun.pplm.framework.poplar.web.audit.service;

import java.util.List;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fun.pplm.framework.poplar.json.Json;
import fun.pplm.framework.poplar.web.audit.model.AuditModel;
import fun.pplm.framework.poplar.web.audit.psa.AuditBatchPsa;
import fun.pplm.framework.poplar.web.resp.Resp;

/**
 * 
 * 默认审计数据批处理服务
 * @author OracleGao
 *
 */
@Service
@ConditionalOnMissingBean(AuditBatchPsa.class)
public class DefaultAuditBatchService extends AuditBatchPsa {
	private static Logger logger = LoggerFactory.getLogger(DefaultAuditBatchService.class);
	
	@Override
	protected void batchProcess(List<AuditModel> data) {
		for (AuditModel auditModel : data) {
			Object result = auditModel.getResult();
			if (!(result instanceof Resp)) {
				auditModel.setResult(null);
			}
			Object[] args = auditModel.getArgs();
			args = Stream.of(args).filter(arg -> {
				if (arg instanceof MultipartFile) {
					return false;
				}
				return true;
			}).toArray();
			auditModel.setArgs(args);
			logger.info("审计数据: {}", Json.string(auditModel));
		}
	}

}
