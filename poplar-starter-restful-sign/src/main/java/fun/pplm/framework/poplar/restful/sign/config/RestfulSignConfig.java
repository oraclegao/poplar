package fun.pplm.framework.poplar.restful.sign.config;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import fun.pplm.framework.poplar.json.Json;

@Configuration
@ConfigurationProperties(prefix = "poplar.restful.sign")
public class RestfulSignConfig {
	/**
	 * app key属性名称
	 * 默认appKey
	 */
	private String appKeyName = "appKey";
	/**
	 * timestamp属性名称
	 * 默认timestamp
	 */
	private String timestampName = "timestamp";
	/**
	 * sign属性名称
	 * 默认sign
	 */
	private String signName = "sign";
	/**
	 * app key
	 */
	private String appKey;
	/**
	 * app secret
	 */
	private String appSecret;
	
	public RestfulSignConfig() {
		super();
	}

	public String getAppKeyName() {
		return appKeyName;
	}

	public void setAppKeyName(String appKeyName) {
		this.appKeyName = appKeyName;
	}

	public String getTimestampName() {
		return timestampName;
	}

	public void setTimestampName(String timestampName) {
		this.timestampName = timestampName;
	}

	public String getSignName() {
		return signName;
	}

	public void setSignName(String signName) {
		this.signName = signName;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}
	
	protected Map<String, Object> memberMap(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("appKeyName", appKeyName);
		map.put("timestampName", timestampName);
		map.put("signName", signName);
		map.put("appKey", appKey);
		map.put("appSecret", appSecret);
		return map;
	}
	
	@Override
	public String toString() {
		return Json.string(memberMap());
	}
	
}
