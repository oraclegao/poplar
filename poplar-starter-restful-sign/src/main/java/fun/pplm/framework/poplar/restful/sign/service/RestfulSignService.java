package fun.pplm.framework.poplar.restful.sign.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.core.type.TypeReference;

import fun.pplm.framework.poplar.common.utils.UrlUtil;
import fun.pplm.framework.poplar.restful.psi.RestfulPsi;
import fun.pplm.framework.poplar.restful.resp.RestfulResp;
import fun.pplm.framework.poplar.restful.service.RestfulService;
import fun.pplm.framework.poplar.restful.sign.config.RestfulSignConfig;

/**
 * 
 * 使用装饰模式实现符合web sign签名规则的restful服务
 * @author OracleGao
 *
 */
@Service
public class RestfulSignService implements RestfulPsi {
	private static Logger logger = LoggerFactory.getLogger(RestfulSignService.class);
	
	@Autowired
	private RestfulService restfulService;
	@Autowired
	private RestfulSignConfig restfulSignConfig;
	
	@Override
	public <T> RestfulResp<T> get(String url, HttpHeaders headers, Class<T> clazz) {
		if (headers == null) {
			headers = new HttpHeaders();
		}
		Map<String, List<String>> signedParamMap = getSignedParams(url);
		headers.putAll(signedParamMap);
		return restfulService.get(url, headers, clazz);
	}

	@Override
	public <T> RestfulResp<T> get(String url, HttpHeaders headers, TypeReference<T> typeReference) {
		if (headers == null) {
			headers = new HttpHeaders();
		}
		Map<String, List<String>> signedParamMap = getSignedParams(url);
		headers.putAll(signedParamMap);
		return restfulService.get(url, headers, typeReference);
	}

	@Override
	public <T> RestfulResp<T> formPost(String url, MultiValueMap<?, ?> formMap, HttpHeaders headers, Class<T> clazz) {
		if (headers == null) {
			headers = new HttpHeaders();
		}
		Map<String, List<String>> signedParamMap = getSignedParams(url);
		headers.putAll(signedParamMap);
		return restfulService.formPost(url, formMap, headers, clazz);
	}

	@Override
	public <T> RestfulResp<T> formPost(String url, MultiValueMap<?, ?> formMap, HttpHeaders headers, TypeReference<T> typeReference) {
		if (headers == null) {
			headers = new HttpHeaders();
		}
		Map<String, List<String>> signedParamMap = getSignedParams(url);
		headers.putAll(signedParamMap);
		return restfulService.formPost(url, formMap, headers, typeReference);
	}

	@Override
	public <T> RestfulResp<T> jsonPost(String url, Object requestBody, HttpHeaders headers, Class<T> clazz) {
		if (headers == null) {
			headers = new HttpHeaders();
		}
		Map<String, List<String>> signedParamMap = getSignedParams(url);
		headers.putAll(signedParamMap);
		return restfulService.jsonPost(url, requestBody, headers, clazz);
	}

	@Override
	public <T> RestfulResp<T> jsonPost(String url, Object requestBody, HttpHeaders headers, TypeReference<T> typeReference) {
		if (headers == null) {
			headers = new HttpHeaders();
		}
		Map<String, List<String>> signedParamMap = getSignedParams(url);
		headers.putAll(signedParamMap);
		return restfulService.jsonPost(url, requestBody, headers, typeReference);
	}

	/**
	 * 获取已签名的参数map
	 * @param url 需要签名的url
	 * @return 已签名的参数map
	 */
	public Map<String, List<String>> getSignedParams(String url) {
		String timestamp = String.valueOf(System.currentTimeMillis());
		Map<String, List<String>> signedParamMap = new HashMap<>();
		List<String> values = new ArrayList<>();
		values.add(timestamp);
		signedParamMap.put(restfulSignConfig.getTimestampName(), values);
		values = new ArrayList<>();
		values.add(restfulSignConfig.getAppKey());
		signedParamMap.put(restfulSignConfig.getAppKeyName(), values);
		Map<String, List<String>> paramMap = UrlUtil.parseQueryParams(url);
		paramMap.putAll(signedParamMap);
		String sign = getSign(paramMap);
		values = new ArrayList<>();
		values.add(sign);
		signedParamMap.put(restfulSignConfig.getSignName(), values);
		return signedParamMap;
	}
	
	private String getSign(Map<String, List<String>> paramMap) {
		String params = UrlUtil.getLexSortedParamStr(paramMap);
		logger.debug("sign params: {}", params);
		HmacUtils hmacUtils = new HmacUtils(HmacAlgorithms.HMAC_SHA_1, restfulSignConfig.getAppSecret());
    	String sign = hmacUtils.hmacHex(params);
		return sign;
	}
	
}
